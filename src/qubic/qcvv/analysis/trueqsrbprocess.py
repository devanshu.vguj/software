
def trueqsrbprocess(filename,qasmresult,sim=True):
        #scode=''.join(combinescode[icode])
    qubicresults=qasmresult['countsum']['vcombinepermeas']
    trueqresults=[{''.join(icode):qubicresult[icode] for icode in qubicresult} for qubicresult in qubicresults]
    import trueq
    circuits=trueq.load(filename) if isinstance(filename,str) else filename
    if sim:
        s=trueq.Simulator()
        s.run(circuits)
        simulation_results=circuits.results
        print('simulation_results',simulation_results)
        for i,simres in enumerate(simulation_results):
            d=trueqresults[i]
            print(simres,[(k,d[k]) for k in sorted(d,key=d.get,reverse=True)])
    circuits.results=trueqresults
    result_rb=circuits.fit()
    fidelity=dict(fidelity=[],fidelity_std=[],SPAM=[],labels=[])
    for r in result_rb:
        if r.key.protocol=='SRB':
            fidelity['labels'].append(r.key.labels)
            fidelity['fidelity'].append(1-r.e_F.val)
            fidelity['fidelity_std'].append(r.e_F.std)
            fidelity['SPAM'].append(r.A.val)
#            https://trueq.quantumbenchmark.com/guides/protocols/srb.html
    return circuits,fidelity
