import json
import pkgutil
import numpy
from qubic.bids.ether import c_ether
from qubic.bids.regmap import c_regmap
from qubic.qubic import init
#    from .cmdgen128 import cmdgen128
from matplotlib import pyplot
def cmdshift(cmds,toffset,debug=0):
    for cmd in cmds:
        if debug >= 4:
            print(cmd['trig_t'],toffset)
        cmd.update(dict(trig_t=cmd['trig_t']+toffset))
    return cmds
def cmdelemcnt(cmds,readelem=[8,9,10],debug=0):
    cmdelemcnt={}
    if len(cmds)>0:
        for cmd in cmds:
            if debug >=4:
                print('debug cmdelemcnt',cmd)
            if 'element' in cmd:
                if cmd['element'] not in cmdelemcnt:
                    cmdelemcnt[cmd['element']]=0
                cmdelemcnt[cmd['element']]+=1
    readelemcnt={k:v for k,v in cmdelemcnt.items() if k in readelem}
    return cmdelemcnt,readelemcnt
#    def cmdgen128(self,trig_t,element,dest,start,length,phrad,freq,cond=0,debug=False):
#        return cmdgen128(trig_t,element,dest,start,length,phrad,freq,cond=cond,debug=debug,dt=self.dt,fpgadacclkratio=self.fpgadacclkratio)
def cmd128decode(cmd,debug=0):
    trig_t=(cmd>>8)&0xffffff;
    element=(cmd>>0)&0xff;
    ph_binary=(((cmd>>32)&0x3f)<<8)+((cmd>>88)&0xff)
    length=(cmd>>38)&0xfff
    start=(cmd>>50)&0xfff
    dest=(cmd>>62)&0x3
    freq_binary=(cmd>>64)&0xffffff
    cond=(cmd>>96)&0x1
    return {'trig_t':trig_t,'element':element,'dest':dest,'start':(start&0xfff,hex(start&0xfff)),'length':length,'ph_binary':ph_binary,'freq_binary':freq_binary}

def cmdgen128(trig_t,element,dest,start,length,phrad,freq,cond=0,debug=False,dt=1e-9,fpgadacclkratio=4,**kwargs):
    trig_t=int(round(trig_t/dt/fpgadacclkratio))
    start=start/fpgadacclkratio
    length=length/fpgadacclkratio

    assert trig_t<2**24, 'trig_t %d exceeds 2**24'%trig_t
    element=int(element)
    dest=int(dest)
    start=int(start)
    length=int(length)
    phdeg=phrad/numpy.pi*180
    freq=freq
    ph_binary = int(phdeg/360.0*2**14)
    freq_binary=int(1e-9*freq*2**24)
#        cmd=((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
#        cmd1=((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
    cmd=((cond&0x1)<<96)+((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)
    cmd1=(((cmd>>96)&0xffffffff)<<96)+((cmd&0xffffffff)<<64)+(((cmd>>32)&0xffffffff)<<32)+((cmd>>64)&0xffffffff)
    if debug>=4:
        print('debug cmdgen128 ','trig_t',trig_t,'deg','%8.1f'%(phdeg%360),'ph_binary',ph_binary,'rad','%8.3f'%(phrad%(2*numpy.pi)),'dest',dest,'length',length,'freq',freq,'freq_binary',freq_binary,'element',element,'start',start&0xfff,hex(start&0xfff),format(cmd1,'032x'))
        #print('debug cmdgen128 ','trig_t',trig_t,'deg','%8.1f'%(phdeg%360),'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_binary',ph_binary,'rad',phrad%(2*numpy.pi),'freq',freq,'freq_binary',freq_binary,format(cmd1,'032x'))
    #print('***v2.0 printing format***','trig_t',trig_t,'element',element,'dest',dest,'start',start&0xfff,hex(start&0xfff),'length',length,'ph_rad:%.3f'%(phrad%(2*numpy.pi)),'freq_MHz:%.3f'%(freq/1e6))
    return cmd1
def cmdchop(circcmds,**kwargs):
    opts=dict(totalcmdlimit=65535,totalreadlimit=2048,totaltimelimit=(2**24-1)*4e-9,starttime=16e-9,readelem=[8,9,10],mark0=True,debug=0)
    opts.update(kwargs)
    debug=opts['debug']
    circuitstarttime=opts['starttime']
    cmdlist=[]
    if opts['mark0']:
        cmdlist.append(dict(trig_t=opts['starttime'],element=12,dest=12,start=0,length=1000,phrad=0,freq=0.0,cond=False))
        circuitstarttime+=1e-6
    cmdlists=[]
    totalcmd=0
    totalread={}
    totaltime=opts['starttime']
    for index,circcmd in enumerate(circcmds):
        fullcmd=circcmd['cmd']
        circcmdelemcnt,circreadelemcnt=    cmdelemcnt(fullcmd)
        circlengtht=circcmd['lengtht']#fulllastpoint+delaybetweenelement

        cmdlen=len(fullcmd)
        totalcmd+=cmdlen
        totalread={k:circreadelemcnt[k]+(totalread[k] if k in totalread else 0)  for k in opts['readelem'] if k in circreadelemcnt}

        totaltime+=circlengtht
        if (totalcmd>=opts['totalcmdlimit'] or ((max(totalread.values()) > opts['totalreadlimit']) if totalread else False) or totaltime>opts['totaltimelimit']):
            if debug>=4:
                print('total cmd',totalcmd,'total read', totalread,'totaltime',totaltime)
                print('long circuit, next part %d'%circuitstarttime)
            cmdlists.append((cmdlist,circuitstarttime))
            totalcmd=cmdlen
            totalread=circreadelemcnt
            totaltime=circlengtht
            circuitstarttime=opts['starttime']
            cmdlist=[]
            if opts['mark0']:
                cmdlist.append(dict(trig_t=opts['starttime'],element=12,dest=12,start=0,length=1000,phrad=0,freq=0.0,cond=False))
                circuitstarttime+=1e-6
            cmdlist.extend(cmdshift(fullcmd,toffset=circuitstarttime))
            circuitstarttime+=circlengtht
        else:
            cmdlist.extend(cmdshift(fullcmd,toffset=circuitstarttime))
            circuitstarttime+=circlengtht
    if debug >=4:
        print('total cmd',totalcmd,'total read', totalread,'totaltime',totaltime)
    cmdlists.append((cmdlist,circuitstarttime))
#    print('debug cmdchop')
#    for index,cmd in enumerate(cmdlists):
#        print('cmdchop,index',index)
#        for l in cmd:
#            print(l)
    return cmdlists
def circuitgaps(circcmdlist,hrldcmdlist=None,**kwargs):
    ops=dict(delaybetweenelement=600e-6,delayafterheralding=12e-6,debug=0)
    ops.update(kwargs)
    debug=ops['debug']
    #debugindex=0
    circcmds=[]
    for index,(circcmd,lastpoint) in enumerate(circcmdlist):
        if debug >=4:
            print('debug heralding and circ len',len(hrldcmdlist) if  hrldcmdlist else None,len(circcmd) if circcmd else  None)
        if hrldcmdlist is None:
            hrldcmd=[]
            hrldlastpoint=0
        elif len(hrldcmdlist)==1:
            if debug>=4:
                print('debug heralding[0]',hrldcmdlist)
            hrldcmd,hrldlastpoint=hrldcmdlist[0]
        elif len(hrldcmdlist)==len(circcmdlist):
            hrldcmd,hrldlastpoint=hrldcmdlist[index]
        else:
            if debug>=4:
                print('heralding length do not understand heralding:%d, circ:%d'%(len(hrldcmdlist),len(circcmd)))
            exit(1)
        if hrldcmdlist is not None:
            #    hrldcmdelemcnt,hrldreadelemcnt=    chassis.cmdelemcnt(hrldcmd)
            hrldlengtht=hrldlastpoint+ops['delayafterheralding']
            fullcmd=[{k:v for k,v in i.items()} for i in hrldcmd]
            fullcmd.extend(cmdshift(circcmd,toffset=hrldlengtht))
            fulllastpoint=lastpoint+hrldlengtht
        else:
            fullcmd=circcmd
            fulllastpoint=lastpoint
        circcmds.append(dict(cmd=fullcmd,lengtht=fulllastpoint+ops['delaybetweenelement']))
    return circcmds
