import os
import imp
import json
import sys
from qubic.qubic.squbic import c_qchip
from qubic.qubic.chassis import c_chassis

def envset(calirepo,**kwargs):
    opts={}
    opts['ip'] = os.environ['QUBICIP']
    opts['qubitcfgfile'] = '/'.join([calirepo,os.environ['QUBIC_CHIP_NAME'],os.environ['QUBICQUBITCFG']])
    opts['wiremapfile']= '/'.join([calirepo,os.environ['QUBIC_CHIP_NAME'],os.environ['QUBICWIREMAP']+'.py'])
    opts['gmixs']='/'.join([calirepo,os.environ['QUBIC_CHIP_NAME']])
    opts['calipath']='/'.join([calirepo,os.environ['QUBIC_CHIP_NAME']])
    opts['sim']=False
    opts['debug']=False
    opts.update(kwargs)
    #print('opts',opts)
#    opts.update(**kwargs)
    opts['chassis']=c_chassis(opts['ip'],sqinit=True,sim=opts['sim'],debug=opts['debug'])
    with open(opts['qubitcfgfile']) as jfile:
        qubitcfg=json.load(jfile)
    opts['wiremap'] = imp.load_source("wiremap",opts['wiremapfile'])
    opts['qchip']=c_qchip(qubitcfg)
    return opts
