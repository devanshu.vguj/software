import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment
class c_rough_pulse_tuning(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def rough_pulse_tuningseqs(self,qubit,gate,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6):
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modq7qdrv={}
		self.arun=numpy.arange(0.4,0.6,0.01)
		#self.arun=numpy.arange(0.27,0.37,0.005)
		repeat180=[1,2,3,5,11]
		repeat90=[2,4,6,10,22]
		gaterepeat={'X180':repeat180,'Y180':repeat180,'X90':repeat90,'Y90':repeat90}
		self.grun=gaterepeat[gate]
		gateobj=self.qchip.gates[qubit+gate]
		lgate=gateobj.tlength()
		run=0;
		index=0
		for g in self.grun:
			for a in self.arun:
				self.readall(trdrv=run,delayread=delayread)
				tgate=run+delay1
				pini=0
				tini=tgate
				modq7qdrv.update(dict(amp=a))
				for i in range(g):
					pnew=self.qchip.gates[qubit+gate].pcalc(dt=tgate-tini)[0]
					modq7qdrv.update(dict(pcarrier=pnew))
					self.seqs.add(tgate,self.qchip.gates[qubit+gate].modify(modq7qdrv))
					tgate=self.seqs.tend()	
					index=index+1
				#print index,a,g
				self.readall(trdrv=self.seqs.tend(),delayread=delayread)
				run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def acqrough_pulse_tuning(self,nget):
		data=self.acqdata(nget)
		return data
	def processrough_pulse_tuning(self,dt,filename,loadname):
		data=self.loadjsondata(filename)
		self.rabi_result=self.process3(data['accout_0'],lengthperrow=self.hf.getbufwidthdict()['accout_0'],training=False,loadname=loadname)
		#[amp,period,fiterr]=self.fitrabi(dx=dt,data=rabi_result['population_norm'])
		return [data,self.rabi_result['separation'],self.rabi_result['iqafterherald'],self.rabi_result['population_norm']]
	def plotrough_pulse_tuning(self,data):
		pyplot.figure('heat')
		d2d=data.reshape((len(self.grun),len(self.arun))).transpose()
		pyplot.imshow(d2d, cmap='hot')
		pyplot.legend()
		pyplot.figure('line')
		pyplot.plot(self.arun,d2d[:,1])
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	rough_pulse_tuning=c_rough_pulse_tuning(**clargs.__dict__)
	rough_pulse_tuning.rough_pulse_tuningseqs(qubit='Q7',gate='X90',delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement)
	rough_pulse_tuning.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			rough_pulse_tuning.sim()
		data=rough_pulse_tuning.acqrough_pulse_tuning(clargs.nget)
		fprocess=rough_pulse_tuning.savejsondata(filename=clargs.filename,extype='rough_pulse_tuning',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm]=rough_pulse_tuning.processrough_pulse_tuning(dt=clargs.elementstep,filename=fprocess,loadname=clargs.dataset)
##	print 'period',period
##	print 'separation',separation
##	print 'amp',amp
##	rough_pulse_tuning.plotrawdata(rawdata)
##	rough_pulse_tuning.plotafterheraldingtest(iqafterherald)
	#rough_pulse_tuning.plotpopulation_norm(population_norm)
	rough_pulse_tuning.plotrough_pulse_tuning(population_norm)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
