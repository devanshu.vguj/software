import trueq as tq
import json
from matplotlib import pyplot
import sys
import re
pattern='(\S+_\S+_\S+_\S+)(_\S+_\S+_\S+).dat'
m=re.match(pattern,sys.argv[1])
if m:
	g=m.groups()
	resultfilename='%s%s.dat'%(g[0],g[1])
	circuits=tq.load('%s.bin'%g[0])
	with open(resultfilename) as resultfile:
		circuits.results=json.load(resultfile)
	circuits.fit().summarize()
	circuits.plot.raw()
	pyplot.show()
else:
	print('wrong result file name')
