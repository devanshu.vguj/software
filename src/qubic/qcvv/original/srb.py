import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
from matplotlib import pyplot,patches
import datetime
import argparse
import sys
import numpy
import time
import init
import pprint
import experiment
import copy
from itertools import product

class c_srb(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid_t=None
		self.qubitid_c=None
		pass
	def srbseqs(self,elementlength=80,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,restart=True,el_clif_step=50,qubitid_t='Q7',qubitid_c='Q6',qubitidread=['Q5','Q4','Q3'],phase_offset_ctrl=0,amp_multiplier_ctrl=0,phase_offset_tgt=0,amp_multiplier_tgt=0):
		self.qubitid_t=qubitid_t
		self.qubitid_c=qubitid_c
		if restart:
			self.initseqs()

		el_clif_list_pair=numpy.arange(2,2+elementlength*el_clif_step,el_clif_step)
		el_clif_list_gate=2*el_clif_list_pair

		rbgates=[]
		c_list=self.rand_clif_1q((elementlength-1)*el_clif_step+2)
		g=['I','Y90','Y180','Y270','X90','X180','X270','Z90','Z270']
		print(['c_list: %s %d'%(i,c_list.count(i)) for i in g])
		for i in range(elementlength):
			c_list_id=self.g_list_add_inverse(c_list[:el_clif_list_gate[i]])
			rbgates.append(c_list_id)
			print('len(c_list_id)',len(c_list_id))

		rbgates_ctrl=[]
		c_list_ctrl=self.rand_clif_1q((elementlength-1)*el_clif_step+2)
		print(['c_list_2: %s %d'%(i,c_list_ctrl.count(i)) for i in g])
		for i in range(elementlength):
			c_list_id_ctrl=self.g_list_add_inverse(c_list_ctrl[:el_clif_list_gate[i]])
			rbgates_ctrl.append(c_list_id_ctrl)
			print('len(c_list_id_ctrl)',len(c_list_id_ctrl))

		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0

		for gates,gates_ctrl in zip(rbgates,rbgates_ctrl):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'readoutdrv'])
			self.seqs.add(therald+delayread,self.qchip.gates[qubitid_t+'readout'])
			run=self.seqs.tend()+delay1
			run_ctrl=run
			tini=run
			padd=0
			padd_ctrl=0
			for gate,gate_ctrl in zip(gates,gates_ctrl):
				gatemodi={}
				modtgt_simul={}
				if gate=='I':
					run=run
				else:
					if 'Z' in gate:
						padd=padd-float(gate[1:])/180.*numpy.pi
						run=run
					else:
						pnew=self.qchip.gates[qubitid_t+gate].pcalc(dt=run-tini,padd=padd)[0]
						gatemodi.update(dict(pcarrier=pnew))
						self.seqs.add(run,self.qchip.gates[qubitid_t+gate].modify(gatemodi))

						#pcarrier_tgt_simul_tmp=self.qchip.gates[qubitid_t+gate].pcalc(dt=run-tini,padd=padd+phase_offset_tgt)[0]
						#amp_tgt_simul_tmp=self.qchip.gates[qubitid_t+gate].paralist[0]['amp']*amp_multiplier_tgt
						#modtgt_simul.update(dict(dest=qubitid_c+'.qdrv',amp=amp_tgt_simul_tmp,pcarrier=pnew+pcarrier_tgt_simul_tmp))
						#self.seqs.add(run,self.qchip.gates[qubitid_t+gate].modify(modtgt_simul))

						run=run+self.qchip.gates[qubitid_t+gate].paralist[0]['twidth']

				gatemodi_ctrl={}
				modctrl_simul={}
				if gate_ctrl=='I':
					run_ctrl=run_ctrl
				else:
					if 'Z' in gate_ctrl:
						padd_ctrl=padd_ctrl-float(gate_ctrl[1:])/180.*numpy.pi
						run_ctrl=run_ctrl
					else:
						pnew_ctrl=self.qchip.gates[qubitid_c+gate_ctrl].pcalc(dt=run_ctrl-tini,padd=padd_ctrl)[0]
						gatemodi_ctrl.update(dict(pcarrier=pnew_ctrl))
						self.seqs.add(run_ctrl,self.qchip.gates[qubitid_c+gate_ctrl].modify(gatemodi_ctrl))

						#pcarrier_ctrl_simul_tmp=self.qchip.gates[qubitid_c+gate_ctrl].pcalc(dt=run_ctrl-tini,padd=padd_ctrl+phase_offset_ctrl)[0]
						#amp_ctrl_simul_tmp=self.qchip.gates[qubitid_c+gate_ctrl].paralist[0]['amp']*amp_multiplier_ctrl
						#modctrl_simul.update(dict(dest=qubitid_t+'.qdrv',amp=amp_ctrl_simul_tmp,pcarrier=pnew_ctrl+pcarrier_ctrl_simul_tmp))
						#self.seqs.add(run_ctrl,self.qchip.gates[qubitid_c+gate_ctrl].modify(modctrl_simul))

						run_ctrl=run_ctrl+self.qchip.gates[qubitid_c+gate_ctrl].paralist[0]['twidth']

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'readoutdrv'])
			self.seqs.add(treaddrv+delayread,self.qchip.gates[qubitid_t+'readout'])
			run=self.seqs.tend()+delaybetweenelement

		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processrb(self,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		rb_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,rb_result['separation'],rb_result['iqafterherald'],rb_result['population_norm']]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	srb=c_srb(**clargs.__dict__)
	para_dict={'Q6':{'phase_offset':4.925-numpy.pi,'amp_multiplier':1.4},'Q5':{'phase_offset':0.85+numpy.pi,'amp_multiplier':0.63}}
	el_clif_step=10#50
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	for nrb in range(clargs.nmeas):
		parser,cmdlinestr=experiment.cmdoptions()
		clargs=parser.parse_args()
		srb=c_srb(**clargs.__dict__)
		if clargs.sim:
			srb.setsim()
		print('measurement: %3d of %3d'%(nrb+1,clargs.nmeas))
		srb.srbseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,el_clif_step=el_clif_step,qubitid_t=clargs.qubitid_t,qubitid_c=clargs.qubitid_c,qubitidread=clargs.qubitidread,phase_offset_ctrl=para_dict[clargs.qubitid_c]['phase_offset'],amp_multiplier_ctrl=para_dict[clargs.qubitid_c]['amp_multiplier'],phase_offset_tgt=para_dict[clargs.qubitid_t]['phase_offset'],amp_multiplier_tgt=para_dict[clargs.qubitid_t]['amp_multiplier'])
		srb.run(bypass=clargs.bypass or not clargs.processfile=='')
		if clargs.processfile=='':
			data=srb.acqdata(clargs.nget)
			fprocess=srb.savejsondata(filename=clargs.filename,extype='srb',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			if clargs.sim:
				srb.sim()
		else:
			fprocess=clargs.processfile
		[rawdata,separation,iqafterherald,population_norm]=srb.processrb(filename=fprocess,loaddataset=clargs.dataset)
		if clargs.readcorr:
			population_norm=srb.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
			print('corrected population_norm',population_norm)
		print('dataset',clargs.dataset)
		population_norm_tmp=population_norm.reshape((1,-1))
		population_norm_array=population_norm_tmp if nrb==0 else numpy.append(population_norm_array,population_norm_tmp,axis=0)
		with open('srb_population_norm_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,population_norm_tmp)
		print('separation',separation)
		print(srb.hf.adcminmax())
	population_norm_array=population_norm_array[2:,:]
	recovery_population_array=1-population_norm_array
	recovery_population_mean=numpy.mean(recovery_population_array,axis=0)
	recovery_population_std=numpy.std(recovery_population_array,axis=0)
	el_clif_list_pair=numpy.arange(2,2+clargs.elementlength*el_clif_step,el_clif_step) 
	numpy.savetxt('srb_recovery_population_array'+fprocess,recovery_population_array)
	srb.plotrb(recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=fprocess)
	#[rb_p,rb_a]=rb.fitrb(el_clif_list_pair,recovery_population_mean,yerr=recovery_population_std,figname=fprocess)
	[rb_p,rb_a]=srb.fitrb_violin(recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=fprocess)
	print('rb_p,rb_a',rb_p,rb_a)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	# twidth(Q5X90)=twidth(Q6X90)
	# python srb.py --plot -n 50 --qubitid_c Q6 --qubitid_t Q5 -el 11 -nm 12
	# python srb.py --plot -n 50 --qubitid_c Q6 --qubitid_t Q5 -el 11 -nm 12 --readcorr
