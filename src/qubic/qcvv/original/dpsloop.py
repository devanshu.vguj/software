import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from dps import c_dps
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	dps=c_dps(**clargs.__dict__)
	dps.dpsseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth)
	if clargs.processfile=='':
		if clargs.sim:
			dps.sim()
		dps.run(bypass=clargs.bypass)
	else:
		fprocess=clargs.processfile
	pyplot.ion()
	fig1=pyplot.figure(1)
	fig2=pyplot.figure(2)
	while True:
		try:
			data=dps.dpsdata(clargs.nget)
			fprocess=dps.savejsondata(filename=clargs.filename,extype='dpsloop',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
			[c0,c1,c2]=dps.processdps(fprocess)
			print('adc min max',dps.hf.adcminmax())
			fig1.clf()
			fig2.clf()
			if clargs.plot:
				dps.dpsplot(c0,c1,c2)
			pyplot.draw()
			pyplot.pause(1e-6)
		except KeyboardInterrupt:
			pyplot.close('all')
			break
	# python dpsloop.py --plot --delayread 532e-9 -n 20
