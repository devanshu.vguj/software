import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment 
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
import scipy.optimize

class c_dragalpha(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,alphas,**kwargs):
        seqs=[]
        for alpha in alphas:
            if 1:
                x90alpha=[{'name': 'X90', 'qubit': qubitid, 'modi' : [{"env":[{"paradict":{"alpha":alpha}}]}]}]
            else: # Ravi using virtual z, 
                x90alpha=[{'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': alpha}}
                ,{'name': 'X90', 'qubit': qubitid}#, 'modi' : [{"env":[{"paradict":{"alpha":alpha}}]}]}
                ,{'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': alpha}}]
            seq=[]
            seq.extend(x90alpha)
            seq.append({'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': -numpy.pi/2}})
            seq.extend(x90alpha)
            seq.extend(x90alpha)
            seq.append({'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': numpy.pi/2}})
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
            seq=[]
            seq.append({'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': -numpy.pi/2}})
            seq.extend(x90alpha)
            seq.append({'name': 'virtualz', 'qubit': qubitid, 'para': {'phase': numpy.pi/2}})
            seq.extend(x90alpha)
            seq.extend(x90alpha)
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
        return seqs

    def run(self,nsample):
        experiment.c_experiment.accbufrun(self,nsample=nsample)
        self.x90y180=self.accresult['countsum']['psingle'][self.opts['qubitid'][0]]['0'][0::2]
        self.y90x180=self.accresult['countsum']['psingle'][self.opts['qubitid'][0]]['0'][1::2]


    def fit(self):
        #pyplot.plot(self.alpha,self.x90y180)
        #pyplot.plot(self.alpha,self.y90x180)
        #pyplot.show()
        fitpara1=fit.fitline(x=self.alpha,y=self.x90y180)
        popt1,pcov1,self.yfit1,rsqr1=[fitpara1['popt'],fitpara1['pcov'],fitpara1['yfit'],fitpara1['rsqr']]
        
        fitpara2=fit.fitline(x=self.alpha,y=self.y90x180)
        popt2,pcov2,self.yfit2,rsqr2=[fitpara2['popt'],fitpara2['pcov'],fitpara2['yfit'],fitpara2['rsqr']]
        self.alphaoptm=(popt2[1]-popt1[1])/(popt1[0]-popt2[0])
        self.debug(4,'popt1,2',popt1,popt2)
#        def func2(xx):
#            x=xx[0]
#            y=xx[1]
#            y0=fit.linepara(x,*popt1)-y
#            y1=fit.linepara(x,*popt2)-y
#        #    print('fit',x,y0,y1)
#            return [y0,y1]
#        self.alphaoptm,self.yatalphaoptm= scipy.optimize.fsolve(func2, [0,0])
        return self.alphaoptm

    def seqs(self,alphas,**kwargs):
        self.opts.update(kwargs)
        self.debug(4,'dragalpha seqs',self.opts)
        self.opts['seqs']=self.baseseqs(qubitid=self.opts['qubitid'],alphas=alphas)
        self.compile()
        self.alpha=alphas

    def plot(self,fig):
        fig.plot(self.alpha,self.x90y180,'r')
        fig.plot(self.alpha,self.y90x180,'g')
        fig.plot(self.alpha,self.yfit1,'b')
        fig.plot(self.alpha,self.yfit2,'k')

    def optimize(self,nsample,alphas=None,**kwargs):
        if self.opts['plot']:
            pyplot.ion()
            fig=pyplot.figure('drag alpha scan optimize')
            pyplot.clf()
            ax=fig.subplots()
            #pyplot.show()
            pyplot.pause(0.1)
        self.seqs(alphas=numpy.arange(-3,3,0.1) if alphas is None else alphas)
        self.run(nsample)
        self.fit()
        if self.opts['plot']:
            fig=pyplot.figure('drag alpha scan optimize')
            self.plot(fig=ax)
            pyplot.draw()
            pyplot.pause(0.1)

        alpha0=self.opts['qchip'].paradict['Gates'][self.opts['qubitid'][0]+'X90'][0]['env'][0]['paradict']['alpha']
        updatedict={('Gates',self.opts['qubitid'][0]+'X90',0,'env',0,'paradict','alpha'):self.alphaoptm}
        updatedict.update({('Gates',self.opts['qubitid'][0]+'rabi',0,'env',0,'paradict','alpha'):self.alphaoptm})
        if self.opts['plot']:
            pyplot.ioff()
        return updatedict





if __name__=="__main__":
    dragalpha=c_dragalpha(qubitid=sys.argv[1],calirepo='../../../../qchip')
    if 0:
        dragalpha.seqs(alphas=numpy.arange(-0.2,0.2,0.01))
        dragalpha.run(400)
#    print(dragalpha.accresult['countsum'])
        print(dragalpha.fit())
    else:
        updatedict=dragalpha.optimize(500)#,alphas=numpy.arange(-5,5,0.2)))
        print(updatedict)
        dragalpha.opts['qchip'].updatecfg(updatedict,dragalpha.opts['qubitcfgfile'])
    fig1=pyplot.figure(figsize=(15,15))
    sub=fig1.subplots(1,1)
    dragalpha.plot(fig=sub)
    #dragalpha.plottyfit(fig=sub)
    pyplot.show()

