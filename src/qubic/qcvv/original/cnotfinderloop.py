import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import numpy
import json
from squbic import c_qchip
import crZXampfinder
import cnotfullxymeas
import cnotfullxyfit
import cnottestbyrb
import datetime
import shutil
import experiment

tcr_list=numpy.arange(120e-9,260e-9,4e-9)
#tcr_list=numpy.arange(128e-9,260e-9,16e-9)
#tcr_list=numpy.array([256e-9])#176e-9
for tcr in tcr_list:
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	cfgname=clargs.qubitcfg[:-5]

	tcr=round(tcr,15)
	delayread=clargs.delayread
	delaybetweenelement=clargs.delaybetweenelement
	nget1=50
	nget2=2
	qubitid_c='Q2'#'Q0'#'Q6'#'Q3'#'Q4'#'Q0'#'Q5'#'Q5'#'Q2'
	qubitid_t='Q1'#'Q7'#'Q7'#'Q2'#'Q3'#'Q1'#'Q4'#'Q6'#'Q1'
	qubitid_list=[qubitid_c,qubitid_t]
	twidth_X90=32e-9
	print('REPORT ******tcr******',tcr)

	acr_center=0.5
	acr_halfspan=0.48
	n_crloop=2
	span_shrink=3

	z1p1deg=246
	z1p2deg=147
	z2p1deg=314
	z2p2deg=234
	z3p1deg=336
	z3p2deg=37
	s00=1
	s01=0
	s10=0
	s11=0
	ZIphase_deg_init=0
	IZphase_deg_init=0
	IXamp_deg_init=0   # (-180,180)
	ZXphase_deg_init=360-IZphase_deg_init

	n_random_cycles=[2,4,8,16,32]
	#n_random_cycles=[1,2,3,4,5]
	n_circuits=30

	with open(cfgname+'.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=c_qchip(chipcfg)

	qchip.updatecfg({
		('Gates','CR'+'('+qubitid_c+qubitid_t+')',0,'twidth'):tcr,
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'t0'):tcr,
		('Gates',qubitid_c+qubitid_t+'CNOT',1,'twidth'):tcr,
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'t0'):tcr},
		wfilename=cfgname+'.json')
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'twidth'):4e-9,
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'twidth'):twidth_X90},
		wfilename=cfgname+'.json')
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'env',0,'paradict','alpha'):qchip.gates[qubitid_c+'rabi'].paralist[0]['env'][0]['paradict']['alpha'],
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'env',0,'paradict','alpha'):qchip.gates[qubitid_t+'rabi'].paralist[0]['env'][0]['paradict']['alpha']},
		wfilename=cfgname+'.json')

	for n_repeat in range(1,2*n_crloop+1,2):
		acr_center=crZXampfinder.crZXampfinder(acr_center=acr_center,acr_halfspan=acr_halfspan,n_repeat=n_repeat,tcr=tcr,delayread=delayread,delaybetweenelement=delaybetweenelement,nget=nget1,qubitid_c=qubitid_c,qubitid_t=qubitid_t,qubitidread=[qubitid_c,qubitid_t],pcr=0,acr_num=81,fixed_ramp=True,readcorr=True)
		acr_halfspan=acr_halfspan/span_shrink
		print('REPORT ###acr_center###',acr_center)
	qchip.updatecfg({
		('Gates','CR'+'('+qubitid_c+qubitid_t+')',0,'amp'):acr_center,
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'amp'):0,
		('Gates',qubitid_c+qubitid_t+'CNOT',1,'amp'):acr_center},
		wfilename=cfgname+'.json')
	print('REPORT $$$acr optimized$$$',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))


	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'pcarrier'):ZIphase_deg_init/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',1,'pcarrier'):ZXphase_deg_init/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'pcarrier'):IZphase_deg_init/180*numpy.pi},
		wfilename=cfgname+'.json')
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'amp'):IXamp_deg_init/90*qchip.gates[qubitid_t+'X90'].paralist[0]['amp'] if IXamp_deg_init>=0 else (IXamp_deg_init+360)/180*qchip.gates[qubitid_t+'X180'].paralist[0]['amp'],
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'twidth'):twidth_X90 if IXamp_deg_init>=0 else round(2*twidth_X90,15)},
		wfilename=cfgname+'.json')
	fullxy_filename=cnotfullxymeas.cnotfullxymeas(qubitid_c=qubitid_c,qubitid_t=qubitid_t,z1p1deg=z1p1deg,z1p2deg=z1p2deg,z2p1deg=z2p1deg,z2p2deg=z2p2deg,z3p1deg=z3p1deg,z3p2deg=z3p2deg,step=10,delaybetweenelement=delaybetweenelement,nget=nget2,readcorr=True)

	popt,perr=cnotfullxyfit.cnotfullxyfit(filename=fullxy_filename,z1p1deg=z1p1deg,z1p2deg=z1p2deg,z2p1deg=z2p1deg,z2p2deg=z2p2deg,z3p1deg=z3p1deg,z3p2deg=z3p2deg,s00=s00,s01=s01,s10=s10,s11=s11)
	ZIphase_deg=ZIphase_deg_init-popt[0]  #theta1
	IZphase_deg=IZphase_deg_init-popt[1]  #theta2
	IXamp_deg=IXamp_deg_init-popt[2]      #theta4
	ZXphase_deg=360-IZphase_deg           #theta3
	IXamp_deg=(IXamp_deg+180)%360-180     #wrap IXamp_deg to (-180,180)
	print('REPORT popt',popt,'perr',perr)
	if perr[0]>2 or perr[1]>2 or perr[2]>5:
		print('REPORT fullxy fitting is not good')

	# amp_X180<0.5
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'pcarrier'):ZIphase_deg/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',1,'pcarrier'):ZXphase_deg/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'pcarrier'):IZphase_deg/180*numpy.pi},
		wfilename=cfgname+'.json')
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'amp'):IXamp_deg/90*qchip.gates[qubitid_t+'X90'].paralist[0]['amp'] if IXamp_deg>=0 else (IXamp_deg+360)/180*qchip.gates[qubitid_t+'X180'].paralist[0]['amp'],
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'twidth'):twidth_X90 if IXamp_deg>=0 else round(2*twidth_X90,15)},
		wfilename=cfgname+'.json')
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	shutil.copyfile(cfgname+'.json',cfgname+'_tcr'+str(tcr)+'_'+timestamp+'.json')
	print('REPORT fit ZIphase_deg',ZIphase_deg)
	print('REPORT fit IZphase_deg',IZphase_deg)
	print('REPORT fit IXamp_deg',IXamp_deg)
	print('REPORT fit ZXphase_deg',ZXphase_deg)
	print('REPORT $$$fitting done$$$',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))


	try:
		result_rb_A=cnottestbyrb.cnottestbyrb(qubitid_list=qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,delaybetweenelement=delaybetweenelement,nget=nget2)
		fidelity_A=1-result_rb_A.e_F.val
		fidelity_std_A=result_rb_A.e_F.std
		SPAM_A=result_rb_A.A.val
		print('REPORT fidelity,fidelity_std',fidelity_A,fidelity_std_A)
		print('REPORT SPAM of the exponential decay A * p ** m',SPAM_A)
		if fidelity_A>0.85 and fidelity_std_A<3e-2 and SPAM_A>0.5:
			print('REPORT first try succeed')
		else:
			print('REPORT first try fail')
	except:
		print('REPORT first rb error')
	print('REPORT $$$first rb test done$$$',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))


	ZIphase_deg=ZIphase_deg-180
	IZphase_deg=IZphase_deg-180
	IXamp_deg=-IXamp_deg
	ZXphase_deg=360-IZphase_deg
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',0,'pcarrier'):ZIphase_deg/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',1,'pcarrier'):ZXphase_deg/180*numpy.pi,
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'pcarrier'):IZphase_deg/180*numpy.pi},
		wfilename=cfgname+'.json')
	qchip.updatecfg({
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'amp'):IXamp_deg/90*qchip.gates[qubitid_t+'X90'].paralist[0]['amp'] if IXamp_deg>=0 else (IXamp_deg+360)/180*qchip.gates[qubitid_t+'X180'].paralist[0]['amp'],
		('Gates',qubitid_c+qubitid_t+'CNOT',2,'twidth'):twidth_X90 if IXamp_deg>=0 else round(2*twidth_X90,15)},
		wfilename=cfgname+'.json')
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	shutil.copyfile(cfgname+'.json',cfgname+'_tcr'+str(tcr)+'_'+timestamp+'.json')
	print('REPORT Reversed')
	print('REPORT reversed ZIphase_deg',ZIphase_deg)
	print('REPORT reversed IZphase_deg',IZphase_deg)
	print('REPORT reversed IXamp_deg',IXamp_deg)
	print('REPORT reversed ZXphase_deg',ZXphase_deg)
	try:
		result_rb_B=cnottestbyrb.cnottestbyrb(qubitid_list=qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,delaybetweenelement=delaybetweenelement,nget=nget2)
		fidelity_B=1-result_rb_B.e_F.val
		fidelity_std_B=result_rb_B.e_F.std
		SPAM_B=result_rb_B.A.val
		print('REPORT fidelity,fidelity_std',fidelity_B,fidelity_std_B)
		print('REPORT SPAM of the exponential decay A * p ** m',SPAM_B)
		if fidelity_B>0.85 and fidelity_std_B<3e-2 and SPAM_B>0.5:
			print('REPORT second try succeed')
		else:
			print('REPORT second try fail')
	except:
		print('REPORT second rb error')
	print('REPORT $$$second rb test done$$$',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
