import os
import datetime
from matplotlib import pyplot
import numpy
from scipy import signal
import sys
sys.path.append('../..')
from qubic.qubic import experiment
#envset,qubicrun,heralding


class c_alignment(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,tlo):
        seqs=[[{'name': 'read', 'qubit': ['alignment'],'modi' : [{},{'t0':tlo}]}]]
        return seqs

    def seqs(self,tlo,**kwargs):
        self.opts.update(kwargs)
        self.opts['heraldcmds']=None
        self.opts['delaybetweenelement']=10e-6
        self.opts['seqs']=self.baseseqs(tlo=tlo)#**self.opts)
        self.opts['gmixs']=None
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
    def run(self,nbuf):
        self.debug(4,'debug c_alignment run, gmixs',self.opts['gmixs'])
        acqval=experiment.c_experiment.acqbufrun(self,nbuf,**self.opts)
        self.acqvalmean=acqval.mean(axis=0)
#        self.s11=(self.result['accval'][self.opts['qubitid']].mean(axis=0))


    def fit(self):
        pass

    def iqplot(self,fig):
        ax1=fig.add_subplot(1,1,1)
        pyplot.subplot(211)
        pyplot.plot(self.acqvalmean[:,0],self.acqvalmean[:,1])
        return fig
    def plot(self,fig):    
        ax1=fig.add_subplot(1,1,1)
        pyplot.subplot(211)
        pyplot.plot(self.opts['chassis'].t,self.acqvalmean[:,0])
        pyplot.subplot(212)
        pyplot.plot(self.opts['chassis'].t,self.acqvalmean[:,1])
        return fig

    def plotmonwithfft(self,fig):
#        #print(type(t),type(buf0),type(buf1))
#        #print(t.shape,buf0.shape,buf1.shape)
        t=self.opts['chassis'].t
        buf0=self.acqvalmean[:,0]
        buf1=self.acqvalmean[:,1]
        fmax=1./(numpy.diff(t).mean())
        df=fmax/(len(t)-1)
        length=len(t)//2#
        f=numpy.arange(length)*df
        fftval0=numpy.fft.fft(buf0)[0:length]
        fftval1=numpy.fft.fft(buf1)[0:length]
        ax1=fig.add_subplot(1,1,1)
        #pyplot.figure(figsize=(8,8))
        pyplot.subplot(221)
        pyplot.plot(t,buf0)
        pyplot.subplot(222)
        pyplot.plot(f,abs(fftval0))
        pyplot.subplot(223)
        pyplot.plot(t,buf1)
        pyplot.subplot(224)
        pyplot.plot(f,abs(fftval1))
#        pyplot.plot(f,numpy.angle(fftval))
##        print(f[0:5],f[-5:-1],df)
##        print(t[0:5],t[-5:-1],fmax)
        return fig
if __name__=="__main__":
    import sys
    sys.path.append('../..')
    alignment=c_alignment(qubitid='alignment',calirepo='../../../../qchip',debug=False)
    tlo=692e-9
    alignment.seqs(tlo=tlo,mon_sel0=0,mon_sel1=3)
    alignment.run(1000)
    fig1=pyplot.figure("vt",figsize=(15,8))
    fig2=pyplot.figure("iq",figsize=(15,8))
    alignment.plot(fig1)
#    alignment.iqplot(fig2)

    pyplot.show()

