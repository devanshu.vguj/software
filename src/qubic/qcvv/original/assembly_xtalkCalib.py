#!/usr/bin/env python3
""" 
combines xtalk summaries for mutiple pairs of qubits
in to hd5 needed by Adam's Julia code computing xtlka mitigated phases for RB

(for now just 2)
"""

import numpy as np
from toolbox.Util_IOfunc import read_yaml
from toolbox.Util_H5io3 import  write3_data_hdf5

import os

import argparse

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-q", "--physQubitIDs", type=int, default=[5,6], nargs='+', help="list of physical qubit IDs, space separated")
    parser.add_argument( "--chipName",  default='chip57',help='xtalk calibration tag name')
    parser.add_argument( "--calTag",  default='q56v4',help='xtalk calibration tag name')
    parser.add_argument("-i", "--inpPath",  default='xtalkSum',help="xtalk summary data location")
    parser.add_argument("-o","--outPath", default='same',help="output path for plots and tables")

    args = parser.parse_args()
    args.basePath='/home/balewski/dataAdamXtalk/'

    args.inpPath=args.basePath+args.inpPath+'/'
    if args.outPath=='same':
        args.outPath=args.inpPath
    else:
        args.outPath=args.basePath+args.outPath+'/'
    
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    assert  os.path.exists(args.inpPath)
    assert  os.path.exists(args.outPath)

    return args

  
#=================================
#=================================
#  M A I N 
#=================================
#=================================
if __name__ == '__main__':
    args=get_parser()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

    nq = len(args.physQubitIDs)
    phys_qubit_ids=np.array(args.physQubitIDs, dtype=np.int64)
    xt_beta=np.ones((nq,nq), dtype=np.float64)
    xt_theta=np.zeros((nq,nq), dtype=np.float64)
    # Anharmonicty assumed based on previous work of this chip (https://arxiv.org/pdf/2003.03307.pdf).
    q_anh=np.array( [-0.271e9]*nq, dtype=np.float64)
    q_freq=np.zeros(nq, dtype=np.float64)
    
    for j1,drQ in enumerate(phys_qubit_ids):
        for j2,rdQ in enumerate(phys_qubit_ids):
            if rdQ==drQ: continue # skip diagonal elements
            qpair_name='%s_rdQ%s-drQ%s'%(args.chipName,drQ,rdQ)
            inpF='%s_xtalkSum.yaml'%(qpair_name)
            blob=read_yaml(args.inpPath+inpF)

            xt_ph=blob['xtalk_phase'][0] % np.pi*2  # Adam wants [0,2p] range
            xt_beta[j1,j2]=blob['xtalk_strength'][0]
            xt_theta[j1,j2]=xt_ph
            q_freq[j1]=blob['read_qubit_drvfreq']+blob['qubit_drive_loc_osc']
    print('beta',xt_beta)
    print('theta',xt_theta)
    print('anh',q_anh)
    print('freq',q_freq)

    outF=args.calTag+'.xtalkCal.h5'
   
    outD={}
    outD['anharmonicity']=q_anh
    outD['crosstalk_beta']=xt_beta
    outD['crosstalk_theta']=xt_theta
    outD['qubit_frequency']=q_freq
    outD['phys_qubit_ids']=phys_qubit_ids
    
    write3_data_hdf5(outD,args.outPath+outF)
  
    print('M: done',outF)
