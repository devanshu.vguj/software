import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip
from cr import c_cr


if __name__=="__main__":
	pcrlist=numpy.arange(0,2*numpy.pi,numpy.pi/8)
	tcrlist=numpy.arange(0,700,48)*1e-9
	for pcr in pcrlist:
		print('pcr',pcr)
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		cr=c_cr()
		parser,cmdlinestr=cr.cmdoptions()
		clargs=parser.parse_args()
		pnts0_traj=[]
		pnts1_traj=[]
		for tcr in tcrlist:
			print('tcr',tcr)
			cr.initseqs()
			cr.crseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,tcr=tcr,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,pcr=pcr,txgate=32e-9)
			cr.run()
			data=cr.rabiacq(clargs.nget)
			fprocess=cr.savejsondata(filename=clargs.filename,extype='cr',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=cr.processrabi(dt=clargs.elementstep,filename=fprocess,dumpname=fprocess[:-4],plot=clargs.plot)
			pnt_traj=1-2*population_norm
			pnts0_traj.append(pnt_traj[0:3])
			pnts1_traj.append(pnt_traj[3:6])
			result=[tcr]
			result.extend(list(pnt_traj.reshape((1,-1))[0]))
			print(result)
			with open('cross_resonance_'+'pcr'+str(pcr)+'_'+timestamp+'.dat','a') as f:
				numpy.savetxt(f,numpy.array(result).reshape((1,-1)))
			print('period, separation, amp',period,separation,amp)
		crdata=numpy.loadtxt('cross_resonance_'+'pcr'+str(pcr)+'_'+timestamp+'.dat')
		for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
			pyplot.figure()
			pyplot.plot(tcrlist*1e9,crdata[:,index+1],label='Control |0>')
			pyplot.plot(tcrlist*1e9,crdata[:,index+4],label='Control |1>')
			pyplot.legend()
			pyplot.xlabel('Pulse Length (ns)')
			pyplot.ylabel(target_projection)
			pyplot.ylim(-1,1)
			pyplot.savefig('cross_resonance_'+'pcr'+str(pcr)+'_'+target_projection[8]+'_'+timestamp+'.png')
		distance=numpy.sqrt((crdata[:,1]-crdata[:,4])**2+(crdata[:,2]-crdata[:,5])**2+(crdata[:,3]-crdata[:,6])**2)/2.0
		pyplot.figure()
		pyplot.plot(tcrlist*1e9,distance)
		pyplot.xlabel('Pulse Length (ns)')
		pyplot.ylabel(r'$|\vec{R}|$')
		pyplot.ylim(0,1)
		print('distance',distance)
		pyplot.savefig('cross_resonance_'+'pcr'+str(pcr)+'_distance_'+timestamp+'.png')
		#pyplot.show()
		bloch=qutip.Bloch()
		bloch.add_points(numpy.array(pnts0_traj).T)
		bloch.add_points(numpy.array(pnts1_traj).T)
		bloch.save('cross_resonance_'+'pcr'+str(pcr)+'_trajectory_'+timestamp+'.png')
		#bloch.show()
