import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from experiment import c_experiment
from rabi_w import c_rabi
from gatepopu import c_gatepopu
from scipy.optimize import minimize,fmin,fminbound
	
def gatepopuscan(gate,ngate,nget,delayread,qubitdrvamp=None):
	gatepopu=c_gatepopu()
	parser,cmdlinestr=gatepopu.cmdoptions()
	print(cmdlinestr)
	clargs=parser.parse_args()
	if clargs.sim:
		gatepopu.setsim()
	para={'delayread':delayread,'delay1':12e-6,'delaybetweenelement':680e-6,'readwidth':2e-6,'elementlength':1,'gate':gate,'ngate':ngate,'qubitid':'Q6','qubitidread':['Q7','Q6','Q5'],'qubitdrvamp':qubitdrvamp}
	gatepopu.gatepopuseqs(**para)
	if clargs.processfile=='':
		gatepopu.run(bypass=clargs.bypass)
		#for i in range(4):
		#	data=gatepopu.gatepopuacq(nget)
		#time.sleep(2)
		data=gatepopu.gatepopuacq(nget)
		fprocess=gatepopu.savejsondata(filename=clargs.filename,extype='gatepopu',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			gatepopu.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm]=gatepopu.processgatepopu(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset,plot=clargs.plot)
	result=-population_norm.mean()
#	print "########qubitdrvamp result########",para['qubitdrvamp'],result
#	sys.stdout.flush()

	return  result

if __name__=="__main__":
	#ngatebase=numpy.array([0,4,8,16,32,64,100,128,200,256,300,400,500,512,600,700,768,800,900,1000,1024])
	ngatebase=numpy.array([0,4,8,16,32,64,128,256,512,768,1024])
	#ngatebase=numpy.array([0,4,8,16,32,48,64,80,128])
	#ngatebase=numpy.array([0,512,1024,2048,4096])
	#ngatebase=numpy.arange(0,200,4)
	#ngatebase=numpy.arange(0,1000,16)
	#for gate in ['X90','Y90','X180','Y180','X270','Y270']:
	for gate in ['X270']:
		ngates=(1 if '180'in gate else 2)+ngatebase
		popu=[]
		for ngate in ngates:
			popuval=-gatepopuscan(gate=gate,ngate=ngate,nget=2,delayread=680e-9,qubitdrvamp=0.462)
			popu.append(popuval)
			print(gate,ngate,popuval)
		pyplot.figure(1)
		pyplot.plot(ngates,popu,'.')
		pyplot.ylim(0.1,1)
		pyplot.title(gate)
		pyplot.figure(2)
		pyplot.semilogx(ngates,popu,'.')
		pyplot.title(gate)
		pyplot.show()
