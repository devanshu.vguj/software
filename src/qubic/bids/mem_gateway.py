#!/usr/bin/python
import sys
import socket
import struct
import time
import sys,getopt
import os
import random
import numpy
import datetime
#from .ether import c_ether
import getopt
class c_mem_gateway():
    " Ethernet IO class for PSPEPS local bus access through mem_gateway "
    def __init__(self, interface,min3=False,memgatewaybug=True,run=True):
        self.interface=interface
        self.min3=min3
        self.memgatewaybug=memgatewaybug
        self.run=run
        pass

    def __del__(self):
        pass

    def transaction_build(self,addr,data=None,write=1):
        cmd_byte = (0x00 if write else 0x10).to_bytes(1,byteorder='big')
        #cmd_byte = '\00' if write else '\x10'
        addr_bytes = struct.pack('!I',addr)[1:4]
        if not data:
            data = 0
        if data < 0:
            data_bytes = struct.pack('!i',data)
        else:
            try:
                data_bytes = struct.pack('!I',data)
            except:
                print(hex(data))
        return cmd_byte + addr_bytes + data_bytes

    def packet_build(self,adwlist=None,adlist=None,alist=None,write=1,dlist=None):
        inputvalid=False
        p=''
        if alist:
            if write:
                if dlist:
                    if len(dlist)==len(alist):
                        wlist=len(alist)*[write]
                        inputvalid=True
                        pass
                    else:
                        print('alist and dlist length do not match')
                else:
                    print('write need dlist if use separated alist and dlist')
            else:
                dlist=len(alist)*[None]
                wlist=len(alist)*[write]
                inputvalid=True
        elif adlist:
            alist=[adlist[i][0] for i in range(len(adlist))]
            dlist=[adlist[i][1] for i in range(len(adlist))]
            wlist=len(adlist)*[write]
            inputvalid=True
        elif adwlist:
            #        print adwlist
            alist=[adwlist[i][0] for i in range(len(adwlist))]
            dlist=[adwlist[i][1] for i in range(len(adwlist))]
            wlist=[adwlist[i][2] for i in range(len(adwlist))]
            inputvalid=True
        else:
            print('provide adwlist [(a,d,w),(a,d,w)] or adlist [(a,d),(a,d),(a,d)] or alist [a,a,a] and dlist[d,d,d]')
        origcnt=len(alist)
        if inputvalid:
            p=bytearray()
            if self.min3:
                if len(alist)==1:
                    alist=3*alist
                    dlist=3*dlist
                    wlist=3*wlist
                elif len(alist)==2:
                    alist.append(alist[1])
                    dlist.append(dlist[1])
                    wlist.append(wlist[1])

            for ix in range(len(alist)):
                p.extend(self.transaction_build(addr=alist[ix],data=dlist[ix],write=wlist[ix]))
#                if (ix%1000==0):
#                    print('memory_gateway packet build',ix,len(alist),type(p))
        return [p,origcnt]

    def unitrw(self,p,rand):
        word1=random.getrandbits(32) if rand else 0xdeadbeef
        word2=random.getrandbits(32)if rand else 0xfacefeed
        h = struct.pack('!I',word1)
        h += struct.pack('!I',word2)
        if self.run:
            self.interface.socket.send(h+p)
            readvalue, addr = self.interface.socket.recvfrom(1450)  # buffer size is 1024 bytes
        else:
            readvalue = h+p
        if (readvalue[0:8] != h[0:8]):
            print("header mismatch read: %s send: %s"%(readvalue[0:8].encode('hex'),p[0:8].encode('hex')))
        if self.memgatewaybug:
            readvalue=readvalue[0:-1]   # for a bug in mem gateway, trim off the last byte
        readvalue=readvalue[8:]   # for a bug in mem gateway, trim off the last byte
        return readvalue

    def readwrite(self, adwlist=None,write=None,adlist=None,alist=None,dlist=None,rand=True):
        [p,origcnt] = self.packet_build(adwlist=adwlist,adlist=adlist,alist=alist, write=write,dlist=dlist)
        #print 'origcnt',origcnt
        #print len(p)
        #print p.encode('hex')
        result=b''
        while (len(p)>150*8):  #  udp max packet size 1472
            readvalue=self.unitrw(p[0:150*8],rand=rand)
            result+=readvalue
            p=p[150*8:]
        if p:
            readvalue=self.unitrw(p,rand=rand)
            result+=readvalue
        return result[0:8*origcnt]

    def parse_readvalue(self,readvalue,packformat=None):
        #print 'parse_readvalue',len(readvalue),type(readvalue)
        if not packformat:
            packformat='!%dQ'%(len(readvalue)/8)
#        print ":".join("{:02x}".format(ord(c)) for c in readvalue)
        #print (readvalue),len(readvalue)
        result = struct.unpack(packformat, readvalue)
        return result

#        print result

if __name__=="__main__":
    from bmb7 import c_bmb7
    b=c_bmb7(ip='192.168.165.38',port=50006,reset=True,powercycle=True,bitfilepath='../gun.bit')
    mg = c_mem_gateway(b.interface, min3=True)
    alist=range(0x130000,(0x130000)+16)
    print([hex(i) for i in alist])
    result=mg.readwrite(alist=alist,dlist=16*[0],write=0)
    print([hex(i) for i in mg.parse_readvalue(result)])
    alist=range(0x10000,(0x10000)+16)
    print([hex(i) for i in alist])
    result=mg.readwrite(alist=alist,dlist=16*[0],write=1)
    print([hex(i) for i in mg.parse_readvalue(result)])
#    result=mg.readwrite(alist=[6],dlist=[0x80008003],write=0)
#    print mg.parse_readvalue(result)
#    mg = c_mem_gateway(b.interface, min3=True)
#    result=mg.readwrite(alist=[1,2,18,19],dlist=[0,0,0x80008003,0],write=0)
#    print mg.parse_readvalue(result)
#
#    opts,args=getopt.getopt(sys.argv[1:],'ha:p:A:D:W:',['help','addr=','port=','Addr=','Data=','Write='])
#    ip_addr = '192.168.21.12'
#    port=50006
#    Addr=0
#    Data=0
#    Write=0
#    for opt,arg in opts:
#        if opt in ('-h', '--help'):
#            usage()
#            sys.exit()
#        elif opt in ('-a', '--address'):
#            ip_addr = arg
#        elif opt in ('-p', '--port'):
#            port = int(arg)
#        elif opt in ('-A', '--Addr'):
#            Addr = eval(arg)
#        elif opt in ('-D', '--Data'):
#            Data = eval(arg)
#        elif opt in ('-W', '--Write'):
#            Write = eval(arg)
#    min3 = port == 3000
#    mg = c_mem_gateway(ip_addr, port, min3=min3)
#    adw=[(Addr,Data,Write)]
#    print adw
#    result=mg.readwrite(adwlist=adw,rand=False)
#    print mg.parse_readvalue(result)[0][2].encode('hex')
#
##    mg=c_mem_gateway(IP_ADDR,3000,min3=True)
##    a=eval(sys.argv[1])
##    d=eval(sys.argv[2])
##    w=eval(sys.argv[3])
##    IP_ADDR = '192.168.21.76'
##    adw=[(0,0,0),(0,0,0)]
##    alladwdata=[(5,0x0102,1)
##            ,(5,0x80,1)
##            ,(5,0x2,1)
##            ,(5,0x192,1)
##            ,(5,0x0,1)
##            ,(5,0x100,1)
##            ,(6,0,0)
##            ]
#
#
#'''
#    result=mg.readwrite(alist=[6],dlist=[0x80008003],write=1)
#    mg.parse_readvalue(result)
#    result=mg.readwrite(alist=[0,1,2,3,4,5,6],write=0)
#    mg.parse_readvalue(result)
#    result=mg.readwrite(adlist=[(6,0x80008003)],write=1)
#    mg.parse_readvalue(result)
#    result=mg.readwrite(adwlist=[(6,0x80008003,1),(6,0x80008001,0)],write=1)
#    mg.parse_readvalue(result)
#'''
