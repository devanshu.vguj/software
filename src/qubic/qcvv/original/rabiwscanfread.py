#import datetime
#import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
#import time
#import init
#import pprint
#from experiment import c_experiment
from rabi_w import c_rabi
from scipy.optimize import minimize,fmin,fminbound

def freadoptimize(x,rabi,qubitid,nget,elementstep):
	fread=x
	rabi.hf.cmd128all=[]
	rabi.hf.cmdgen2(destfrdrv={qubitid+'.rdrv':fread},destfread={qubitid+'.read':fread})
	rabi.hf.run(seqs=rabi.seqs,memclear=False,cmdclear=False)
	data=rabi.rabiacq(nget)
	fprocess=rabi.savejsondata(filename='',extype='tmp',cmdlinestr='',data=data,timeinfo=False)
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset='',plot=False,isfitdecay=False)
	result=abs(0.5-amp)
	print("######################fread###########################",fread)
	print("################result,amp,separation#################",result,amp,separation)
	sys.stdout.flush()
	return result

def rabiwscanfread(fread_x0,fread_x1,fread_x2,fqubit,readoutdrvamp,qubitdrvamp,delayread,nget,delay1,delaybetweenelement,elementlength,elementstep,qubitid,qubitidread=['Q7','Q6','Q5'],qubitcfg='qubitcfg.json',ip='192.168.1.124',wiremapmodule='wiremap_X6Y8_20210629',xtol=50e3):
	rabi=c_rabi(qubitcfg=qubitcfg,ip=ip,wiremapmodule=wiremapmodule)
	rabi.rabiseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength,elementstep=elementstep,rdc=0,fqubit=fqubit,fread=fread_x0,qubitdrvamp=qubitdrvamp,readoutdrvamp=readoutdrvamp,qubitid=qubitid,qubitidread=qubitidread)
	rabi.run()
	fread=fminbound(func=freadoptimize,x1=fread_x1,x2=fread_x2,xtol=xtol,disp=3,args=(rabi,qubitid,nget,elementstep))
	return fread

if __name__=="__main__":
	fread=rabiwscanfread(153e6,152e6,156e6,166.4e6,0.40,0.4,680e-9,20,12e-6,600e-6,80,4e-9,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123','wiremap_X6Y8_20210629')
	#fread=rabiwscanfread(154e6,153.6e6,154.3e6,166.4e6,0.40,0.4,680e-9,20,12e-6,600e-6,80,4e-9,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123')
	#fread=rabiwscanfread(153e6,153.7e6,154.7e6,166.4e6,0.40,0.4,680e-9,20,12e-6,600e-6,80,4e-9,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123')
