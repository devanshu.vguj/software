import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.analysis import gmm,readcorr
class c_rcal(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,**kwargs):
        seqs=[]
        seq=[]
        seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid})
        seqs.append(seq)
        if 'ef' in kwargs:
            if kwargs['ef']:
                seq=[]
                seq.append({'name': 'X90', 'qubit': qubitid})
                seq.append({'name': 'X90', 'qubit': qubitid})
                seq.append({'name': 'X90_ef', 'qubit': qubitid})
                seq.append({'name': 'X90_ef', 'qubit': qubitid})
                seq.append({'name': 'read', 'qubit': qubitid})
                seqs.append(seq)

        return seqs

    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(**self.opts)
        #print(self.opts['seqs'])
        self.compile()

    def run(self,nsample,**kwargs):
        self.accbufrun(nsample=nsample,**kwargs)
        self.psingle=self.accresult['countsum']['psingle']
        self.pcombine=self.accresult['countsum']['pcombine']
#        print(list(self.pcombine.keys()))
        self.prep0read1=1-self.pcombine[('0',)][0]
        self.prep1read0=self.pcombine[('0',)][1]

    def savercal(self,labels=['0','1'],rcalpath='.'):
        #        combineorder=self.accresult['countsum']['combineorder']
        #corrfiledict={combineorder[0]:'/'.join([savepath,'%s_rcal.npz'%combineorder[0]])}
        rcal=readcorr.readcorr(countsum=self.accresult['countsum'],rcalpath=rcalpath)#combineorder=combineorder,corrfiledict=corrfiledict)
        rcal.save(pmeas=self.pcombine,labels=labels)

#        m=numpy.zeros((len(labels),len(labels)))
#        print('psingle')
#        print(self.psingle)
#        print('pcombine')
#        print(self.accresult['countsum']['combineorder'])
#        print(self.pcombine)
#        for ii,i in enumerate(labels):
#            for jj,j in enumerate(labels):
#                m[jj,ii]=self.psingle[self.opts['qubitid'][0]][i][jj]
#        #numpy.savez('%s_rcal.npz'%self.opts['qubitid'][0],**{self.opts['qubitid'][0]:dict(labels=labels,matrix=numpy.linalg.inv(m))})
#        numpy.savez('%s_rcal.npz'%self.opts['qubitid'][0],**{self.opts['qubitid'][0]:dict(labels=labels,matrix=m)})
    #    print(m)
    #    print(numpy.linalg.inv(m))
    #    print(m.shape)
    #    print(type(m))
#
#        print(self.accresult['gmixs'][self.opts['qubitid']].labels)
    def hearaldiqplot(self,index,ax):
        accvaliq=self.accresult['accvalmafterheralding'][self.opts['qubitid'][0]][index]
        ax.tick_params(axis='both',which='both',top=False,bottom=False,labelbottom=False,left=False,right=False,labelleft=False)

        ax.set_ylabel('Measured I/Q after Heralding')
        return plot.iqplot(iq=accvaliq,fig=ax)
    def accvalhplot(self,index,ax):
        accvaliq=self.accresult['accvalh'][self.opts['qubitid'][0]][:,index]
        ax.tick_params(axis='both',which='both',top=False,bottom=False,labelbottom=False,left=False,right=False,labelleft=False)
        ax.set_ylabel('Heralding I/Q')
        return plot.iqplot(iq=accvaliq,fig=ax)
    def accvalmplot(self,index,ax):
        accvaliq=self.accresult['accvalm'][self.opts['qubitid'][0]][:,index]
        ax.tick_params(axis='both',which='both',top=False,bottom=False,labelbottom=False,left=False,right=False,labelleft=False)
        ax.set_ylabel('Measured I/Q')
        return plot.iqplot(iq=accvaliq,fig=ax)

if __name__=="__main__":
    #rcal=c_rcal(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=1,gmixs='default')
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--live',help='live update',dest='live',default=False,action='store_true')
    parser.add_argument(help='qubitid',dest='qubitid',type=str)
    parser.add_argument('-s','--save',help='save rcal',dest='save',default=False,action='store_true')
    parser.add_argument('-n','--nshot',help='shot count',dest='nshot',default=10000,type=int)
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    clargs=parser.parse_args()
    qubitid=clargs.qubitid
    rcal=c_rcal(qubitid=qubitid,calirepo='../../../../qchip',debug=1,gmixs='./')
    rcal.seqs(elementlength=80,elementstep=4e-9,ef=clargs.ef)
    firsttime=True
    while clargs.live or firsttime:
        firsttime=False
        rcal.run(clargs.nshot)
        if clargs.save:
            rcal.savercal()
            rcalload=numpy.load('%s_rcal.npz'%qubitid,allow_pickle=True)
            print(rcalload['matrix'])
            print(rcal.pcombine)
            print('corr',rcal.accresult['pcombinecorr'])
        #print(list(rcaldict.keys()))
#    gmm.rcal(rcal.accresult['countsum'],[rcal.opts['qubitid'][0]],rcaldict)
        #print(rcal.accresult['countsum']['psingle'])
#    print(rcal.accresult['countsum']['pcombine'])
        print('prep0read1','%8.3f'%rcal.prep0read1, 'prep1read0','%8.3f'%rcal.prep1read0)
    print(rcal.accresult['countsum']['psingle'])
