import trueq
config = trueq.Config("""
    Name: MyDevice
    N_Systems: 2
    Mode: ZXZXZ
    Dimension: 2
    Gate Z(phi):
        Hamiltonian:
            - ["Z", phi]
        Involving: {'(5,)': (), '(6,)': ()}
    Gate X(phi):
        Hamiltonian:
            - ["X", phi]
        Involving: {'(5,)': (), '(6,)': ()}
    Gate CNOT:
        Matrix:
            - [1, 0, 0, 0]
            - [0, 1, 0, 0]
            - [0, 0, 0, 1]
            - [0, 0, 1, 0]
        Involving:
            (6, 5) : ()
    """
)
