import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_rabia(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def rabiseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,qubitid='Q7',qubitidread=['Q7','Q6','Q5']):
		self.qubitid=qubitid
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		if readoutdrvamp:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp:
			modrabi.update(dict(amp=qubitdrvamp))
		if fqubit:
			modrabi.update(dict(fcarrier=fqubit))
		if readwidth:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))

		run=0
		self.da=0.95/(elementlength-1)
		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			trabi=self.seqs.tend()+delay1
			arabi=self.da*irun
			modrabi.update(dict(amp=arabi))
			self.seqs.add(trabi,         	 self.qchip.gates[qubitid+'rabi'].modify(modrabi))

			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def rabiacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabi(self,dt,filename,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		c=self.loadjsondata(filename)
		return self.processrabidata(dt=dt,data=c[list(c.keys())[0]],dumpdataset=dumpdataset,loaddataset=loaddataset,plot=plot,isfitdecay=isfitdecay)
	def processrabidata(self,dt,data,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		rabi_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		if isfitdecay:
			[amp,period,tdecay,fiterr]=self.fitrabidecay(dt=self.da,data=rabi_result['population_norm'],plot=plot)
		else:
			[amp,period,fiterr]=self.fitrabi(dx=self.da,data=rabi_result['population_norm'],plot=plot)
		return [data,rabi_result['separation'],rabi_result['iqafterherald'],rabi_result['population_norm'],amp,period,fiterr]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	rabi=c_rabia(**clargs.__dict__)
	if clargs.sim:
		rabi.setsim()
	rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		rabi.run(bypass=clargs.bypass)
		data=rabi.rabiacq(clargs.nget)
		fprocess=rabi.savejsondata(filename=clargs.filename,extype='rabia',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			rabi.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
	print('period',period)
	print('separation',separation)
	print('amp',amp)
	print(rabi.hf.adcminmax())
	rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
#	rabi.plotafterheraldingtest(iqafterherald)
	rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
