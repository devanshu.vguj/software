import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
if __name__=="__main__":
	sim=1
	interface=c_ether('192.168.1.122',port=3000)
	mg = c_mem_gateway(interface, min3=True,memgatewaybug=False)
	with open('qubitcfg.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=c_qchip(chipcfg)
	seq=[]
	seq.extend([(0,				qchip.gates['Q1trig'].modify({"twidth":20e-9}))])
#	trun=numpy.arange(0,50*1000e-9,1000e-9)
	trun=0;
	delayread=524e-9#32e-9#528e-9#32e-9
	delay1=600e-9#900e-9
	delay2=120e-9
	delay3=200e-9
	for irun in range(20):
		trun=trun+800e-9#1500e-9
		run=trun
		wrun1=40e-9+40e-9*irun#(20 if irun <7 else 40 if irun <14 else 80 if irun < 21 else 160 if irun <23 else 320 if irun <24 else 400)*1e-9#(80e-9 if irun <1 else 120e-9 if irun <2 else 160e-9 if irun < 4 else 200e-9 if irun < 7 else 240e-9)#(10 if irun <7 else 10 if irun <14 else 20 if irun < 21 else 40 if irun <28 else 50 if irun <35 else 60)*1e-9
		wrun2=20e-9+20e-9*(2*irun+1)
#		rrun0=(20 if irun <7 else 40 if irun <14 else 80 if irun < 21 else 160 if irun <28 else 320 if irun <35 else 400)*1e-9#(80e-9 if irun <1 else 120e-9 if irun <2 else 160e-9 if irun < 4 else 200e-9 if irun < 7 else 240e-9)#(10 if irun <7 else 10 if irun <14 else 20 if irun < 21 else 40 if irun <28 else 50 if irun <35 else 60)*1e-9
		rrun0=12e-9+20e-9*irun#16e-9+16e-9*irun;#(20 if irun <7 else 40 if irun <14 else 80 if irun < 21 else 160 if irun <23 else 320 if irun <24 else 400)*1e-9#(80e-9 if irun <1 else 120e-9 if irun <2 else 160e-9 if irun < 4 else 200e-9 if irun < 7 else 240e-9)#(10 if irun <7 else 10 if irun <14 else 20 if irun < 21 else 40 if irun <28 else 50 if irun <35 else 60)*1e-9
		rrun2=(400 if irun <7 else 320 if irun <14 else 160 if irun < 21 else 80 if irun <23 else 40 if irun <24 else 20)*1e-9#(80e-9 if irun <1 else 120e-9 if irun <2 else 160e-9 if irun < 4 else 200e-9 if irun < 7 else 240e-9)#(10 if irun <7 else 10 if irun <14 else 20 if irun < 21 else 40 if irun <28 else 50 if irun <35 else 60)*1e-9
#		rrun0=(80 if irun <1 else 120 if irun <2 else 160 if irun < 4 else 200 if irun < 7 else 240)*1.0e-9#(10 if irun <7 else 10 if irun <14 else 20 if irun < 21 else 40 if irun <28 else 50 if irun <35 else 60)*1	  
		#print 'rrun0',rrun0
		rrun1=48e-9#80e-9
		arun0=(0.1 if irun <7 else 0.2 if irun <14 else 0.5 if irun < 21 else 0.7 if irun <23 else 0.8 if irun <24 else 0.9)
		arun=0.2*(irun+1)
		seq.extend([
			(run+40e-9,				qchip.gates['Q1readoutdrv'].modify({"amp":1.0,"twidth":rrun0}))
			,(run+40e-9+delayread,			qchip.gates['Q1readout'].modify({"twidth":rrun0,"amp":1.0}))
			#,(run+40e-9,				qchip.gates['Q0readoutdrv'].modify({"amp":0.25,"twidth":rrun2}))
			#,(run+40e-9,				qchip.gates['Q2play'].modify({"amp":0.5,"twidth":rrun2}))
			#,(run+40e-9+delayread,			qchip.gates['Q0readout'].modify({"twidth":rrun2,"amp":1.0}))
			#,(run+40e-9+delayread,			qchip.gates['Q2readout'].modify({"twidth":rrun0,"amp":1.0}))
			#,(run+40e-9+delay2,				qchip.gates['Q0rabi_gh'].modify({"twidth":wrun1,"amp":1.0}))
			#,(run+40e-9+delay2+delay3,				qchip.gates['Q1rabi_gh'].modify({"twidth":wrun2,"amp":1.0}))
			,(run+40e-9+delay1+20e-9,		qchip.gates['Q1readoutdrv'].modify({"amp":1.0,"twidth":rrun1}))
			#,(run+40e-9+delay1+wrun+20e-9,		qchip.gates['Q0readoutdrv'].modify({"amp":0.25,"twidth":rrun1}))
			,(run+40e-9+delay1+20e-9+delayread,	qchip.gates['Q1readout'].modify({"twidth":rrun1}))
			#,(run+40e-9+delay1+wrun+20e-9+delayread,	qchip.gates['Q0readout'].modify({"twidth":rrun1}))
			#,(run+40e-9+delay1+wrun+20e-9+delayread,	qchip.gates['Q2readout'].modify({"twidth":rrun1}))
			#,(run+40e-9+delay1+wrun+20e-9+delayread,	qchip.gates['Q2play'].modify({"amp":0.5,"twidth":rrun1}))
			])


		#					(run+40e-9,				qchip.gates['Q1readoutdrv'].modify({"amp":arun0}))
#					,(run+40e-9+delayread,			qchip.gates['Q1readout'].modify({"amp":arun0}))
		   #(run+40e-9,				qchip.gates['Q1readoutdrv'].modify({"amp":0.25,"twidth":rrun0}))
				#			,(run+40e-9+delayread,			qchip.gates['Q0readout'].modify({"twidth":rrun0,"amp":0.0}))
					#,(run+40e-9+delayread,			qchip.gates['Q2readout'].modify({"twidth":rrun0,"amp":0.0}))
							#(run+40e-9,				qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
					#,
#				,(run+40e-9+delayread,			qchip.gates['Q0readout'])
#				,(run+delay1+wrun,		qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
							#				,(run+delay1+wrun+delayread,	qchip.gates['Q1readout'])
							#,(None,qchip.gates['Q0rabi_ef'])
					#	,(None,qchip.gates['Q1rabi_gh'].modify({"amp":0.5}))
					#			,(None,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
					#,(None,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
			#	   seq.extend([(run+0,qchip.gates['Q1rabi_gh'])
			#   ,(None,qchip.gates['Q1rabi_gh'].modify({"twidth":wrun}))
			#   ,(None,qchip.gates['Q1rabi_gh1'])
			#   ,(None,qchip.gates['Q1rabi_gh2'])
			#   ])
	seqs=c_seqs(seq)
	hf=c_hfbridge()
	((cmda,cmdv),ops)=hf.cmdgen(seqs,dt=1.0e-9)

	alist=[]
	dlist=[]
	alist.extend(range(0x38000,0xc0000))
	dlist.extend((0xc0000-0x38000)*[000])  # amplitude to internal lo for demodulating
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)

#	print cmdv
#	cmdv=numpy.array(cmdv).astype('Complex64')
	alist=list(cmda)
	dlist=[((int(v.real*19894)<<16)+int(v.imag*19894)) for v in cmdv]
#	list((int(numpy.round((cmdv*19800).real))<<16)+(int(numpy.round((cmdv*19800).imag))))
	cmds=[]
	for i in range(len(alist)):
		cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
#		print('\n'.join(cmds))
#	print [hex(i) for i in alist]
#	print [hex(i) for i in dlist]
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	alist=[]
	dlist=[]

	digiloopback=1
	alist.append(0x50004)
	dlist.append(digiloopback)

	bufsel=(0b000<<9)+(0b000<<6)+(0b001<<3)+0b000
	alist.append(0x50001)
	dlist.append(bufsel)
	
	opsel=2 # 0 for navg; 1 for min; 2 for max;
	alist.append(0x5000f)
	dlist.append(opsel)
	mon_navr=3
	alist.append(0x50006)
	dlist.append(mon_navr)
	mon_dt=500
	alist.append(0x50007)
	dlist.append(mon_dt)
	mon_slice=0
	alist.append(0x50008)
	dlist.append(mon_slice)
	mon_sel0=0
	alist.append(0x50009)
	dlist.append(mon_sel0)
	mon_sel1=17
	alist.append(0x5000a)
	dlist.append(mon_sel1)
	alist.append(0x5000b)
	dlist.append(1)
	alist.append(0x5000c)
	dlist.append(0)
	
	#period=5100/4 if sim else 3000000/4
	period=5100*15/4 if sim else 30000000/4
	#period=5100*15/4 if sim else 3000000/4
	alist.extend((0x7,0x8,16))  # period_adc,period_dac0,period_stream
	dlist.extend((period,period,period))

	alist.append(0x50000)
	dlist.append(0xff)

	cmddict={}
	for cmd in ops:
		cmdadd(cmddict,cmdgen(trig_t=int(ops[cmd]['trig_t'])
			,element=int(ops[cmd]['element'])
			,dest=int(ops[cmd]['dest'])
			,start=int(ops[cmd]['start'])
			,length=int(ops[cmd]['length'])
			,phini=ops[cmd]['phini']/2/numpy.pi*360
			,freq=ops[cmd]['freq']
			))
		#print 'element',int(ops[cmd]['element']),'start',int(ops[cmd]['start']),'length',int(ops[cmd]['length'])
		#print 'element',(ops[cmd]['phini'])
	cmdlist=[v for k,v in sorted(cmddict.items())]
	for i in range(len(cmdlist)):
		#alist.extend([0x40000+2*i,0x40400+2*i,0x40400+2*i+1])
		alist.extend([0x80000+4*i,0x80000+4*i+1,0x80000+4*i+2])
		dlist.extend([(cmdlist[i]>>64)&0xffffffff,(cmdlist[i]>>32)&0xffffffff,cmdlist[i]&0xffffffff])
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	for i in range(len(alist)):
		cmds.append("lb_write_task(20'h%x,32'h%x);"%(alist[i],dlist[i]))
	f=open('cmdsimlist.vh','w')
	f.write('\n'.join(cmds))
	f.close()

	tend=20000e-9
	from cmdsim import cmdsim
	sim1=cmdsim((cmda,cmdv),ops)
	for chan in range(7):
		tv=sim1.getiqmod(chan)
		#	tv=sim1.getiq(chan)
		pyplot.subplot(7,2,chan*2+1)
		pyplot.plot(tv[:,0],tv[:,1].real,'.')
		pyplot.xlim([0,tend/1e-9])
		pyplot.subplot(7,2,chan*2+2)
		pyplot.plot(tv[:,0],tv[:,1].imag,'.')
		pyplot.xlim([0,tend/1e-9])
		#print 'chan %d max %8.2f'%(chan,max(abs(tv[:,1])))
	pyplot.show()
