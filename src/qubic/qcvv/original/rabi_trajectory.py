import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip

class c_rabitrajectory(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid=None
		pass
	def rabitrajectoryseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,postgate='I',qubitid='Q7',qubitidread=['Q5','Q4','Q3']):
		self.qubitid=qubitid
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0;
		for irun in range(elementlength):

			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			trabi=run+delay1
			wrabi=elementstep*(irun)
			if wrabi!=0:
				modrabi.update(dict(twidth=wrabi))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid+'rabi'].modify(modrabi))
			pnewx=self.qchip.gates[qubitid+'rabi'].pcalc(dt=wrabi,padd=0,freq=fqubit)[0]
			pnewy=self.qchip.gates[qubitid+'rabi'].pcalc(dt=wrabi,padd=-numpy.pi/2.0,freq=fqubit)[0]
			if postgate=='I':
				pass
			else:
				if postgate=='X90':
					self.seqs.add(self.seqs.tend(),self.qchip.gates[qubitid+'X90'].modify(dict(pcarrier=pnewx)))
				if postgate=='Y-90':
					self.seqs.add(self.seqs.tend(),self.qchip.gates[qubitid+'Y-90'].modify(dict(pcarrier=pnewy)))
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		#self.bufwidth=[self.seqs.countdest('Q5.read'),self.seqs.countdest('Q4.read'),self.seqs.countdest('Q3.read')]
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	postgate_list=['Y-90','X90','I']
	pnts_traj=[]
	for postgate in postgate_list:
		rabitrajectory=c_rabitrajectory()
		parser,cmdlinestr=rabitrajectory.cmdoptions()
		clargs=parser.parse_args()
		rabitrajectory.rabitrajectoryseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,postgate=postgate,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
		rabitrajectory.run()
		data=rabitrajectory.rabiacq(clargs.nget)
		fprocess=rabitrajectory.savejsondata(filename=clargs.filename,extype='rabitrajectory'+postgate,cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabitrajectory.processrabi(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
		if clargs.readcorr:
			population_norm=rabitrajectory.readoutcorrection(qubitid_list=[clargs.qubitid],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
			print('corrected population_norm',population_norm)
		pnts_traj.append(1-2*population_norm)
		with open('rabi_trajectory_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,population_norm.reshape((1,-1)))
		print('period, separation, amp',period,separation,amp)
		#rabitrajectory.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
		rabitrajectory.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
		if clargs.plot:
			pyplot.grid()
			#pyplot.show()
		print(rabitrajectory.hf.adcminmax())
	bloch=qutip.Bloch()
	bloch.add_points(pnts_traj)
	bloch.show()
	pyplot.show()
	#python rabi_trajectory.py --plot -n 50 -r 0.69 -fr 103.3e6 -q 0.7 -fq  287e6 -d 100e-6 --qubitid Q5
