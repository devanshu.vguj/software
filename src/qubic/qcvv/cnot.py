import re
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qubic import heralding
from qubic.qcvv.gatesim import gatesim
import numpy.linalg
class c_cnot(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
        self.gatename=''.join(self.opts['qubitid'])+'CNOT'
    def baseseqs(self,qubitid,repeat=1,**kwargs):
        opts=dict(pzctl=None,pztgt=None,axtgt=None,acr=None)
        opts.update({k:v for k,v in kwargs.items() if k in opts})
        crmodi={}
        cnotmodi0={}
        cnotmodi1={}
        cnotmodi2={}
        cnotmodi3={}
        cnotmodi4={}
        crmodi.update({} if opts['acr'] is None else {'amp':opts['acr']})
        cnotmodi1=crmodi
        cnotmodi0.update({} if opts['pztgt'] is None else {'pcarrier':opts['pztgt']})
        cnotmodi2.update({} if opts['pztgt'] is None else {'pcarrier':-opts['pztgt']})
        cnotmodi3.update({} if opts['pzctl'] is None else {'pcarrier':opts['pzctl']})
        cnotmodi4.update({} if opts['axtgt'] is None else {'amp':opts['axtgt']})
        seqs=[]
        for ctlinit in [0,1]:
            for tgtinit in [0,1]:
                seq=[]
                if ctlinit==1:
                    seq.append({'name': 'X90', 'qubit': qubitid[0]})
                    seq.append({'name': 'X90', 'qubit': qubitid[0]})
                if tgtinit==1:
                    seq.append({'name': 'X90', 'qubit': qubitid[1]})
                    seq.append({'name': 'X90', 'qubit': qubitid[1]})
                for r in range(repeat):
                    seq.append({'name': 'CNOT', 'qubit': qubitid, 'modi':[cnotmodi0,cnotmodi1,cnotmodi2,cnotmodi3,cnotmodi4]})
                seq.extend([{'name': 'read', 'qubit': qubit} for qubit in qubitid])
                seqs.append(seq)
        #print(seqs)        
        return seqs

    def runseqs(self,nsample,repeat=1,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(self.opts['qubitid'],repeat=repeat)
        self.compile()
        self.accbufrun(nsample=nsample)
        self.result=self.accresult['countsum']['pcombine']
    def error(self):
        resultmatrix=numpy.zeros((4,4))
        resultmatrix[:,0]=self.accresult['countsum']['pcombine'][('0','0',)]
        resultmatrix[:,1]=self.accresult['countsum']['pcombine'][('0','1',)]
        resultmatrix[:,2]=self.accresult['countsum']['pcombine'][('1','0',)]
        resultmatrix[:,3]=self.accresult['countsum']['pcombine'][('1','1',)]
        print('resultmatrix')
        print(resultmatrix)
        error=numpy.linalg.norm(resultmatrix-gatesim.cnot())
        return error
    def optimizesetup(self,**kwargs):
        self.center={}
        self.center['pztgt']=self.opts['qchip'].gates[self.gatename].pulses[0].pcarrier
        if self.opts['qchip'].gates[self.gatename].pulses[2].pcarrier+self.opts['qchip'].gates[self.gatename].pulses[0].pcarrier > 0.0001:
            exit('cnot pztgt should be +/- pairs')
        self.center['pzctl']=self.opts['qchip'].gates[self.gatename].pulses[3].pcarrier
        self.center['axtgt']=self.opts['qchip'].gates[self.gatename].pulses[4].amp
        self.center['acr']=self.opts['qchip'].gates[self.gatename].pulses[1].amp
        self.span=dict(pztgt=0.3,pzctl=0.3,axtgt=0.2,acr=0.1)
        self.steps=dict(pztgt=10,pzctl=10,axtgt=10,acr=10)
        self.update={}
        self.update['acr']=('Gates',''.join(self.opts['qubitid'])+'CR',0,'amp')
        self.update['pzctl']=('Gates',''.join(self.opts['qubitid'])+'CNOT',3,'pcarrier')
        self.update['pztgt']=('Gates',''.join(self.opts['qubitid'])+'CNOT',0,'pcarrier')
        self.scanmax=dict(pztgt=numpy.pi*2,pzctl=numpy.pi*2,axtgt=1.0,acr=1.0)
        self.scanmin=dict(pztgt=0,pzctl=0,axtgt=0.0,acr=0.0)
        
        self.opts.update(dict(ngatemult=2,ngateadd=1,xtol=0.01))
        self.opts.update(kwargs)
        self.acr=None
        self.axtgt=None
        self.pztgt=None
        self.pzctl=None
    def scan(self,key,nsample,center,span,steps,repeat):
        if center is None:
            center=self.center[key]
        if span is None:
            span=self.span[key]
        if steps is None:
            steps=self.steps[key]
        x0=max(self.scanmin[key],center-span/2)
        x1=min(self.scanmax[key],center+span/2)
        scanvars=numpy.linspace(x0,x1,steps)
        print('center,span,step,repeat',center,span,steps,repeat)
        errors=numpy.zeros(len(scanvars))
        for isv,sv in enumerate(scanvars):
            opts=dict(qubitid=self.opts['qubitid'],repeat=repeat)
            opts[key]=sv
            self.opts['seqs']=self.baseseqs(**opts)
            self.compile()
            self.accbufrun(nsample=nsample)
            errors[isv]=self.error()
            print('scan',key,isv,sv,errors[isv])
        
        est=[(min(errors)-max(errors))/(max(scanvars)-min(scanvars))**2,scanvars[int(len(scanvars)/2)],1]
        fitpara=fit.fitquadratic(scanvars,errors,p0=est)
        if self.opts['plot']:
            pyplot.figure('optimize each')
            pyplot.clf()
            pyplot.plot(scanvars,errors,'*')
            pyplot.plot(scanvars,fitpara['yfit'],'-')
            pyplot.draw()
            pyplot.figure('optimize all')
            pyplot.plot(scanvars,errors,'*')
            pyplot.plot(scanvars,fitpara['yfit'],'-')
            pyplot.draw()
            pyplot.pause(0.1)
        return fitpara['popt'][1] if fitpara['popt'] is not None else center
        #pyplot.show()
#    def scanacr(self,nsample,repeat=1,delta=0.2,steps=10):
#        acrs=numpy.linspace(acr0-delta,acr0+delta,steps)
#
#
##        pztgts=numpy.linspace(0,numpy.pi*2,20)#pztgt0-numpy.pi/5,pztgt0+numpy.pi/5,20)
##        pzctls=numpy.linspace(0,numpy.pi*2,20)#pzctl0-numpy.pi/3,pzctl0+numpy.pi/3,10)
##        axtgts=numpy.linspace(0,1,20)#axtgt0-numpy.pi/3,pzctl0+numpy.pi/3,10)
#    def scanaxtgt(self,nsample,repeat=1,delta=0.2,steps=10):
#        axtgt0=self.opts['qchip'].gates[self.gatename].pulses[1].amp
#        axtgts=numpy.linspace(axtgt0-delta,axtgt0+delta,steps)
#        errors=numpy.zeros(len(axtgts))
#        for iaxtgt,axtgt in enumerate(axtgts):
#            self.opts['seqs']=self.baseseqs(self.opts['qubitid'],repeat=repeat,axtgt=axtgt)
#            self.compile()
#            self.accbufrun(nsample=nsample)
#            errors[iaxtgt]=self.error()
#            print('scan',iaxtgt,axtgt,errors[iaxtgt])
#        pyplot.plot(axtgts,errors)
#        pyplot.show()
#    def scanacr(self,nsample,repeat=1,delta=0.2,steps=10):
#        acrs=numpy.linspace(acr0-delta,acr0+delta,steps)
#        errors=numpy.zeros(len(acrs))
#        for iacr,acr in enumerate(acrs):
#            self.opts['seqs']=self.baseseqs(self.opts['qubitid'],repeat=repeat,acr=acr)
#            self.compile()
#            self.accbufrun(nsample=nsample)
#            errors[iacr]=self.error()
#            print('scan',iacr,acr,errors[iacr])
#        
#        est=[(min(errors)-max(errors))/(max(acrs)-min(acrs))**2,acrs[int(len(acrs)/2)],1]
#        fitpara=fit.fitquadratic(acrs,errors,p0=est)
#        if self.opts['plot']:
#            pyplot.figure('optimize each')
#            pyplot.clf()
#            pyplot.plot(acrs,errors,'*')
#            pyplot.plot(acrs,fitpara['yfit'])
#            pyplot.draw()
#            pyplot.figure('optimize all')
#            pyplot.plot(acrs,errors,'*')
#            pyplot.plot(acrs,fitpara['yfit'])
#            pyplot.draw()
#            pyplot.pause(0.1)
#
#        pyplot.plot(acrs,errors)
#        pyplot.show()
#
    def optimize(self,key,nsample,nsteps,**kwargs):
        self.optimizesetup(**kwargs)
        if self.opts['plot']:
            pyplot.figure('optimize all')
            pyplot.ion()
            pyplot.clf()
            pyplot.show()
            pyplot.figure('optimize each')
            pyplot.ion()
            pyplot.clf()
            pyplot.show()
            pyplot.pause(0.1)
        self.nsample=nsample
        #center=self.opts['qchip'].gates[self.opts['qubitid']+self.opts['gate']].pulses[0].amp
        #span=center*2.0/3.0#self.opts['initspan']
        #key='acr'
        center=self.center[key]
        span=self.span[key]
        steps=self.steps[key]
        repeat=self.opts['ngateadd']
        x1=center-span/2
        x2=center+span/2
        for istep,step in enumerate(range(nsteps)):
            #            center=self.optimizestep(nsample=nsample,center=center,span=span)
            self.debug(4,'optimize step',x1,x2,self.opts['xtol'],center)
            self.debug(4,'optimize val n,x1,x2=[',istep,',',x1,',',x2,']')#,self.opts['xtol'],center)
#            center=scipy.optimize.fminbound(func=self.funamp,x1=x1,x2=x2,xtol=self.opts['xtol'],disp=3)
            scan=True
            failcnt=0
            while scan and failcnt<5:
                center=self.scan(key=key,nsample=nsample,repeat=repeat,center=center,span=span,steps=steps)
                if center is not None:
                    if center >x1 and center<x2:
                        self.debug(4,'optimize step:',istep,step,center)
                        span=span/2
                        x1=center-span/2
                        x2=center+span/2
                        self.opts['xtol']=self.opts['xtol']/2
                        repeat=self.opts['ngatemult']*2**istep+self.opts['ngateadd']
                        scan=False
                    else:
                        failcnt+=1
                        self.debug(0,'fit error cnt',failcnt)
                        scan=True
                else:
                    failcnt+=1
                    self.debug(0,'fit error cnt',failcnt)
                    scan=True
            print('center',center)

        #updatedict={('Gates',self.opts['qubitid']+'X90',0,'amp'):center}
        updatedict={}


        if self.opts['plot']:
            pyplot.ioff()
        return updatedict

    def fit(self):
        pass


if __name__=="__main__":
    qubitid=['Q1','Q2']
    qubitid=['Q2','Q3']
    qubitid=['Q3','Q2']
    qubitid=['Q0','Q1']
    qubitid=['Q5','Q4']
    qubitid=['Q3','Q2']
    qubitid=['Q4','Q3']
    qubitid=['Q2','Q1']
    qubitid=['Q3','Q2']
    qubitid=['Q6','Q5']
    qubitid=['Q1','Q0']
    #cnot=c_cnot(qubitid=qubitid,calirepo='../../../../qchip',debug=2,sim=True,xlim=(15609,16203))#,gmixs=None)
    cnot=c_cnot(qubitid=qubitid,calirepo='../../../../qchip',debug=2,sim=0,xlim=(0,2.5e4))#,gmixs=None)
    if 0:
        cnot.optimize(nsample=20,key='pzctl',nsteps=3,plot=True)
        pyplot.show()
    #cnot.scanaxtgt(200,repeat=3,delta=0.1)
#    cnot.scanacr(200,repeat=3,delta=0.1)
    if 1:
        repeat=3
        cnot.runseqs(nsample=400,repeat=repeat,combineorder=qubitid)#,heraldcmds=None)
        print(cnot.gatename,repeat)
        for k,v in sorted(cnot.result.items()):
            print(k,''.join(['%8.3f'%i for i in  v]))
    if 1:
        cnot.optimizesetup(plot=True)
        #cnot.scan(key='axtgt',nsample=10,center=None,span=0.2,steps=20,repeat=3)
        #cnot.scan(key='pztgt',nsample=100,center=None,span=0.1,steps=11,repeat=3)
        #cnot.scan(key='acr',nsample=100,center=None,span=0.01,steps=11,repeat=3)
        cnot.scan(key='pzctl',nsample=100,center=None,span=0.1,steps=11,repeat=3)
        pyplot.ioff()
        pyplot.show()
#        scan(self,key,nsample,center,span,steps,repeat):
