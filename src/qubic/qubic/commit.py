print('__name__',__name__)
import sys
sys.path.append('../../')
from qubic.bids.ether import c_ether
from qubic.bids.regmap import c_regmap

import argparse
import json
import pkgutil
def commit(ip):
    interface=c_ether(ip,port=3000)
    if __name__ is not None and "." in __name__:
        pkg=__package__
    else:
        pkg='chassis'
    registers=json.loads(pkgutil.get_data(pkg,'regmap.json'))
    wavegrp=json.loads(pkgutil.get_data(pkg,'wavegrp.json'))
    regmap=c_regmap(interface, min3=True,memgatewaybug=False,registers=registers,wavegrp=wavegrp)
    regmap.read(['config_rom_out'])
    commit=''.join([format(i,'x') for i in [regmap.getregval(i) for i in ['config_rom_out']][0][0x0c:0x16]])
    return commit
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip','--ip',help='ip address for the board',dest='ip',type=str,required=True)
    clargs=parser.parse_args()
    print(commit(ip=clargs.ip))
