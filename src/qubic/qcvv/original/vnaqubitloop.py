import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vnaqubit import c_vnaqubit
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vnaqubit=c_vnaqubit(**clargs.__dict__)
	#vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitid=clargs.qubitid,qubitdrvamp=clargs.qubitdrvamp,fread=clargs.fread,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			vnaqubit.sim()
		vnaqubit.run(bypass=clargs.bypass)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vnaqubit.processvnaqubit(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	pyplot.ion()
	fig=pyplot.figure(1)
	while True:
		try:
			data=vnaqubit.vnaqubitdata(clargs.nget)
			fprocess=vnaqubit.savejsondata(filename=clargs.filename,extype='vnaqubitloop',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
			c=vnaqubit.processvnaqubit(fprocess)
			print('adc minmax',vnaqubit.hf.adcminmax())
			fig.clf()
			if clargs.plot:
				vnaqubit.vnaqubitplot(c)
			pyplot.draw()
#			pyplot.subplot(211)
#			pyplot.ylim([4e4,4e5])
#			pyplot.subplot(212)
#			pyplot.ylim([-3.5,3.5])
			pyplot.pause(1e-6)
		except KeyboardInterrupt:
			pyplot.close('all')
			break
