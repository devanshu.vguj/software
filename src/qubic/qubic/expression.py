class c_expression():
    def __init__(self,expression,variable,index=None):
        #        print('init c_expression','expression',expression,'variable',variable,index)
        self.expression=expression
        self.variable=self.getvariable(variable) #if isinstance(v,c_expression) else v for v in variable
#        print('expression',expression,'variable',variable,'self.variable',self.variable,'getvariable',self.getvariable())
        self.index=index
        if self.index:
            self.lam='lambda %s:%s[%s]'%(','.join(self.variable),self.expression,self.index)
        else:
            #            print('self.getvariable()',self.getvariable())
            self.lam='lambda %s:%s'%(','.join(self.getvariable()),self.expression)
#        print('debug lam',self.lam)
#        print('debug c_expression: variable',variable,'expression',expression)
    def __expr__(self):
        #        print('debug variable',self.variable)
        return self.lam
    def getvariable(self,vlist=None):
        #        var=[v.getvariable() if isinstance(v,c_expression) else v for v in self.variable]
        var=[]
        if isinstance(vlist,list) or isinstance(vlist,tuple):
            for v in self.variable if vlist is None else vlist:
                if isinstance(v,c_expression):
                    #for vv in v.getvariable():
                    #    var.append(vv)
                    var.append(v)
                else:
                    var.append(v)
        elif isinstance(vlist,c_expression):
            var.append(vlist)
        elif isinstance(vlist,str):
            var.append(vlist)
        elif isinstance(vlist,set):
            var.extend(list(vlist))
        elif vlist is None:
            var.extend(self.variable)
        else:
            print('vlist is ',vlist,type(vlist), 'do not know what to do')

#        print('debug getvariable',var,vlist)
        return  var
    def __str__(self):
        return self.__expr__()
    def __call__(self,**kwargs):
        func=eval(self.lam) #'lambda %s:%s'%(','.join(self.variable),self.expression))
        #print(self.lam,self.variable)
        usefularg={k:kwargs[k] for k in self.variable}
#        print(usefularg,self.expression)
        return func(**usefularg)
    def op2(self,other,op,rev=False):
        ex1=self.expression
        var1=set(self.variable)
        if isinstance(other,c_expression):
            ex2=other.expression
            var2=set(other.variable)
        else:
            ex2=str(other)
            var2=set([])
        if rev:
            robj=c_expression(expression='(%s)%s(%s)'%(ex2,op,ex1),variable=set(var1|var2))
        else:
            robj=c_expression(expression='(%s)%s(%s)'%(ex1,op,ex2),variable=set(var1|var2))
        return robj
    def __add__(self,other):
        return self.op2(other,'+',rev=False)
    def __radd__(self,other):
        return self.op2(other,'+',rev=True)
    def __rpow__(self,other):
        return self.op2(other,'**',rev=True)
    def __pow__(self,other):
        return self.op2(other,'**',rev=False)
    def __iadd__(self,other):
        return self.op2(other,'+',rev=False)
    def __isub__(self):
        return self.op2(other,'-',rev=False)
    def __neg__(self):
        return c_expression(expression='-1*(%s)'%(self.expression),variable=set(self.variable))
#    def __bool__(self):
#        print('__bool__')
#        return False if (self==0 | self==False) else True
    def __not__(self):
        # maybe wrong, don't know how do define __bool__ yet
        #        print('__not__')
        return c_expression(expression='not(%s)'%(self.expression),variable=set(self.variable))
    def __invert__(self):
        return c_expression(expression='~(%s)'%(self.expression),variable=set(self.variable))
    def __mul__(self,other):
        return self.op2(other,'*',rev=False)
    def __rmul__(self,other):
        return self.op2(other,'*',rev=True)
    def __truediv__(self,other):
        return self.op2(other,'/',rev=False)
    def __rtruediv__(self,other):
        return self.op2(other,'/',rev=True)
    def __mod__(self,other):
        return self.op2(other,'%',rev=False)
    def __rmod__(self,other):
        return self.op2(other,'%%',rev=True)
    def __sub__(self,other):
        return self.op2(other,'-',rev=False)
    def __rsub__(self,other):
        return self.op2(other,'-',rev=True)
    def __rshift__(self,other):
        return self.op2(other,'>>',rev=False)
    def __rrshift__(self,other):
        return self.op2(other,'>>',rev=True)
    def __lshift__(self,other):
        return self.op2(other,'<<',rev=False)
    def __rlshift__(self,other):
        return self.op2(other,'<<',rev=True)
    def __le__(self,other):
        return self.op2(other,'<=',rev=False)
    def __lt__(self,other):
        return self.op2(other,'<',rev=False)
    def __ge__(self,other):
        return self.op2(other,'>=',rev=False)
    def __gt__(self,other):
        return self.op2(other,'>',rev=False)
    def __eq__(self,other):
        return self.op2(other,'==',rev=False)
    def __ne__(self,other):
        return self.op2(other,'!=',rev=False)
    def __rand__(self,other):
        return self.op2(other,'&',rev=True)
    def __and__(self,other):
        return self.op2(other,'&',rev=False)
    def __rxor__(self,other):
        return self.op2(other,'^',rev=True)
    def __xor__(self,other):
        return self.op2(other,'^',rev=False)
    def __ror__(self,other):
        return self.op2(other,'|',rev=True)
    def __or__(self,other):
        return self.op2(other,'|',rev=False)
    def function(self):
        def _function(**kwargs):
            return self.__call__(**kwargs)
        return _function

if __name__=="__main__":
    v=c_expression('v','v')
    y=c_expression('y','y')
    p=c_expression('paraexp',['paraexp'])
    print('print p',p)
    if 0:
        print(v+3)
        print(3+v)
        print(y**3+v)
        print(v**3)
        func=(-y**3+v)
        print(func.variable)
        print(func(v=9,y=3))
        print(not(y))
        print((~y)(y=3))
        print(y/3-v%y)
        print(y%v-y/3-v%y)
        print(3/y)
        print(y/v)
        print(3%y)
        print(v%7)
        print(y%v)
        print(y>v)
        print(((y**3)<<(v-4))(y=234,v=6))
        print(y>v)
        print(9**v)
        print(v>=3)
        print(3.0>(v))
        print(y==9)
        print(9!=y)
        print(3&y^5^(6|v|7))
        print(bool(y&v))
        print(~y)
        print(not(y))
#    print((not(y))(y=0))
#    tvar={v:v for v in t.variable if v in locals()}
#    print(t(tvar))
