import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
class c_rabi(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        self.opts=dict(updatedlophase=False)
        self.opts.update(kwargs)
        experiment.c_experiment.__init__(self,qubitid,calirepo,**self.opts)
    def rabiseq(self,qubitid,ef=False,tomoaxis='z',**kwargs):
        tomogatemap={'x':'Y-90','y':'X90','z':None}
        opts=dict(frabi=None,trabi=None,arabi=None,fread=None,tread=None,aread=None,pread=None)
        opts.update({k:v for k,v in kwargs.items() if k in opts})
        self.debug(4,'debug rabiseq ',opts)
        rabimodi={}
        readmodi={}
        rdrvmodi={}
        rabimodi.update({} if opts['trabi'] is None else {'twidth':opts['trabi']})
        rabimodi.update({} if opts['frabi'] is None else {'fcarrier':opts['frabi']})
        rabimodi.update({} if opts['arabi'] is None else {'amp':opts['arabi']})
        readmodi.update({} if opts['tread'] is None else {'twidth':opts['tread']});    rdrvmodi.update({} if opts['tread'] is None else {'twidth':opts['tread']})
        readmodi.update({} if opts['fread'] is None else {'fcarrier':opts['fread']});    rdrvmodi.update({} if opts['fread'] is None else {'fcarrier':opts['fread']})
        readmodi.update({} if opts['pread'] is None else {'pcarrier':opts['pread']});    rdrvmodi.update({} if opts['pread'] is None else {})
        readmodi.update({} if opts['aread'] is None else {});    rdrvmodi.update({} if opts['aread'] is None else {'amp':opts['aread']})
        seq=[]
        self.ef=ef
        if ef :
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'rabi_ef' ,'qubit': qubitid,'modi' : [rabimodi]})
            if 0:
                seq.append({'name': 'X90', 'qubit': qubitid})
                seq.append({'name': 'X90', 'qubit': qubitid})
        else:
            seq.append({'name': 'rabi', 'qubit': qubitid,'modi' : [rabimodi]})
        tomogate=tomogatemap[tomoaxis]
        if tomogate is not None:
#            print('tomogate',tomogate)
            seq.append({'name': tomogate, 'qubit': qubitid})
#            seq.append({'name': tomogate, 'qubit': qubitid})
#        else:
#            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': 64e-9}})
#            print("Apply delay 64 ns**************************************************")

        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        return seq
    def baseseqs_a(self,qubitid,arabis,**kwargs):
        seqs=[]
        for arabi in arabis:
            seq=self,rabiseq(qubitid=qubitid,arabi=arabi,**kwargs)
            seqs.append(seq)
        return seqs
    def baseseqs_w(self,qubitid,trabis,**kwargs):
        seqs=[]
        for trabi in trabis:
            seq=self.rabiseq(qubitid=qubitid,trabi=trabi,**kwargs)
            seqs.append(seq)
        return seqs


    def seqs_a(self,**kwargs):
        self.opts.update(kwargs)
        para=dict(qubitid=None,astart=0,astop=1,asteps=500)
        para.update({k:v for k,v in self.opts.items() if k in para})
        arabis=numpy.linspace(para['astart'],para['astop'],para['asteps'])
        self.opts['seqs']=self.baseseqs_a(qubitid=para['qubitid'],arabis=arabis,**kwargs)
        self.x=arabis
#        self.cmdlists=qubicrun.compile(**self.opts)
        self.compile()
    def seqs_w(self,**kwargs):
        self.opts.update(kwargs)
        para=dict(qubitid=None,elementlength=80,elementstep=4e-9)
        para.update({k:v for k,v in self.opts.items() if k in para})
        trabis=numpy.arange(para['elementlength'])*para['elementstep']
        self.opts['seqs']=self.baseseqs_w(qubitid=para['qubitid'],trabis=trabis,**kwargs)
        self.x=trabis
#        self.cmdlists=qubicrun.compile(**self.opts)
        self.compile()

    def run(self,nsample,**kwargs):
        experiment.c_experiment.accbufrun(self,nsample=nsample,**kwargs)
#        rcal=readcorr.readcorr(countsum=self.accresult['countsum'],rcalpath=self.opts['calipath'])
#        self.pcorr={self.opts['qubitid'][0]:rcal.corr(pmeas=self.accresult['countsum']['pcombine'])}
        self.pcombinecorr=self.accresult['pcombinecorr']
        self.psingle=self.accresult['countsum']['psingle']
        self.pcombine=self.accresult['countsum']['pcombine']
#        self.psingle=self.psinglecorr
        #print('psingle')
        #print(self.psingle)
        gmixs=self.accresult['gmixs'][self.opts['qubitid'][0]]
        self.debug(4,'mean',gmixs.means_)
        self.debug(4,'covariances',gmixs.covariances_)
        separation=gmixs.separation()#numpy.sqrt(numpy.sum(numpy.diff(gmixs.means_.T)**2)/numpy.sqrt(numpy.product(gmixs.covariances_)))
        self.result['separation']= separation[list(separation.keys())[0]] if len(separation)==1 else separation


    def fit(self):
        try:
            est=fit.sinestimate(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'])
        except:
            est=None
        fitpara=fit.fitsin(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'],p0=est)
        self.yfit=fitpara['yfit']
        self.result.update(dict(amp=fitpara['popt'][0],period=1.0/fitpara['popt'][1] if fitpara['popt'][1]!=0 else 0,err=fitpara['pcov']))
        return self.result

    def psingleplot(self,fig):
        return plot.psingleplot(x=self.x,psingle=self.psingle,fig=fig,marker='.',linestyle=None,linewidth=0)
    def plottyfit(self,fig):
        fig.plot(self.x,self.yfit)
        return fig
    def gmmplot(self,fig):
        for kq,gmix in self.accresult['gmixs'].items():
            plot.gmmplot(gmixs=gmix,fig=fig)
        return fig

    def iqplot(self,fig):
        accvaliq=self.accresult['accval'][self.opts['qubitid'][0]]
        return plot.iqplot(iq=accvaliq,fig=fig,log=0,colorbar=True)#,cmap='YlOrBr')
    def savegmm(self):
        if self.opts['gmixs'] is None:
            if not self.opts['updatedlophase']:
                for q in self.accresult['gmixs']:
                    filename='%s_gmix.npz'%q
                    numpy.savez(filename,**self.accresult['gmixs'][q].modelpara())
                    self.debug(1,'Save GMM model %s'%filename)
            else:
                self.debug(1,'Updating calibration file, NO GMM model saved')
        else:
            self.debug(1,'Using existing GMM, NO GMM model saved')

    def readphupdate(self,labels=['0','1'],**kwargs):
        self.opts.update(kwargs)
        gmix=self.accresult['gmixs'][self.opts['qubitid'][0]]
        p01=gmix.means_
        p0=p01[gmix.labelsrev[labels[0]]][0]+1j*p01[gmix.labelsrev[labels[0]]][1]
        p1=p01[gmix.labelsrev[labels[1]]][0]+1j*p01[gmix.labelsrev[labels[1]]][1]
        angle=numpy.angle(p0-p1)%(2*numpy.pi)
        center=p0+(p1-p0)*1.0/2.0
        self.debug(4,'mean 0',p0,'mean 1',p1,'angle',angle,'center',center)
        rph0=self.opts['qchip'].gates[self.opts['qubitid'][0]+'read'].pulses[1].pcarrier
        rph1=(rph0+angle)%(2*numpy.pi)
        updatedict={} if self.ef else {('Gates',self.opts['qubitid'][0]+'read',1,'pcarrier'):rph1}
        if self.opts['updatedlophase']:
            self.opts['qchip'].updatecfg(updatedict,self.opts['qubitcfgfile'])
        return updatedict
    def ampwidth(self,targetwidth=32e-9):
        amp0=self.opts['qchip'].paradict['Gates']['%srabi'%self.opts['qubitid'][0]+('_ef'if self.ef else '')][0]['amp']
        twidth0=self.opts['qchip'].paradict['Gates']['%sX90'%self.opts['qubitid'][0]+('_ef' if self.ef else '')][0]['twidth']
        amptarget=amp0*self.result['period']/targetwidth/4.0
        #print('amptarget',amptarget)
        amp=amptarget/numpy.ceil(amptarget)
        twidth=targetwidth*numpy.ceil(amptarget)
        updatedict.update({('Gates',self.opts['qubitid'][0]+('X90_ef' if self.ef else 'X90'),0,'amp'):amp})
        updatedict.update({('Gates',self.opts['qubitid'][0]+('rabi_ef' if self.ef else 'rabi'),0,'amp'):amp})
        updatedict.update({('Gates',self.opts['qubitid'][0]+('X90_ef' if self.ef else 'X90'),0,'twidth'):twidth})
        return updatedict
if __name__=="__main__":
    '''
import sys
sys.path.append(pathtoreposrc)

pathtoreposrc='/home/ghuang/git/paraqasmqubic/submodules'
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.rabi import c_rabi
rabi=c_rabi(qubitid='Q6',calirepo=pathtoreposrc+'/qchip/',debug=False)
rabi.seqs()#elementlength=10)
rabi.run(50)
rabi.fit()
rabi.savegmm()
print(rabi.result)
from matplotlib import pyplot
fig,(sub1,sub2)=pyplot.subplots(2,1)
fig.suptitle('Q6')
rabi.psingleplot(sub1)
rabi.plottyfit(sub1)
rabi.iqplot(sub2)
#pyplot.savefig('fig1.pdf')
pyplot.show()

    '''
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('-l','--live',help='live update',dest='live',default=False,action='store_true')
    parser.add_argument('-u','--update',help='update gate length from fitting',dest='update',default=False,action='store_true')
    parser.add_argument(help='qubitid list',dest='qubitid')#,nargs='+')
    parser.add_argument('-tomo','--tomoaxis',help='tomoaxis',dest='tomoaxis',type=str,choices=['x','y','z'],default='z')
    parser.add_argument('-m','--mode',help='mode:0 use data generate gmix store npz, 1 use data generate gmix and update lo phase, 2 use local saved gmix, 3 use gmix in default path',dest='mode',type=int,default=0)
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('--elementlength',help='number of elements',dest='nelem',default=80,type=int)
    clargs=parser.parse_args()
    ef=clargs.ef
    qubitid=clargs.qubitid
    mode=clargs.mode #0 if len(sys.argv)<=2 else int(sys.argv[2]) # update lo phase
    if mode==3:  # use default gmix in calibration, check rabi
        updatelophase=False
        gmix='default'
    elif mode==1:  # use data generate gmix, update lo phase
        updatelophase=True
        gmix=None
    elif mode==0:  # use data generate gmix  just check rabi
        updatelophase=False
        gmix=None
    elif mode==2:  # use local saved gmix confirm repeatable
        updatelophase=False
        gmix='./'

    rabi=c_rabi(qubitid=qubitid,calirepo=clargs.cpath,debug=1,gmixs=gmix
            #,delayafterheralding=0.04e-6
            )#,cfgpath='../../../../qchip/chip57/qubiccfg.json')
#    rabi.qubicrun.setvat(vatdevice='readvat',val=20)
    #rabi=c_rabi(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=False,gmixs='.')
#    rabi=c_rabi(qubitid=sys.argv[1],calirepo='../../../../qchip',debug=False)#,gmixs='.')
    #rabi.seqs_a(asteps=48,trabi=32e-9,ef=True)
    #rabi.run(50)
    livecnt=0
    if clargs.live:
        pyplot.ion()
    figrabi=pyplot.figure("figrabi",figsize=(13,5))
    sub1,sub2=figrabi.subplots(1,2)
    figcorr=pyplot.figure('figcorr')
    subcorr=figcorr.subplots()
    while clargs.live or livecnt==0:
        livecnt=livecnt+1
        nshot=clargs.nshot #250 if ef else (10 if clargs.live else 100)
#        if ef:
        rabi.seqs_w(elementlength=clargs.nelem,elementstep=4e-9,delaybetweenelement=1200e-6,ef=ef,heraldcmds=None,heraldingsymbol=None,tomoaxis=clargs.tomoaxis)#,ef=True,frabi=5.1675e9)#,arabi=0.1628)
#        else:
#            rabi.seqs_w(elementlength=60,elementstep=8e-9,ef=ef)#,ef=True,frabi=5.1675e9)#,arabi=0.1628)
        rabi.run(nshot)#,n_components=3,labels=['0','1','2'])#,labelmeasindex=(('0',0),('1',1),('2',None)))#,'0']))
        print('adcminmax',rabi.opts['chassis'].adcminmax())
        rabi.fit()
        print('result',{k:rabi.result[k] for k in ['separation', 'amp', 'period']})

        pyplot.figure("figcorr")
        if clargs.live:
            pyplot.clf()
            subcorr=figcorr.subplots()
        subcorr.plot(rabi.psingle[rabi.opts['qubitid'][0]]['0'],'r')
        try:
            subcorr.plot(rabi.pcombinecorr[('0',)],'g')
    #        subcorr.plot(rabi.pcombinecorr[('0',)],'g')
            subcorr.set_ylim((-0.1,1.1))
            subcorr.grid()
        except:
        #    print(rabi.pcombinecorr)
    #        print(rabi.pcombinecorr[('0',)])
    #        print('pyplot pcombinecorr')
            pass
        #print(rabi.qubicrun.rcal.minv)
        pyplot.figure("figrabi")
        if clargs.live:
            figrabi=pyplot.figure("figrabi")
            pyplot.clf()
            sub1,sub2=figrabi.subplots(2,1)
        figrabi.suptitle(str(clargs.qubitid)+rabi.opts['tomoaxis'])#sys.argv[1])
        rabi.psingleplot(sub1)
        sub1.set_ylim((-0.1,1.1))
    #    rabi.plottyfit(sub1)
        rabi.iqplot(sub2)
        rabi.gmmplot(sub2)
    #pyplot.savefig('fig1.pdf')
        if clargs.live:
            pyplot.draw()
            pyplot.pause(0.1)
        else:
            if clargs.noplot:
                pass
            else:
                pyplot.show()
            if clargs.savefig:
                tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
                title=('%s_%s'%(clargs.qubitid,'rabi_ef' if rabi.ef else 'rabi'))
                pyplot.savefig(title+'.pdf')
                #pyplot.savefig('fig1.pdf')
        updatedict=rabi.readphupdate(labels=['0','1'],updatedlophase=updatelophase)
    #    print(updatedict)
        #if rabi.opts['gmixs'] is None and not rabi.opts['updatedlophase']:
        rabi.savegmm()
        updatedict=rabi.ampwidth(32e-9)
        print('current rabi amp',rabi.opts['qchip'].gates[rabi.opts['qubitid'][0]+('X90_ef' if rabi.ef else 'X90')].pulses[0].amp)
        print(updatedict)
    if clargs.update:
        rabi.opts['qchip'].updatecfg(updatedict,rabi.opts['qubitcfgfile'])

