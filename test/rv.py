import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
class c_rv(experiment.c_experiment):
	def __init__(self,ip='192.168.1.123',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,simpseqs=True,**kwargs)
		pass
	def rvseqsgen(self,delayafterheralding=12e-6,delaybetweenelement=600e-6,cmdarray=None,gates=None,elementlist={},destlist={},patchlist={}):
		self.hf.setmap(elementlist=elementlist,destlist=destlist,patchlist=patchlist)
		if gates is not None:
			self.seqs.setgates(gates)
			self.hf.simpmemcmd(self.seqs.gatelist)
		self.seqs.updatecmd(cmdarray)
		(cmdlen,period)=self.hf.simpcmdgen(self.seqs.cmdarray,delaybetweenelement=delaybetweenelement,delayafterheralding=delayafterheralding)
		print('experiment period',period)
		self.seqs.setperiod(period)
		self.seqs.setperiod(2e-6)
		self.bufwidth_dict=self.hf.bufwidth_dict
		return cmdlen

	def rvacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrv(self,filename,loaddataset=None,loaddataset_list=None):
		pass


if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	rv=c_rv(**clargs.__dict__)

	gates=[rv.qchip.gates['M0mark']
,rv.qchip.gates['Q6X90']
,rv.qchip.gates['Q6read']
,rv.qchip.gates['Q5X90']
,rv.qchip.gates['Q7X90']
,rv.qchip.gates['Q5read']
,rv.qchip.gates['Q6read']
]
	elementlist={'Q6.qdrv':0,'Q5.qdrv':1,'Q7.qdrv':2,'Q6.rdrv':5,'Q5.rdrv':6,'Q7.rdrv':4,'Q6.read':9,'Q5.read':10,'M0.mark':12}
	destlist=   {'Q6.qdrv':1,'Q5.qdrv':2,'Q7.qdrv':3,'Q6.rdrv':0,'Q5.rdrv':0,'Q7.rdrv':0,'Q6.read':5,'Q5.read':6,'M0.mark':13}
	patchlist={
'Q6.rdrv':0e-9
,'Q6.read':0e-9
,'Q6.qdrv':0e-9
,'Q5.rdrv':16e-9
,'Q5.read':16e-9
,'Q5.qdrv':16e-9
,'M0.mark':0
,'Q7.qdrv':8e-9}
	testarray=[
					(False, [('M0mark', None, None)])
					,	(False, [('Q6X90',None,90),('Q5X90',None,0)])
					#			,	(False, [('Q6X90',None,90),('Q5X90',None,0)])
					#			,	(False, [('Q6X90',None,90)])
			,	(False, [('Q5X90',None,90)])
			,	(False, [('Q6X90',None,90)])
			,	(False, [('Q7X90',None,90)])
			#,	(False, [('Q6X90',None,90)])
			#,	(False, [('Q5X90',None,90)])
#,	(False, [('Q5X90',None,90)])
	#,(False, [('Q6X90',None,90),('Q5X90',None,90)])
	#	,(False, [('Q6read', None, None)])
	]

	delayafterheralding=12e-6
	delaybetweenelement=600e-6
	firsttime=True

	if clargs.sim:
		rv.setsim()
	rv.rvseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=testarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist)
	rv.run(preread=False,memclear=False,cmdclear=False)
	if clargs.sim:
		rv.sim()

