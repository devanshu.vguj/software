import sys
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
def modulation(length=1024,scale=32767,usepad=True):
	if usepad:
		length = length-4
	t=1.0*numpy.arange(length)
	m1=-abs(t-length/2.0)**2
	m1=m1/max(abs(m1))
	m1=m1-min(m1)
	pad=numpy.zeros(4)
	m2=numpy.exp(-(((t-length/2.0))/(length/5.0))**2)
	m2=m2/max(m2)
	m3=numpy.ones(length)/2.0
	m4=length/2.0-abs(t-length/2.0)
	m4=m4/max(m4)
#	return (m1,m2,m3,m4)
#	return (numpy.concatenate((pad,m1,pad,m3,pad,m1,pad,m3))*scale).astype(int)
	if usepad:
		result=(numpy.concatenate((pad,m3,pad,m3,pad,m3,pad,m3))*scale).astype(int)
	else:
		result=(numpy.concatenate((m3,m3,m3,m3))*scale).astype(int)
	return  result
def to32bits(arrayin):
	#	return numpy.array([((i&0xffff)<<16)+i for i in arrayin])
	return arrayin
'''
#print m4
(m1,m2,m3,m4)=modulation(length=64*4*4,scale=4095)

pyplot.plot(m1)
pyplot.plot(m2)
pyplot.plot(m3)
pyplot.plot(m4)
pyplot.show()
'''
# dest is qubit number
# start is waveform address
def cmdgen(trig_t,element,dest,start,length,phini,freq):
	ph_binary = int(phini/360.0*2**14)
	freq_binary=int(1e-9*freq*2**24)
	cmd={trig_t:((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)}
#	print trig_t,element,dest,start,length,ph_binary,freq_binary,hex(cmd[trig_t])
	return cmd
def cmdadd(cmddict,newcmddict):
	commonkeys=set(cmddict.keys())&set(newcmddict.keys())
	if not commonkeys:
		cmddict.update(newcmddict)
	else:
		print
		print("ERROR: commonkeys ",commonkeys)
		print
	return cmddict

if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg = c_mem_gateway(interface, min3=True,memgatewaybug=False)
	#wlen=12# FPGA cycles
	tslice=4
	nwave=4
	nel=8
	dlist=[]
	alist=[]
	#alist=range(0x38000,0x40000);
	#dlist=(0x40000-0x38000)*[5096] # amplitude to all the dac
	#dlist1=list(to32bits(modulation(length=wlen*tslice,scale=4095,usepad=False)))
	for iel in range(nel):
		dlist.extend((100)*[16383])
		dlist.extend((0x800-100)*[2500])
		dlist.extend(0x800*[5000])
		alist.extend(range(0x38000+iel*0x1000,0x38000+iel*0x1000+0x1000))
		print('len dlist,alist:',len(dlist),len(alist),dlist[-1],hex(alist[-1]))
	alist.extend(range(0x40000,0x41000))
	dlist.extend(numpy.zeros(len(range(0x40000,0x41000))))
	alist.extend(range(0x41000,0x50000))
	dlist.extend((0x50000-0x41000)*[16383])  # amplitude to internal lo for demodulating
	pulselength=int(sys.argv[1])
	print(pulselength,hex(alist[-1]),dlist[-1])

#	dlist=list(to32bits(modulation(length=wlen*tslice,scale=8191)))
#	alist=range(0x38000,0x38000+wlen*tslice*nwave)
#	print [hex(i) for i in alist]
	#cmddict=cmdgen(trig_t=0,element=0,dest=0,start=0,length=0,phini=0,freq=125e6)
#	cmdadd(cmddict,cmdgen(trig_t=2,element=8,dest=8,start=0,length=1,phini=0,freq= -125))
#	cmdadd(cmddict,cmdgen(trig_t=4,element=9,dest=9,start=0,length=1,phini=0,freq= -125))
	cmddict={}
	cmdadd(cmddict,cmdgen(trig_t=14,element=2,dest=1,start=25,length=pulselength,phini=0,freq= 66.47885e6))
	cmdadd(cmddict,cmdgen(trig_t=12,element=3,dest=3,start=25,length=pulselength,phini=0,freq= 66.47885e6))
	cmdadd(cmddict,cmdgen(trig_t=2,element=1,dest=2,start=0,length=25,phini=0,freq= 2e6))
	cmdadd(cmddict,cmdgen(trig_t=256,element=4,dest=0,start=0x800/4,length=500,phini=0,freq= 218e6))
	cmdadd(cmddict,cmdgen(trig_t=400,element=5,dest=3,start=0x800/4,length=500,phini=0,freq= 218e6))
	cmdadd(cmddict,cmdgen(trig_t=402,element=8,dest=8,start=0,length=500,phini=0,freq= 218e6))
#   read at begining
	#cmdadd(cmddict,cmdgen(trig_t=2,element=1,dest=0,start=0,length=wlen,phini=0,freq= 218e6))
	#cmdadd(cmddict,cmdgen(trig_t=54,element=2,dest=0,start=0,length=wlen,phini=0,freq= 66.47885e6))
	#cmdadd(cmddict,cmdgen(trig_t=56,element=3,dest=1,start=0,length=50,phini=0,freq= 66.47885e6))
#	cmdadd(cmddict,cmdgen(trig_t=10,element=3,dest=1,start=1*wlen,length=150,phini=0,freq= 66.47885e6))
#	cmdadd(cmddict,cmdgen(trig_t=280,element=3,dest=1,start=0,length=100,phini=0,freq= 66.47885e6))
#	cmdadd(cmddict,cmdgen(trig_t=124,element=1,dest=0,start=128,length=wlen,phini=0,freq= 200e6))
#	cmdadd(cmddict,cmdgen(trig_t=4,element=1,dest=0,start=192,length=wlen,phini=0,freq= 210e6))
#	cmdadd(cmddict,cmdgen(trig_t=6,element=2,dest=0,start=64,length=wlen,phini=0,freq= 110e6))
#	cmdadd(cmddict,cmdgen(trig_t=128 ,element=9,dest=9,start=0,length=500,phini=0,freq= -200e6))
#	cmdadd(cmddict,cmdgen(trig_t=129 ,element=10,dest=10,start=0,length=100,phini=0,freq= -200e6))
#   drive qubit
#	cmdadd(cmddict,cmdgen(trig_t=4,element=3,dest=1,start=64,length=wlen,phini=0,freq= 125e6))
#	cmdadd(cmddict,cmdgen(trig_t=6,element=4,dest=2,start=64,length=wlen,phini=0,freq= 125e6))
#	cmdadd(cmddict,cmdgen(trig_t=8,element=5,dest=3,start=64,length=wlen,phini=0,freq= 125e6))
#	cmdadd(cmddict,cmdgen(trig_t=68,element=4,dest=1,start=192,length=wlen,phini=0,freq= 210e6))
#	cmdadd(cmddict,cmdgen(trig_t=70,element=5,dest=1,start=64,length=wlen,phini=0,freq= 110e6))

#	cmdadd(cmddict,cmdgen(trig_t=136,element=3,dest=1,start=192,length=wlen,phini=0,freq= 210e6))
#	cmdadd(cmddict,cmdgen(trig_t=138,element=3,dest=1,start=192,length=wlen,phini=0,freq= 210e6))
#   read again
#	cmdadd(cmddict,cmdgen(trig_t=200,element=0,dest=0,start=0,length=wlen,phini=0,freq= 50e6))
#	cmdadd(cmddict,cmdgen(trig_t=202,element=1,dest=0,start=192,length=wlen,phini=0,freq= 210e6))
#	cmdadd(cmddict,cmdgen(trig_t=204,element=2,dest=0,start=64,length=wlen,phini=0,freq= 110e6))
#	cmdadd(cmddict,cmdgen(trig_t=320,element=8,dest=8,start=128,length=1000,phini=0,freq= -50e6))

	cmdlist=[v for k,v in sorted(cmddict.items())]
	#print [hex(i>>72) for i in cmdlist]
#		cmddict.update(cmdgen(trig_t=34,element=3,dest=0,start= 128,length=wlen,phini=0,freq= 10e6))
#		cmddict.update(cmdgen(trig_t=100,element=2,dest=1,start=0,length=wlen,phini=0,freq=200e6))
#		cmddict.update(cmdgen(trig_t=170,element=0,dest=0,start= 0,length=wlen,phini=0,freq=100e6))
#		cmddict.update(cmdgen(trig_t=172,element=1,dest=0,start=64,length=wlen,phini=0,freq= 50e6))
#		cmddict.update(cmdgen(trig_t=174,element=3,dest=0,start= 128,length=wlen,phini=0,freq= 10e6))
	bufsel=(0b010<<12)+(0b010<<9)+(0b010<<6)+0b010

	for i in range(len(cmdlist)):
		alist.extend([0x40000+2*i,0x40400+2*i,0x40400+2*i+1])
		dlist.extend([(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff])
		print([hex(j) for j in [0x40000+2*i,0x40400+2*i,0x40400+2*i+1]])
		print([hex(j) for j in [(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff]])

	alist.append(0x50000)
	dlist.append(0xff)
	period=300000/4
	alist.extend((0x7,0x8,16))  # period_adc,period_dac0,period_stream
	dlist.extend((period,period,period))
	print('len dlist,alist:',len(dlist),len(alist))
#	bufsel=0;
	print('bufsel',bufsel)
	alist.append(0x50001)
	dlist.append(bufsel)
	print('len dlist,alist:',len(dlist),len(alist))
#
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	alist=[(0x50000)]
	dlist=[(0xff)]
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	print([hex(i) for i in mg.parse_readvalue(result)])
	result=mg.readwrite(alist=alist,dlist=dlist,write=0)
	print([hex(i) for i in mg.parse_readvalue(result)])
	alist=[(0x50002)]
	dlist=[(1)]
	result=mg.readwrite(alist=alist,dlist=dlist,write=0)
#	time.sleep(1)
#	alist=(range(0x60000+0*1024,0x60000+1*1024))
#	alist.extend(range(0x60000+4*1024,0x60000+5*1024))
#	dlist=(numpy.zeros(2*1024))
#	result=mg.readwrite(alist=alist,dlist=dlist,write=0)
#	xy=numpy.array([i&0xffffffff for i in mg.parse_readvalue(result)])
#	x=[i-65536 if i>>15 else i for i in xy[0:1024]]
#	y=[i-65536 if i>>15 else i for i in xy[1024:2048]]
#	numpy.savetxt('qubi1.dat',numpy.transpose(numpy.array([x,y])),fmt='%d')
##	print x,y
#	print [hex(i) for i in xy]

#	alist.extend(range(0x60000,0x62000))
#	dlist.extend(numpy.zeros(0x1000))
#	result=mg.readwrite(alist=alist,dlist=dlist,write=0)
#	xy=numpy.array([i&0xffffffff for i in mg.parse_readvalue(result)])
#	x=[i-65536 if i>>15 else i for i in xy>>16]
#	y=[i-65536 if i>>15 else i for i in xy&0xffff]
#	pyplot.figure(1)
#	pyplot.plot(x,'r.')
#	pyplot.plot(y,'g.')
#	pyplot.figure(2)
#	pyplot.plot(x,y,'.')
#	pyplot.show()
	'''	alist.extend([0x40000,0x40002,0x40004,0x40006])

	dlist.extend(((0<<8)+0
			,(6<<8)+1
			,(30<<8)+2
			,(80<<8)+3
			))
	alist.extend(range(0x40400,0x40408))
	cmd=[(0<<62)+(0<<50)+(wlen<<38)+int(1e-9*100e6*2**24)
		,(0<<62)+(64<<50)+(wlen<<38)+int(1e-9*50e6*2**24)
		,(1<<62)+((0)<<50)+(wlen<<38)+int(1e-9*10e6*2**24)
		,(2<<62)+(0<<50)+(wlen<<38)+int(1e-9*200e6*2**24)
		]
	dlist.extend((cmd[0]&0xffffffff,cmd[0]>>32
				,cmd[1]&0xffffffff,cmd[1]>>32
				,cmd[2]&0xffffffff,cmd[2]>>32
				,cmd[3]&0xffffffff,cmd[3]>>32
				))
				'''
