#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable

if [[ ! -v QUBIC_CHIP_NAME ]]; then
    echo QUBIC_CHIP_NAME is undefined, source your chip first, aborting.
    exit 66
else
    echo monitor chip named: ${QUBIC_CHIP_NAME}
fi

# Disable canvas from popping up for:  rabi_w.py,meas_xtalk.py 
export QUBICnoXTERM=1

# set defaults
dataPath=outXtalk

numIter=1

while [ "$#" -gt 0 ]; do
  case "$1" in
    -n) numIter="$2"; shift 2;;
    -d) dataPath="$2"; shift 2;;

    --numIter=*) numIter="${1#*=}"; shift 1;;
    --dataPath=*) dataPath="${1#*=}"; shift 1;;
    --numIter|--dataPath) echo "$1 requires an argument" >&2; exit 1;;

    -*) echo "unknown option: $1" >&2; exit 1;;
    *) handle_argument "$1"; shift 1;;
  esac
done
passFile=$dataPath/run.pass

echo numIter=$numIter
echo dataPath=$dataPath
echo passFile=$passFile

if [ ! \( -d "$dataPath" \) ]; then
    echo S:create $dataPath
    mkdir -p $dataPath
fi

echo start `date`  > $passFile

datetime=`date '+%Y%m%d_%H%M%S'`
logFile=$dataPath/log_$datetime
echo logFile=$logFile  

function confirm_runpass {
    # check if run.pass-file exist to continue
    if [ ! \( -e "${passFile}" \) ] ;then
	echo "S: early finished ,  file=${passFile}= does not exist!"
	echo time now : `date '+%Y%m%d_%H%M%S'`  >> $logFile
	echo "S: early finish scan at i=$i " >> $logFile
	exit 1
    fi
    echo "started iter $i of $numIter  date: `date '+%Y%m%d_%H%M%S'` " >> $passFile
}

declare -A pax_map
pax_map['Z']=I
pax_map['X']=X90
pax_map['Y']=Y-90


#=================================
#  M A I N 
#=================================
numSteps=80
#rabiAmp=0.1;timeStep=16e-9;numSteps=60
rabiAmp=0.3;timeStep=8e-9
#rabiAmp=0.7;timeStep=4e-9

numBuf=60  # 60:1000 shots

for i in `seq $numIter`; do
	echo "*****************"
	echo loop index : $i of $numIter
	confirm_runpass	
	for rdQ in   Q6 Q5 ; do	    
		for drQ in Q5 Q6 ; do  
		    for pax in  X Y Z; do
			name=xtalk_rd${rdQ}_ax${pax}_dr${drQ}
			#echo "   measure $name  "`date `
			echo "   measure $name  date: `date '+%Y%m%d_%H%M%S'` " >> $passFile
			axG=${pax_map[$pax]}
			#./meas_xtalk.py --qubitid ${rdQ} --meas $axG  --qubitid_x $drQ --nget $numBuf -es $timeStep -el $numSteps --qubitdrvamp $rabiAmp --outpath $dataPath --outname $name >> $logFile
		       echo ./meas_xtalk.py --qubitid ${rdQ} --meas $axG  --qubitid_x $drQ --nget $numBuf -es $timeStep -el $numSteps --qubitdrvamp $rabiAmp --outpath $dataPath --outname $name 
		    done
		done
	done
	# regenerate summary page
#	time ./statics_timeplot.py --startDate $datetime --outPath /tmp/cohMonWeb/ --noXterm
	echo "###########  completed iteration $i of  $numIter date:"`date`
done

# testing:  ./meas_xtalk.py --qubitid Q5 --meas Y-90 --qubitid_x Q6  --nget 5  -es 8e-9 -el 80 --qubitdrvamp 0.3 --outpath out --outname xtalk_rdQ5_axZ_drQ6

