import os
import yaml
import pickle
import json
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.analysis.trueqsrbprocess import trueqsrbprocess
import trueq

def cfgdict(xlabels,zlabels,cnotlabels):
    cfgdict={}
    cfgdict['Name']='TrueQQubiC'
    cfgdict['N_Systems']= 2
    cfgdict['Mode']= 'ZXZXZ'
    cfgdict['Dimension']= 2
    zgate={}
    zgate['Hamiltonian']=[['Z', 'phase']]
    zgate['Involving']= {str((l,)):str(()) for l in zlabels}
    xgate={}
    xgate['Hamiltonian']=[['X', 90]]
    xgate['Involving']= {str((l,)):str(()) for l in xlabels}
    cnotgate={}
    cnotgate['Matrix']=[[1, 0, 0, 0],[0, 1, 0, 0],[0, 0, 0, 1],[0, 0, 1, 0]]
    cnotgate['Involving']= {str(l):str(()) for l in cnotlabels}
    gates=[dict(Z=zgate),dict(X=xgate),dict(CNOT=cnotgate)]
    cfgdict['Gates']=gates
    return cfgdict
def trueqqubic(trueqcircuits,qubitid,gatemap={'Z':'virtualz','X':'X90','CNOT':'CNOT','meas':'read'},cnotlabels=None,phunit='deg'):
    ph2rad=numpy.pi/180.0 if phunit=='deg' else 1.0
    labels=trueqcircuits.labels
    #print('qubitid',qubitid)
    if isinstance(qubitid,list) or isinstance(qubitid,tuple):
        qubits=[qubitid[i] for i in labels]
        #print('qubits0',qubits)
    elif isinstance(qubitid,str):
        qubits=[qubitid]
        #print('qubits1',qubits)
    else:
        print('qubits not assigned',qubitid)
    if len(qubitid)<len(labels):
        print('not enough qubit to be mapped')
    cnotlabels=[(labels[i],labels[(i-1)%len(labels)]) for i in labels[1:]] if cnotlabels is None else cnotlabels
    print('cnotlabels,',cnotlabels)
    configdict=cfgdict(xlabels=labels,zlabels=labels,cnotlabels=cnotlabels)
    config=trueq.Config.from_yaml(yaml.dump(configdict))
    transpiler=trueq.Compiler.from_config(config)
    transpiled_circuits=transpiler.compile(trueqcircuits)
    qubiccircuits=[]
    for icircuit,circuit in enumerate(transpiled_circuits):
        qubiccircuit=[]
        for igate,gate in enumerate(circuit):
            for k,v in gate.gates.items():
                gatedict=dict(name=gatemap[v.name],qubit=[qubits[i] for i in k])
                itemindex=0
                if v.name=='Z':
                    if 'phase' in v.parameters:
                        gatedict.update(dict(para=dict(phase=v.parameters['phase']*ph2rad)))
                    else:
                        print('Z gate expecting phase parameter')
                else:
                    if v.parameters:
                        gatedict.update(dict(para=v.parameters))
                qubiccircuit.append(gatedict)
            for k,v in gate.meas.items():
                gatedict=dict(name=gatemap['meas'],qubit=[qubits[i] for i in k])
                qubiccircuit.append(gatedict)
        qubiccircuits.append(qubiccircuit)
    #print(qubits,qubiccircuits)
    return qubits,qubiccircuits

class c_trueqqubic(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        #print('qubitid0',qubitid)
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
        #print('qubitid1',self.opts['qubitid'])
        self.gatemap={'Z':'virtualz','X':'X90','CNOT':'CNOT','meas':'read'}

    def seqs(self,trueqcircfilename,**kwargs):
        opts=dict(heraldingsymbol='0',combineorder=self.opts['qubitid'])
        opts.update(kwargs)
        self.opts.update(opts)
        self.trueqcircfilename=trueqcircfilename
        qubits,qubiccircuits=trueqqubic(trueqcircuits=trueq.load(trueqcircfilename) if isinstance(trueqcircfilename,str) else trueqcircfilename,qubitid=self.opts['qubitid'],gatemap=self.gatemap)
        self.opts['seqs']=qubiccircuits
        #print(qubiccircuits)
        self.compile(overlapcheck=False,**self.opts)
    def run(self,nsample,**kwargs):
        self.opts.update(kwargs)
        self.opts['nsample']=nsample
        self.accbufrun(**self.opts)
        self.circuits,self.fidelity=trueqsrbprocess(self.trueqcircfilename,self.accresult)
        self.result=self.fidelity
        self.result.update(dict(qubitid=self.opts['qubitid']))
        return self.fidelity
    def plot(self):
        self.circuits.plot.raw()
if __name__=="__main__":
    import trueq
    from matplotlib import pyplot
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(help='qubitid list',dest='qubitid',nargs='+')
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-tq','--trueq',help='trueq circuit',dest='tq',type=str)
    clargs=parser.parse_args()
    #qubitid=['Q3','Q1','Q2']
    qubitid=['Q2','Q1','Q3']
#    qubitid=[sys.argv[2]]
 #   qubitid=['Q2','Q3']
    #qubitid=['Q5','Q6']
    qubitid=['Q2','Q3']
    qubitid=['Q1','Q2']
    qubitid=['Q0','Q5']
#    qubitid=[sys.argv[2]]
    qubitid=['Q0','Q1']
    qubitid=clargs.qubitid
    tq=c_trueqqubic(qubitid,calirepo=clargs.cpath,debug=2)
    tq.seqs(clargs.tq)#sys.argv[1])
    print(tq.run(clargs.nshot))
    tq.plot()
    pyplot.show()

    if 0:

#    sys.path.append('../qcvv/analysis')
        sys.path.append('../qcvv')
        from qubic.qubic import envset
        from analysis.trueqsrbprocess import trueqsrbprocess
        from qubic.qubic.qubicrun import c_qubicrun
        from qubic.qubic.heralding  import  heralding
        gatemap={'Z':'virtualz','X':'X90','CNOT':'CNOT','meas':'read'}
        trueqcircfilename=sys.argv[1] #'circuits_20210827_004108_239668.bin'
        qubitid=['Q3','Q1','Q2']
        qubitid=['Q3']
        #qubitid=[sys.argv[2]]
        qubits,qubiccircuits=trueqqubic(trueqcircuits=trueq.load(trueqcircfilename),qubitid=qubitid,gatemap=gatemap)
        chassis,wiremap,qchip,gmixspath=envset.envset(calirepo=clargs.cpath,sim=False,debug=True)
        qubicrun=c_qubicrun(chassis=chassis,wiremap=wiremap,qchip=qchip,gmixs=gmixspath)
        qubicrun.compile(seqs=qubiccircuits,delaybetweenelement=1000e-6,heraldcmds=heralding(qubits=qubits),heraldingsymbol='0',npreread=2,overlapcheck=False)
        qubicrun.accbufrun(nsample=1000,combineorder=qubits)
        result=qubicrun.gmmcount()#(nsample=400,combineorder=qubits)
#,circcmds=qubiccircuits
#,qchip=qchip
#,nsample=400
#,wiremap=wiremap
#,delaybetweenelement=1000e-6
#,heraldcmds=heralding.heralding(qubits=qubits)
#,heraldingsymbol='0'
#,npreread=2
#,overlapcheck=False
#,combineorder=qubits
#,gmixs=gmixpath)
        circuits,fidelity=trueqsrbprocess(trueqcircfilename,result)
        print('combine order',result['countsum']['combineorder'])
        print('qubits',qubits)
        print(fidelity)
        circuits.plot.raw()
        pyplot.show()
