import time
import numpy
import vxi11
import sys

class yokogawa():
    def __init__(self,ip="192.168.1.62"):
        self.instr=vxi11.Instrument(ip)
        self.readstatus()
    def idn(self):
        return self.ask('*IDN?')
    def setoutput(self,on=None):
        cmd=None
        if (on==1 or on.lower()=='on'.lower()):
            cmd='OUTP 1'
        elif (on==0 or on.lower()=='off'.lower()):
            cmd='OUTP 0'
        if cmd:
            self.write(cmd)    
        else:
            print('output select from 0/off, 1/on')
    def ask(self,cmd):
        return self.instr.ask(cmd+'\n')
    def write(self,cmd):
        return self.instr.write(cmd+'\n')
    def readstatus(self,keys=['LEVEL','RANGE','FUNCTION','OUTPUT']):
        self.level=float(self.ask('SOUR:LEVEL?'))
        self.range=float(self.ask('SOUR:RANGE?'))
        self.function=self.ask('SOUR:FUNCTION?')
        self.output='ON' if self.ask('OUTPUT?')=='1' else 'OFF'
        return {'level':self.level,'range':self.range,'function':self.function,'output':self.output}
    def setrange(self,rangeval):
        cmd='SOUR:RANGE %e'%rangeval
        self.write(cmd)
    def setlevel(self,level):
        cmd='SOUR:LEVEL %e'%level
        self.write(cmd)
    def setfunc(self,func=None):
        cmd='SOUR:FUNC'+ ' '+func
        self.write(cmd)

if __name__=="__main__":
    yoko=yokogawa()
    print(yoko.idn())
    print(yoko.readstatus())
    yoko.setoutput('off')
    yoko.setfunc('CURR')
    print(yoko.readstatus())
#    import random
#    l=random.random()
    yoko.setoutput('on')
    yoko.setrange(1.0e-3)
#    for l in numpy.arange(-1e-3,1e-3,0.1e-3):
    #for l in [1e-3]:
    print(len(sys.argv))
    if len(sys.argv)>1:
        l=float(sys.argv[1])
        yoko.setlevel(l)
    print(yoko.readstatus())
    #time.sleep(5)
#    print(yoko.readstatus())
#    print(yoko.readstatus())

