# Generated from qasm3.g4 by ANTLR 4.7.2
from antlr4 import *
#if __name__ is not None and "." in __name__:
#    from .antlr4qasm3.qasm3Parser import qasm3Parser
#else:
import sys
sys.path.append('../../')
from qubic.qasmqubic.antlr4qasm3.qasm3Parser import qasm3Parser
import os
# This class defines a complete generic visitor for a parse tree produced by qasm3Parser.
import inspect
class preVisitor(ParseTreeVisitor):
    def __init__(self,tree,debug=0):
        self.include=None
        self.visit(tree)
        self.replace={}
        if self.include:
            path=os.getcwd()+'/'+self.include
            if os.path.isfile(self.include):
                with open(self.include) as finclude:
                    includestr=finclude.read()
                    self.replace[self.includeorig]=includestr
            else:
                if debug>3:
                    print('preVisitor: %s is not a file'%self.include)
    def getreplace(self):
        return self.replace
    # Visit a parse tree produced by qasm3Parser#include.
    def visitInclude(self, ctx:qasm3Parser.IncludeContext):
        self.include=ctx.StringLiteral().getText()[1:-1]
        self.includeorig=ctx.start.getInputStream().getText(ctx.start.start,ctx.stop.stop)
        return [self.include,self.includeorig]


del qasm3Parser
