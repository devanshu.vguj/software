#!/usr/bin/env python3
'''
 measure data used for xtalk characterization fo a chip

quick test:          read-Q                    drive-Q
 ./meas_xtalk.py --qubitid Q6 --meas Y-90 --qubitid_x Q5  --nget 5  -es 8e-9 -el 80 --qubitdrvamp 0.3   --outpath out --outname xtalk_123


'''

import datetime
import numpy
import time
from ..qubic import experiment
from .rabi_xtalk_matrix import c_rabixtalk
import os,sys

# Jan added:
from ..toolbox.Util_H5io3 import  write3_data_hdf5
from ..toolbox.Util_Qubic2 import  md5hash, dateT2Str
from pprint import pprint
try:
    noXterm=os.environ['QUBICnoXTERM']
    print('noXterm',noXterm,'Using matplotlib / pylab without a DISPLAY')
    import matplotlib
    matplotlib.use('Agg')
except:
    pass
from matplotlib import pyplot

#...!...!..................
def build_metaData(outF,args,numShots):
    ac={}
    t1=time.localtime()
    ac['meas_date']=dateT2Str(t1)
    
    if args.outname==None:
        myHN8=md5hash(outF)
        ac['hash8']=myHN8
        ac['short_name']='xtalk_'+myHN8
    else:
        ac['short_name']=args.outname
        ac['hash8']=md5hash(ac['short_name']+ac['meas_date'])
        
    axisMap={'I':'z','X90':'x','Y-90':'y'}
    ac['meas_axis']=axisMap[args.meas]
    ac['IQ_name']=outF    
    ac['num_shots']=numShots
    
    ac['read_qubit']=args.qubitid
    ac['drive_qubit']=args.qubitid_x

    ac['drive_rabi_amp']=args.qubitdrvamp
    ac['time_step']=args.elementstep
    ac['num_steps']=args.elementlength
    ac['chip_name']=os.environ['QUBIC_CHIP_NAME']
    ac['fridge_name']=os.environ['QUBIC_FRIDGE_NAME']
    ac['chip_calib']=os.environ['QUBICQUBITCFG']
    ac['chip_calib']=os.environ['QUBICQUBITCFG']
    ac['qubit_drive_loc_osc']=float(os.environ['QUBIC_DRIVER_LO'])
    return ac

#...!...!..................
def collect_qubit_calib(expMD,exper):
    # read-Q
    qfun='read_qubit';  qid=expMD[qfun]
    qdrv=exper.qchip.qubits[qid]
    expMD[qfun+'_drvfreq']=int(qdrv.freq)
    expMD[qfun+'_readfreq']=int(qdrv.readfreq)

    # drive-Q
    qfun='drive_qubit';  qid=expMD[qfun]
    qdrv=exper.qchip.qubits[qid]
    expMD[qfun+'_drvfreq']=int(qdrv.freq)
            
    # rabi pulse params
    gate=exper.qchip.gates[qid+'rabi']
    #print(gate)
    pulse=gate.pulses[0]
    #print(pulse)
    paraD=pulse.paradict
    #print(sorted(paraD))
    envel=paraD['env']
    #print(envel)
    expMD[qfun+'_envelope']=envel
    #pprint( expMD)
#...!...!..................
def meas_xtalk(cmdlinestr,clargs):
    timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d_%H%M%S_%f')
    rabixtalk = c_rabixtalk(**clargs.__dict__)
       
    rabixtalk.rabixtalkseqs(delayread=clargs.delayread, delay1=12e-6, delaybetweenelement=clargs.delaybetweenelement, readwidth=clargs.readwidth, readoutdrvamp=clargs.readoutdrvamp, qubitdrvamp=clargs.qubitdrvamp, elementlength=clargs.elementlength, elementstep=clargs.elementstep, fqubit=clargs.fqubit, fread=clargs.fread, qubitid=clargs.qubitid, qubitidread=[clargs.qubitid], qubitid_x=clargs.qubitid_x, meas=clargs.meas)
    if clargs.processfile == '':
        rabixtalk.run(bypass=clargs.bypass)
        data = rabixtalk.rabixtalkacq(clargs.nget)
        outF=clargs.filename + '_qubitid' + clargs.qubitid + '_qubitidx' + clargs.qubitid_x
        outF=clargs.outpath+'/'+outF
        fprocess = rabixtalk.savejsondata(filename=outF, extype='rabixtalk', cmdlinestr=cmdlinestr, data=data)
        print('save data to ', fprocess)
    else:
        fprocess = clargs.processfile
    [rawdata, separation, iqafterherald, population_norm, amp, period, fiterr] = rabixtalk.processrabixtalk(dt=clargs.elementstep, filename=fprocess, loaddataset=clargs.dataset, plot=clargs.plot, isfitdecay=True, timeinfo=timestamp)
    
    timesteps = numpy.arange(clargs.elementlength) * clargs.elementstep
    counts=numpy.zeros((2,clargs.elementlength))  # just placeholder

    if clargs.readcorr:
        population_norm = rabixtalk.readoutcorrection(qubitid_list=[clargs.qubitid], meas_binned=numpy.vstack((1 - population_norm, population_norm)), corr_matrix=numpy.load(clargs.corrmx))[1]
        counts[1,:]=population_norm
        counts[0,:]=1.0-population_norm
    else:
        counts[1,:]=rabixtalk.rabixtalk_result['1']
        counts[0,:]=rabixtalk.rabixtalk_result['0']

    # Jan's additional code
    numShots= rawdata.shape[0]//population_norm.shape[0]//2  # '2'=IQ pair 
    #print('M:rawdata IQ',rawdata.shape, 'shots=',numShots,'pop:',population_norm.shape)
    
    metaD=build_metaData(fprocess,clargs,numShots)
    collect_qubit_calib(metaD,rabixtalk)
    pprint(metaD)
    bigD={'counts':counts.astype('float32'),'rabi_width':timesteps.astype('float32')}
    outF=metaD['short_name']+'.h5'
    outF=clargs.outpath+'/'+outF
    write3_data_hdf5(bigD,outF,metaD)
    # Jan end
    
    return timesteps, counts, metaD, outF

#=================================
#=================================
#   MAIN
#=================================
#=================================

def main():
    parser, cmdlinestr = experiment.cmdoptions()
    parser.set_defaults(elementstep=4e-9)
    clargs = parser.parse_args()
    print('debug main',sys.argv)
    print('debug main',clargs)
    if clargs.outpath!=None:    assert os.path.exists(clargs.outpath)
    timesteps, counts, metaD,outF = meas_xtalk(cmdlinestr,clargs)
    timesteps*=1e9 # now in ns
    pyplot.plot(timesteps, counts[0]+counts[1], label='m=0^1')
    pyplot.plot(timesteps, counts[0], label='m=0')
    pyplot.plot(timesteps, counts[1], label='m=1')
    pyplot.legend(title='meas axis:'+metaD['meas_axis'])

    tit='%s  shots=%d, rabiAmp=%.1f FS'%(metaD['short_name'],metaD['num_shots'],metaD['drive_rabi_amp'])
    pyplot.title(tit)
    pyplot.ylabel('read %s counts '%(metaD['read_qubit']))
    pyplot.xlabel('%s Rabi drive width (ns),   hash=%s'%(metaD['drive_qubit'],metaD['hash8']))
    pyplot.grid()
    outF2=outF.replace('.h5','.png')
    pyplot.savefig(outF2)
    pyplot.show()    

if __name__ == "__main__":
	main()
