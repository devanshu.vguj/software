import scipy
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.analysis import gmm,readcorr
class c_r0(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,fread=None,aread=None,tread=None,**kwargs):
        readmodi={}
        rdrvmodi={}
        rdrvmodi.update({} if fread is None else {'fcarrier':fread})
        rdrvmodi.update({} if aread is None else {'amp':aread})
        rdrvmodi.update({} if tread is None else {'twidth':tread});
        readmodi.update({} if fread is None else {'fcarrier':fread});
        readmodi.update({} if tread is None else {'twidth':tread});
        seqs=[]
        seq=[]
        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        seqs.append(seq)
        seq=[]
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'X90', 'qubit': qubitid})
        seq.append({'name': 'read', 'qubit': qubitid,'modi' : [rdrvmodi,readmodi]})
        seqs.append(seq)

        return seqs

    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        #print(self.opts['seqs'])
        self.opts['seqs']=self.baseseqs(**self.opts)

    def run(self,nsample,fread,aread,tread):
        self.opts['nsample']=nsample
#        return self.runr0((fread,aread,tread))
#    def runr0(self,fatread):
#        fread,aread,tread=fatread
        tread=round(tread/4e-9)*4e-9 if tread is not None else tread
        self.opts['seqs']=self.baseseqs(qubitid=self.opts['qubitid'],fread=fread,aread=aread,tread=tread)
        self.opts['heraldcmds'] =None
        self.compile()
        self.accbufrun(**self.opts)
        self.pcombine=self.accresult['countsum']['pcombine']
#        print(list(self.pcombine.keys()))
        prep0read1=1-self.pcombine[('0',)][0].mean()
        prep1read0=self.pcombine[('0',)][1].mean()
        print((fread,aread,tread,prep0read1,prep1read0))
        return prep0read1,prep1read0
    def scan(self,nsample,**kwargs):
        default=dict(fread=[None],aread=[None],tread=[None])
        self.opts.update(**default)
        self.opts['nsample']=nsample
        self.opts.update(**kwargs)
        lenfread=len(self.opts['fread'])
        lenaread=len(self.opts['aread'])
        lentread=len(self.opts['tread'])
        self.prep0read1=numpy.zeros((lenfread,lenaread,lentread))
        self.prep1read0=numpy.zeros((lenfread,lenaread,lentread))
        for ifread,fread in enumerate(self.opts['fread']):
            for iaread,aread in enumerate(self.opts['aread']):
                for itread,tread in enumerate(self.opts['tread']):
                    p0r1,p1r0=self.run(nsample,fread,aread,tread)
                    self.prep0read1[ifread,iaread,itread]=p0r1
                    self.prep1read0[ifread,iaread,itread]=p1r0
                    print(ifread,iaread,itread,p0r1,p1r0)
        return self.prep0read1,self.prep1read0
    def optimize(self,nsample,x0,freadrange,areadrange,treadrange):
        self.opts['nsample']=nsample
        res=scipy.optimize.minimize(fun=self.runr0,x0=x0,bounds=(freadrange,areadrange,treadrange),options={'disp':True})
        return res

if __name__=="__main__":
    qubitid=sys.argv[1]
    r0=c_r0(qubitid=qubitid,calirepo='../../../../qchip',debug=1,gmixs=None)#'./')
    fread0=r0.opts['qchip'].getfreq(r0.opts['qubitid'][0]+'.readfreq')
    aread0=r0.opts['qchip'].gates[r0.opts['qubitid'][0]+'read'].pulses[0].amp
    tread0=r0.opts['qchip'].gates[r0.opts['qubitid'][0]+'read'].pulses[0].twidth
    nshot=1000
    if 1:
        print('aread0',aread0)
        da=0.08
        da=0.1
        aread=numpy.linspace(max(0,aread0-da/2),min(aread0+da/2,1),11)
        p0r1aread,p1r0aread=r0.scan(nsample=nshot,aread=aread)
        figaread=pyplot.figure('aread')
        axaread=figaread.subplots()
        figaread.suptitle(qubitid)
        axaread.plot(aread,p0r1aread[0,:,0],'r',label='prep0read1')
        axaread.set_ylabel('prep0read1',color='r')
        axaread.set_xlabel('read out drive amplitude (Fullscale)')
        axaread1=axaread.twinx()
        axaread1.plot(aread,p1r0aread[0,:,0],'g',label='prep1read0')
        axaread1.set_ylabel('prep1read0',color='g')
        axaread.axvline(x=aread0,color='black')
        axaread.legend()

    if 1:
        print('fread0',fread0)
        bw=0.4e6
        bw=1e6
        fread=numpy.linspace(fread0-bw/2.0,fread0+bw/2.0,11)
        p0r1fread,p1r0fread=r0.scan(nsample=nshot,fread=fread)
        figfread=pyplot.figure('fread')
        axfread=figfread.subplots()
        figfread.suptitle(qubitid)
        axfread.plot(fread,p0r1fread[:,0,0])
        axfread.set_ylabel('prep0read1',color='r')
        axfread1=axfread.twinx()
        axfread1.plot(fread,p1r0fread[:,0,0],'g')
        axfread1.set_ylabel('prep1read0',color='g')
        axfread.set_xlabel('read out drive frequency (Hz)')
        axfread.axvline(x=fread0,color='black')
    #res=r0.optimize(nsample=1000,x0=(fread0,aread0,tread0),freadrange=(fread0-bw/2.0,fread0+bw/2.0),areadrange=(0.05,0.33),treadrange=(1e-6,4e-6))
    #print(res)

    if 1:
        print('tread0',tread0)
        bw=1e6
        tread=numpy.arange(1000e-9,4000e-9,200e-9)#tread0-bw/2.0,tread0+bw/2.0,11)
    #    tread=numpy.array([3000e-9,4e-6])
        p0r1tread,p1r0tread=r0.scan(nsample=nshot,tread=tread)
        figtread=pyplot.figure('tread')
        axtread=figtread.subplots()
        figtread.suptitle(qubitid)
        axtread.plot(tread/1e-6,p0r1tread[0,0,:],'r')
        axtread.set_ylabel('prep0read1',color='r')
        axtread1=axtread.twinx()
        axtread1.plot(tread/1e-6,p1r0tread[0,0,:],'g')
        axtread1.set_ylabel('prep1read0',color='g')
        axtread.set_xlabel('read out drive pulse length (us)')
        axtread.axvline(x=tread0/1e-6,color='black')
    #res=r0.optimize(nsample=1000,x0=(fread0,aread0,tread0),freadrange=(fread0-bw/2.0,fread0+bw/2.0),areadrange=(0.05,0.33),treadrange=(1e-6,4e-6))
    #print(res)
    pyplot.show()
