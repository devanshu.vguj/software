import argparse
import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import squbic
import memwrite
if __name__=="__main__":
#	parser = argparse.ArgumentParser()
#	parser.add_argument('-b','--bypasswritecommand',help='bypass write command',dest='bypass',default=False,action='store_true')
#	clargs=parser.parse_args()
#	trig_t=20
#	element=0xc
#	start=0
#	length=200
#	dest=0
#	phrad=0
#	freq=0
	hf=c_hfbridge(ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',min3=True,memgatewaybug=False,run=True,sim=False)
#	hf.cmdclear()
#	period=int(numpy.ceil(10e-6/hf.dt/4.0))
#	hf.setperiod(period)
#	hf.cmdwrite()

	qubitcfg='qubitcfg.json'
	with open(qubitcfg) as jfile:
		chipcfg=json.load(jfile)
	qchip=squbic.c_qchip(chipcfg)
	import memcfg
	dt=1e-9
	membuf,gatecmd=memwrite.memgen(qchip,memcfg,dt=dt)
	#circuits=[
	#		["Q4read"]
	#		,["Q4X90","Q4read"]
	#		,["Q4X180","Q4read"]
	#		]
	circuit=[{"trig_t":10e-9,"gate":"Q6X180"}
			,{"trig_t":42e-9,"gate":"Q6read"}
			,{"trig_t":3e-6+10e-9,"gate":"Q5X180"}
			,{"trig_t":3e-6+42e-9,"gate":"Q5read"}
			,{"trig_t":6e-6+10e-9,"gate":"Q4X180"}
			,{"trig_t":6e-6+42e-9,"gate":"Q4read"}
			#			,{"trig_t":74e-9,"gate":"Q4read"}
			#,{"trig_t":106e-9,"gate":"Q6read"}
			#,{"trig_t":106e-9,"gate":"Q6X90"}
			]
	for g in circuit:
		trig_t=g["trig_t"]
		gate=gatecmd[g["gate"]]
		for pulse in gate:
			t0=pulse['pulse'].t0
			#print(gate,t0,pulse['pulse'].pcarrier,pulse['pulse'].fcarrier)
			#print(trig_t,t0)
			hf.cmd128all.append(hf.cmdgen128(trig_t=int((trig_t+t0)/4.0/1e-9),element=pulse['element'],dest=pulse['dest'],start=pulse['start']/4,length=pulse['length']/4,phrad=pulse['pulse'].pcarrier,freq=0.5e6 if 1 else pulse['pulse'].fcarrier))
		trig_t+=500e-6
	hf.cmdclear()
	print([format(i,'032x') for i in hf.cmd128all])
	hf.periodwrite(period=int(numpy.ceil(600e-6/hf.dt/4.0)))
	hf.cmdwrite()
