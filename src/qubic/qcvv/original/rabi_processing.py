import numpy
import sys
import argparse
import time
import read
from matplotlib import pyplot,patches
from sklearn import mixture

if __name__=="__main__":
	parser=argparse.ArgumentParser()
	parser.add_argument('-s','--swap',help='swap label 0 and label 1 of Gaussian Mixture components',dest='swap',type=int,default=0)
	parser.add_argument('-f','--filename',help='filename',dest='filename',type=str,default='')
	parser.add_argument('-l','--rabilength',help='rabi length',dest='rabilength',type=int,default=80)
	clargs=parser.parse_args()

	rabiperiod=clargs.rabilength
	[d1,d2,d3]=read.readrabi(clargs.filename)
	d1_herald=d1.reshape((-1,2))[:,0]
	d1_regular=d1.reshape((-1,2))[:,1]
	lim=1.1*max(max(abs(d1.real)),max(abs(d1.imag)))

	d1_iq=numpy.vstack((d1.real,d1.imag)).transpose()
	d1_herald_iq=numpy.vstack((d1_herald.real,d1_herald.imag)).transpose()
	d1_regular_iq=numpy.vstack((d1_regular.real,d1_regular.imag)).transpose()
	pyplot.figure(1,figsize=(30,8))
	ax1=pyplot.subplot(131)
	ax1.set_aspect('equal')
	ax1.set_xlim([-lim,lim])
	ax1.set_ylim([-lim,lim])
	ax1.set_title('all')
	pyplot.hexbin(d1.real,d1.imag,gridsize=30,cmap='Greys',bins='log')
	ax2=pyplot.subplot(132)
	ax2.set_aspect('equal')
	ax2.set_xlim([-lim,lim])
	ax2.set_ylim([-lim,lim])
	ax2.set_title('heralding')
	pyplot.hexbin(d1_herald.real,d1_herald.imag,gridsize=30,cmap='Greys',bins='log')
	ax3=pyplot.subplot(133)
	ax3.set_aspect('equal')
	ax3.set_xlim([-lim,lim])
	ax3.set_ylim([-lim,lim])
	ax3.set_title('regular')
	pyplot.hexbin(d1_regular.real,d1_regular.imag,gridsize=30,cmap='Greys',bins='log')

	n_gaussians=2
	colors=['navy','turquoise']
	gmix=mixture.GaussianMixture(n_components=n_gaussians, covariance_type='spherical')
	gmix.fit(d1_iq)
	order=numpy.argsort(gmix.means_[:,0])
	gmix.means_=gmix.means_[order]
	gmix.covariances_=gmix.covariances_[order]
	print('mean covariance',gmix.means_,gmix.means_.shape,gmix.covariances_,gmix.covariances_.shape)
	separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.transpose())**2)/numpy.sqrt(numpy.product(gmix.covariances_)))
	#separation_fidelity=0.5+numpy.erf(numpy.sqrt(separation/8.0))/2.0
	#separation_absolute=numpy.sqrt(numpy.sum((numpy.diff(gmix.means_.transpose()))**2))
	print('separation',separation)
	#print 'separation,separation_fidelity,separation_absolute',separation,separation_fidelity,separation_absolute

	pyplot.figure(2)
	bx=pyplot.subplot(111)
	for i,color in enumerate(colors):
		covariances=numpy.eye(gmix.means_.shape[1])*gmix.covariances_[i]
		v,w=numpy.linalg.eigh(covariances)
		u=w[0]/numpy.linalg.norm(w[0])
		angle=numpy.arctan2(u[1],u[0])
		angle=180.*angle/numpy.pi
		v=2.*numpy.sqrt(2.)*numpy.sqrt(v)
		ell=patches.Ellipse(gmix.means_[i,:2],v[0],v[1],180+angle,color=color)
		ell.set_clip_box(bx.bbox)
		ell.set_alpha(0.3)
		bx.add_artist(ell)
		bx.set_aspect('equal')
		pyplot.xlim(-lim,lim)
		pyplot.ylim(-lim,lim)
	bx.set_aspect('equal')
	bx.set_xlim([-lim,lim])
	bx.set_ylim([-lim,lim])
	pyplot.hexbin(d1.real,d1.imag,gridsize=30,cmap='Greys',bins='log')

	prediction=gmix.predict(d1_iq)
	prediction_proba=gmix.predict_proba(d1_iq)
	print('all iq',d1_iq)
	print('all prediction',prediction)
	#print 'all prediction proba',prediction_proba
	prediction_herald=gmix.predict(d1_herald_iq).reshape((-1,rabiperiod))
	prediction_regular=gmix.predict(d1_regular_iq).reshape((-1,rabiperiod))

	if (clargs.swap):
		prediction_herald=1-prediction_herald
		prediction_regular=1-prediction_regular
	prediction_modified=(1-prediction_herald)&prediction_regular

	prediction_herald_1col=prediction_herald.reshape((-1,1))
	d1_regular_modified=[]
	for i in range(len(prediction_herald_1col)):
		if prediction_herald_1col[i]==0:
			d1_regular_modified.append(d1_regular[i])
	d1_regular_modified=numpy.array(d1_regular_modified)
	pyplot.figure(3)
	cx=pyplot.subplot(111)
	cx.set_aspect('equal')
	cx.set_xlim([-lim,lim])
	cx.set_ylim([-lim,lim])
	cx.set_title('taking off points according to the heralding')
	pyplot.hexbin(d1_regular_modified.real,d1_regular_modified.imag,gridsize=30,cmap='Greys',bins='log')
	#pyplot.savefig(clargs.filename+'_iq.png')

	#count_herald_zero=prediction_herald.shape[0]*numpy.ones(prediction_herald.shape[1])-numpy.count_nonzero(prediction_herald,axis=0)
	count_herald_zero=numpy.sum(1-prediction_herald,axis=0)
	population=numpy.sum(prediction_modified,axis=0)
	population_norm=1.0*population/count_herald_zero

	pyplot.figure(4)
	pyplot.plot(population_norm,'.-')
	pyplot.ylim(0,1)
	pyplot.title(clargs.filename)
	#pyplot.savefig(clargs.filename+'.png')
	pyplot.show()
