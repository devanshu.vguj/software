import datetime
from matplotlib import pyplot
import numpy
from ..qubic import experiment
import os
import time
from matplotlib.ticker import MaxNLocator


class c_rabixtalk(experiment.c_experiment):
	def __init__(self, ip='192.168.1.124', port=3000, dt=1.0e-9, regmappath='regmap.json', wavegrppath='wavegrp.json', qubitcfg='qubitcfg.json', initcfg='sqinit', **kwargs):
		experiment.c_experiment.__init__(self, ip=ip, port=port, dt=dt, regmappath=regmappath, wavegrppath=wavegrppath, qubitcfg=qubitcfg, initcfg=initcfg, **kwargs)
		self.qubitid = None
		self.qubitid_x = None
		pass

	def rabixtalkseqs(self, delayread=668e-9, delay1=12e-6, delaybetweenelement=600e-6, elementlength=80, elementstep=4e-9, readoutdrvamp=None, qubitdrvamp=None, readwidth=None, fqubit=None, fread=None, qubitid='Q5', qubitidread=['Q7', 'Q6', 'Q5'], qubitid_x='Q7',meas='I'):
		self.qubitid = qubitid
		self.qubitid_x = qubitid_x
		self.seqs.add(360e-9, self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv = {}
		modread = {}
		modrabi = {}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))

		run = 0
		for irun in range(elementlength):
			therald = run
			self.seqs.add(therald, self.qchip.gates[self.qubitid + 'read'].modify([modrdrv, modread]))
			trabi = self.seqs.tend() + delay1
			wrabi = elementstep * irun
			if wrabi != 0:
				modrabi.update(dict(twidth=wrabi, dest=self.qubitid_x + '.qdrv'))
				self.seqs.add(trabi, self.qchip.gates[self.qubitid + 'rabi'].modify(modrabi))
				run= self.seqs.tend()
			else:
				run= self.seqs.tend() + delay1

			if meas=='I':
				pass
			else:
				pmeas=self.qchip.gates[qubitid+meas].pcalc(dt=run-trabi)[0]
				self.seqs.add(run,self.qchip.gates[qubitid+meas].modify(dict(pcarrier=pmeas)))
			run= self.seqs.tend()

			self.seqs.add(run, self.qchip.gates[self.qubitid + 'read'].modify([modrdrv, modread]))
			run = self.seqs.tend() + delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth = [self.seqs.countdest(qid + '.read') for qid in qubitidread]
		self.bufwidth_dict = dict(zip(qubitidread, bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp', fread, fqubit, readoutdrvamp, qubitdrvamp)

	def rabixtalkacq(self, nget):
		data = self.acqdata(nget)
		return data

	def processrabixtalk(self, dt, filename, loaddataset, plot=False, isfitdecay=True, timeinfo=''):
		c = self.loadjsondata(filename)
		return self.processrabixtalkdata(dt=dt, data=c[list(c.keys())[0]], loaddataset=loaddataset, plot=plot, isfitdecay=isfitdecay, timeinfo=timeinfo)

	def processrabixtalkdata(self, dt, data, loaddataset, plot=False, isfitdecay=True, timeinfo=''):
		self.rabixtalk_result = self.process3(data, qubitid=self.qubitid, lengthperrow=self.bufwidth_dict[self.qubitid], training=False, loaddataset=loaddataset)
		figname = 'rabixtalk_matrix' + '_rabipulse' + self.qubitid + '_driveline' + self.qubitid_x + '_' + timeinfo
		if isfitdecay:
			[amp, period, tdecay, fiterr] = self.fitrabidecay(dt=dt, data=self.rabixtalk_result['population_norm'], plot=plot, figname=figname)
		else:
			[amp, period, fiterr] = self.fitrabi(dx=dt, data=self.rabixtalk_result['population_norm'], plot=plot, figname=figname)
		return [data, self.rabixtalk_result['separation'], self.rabixtalk_result['iqafterherald'], self.rabixtalk_result['population_norm'], amp, period, fiterr]


if __name__ == "__main__":
	qubitid_list = ['Q0', 'Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7']
	switch_flag = True
	switch_map = {'Q0': 6, 'Q1': 0, 'Q2': 2, 'Q3': 3, 'Q4': 5, 'Q5': 7, 'Q6': 1, 'Q7': 4}

	chip_name=os.environ['QUBIC_CHIP_NAME']
	if  'chip57' in chip_name:
		switch_flag = False
		qubitid_list = ['Q6', 'Q5']

	timestamp = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d_%H%M%S_%f')
	rabi_rate = []

	from switch import switch

	for qubitid in qubitid_list:
		rabi_rate_tmp = []
		for qubitid_x in qubitid_list:
			if switch_flag:
				switch(port=switch_map[qubitid_x])
				time.sleep(5)
			parser, cmdlinestr = experiment.cmdoptions()
			parser.set_defaults(elementstep=4e-9)
			clargs = parser.parse_args()
			rabixtalk = c_rabixtalk(**clargs.__dict__)
			rabixtalk.rabixtalkseqs(delayread=clargs.delayread, delay1=12e-6, delaybetweenelement=clargs.delaybetweenelement, readwidth=clargs.readwidth, readoutdrvamp=clargs.readoutdrvamp, qubitdrvamp=clargs.qubitdrvamp, elementlength=clargs.elementlength, elementstep=clargs.elementstep, fqubit=clargs.fqubit, fread=clargs.fread, qubitid=qubitid, qubitidread=[qubitid], qubitid_x=qubitid_x)
			if clargs.processfile == '':
				rabixtalk.run(bypass=clargs.bypass)
				data = rabixtalk.rabixtalkacq(clargs.nget)
				fprocess = rabixtalk.savejsondata(filename=clargs.filename + '_qubitid' + qubitid + '_qubitidx' + qubitid_x, extype='rabixtalk', cmdlinestr=cmdlinestr, data=data)
				print('save data to ', fprocess)
			else:
				fprocess = clargs.processfile
			[rawdata, separation, iqafterherald, population_norm, amp, period, fiterr] = rabixtalk.processrabixtalk(dt=clargs.elementstep, filename=fprocess, loaddataset=clargs.dataset, plot=clargs.plot, isfitdecay=True, timeinfo=timestamp)
			rabi_rate_tmp.append(1 / period)
			print('period', period)
			print('separation', separation)
			print('amp', amp)
			print(rabixtalk.hf.adcminmax())
			if clargs.readcorr:
				population_norm = rabixtalk.readoutcorrection(qubitid_list=[clargs.qubitid], meas_binned=numpy.vstack((1 - population_norm, population_norm)), corr_matrix=numpy.load(clargs.corrmx))[1]
				print('corrected population_norm', population_norm)
			if clargs.removefile:
				os.remove(fprocess)
		rabi_rate.append(rabi_rate_tmp)
		outF=clargs.filename + '_rabixtalk_matrix_' + timestamp + '.dat'
		with open(outF, 'a') as f:
			numpy.savetxt(f, numpy.array(rabi_rate_tmp).reshape((1, -1)))
		print('M:matrix saved to:',outF)
	ax = pyplot.figure().gca()
	ax.xaxis.set_major_locator(MaxNLocator(integer=True))
	ax.yaxis.set_major_locator(MaxNLocator(integer=True))
	pyplot.pcolormesh(rabixtalk.repack1D_cent2edge(numpy.arange(len(qubitid_list))), rabixtalk.repack1D_cent2edge(numpy.arange(len(qubitid_list))), rabi_rate, cmap='Blues')
	pyplot.xlabel('Drive Line')
	pyplot.ylabel('Rabi Pulse')
	pyplot.colorbar(label='Rabi Rate (Hz)')
	pyplot.savefig(clargs.filename + '_rabixtalk_matrix_' + timestamp + '.pdf')
	# python rabi_xtalk_matrix.py --plot -n 20 -es 16e-9 -el 60
