import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import re
from simp2q import *

if __name__=="__main__":
	import trueq
	import qubic_trueq
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'

	config = trueq.Config("""
        Name: MyDevice
        N_Systems: 2
        Mode: ZXZXZ
        Dimension: 2
        Gate Z(phi):
            Hamiltonian:
                - ["Z", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate X(phi):
            Hamiltonian:
                - ["X", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate CNOT:
            Matrix:
                - [1, 0, 0, 0]
                - [0, 1, 0, 0]
                - [0, 0, 0, 1]
                - [0, 0, 1, 0]
            Involving:
                (6, 5) : ()
        """
	)	
	
	#n_random_cycles = [2, 64 ,256]
	#n_random_cycles = [2, 4]
	#n_random_cycles = [2, 4, 8]
	#n_random_cycles = [2, 64,256]
	#n_random_cycles = [4, 32, 128]
	#n_random_cycles = [4, 64, 256]
	#n_random_cycles = [1]
	#n_random_cycles = [4,32,256 ]
	#n_random_cycles = [2,4]#32,256 ]
	n_random_cycles = [1,2,3]
	#n_random_cycles = [4,32,64]
	#n_random_cycles = [1]
	#n_random_cycles = [4,  64,  512]
	n_circuits = 4
	#n_circuits = 2
	#n_circuits = 10

	from simp2q import c_simp2q
	simp2q=c_simp2q()
	parser,cmdlinestr=simp2q.cmdoptions()
	clargs=parser.parse_args()
	if clargs.processfile=='':
		gates=[simp2q.qchip.gates['M0mark']
				,simp2q.qchip.gates['Q6X90']
				,simp2q.qchip.gates['Q6read']
				,simp2q.qchip.gates['Q5X90']
				,simp2q.qchip.gates['Q5read']
				,simp2q.qchip.gates['Q6Q5CNOT']
				]
		elementlist={'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':5,'Q5.rdrv':6,'Q7.rdrv':4,'Q6.read':9,'Q5.read':10,'M0.mark':12}
		destlist=   {'Q6.qdrv':2,'Q5.qdrv':3,'Q7.qdrv':1,'Q6.rdrv':0,'Q5.rdrv':0,'Q7.rdrv':0,'Q6.read':5,'Q5.read':6,'M0.mark':13}
		patchlist={'Q6.rdrv':0,'Q6.read':0,'Q6.qdrv':0,'Q5.rdrv':8e-9,'Q5.read':8e-9,'Q5.qdrv':8e-9,'M0.mark':0}
		delayafterheralding=12e-6
		delaybetweenelement=600e-6
		#circuits_file=clargs.qubic_file
		firsttime=True

		if clargs.sim:
			simp2q.setsim()
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('timestamp to start',(datetime.datetime.strptime(timestamp,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		tt=trueq.load('tcircuit.bin')
		#cirlen=8
#		for cirlen in [7,8,9]:#range(len(tt[0])-1):
		#cnotphdeg=292 -135#-180#-90 #-5# -180#-90 #-180 
		step=20
		measurement_results={}
		for cnotphdeg in [0]:#range(-10,10):#range(0,360,20):#,292-135,292-90]:#range(0,360,5):
			keycnotphdeg=int(cnotphdeg)
			measurement_results[keycnotphdeg]={}
			for measphdeg in numpy.arange(0,360+step+1,step):
				print(type(measphdeg))
				keymeasphdeg=int(measphdeg)
				if (1):
					tt_list=tt[0][0:34]


				if (0):
					tt_list=[]#tt[0][0:0]
					tt_list.append(trueq.Cycle({(5,):config.z(-22.7),(6,):config.z(-180)}))
					tt_list.append(trueq.Cycle({(5,):config.x(90),(6,):config.x(90)}))
					tt_list.append(trueq.Cycle({(5,):config.z(68),(6,):config.z(180)}))
					tt_list.append(trueq.Cycle({(5,):config.x(90),(6,):config.x(90)}))
					tt_list.append(trueq.Cycle({(5,):config.z(0),(6,):config.z(90)}))
					tt_list.append(trueq.Cycle({(6,5):config.cnot()}))
				if (0):
					tt_list=[]#tt[0][0:0]
					tt_list.append(trueq.Cycle({(5,):config.z(-22.73120305953306),(6,):config.z(-180)}))
					tt_list.append(trueq.Cycle({(5,):config.x(90),(6,):config.x(90)}))
					tt_list.append(trueq.Cycle({(5,):config.z(68.67682384718997),(6,):config.z(100.31578875780873)}))
					tt_list.append(trueq.Cycle({(5,):config.x(90),(6,):config.x(90)}))
					tt_list.append(trueq.Cycle({(5,):config.z(-8.661934773590986),(6,):config.z(-127.12950576043217)}))
					tt_list.append(trueq.Cycle({(6,5):config.cnot()}))
				if 0:
					tt_list=[]#tt[0][0:0]
					tt_list.append(trueq.Cycle({(6,):config.z(0)}))
					tt_list.append(trueq.Cycle({(6,):config.x(90)}))
#			tt_list.append(tt[0][8])
#			tt_list.append(tt[0][-1])
					tt_list.append(trueq.Cycle({(6,5):config.cnot()}))
				if (measphdeg<=360):
					tt_list.append(trueq.Cycle({(5,):config.z(measphdeg),(6,):config.z(measphdeg)}))
					tt_list.append(trueq.Cycle({(5,):config.x(90),(6,):config.x(90)}))
				tt_list.append(trueq.Cycle({(5,):trueq.Meas(),(6,):trueq.Meas()}))
				newcirc=trueq.Circuit(tt_list) 
				transpiled_circuit=trueq.CircuitCollection(newcirc)#[tt[0][0:3],tt[0][-1]]))

				print('cirgentime to start',(datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
				trueq_result=[]
				measbinned_result=[]
				simulation_results=[]
				index=0
				lastcmdarraylength=0
				for circuit in transpiled_circuit:
					print('index',index)
					index=index+1
					[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0,cnotphdeg=cnotphdeg)
#		#print('cmdarray len',len(cmdarray))
					for l in cmdarray:
						print(l)
					cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
					simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
					lastcmdarraylength=cmdarraylength
					print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
					data=simp2q.simp2qacq(clargs.nget)
					fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
					print('save data to ',fprocess)
					if clargs.sim:
						simp2q.sim()
					[trueq_return,meas_binned]=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset,readcorr=clargs.readcorr,corr_matrix=numpy.load(clargs.corrmx))
					print('meas_binned',meas_binned)
					print(trueq_return)
					trueq_result.extend(trueq_return)
					measbinned_result.extend([{'00':meas_binned[0][0],'01':meas_binned[1][0],'10':meas_binned[2][0],'11':meas_binned[3][0]}])
					firsttime=False

#			print(trueq_result)
				timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')

				s=trueq.Simulator()
				s.run(newcirc,n_shots=4096)
				contval=newcirc.results.to_dict()['content']['value']
				contvalsum=sum(contval.values())
				contvalfrac={k:v*1.0/contvalsum for k,v in contval.items()}
				simulation_results.append(contvalfrac)
				print('newcirc.results',simulation_results)
				#with open(resultfilename) as resultfile:
				measurement_results[keycnotphdeg][keymeasphdeg]=measbinned_result  #trueq_result
				print('measurement',measurement_results[keycnotphdeg][keymeasphdeg])
				#if cirlen>1:
			#	print('2ndfromlast',transpiled_circuit[0][-2],cmdarray[-3])
				for i in range(len(measurement_results[keycnotphdeg][keymeasphdeg])):
					print('cnotphdeg',cnotphdeg,'measphdeg',measphdeg,'Sim',{k: v for k, v in sorted(simulation_results[i].items(), key=lambda item: item[1], reverse=True)},'Meas',{k: v for k, v in sorted(measurement_results[keycnotphdeg][keymeasphdeg][i].items(), key=lambda item: item[1], reverse=True)})
	else:
		simp2q.bufwidth_dict={'Q6': 2, 'Q5': 2}
		simp2q.qubitid=['Q6','Q5']
		simp2q.hf.accout_dict={'Q6': 'accout_1', 'Q5': 'accout_2'}
		fprocess=clargs.processfile
		print('process',fprocess)
		trueq_return=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset)
	print(measurement_results)
	json.dumps(measurement_results)
	with open('measurement_results.json','w+') as f:
		json.dump(measurement_results,f)
	
