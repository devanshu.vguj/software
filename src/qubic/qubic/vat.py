import numpy
import os
import sys
import bz2
sys.path.append('../..')
from qubic.qubic import commands
from qubic.qcvv.analysis import gmm
from qubic.qubic.compile import c_compile,c_circbasegate#,elememory,circtocmds
from qubic.instrument.ttyclient import c_ttyclient
from matplotlib import pyplot
import pickle
from qubicrun import c_qubicrun

if __name__=="__main__":
    import argparse
    from squbic import c_qchip
    from chassis import c_chassis
    import imp
    import os
    import json
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--name',help='variable attentuator name',dest='name',type=str)#,nargs='?',default=None)
    parser.add_argument('-v','--val',help='variable attentuator value',dest='val',type=float,nargs='?',default=None)
    clargs=parser.parse_args()
    print(clargs)
    ops={}
    ops['cfgpath'] = '../../../../qchip/' + os.environ['QUBIC_CHIP_NAME'] + '/'
    ops['ip'] = os.environ['QUBICIP']
    ops['qubitcfgfile'] = ops['cfgpath'] + os.environ['QUBICQUBITCFG']
    ops['wiremapfile']= ops['cfgpath'] + os.environ['QUBICWIREMAP'] + '.py'
    ops['sim']=True
    ops['debug']=True
#    ops.update(**kwargs)
    chassis=c_chassis(ops['ip'],sqinit=False,sim=ops['sim'],debug=ops['debug'])
    with open(ops['qubitcfgfile']) as jfile:
        qubitcfg=json.load(jfile)
    wiremap = imp.load_source("wiremap",ops['wiremapfile'])
    qchip=c_qchip(qubitcfg)
    qr=c_qubicrun(chassis=chassis,qchip=qchip,wiremap=wiremap)#,tend=4e-6,**ops)
    print(qr.setvat(clargs.name,clargs.val))
