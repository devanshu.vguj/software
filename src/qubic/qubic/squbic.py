from qubic.qubic.envelop_pulse import c_envelop_pulse

import inspect
import time
import copy
import json
import numpy
import re
from matplotlib import pyplot
def recuname(dl2):
    if isinstance(dl2, list) or isinstance(dl2,tuple):
        val=tuple([recuname(v) for v in dl2])
    elif isinstance(dl2,dict):
        val=tuple(sorted([(k,recuname(v)) for k,v in dl2.items()]))
    else:
        val=dl2
    return val
def recuupdate(dl1,dl2):
    #print(dl1,dl2)
    #    print('recuupdate',dl2)
    if isinstance(dl2, list) or isinstance(dl2,tuple):
        if isinstance(dl1,list) or isinstance(dl1,tuple):
            if len(dl2)==len(dl1):
                for index,val in enumerate(dl2):
                    recuupdate(dl1[index],dl2[index])
            else:
                exit("list length mismatch")
        else:
            exit("recuupdate type mismatch: %s %s"%(str(dl1),str(dl2)))
    elif isinstance(dl2,dict):
        if isinstance(dl1,dict):
            for k,val in dl2.items():
                if k in dl1:
                    if isinstance(val, list) or isinstance(val,tuple):
                        recuupdate(dl1[k],dl2[k])
                    elif isinstance(val, dict):
                        recuupdate(dl1[k],dl2[k])
                    else:
                        dl1.update(dl2)
                        #print('after update',dl1)
                else:
                    exit('dict key %s not match'%k)
        else:
            exit("recuupdate type mismatch: %s"%str(dl2))
    elif dl2 is None:
        pass
    else:
        exit('list? dict? ')
    #print(dl1,dl2)
    return dl1

#try:
#    basestring
#except NameError:
#    basestring = str
class c_qubit:
    def __init__(self,**paradict):
        self.paradict=paradict
        self.phase={}
        for k,v in paradict.items():
            setattr(self,k,v)
            self.phase[k]=0
#            if k in ['freq','freq_ef','readfreq']:
#                setattr(self,k,paradict[k])
#            elif k in ['rabi','rabi_ef','180','90','180_ef','90_ef','readout','readoutdrv']:
#                setattr(self,k,c_gate1(paradict[k] if isinstance(paradict[k],list) else [paradict[k]],qubit=self))
        self.qdrv=[]
        self.rdrv=[]
        self.read=[]
        self.mark=[]
    def addphase(self,freqname,phase):
        self.phase[freqname]=(self.phase[freqname]+phase)%(2*numpy.pi)
    def addtimephase(self,freqname,time):
        self.phase[freqname]=(self.phase[freqname]+2*numpy.pi*self.paradict[freqname]*time)%(2*numpy.pi)
class c_qchip:
    def __init__(self,paradict,name=''):#,"gate1":c_gate}):
        self.name=name
        self.paradict=copy.deepcopy(paradict)
        self.gatesdict={}
        for gatename in self.paradict['Gates']:
            self.gatesdict[gatename]=self.gatedictexpand(self.paradict['Gates'],gatename)
        self.qubits={}
        for k,v in self.paradict['Qubits'].items():
            self.qubits.update({k:c_qubit(**v)})
        self.gates={}

        for k,pulselist in self.gatesdict.items():
            self.gates.update({k:c_gate(pulselist,chip=self,name=k)})
    def gatedictexpand(self,gatesdict,gatename):
        pulseout=[]
        for p in gatesdict[gatename]:
            if isinstance(p,str):
                if p in gatesdict:
                    pulseout.extend(self.gatedictexpand(gatesdict,p))
                else:
                    exit('Error gate description: %s'%p)
            else:
                pulseout.append(p)
        return pulseout


    def updatecfg(self,path_value,wfilename=None):
        for path,value in path_value.items():
            obj=self.paradict
            for index in range(len(path)):
                if index==len(path)-1:
                    obj[path[index]]=value
                else:
                    obj=obj[path[index]]
        if wfilename:
            fwrite=open(wfilename,'w')
            json.dump(self.paradict,fwrite,indent=4)
            print('c_qchip.updatecfg',wfilename)
            fwrite.close()
        paradict=copy.deepcopy(self.paradict)
        return paradict

#    def updatefreq(self,freqname,val):
#        [q,f]=freqname.split('.')
#        setattr(self.qubits[q],f,val)
    def getfreq(self,freqname):
        if isinstance(freqname,str):
            m=re.match('(?P<qname>\S+)\.(?P<fname>\S+)',freqname)
            if m:
                q=m.group('qname')
                f=m.group('fname')
                if q in self.qubits:
                    if hasattr(self.qubits[q],f):
                        freq=getattr(self.qubits[q],f)
                    else:
                        print("qubit %s doesn't have %s"%(str(q),str(f)))
                        freq=freqname
                else:
                    print("Qubit %s not found"%str(q))
                    freq=freqname
            else:
                print("%s is not match qubit.freqname style,use it directly"%(str(freqname)))
                freq=freqname
        elif isinstance(freqname,float) or isinstance(freqname,int):
            freq=freqname
        else:
            print("%s is not a string, use it directly"%str(freqname))
            freq=freqname
    #    print('freq',freq)
        return freq
    def getdest(self,destname):
        [q,d]=destname.split('.')
        return getattr(self.qubits[q],d)

class c_gate:
    def __init__(self,paralist,chip,name):
        self.chip=chip
        self.name=name
        self.pulses=[]
        self.paralist=copy.deepcopy(paralist)
        self.normalize=1.0
        for paradict in self.paralist:
            pulse=c_gatepulse(gate=self,paradict=paradict)
        #    pulse.setfreq(chip=self.chip)
            self.pulses.append(pulse)
    #        self.paralist.append(pulse.paradict)
        self.tstart=None
#        for pulse in self.pulses:
#            pulse.settstart(self.tstart)
#self.tlength=None
#self.tgatelength=
        self.calctlength()
    def calcnorm(self,dt,FS):
        if len(self.pulses)==2:
            p0=self.pulses[0].pulseval(dt,self.pulses[0].fcarrier,mod=False).nvval(dt)[1]
            p1=self.pulses[1].pulseval(dt,self.pulses[1].fcarrier,mod=False).nvval(dt)[1]
#            print(p0,p1)
            self.normalize=numpy.sum(p0*p1)*FS
#            print(self.normalize)
#            pyplot.plot(p0.real)
#            pyplot.plot(p0.imag)
#            pyplot.show()
    def settstart(self,tstart):
        self.tstart=tstart
        for pulse in self.pulses:
            pulse.settstart(tstart)
        self.calctlength()
#    def getval(self,dt,flo=0,mod=True):
#        tval=None
#        for pulse in self.pulses:
#            newtval=pulse.val(dt=dt,flo=flo,mod=mod)
#            if tval==None:
#                tval=newtval
#            else:
#                tval.add(newtval)
#        return tval
#    def get_pulses(self):
#        return self.pulses
    def calctlength(self):
        #return max([pulse.tlength() for pulse in self.pulses])
        self.tlength=max([p.t0+p.twidth for p in self.pulses])-min([p.t0 for p in self.pulses])
#        print('debug calctlength',self.name,self.tlength)
#        if self.length==None or 1:
#            self.length=max([pulse.tend() for pulse in self.pulses])-min([pulse.tstart for pulse in self.pulses])
#        return self.length
    def tend(self):
        #print 'self.tstart',self.tstart,'self.tlength',self.tlength()
        return max([pulse.tend() for pulse in self.pulses])#self.tstart+self.t0+self.tlength()
    def modify(self,paradict=None):
        newlist=recuupdate(copy.deepcopy(self.paralist),paradict)#copy.deepcopy(self.paralist)
        return c_gate(newlist,self.chip,name=self.name)
        #print('paradict',paradict)
        #print(type(paradict),len(paradict),len(newlist))
#        if paradict is not None:
#            if len(newlist)==1:
#                for pd in newlist:
#                    pd.update(paradict)
#            else:
#                if isinstance(paradict,dict):
#                    paradict=[{k:v[index] for k,v in paradict.items()} for index in range(len(newlist))]
#                if isinstance(paradict,list):
#                    if len(paradict)==len(newlist):
#                        for index in range(len(newlist)):
#                            newlist[index].update(paradict[index])
#                    else:
#                        print('modify multiple pulse gate need to be same length as the original gate')
#                else:
#                    print('modify multiple pulse gate need to be same length as the original gate')
#        return c_gate(newlist,self.chip,name=self.name)
    def dup(self):
        return self.modify()
    def pcalc(self,dt=0,padd=0,freq=None):
        #print numpy.array([(freq  if freq else p.fcarrier) for p  in  self.pulses])
        return numpy.array([p.pcarrier+2*numpy.pi*(freq  if freq else p.fcarrier)*(dt+p.t0)+padd for p  in  self.pulses])

class c_gatepulse:
    def __init__(self,gate,paradict):
        '''
        t0: pulse start time relative to the gate start time
        tstart: pulse start time relative to circuit or circuits start time, this will be directly used for the trig_t
        twidth: pulse env function parameter for the pulse width
        patch: patch time added before the pulse, during this time, the pulse envelope value is 0
        tlength: pulse length including patch time, so that include the zeros at the begining, tlength=twidth+patch*dt
        '''
        self.paradict=copy.deepcopy(paradict)
        self.gate=gate
        self.chip=gate.chip
        self.dest=None
        self.amp=0
        self.twidth=0
        for k in paradict:
            if k in ['amp','twidth','t0','pcarrier','dest','fcarrier']:
                try:
                    v=eval(str(paradict[k]))
                except Exception as e:
                    v=paradict[k]
                setattr(self,k,v)
                self.paradict[k]=paradict[k]
            elif k in ['env']:
                #print 'envdict',paradict[k]
                setattr(self,k,c_envelop(paradict[k]))
                #self.paradict[k]=sorted(paradict[k])
            else:
                pass
                #print('what',k)
        self.tstart=0
        self.npatch=0
        self.tpatch=0
        self.tlength=self.twidth
        self.setfreq(self.chip)

    def settstart(self,tstart):
        self.tstart=tstart+self.t0
    def setfreq(self,chip):
        self.fcarriername=self.fcarrier
        self.fcarrier=chip.getfreq(self.fcarrier)

    def pulseval(self,dt,flo=0,mod=True):
        #print (self.env.env_desc[0]['env_func'])
        #print dir(self)
        fcarrier=self.chip.getfreq(self.fcarrier)
        fif=0 if self.fcarrier==0 else (fcarrier-flo)
        if self.dest is not None:
            tv=self.env.env_val(dt=dt,twidth=self.twidth,fif=0*fif,pini=self.pcarrier,amp=self.amp,mod=mod)
            tv.tstart(self.tstart)
        else:
            tv=None
#        pyplot.plot(ti,val)
#        pyplot.title('debug')
#        pyplot.show()
        return tv

    def tend(self):
        return self.tstart+self.twidth
#    def tlength(self):
#        return self.twidth
    def __ne__(self,other):
        return not self.__eq__(other)
    def __eq__(self,other):
        #return all([self.amp==other.amp,self.twidth==other.twidth,self.fcarrier==other.fcarrier,self.dest==other.dest,self.env==other.env])
#        print 'gatepulse eq',self.amp,other.amp,self.twidth,other.twidth,self.dest,other.dest,self.env,other.env
        ampeq=False
        twidtheq=False
        desteq=False
        enveq=False
        if hasattr(self,'env'):
            if hasattr(other,'env'):
                if self.env==other.env:
                    enveq=True
        if hasattr(self,'amp'):
            if hasattr(other,'amp'):
                if self.amp==other.amp:
                    ampeq=True
        if hasattr(self,'twidth'):
            if hasattr(other,'twidth'):
                if self.twidth==other.twidth:
                    twidtheq=True
        if hasattr(self,'dest'):
            if hasattr(other,'dest'):
                if self.dest==other.dest:
                    desteq=True
#        return all([self.amp==other.amp,self.twidth==other.twidth,self.dest==other.dest,self.env==other.env])
        return all([ampeq,twidtheq,desteq,enveq])
    def __hash__(self):
        #return hash(str(self.paradict))
        return hash(str({k:self.paradict[k] for k in ("amp","twidth","t0","dest") if k in self.paradict}))
    def timeoverlap(self,other):
        overlap=False
        if self.tstart<other.tstart:
            overlap=self.tend() < other.start
        else:
            overlap=other.tend() < self.tstart
        return overlap

class c_envelop:
    def __init__(self,env_desc):
        self.pulses=c_envelop_pulse()
        self.availfunc=self.pulses.listpulses()
        if not isinstance(env_desc,list):
            env_desc=[env_desc]
        self.env_desc=copy.deepcopy(env_desc)
    def env_val(self,dt,twidth,fif=0,pini=0,amp=1.0,mod=True):
        vbase=None
        ti=None
        twidth=round(twidth,10)
        for env in self.env_desc:
            #print 'c_envelop.env_val',getattr(self.pulses,env['env_func']),dt,env['paradict'],self.twidth
            #print 'twidth in c_envelop env_val',twidth
            if vbase is None:
                ti,vbase =getattr(self.pulses,env['env_func'])(dt=dt,twidth=twidth,**env['paradict'])
            else:
                ti1,vbasenew=(getattr(self.pulses,env['env_func'])(dt=dt,twidth=twidth,**env['paradict']))
                if any(ti!=ti1):
                    print('different time!!?')
            #    print val.shape,valnew.shape
                vbase=vbase*vbasenew
        if mod:
            vlo=numpy.cos(2*numpy.pi*fif*ti+pini)
        else:
            vlo=1
        #print 'amp', amp
        val=amp*vlo*vbase
        #if carrier and len(carrier)>=len(val):
        #    val=val*carrier[:len(val)]
#        print 'val in c_envelop',val
#                print 'debug',len(ti),len(val),twidth
        tv=c_tv(ti,val)
        #print type(tv),tv.__class__
        return tv
    def __eq__(self,other):
        return sorted(self.env_desc)==sorted(other.env_desc)
class c_tv:
    def __init__(self,t,val):
        self.tv={}
        if (len(t)==len(val)):
            for i in range(len(t)):
                self.append(t[i],val[i])
#        print t
#        print self.tv
        self.val=val
    def append(self,t,val):
        if t in self.tv:
            self.tv[t]+=val
        else:
            self.tv[t]=val
    def add(self,tval):
        for it in tval.tv:
            self.append(it,tval.tv[it])
    def tstart(self,tstart):
        newtv=dict((t+tstart,val) for (t,val) in self.tv.items())
        self.tv=newtv
    def tvval(self):
        self.t=[]
        self.val=[]
        for i in sorted(self.tv):
            self.t.append(i)
            self.val.append(self.tv[i])
        return(numpy.array(self.t),numpy.array(self.val))
    def nvval(self,dt):
        t,v=self.tvval()
        n=numpy.array([int(round(it/dt,10)) for it in t])

#        return (numpy.array(numpy.round(t/dt)).astype(int),v)
        return (n,v)
#class c_seq:
#    def __init__(self,tstart,gate,cond=0):
#        self.tstart=tstart
#        self.gate=gate.dup()
#        self.gate.settstart(self.tstart)
#        self.cond=cond
#        pass
#    def tend(self):
#        return self.gate.tend()
#    def countdest(self,dest):
#        return len([pulse for pulse in self.gate.get_pulses() if pulse.dest ==dest])
#    def allocate(self):
#        for pulse in self.gate.get_pulses():
#            self.gate.chip.getdest(pulse.dest).append(pulse)
##            print self.gate.chip.getdest(pulse.dest)
#        #    print 'in allocate',pulse.dest,self.gate.chip.getdest(pulse.dest)
#
#        #pass
#    def pulses(self):
#        return self.gate.pulses
#    def gettv(self,dt,flo,mod):
#        tv=(self.gate.getval(dt=dt,flo=flo,mod=mod))
#        #tv.tstart(self.tstart)
#        return tv
#
#
#class c_seqs:
#    def __init__(self,seqlist=[]):
#        self.seqlist=[]
#        tend=0
#        for seq in seqlist:
#            if seq:
#            #            if seq[0]==None:
#            #    tstart=tend
#            #else:
#            #    tstart=seq[0]
#            #newseq=c_seq(tstart,seq[1])
#            #self.seqlist.append(newseq)
#            #tend=newseq.tend()
#                self.add(seq[0],seq[1])
#        #print self.seqlist
##        self.allocate()
##        seqend=self.tend()
##        self.period=(period if period else (( seqend+periodextra) if periodextra else seqend))
##        if self.period < seqend:
##            print "ERROR: period is too small: period:", period, "sequence end:", seqend
##        print 'c_seqs period',self.period
#        self.period=None
#        self.periodextra=None
#    def add(self,t,gate,cond=0):
#        #print t/1e-9,gate.name
#        newseq=c_seq(t,gate,cond=cond)
#        self.seqlist.append(newseq)
#    def countdest(self,dest):
#        return sum([s.countdest(dest) for s in self.seqlist])
#    def setperiod(self,period=None,periodextra=None):
#        self.period=period
#        self.periodextra=periodextra
#    def calcperiod(self):
#        self.period=(self.period if self.period else (( self.tend()+self.periodextra) if self.periodextra else self.tend()))
#        if self.period < self.tend():
#            print("ERROR: period is too small: period:", period, "sequence end:", seqend)
#    def getperiod(self):
#        return self.period
#    def tend(self):
#        return max([seq.tend() for seq in self.seqlist])
#    def allocate(self):
#        for seq in self.seqlist:
#            seq.allocate()
#    def gettv(self,dt,flo,mod):
#        tv=None
#        for seq in self.seqlist:
#            #print 'loop seq',seq
#            if tv==None:
#                tv=seq.gettv(dt=dt,flo=flo,mod=mod)
#            else:
#                tv.add(seq.gettv(dt=dt,flo=flo,mod=mod))
#        #print 'c_seqs.gettv',tv,tv.__class__
#        return tv
#    def env_memory(self,dt):
#        envdict={}
#        for l in self.linepulsedict:
#            if l not in envdict:
#                envdict[l]=[]
#            envdict[l].append(l)
#        for l in envdict:
#            envdict[l]=set(envdict[l])
#        return envdict
#
#    def pulses(self):
#        p=[]
#        for seq in self.seqlist:
#            p.extend(seq.pulses())
#        return p
#    def linepulsedict(self):
#        lpdict={}
#        for p in self.pulses():
#            line=p.dest
#            pulse=p
#            if line not in lpdict:
#                lpdict[line]=[]
#            lpdict[line].append(p)
#        return lpdict
##    def c
#

if __name__=="__main__":
    #    with open('qubitcfg.json') as jfile:
    with open('../../qchip/chip57/qubitcfg_chip57.json') as jfile:
        chipcfg=json.load(jfile)
    qchip=c_qchip(chipcfg)
    m=(qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
    m=(qchip.gates['Q5read'].modify({"amp":0.5}))
    print(m.paralist)
#    seq=[]
#    trun=numpy.arange(0,50*3000e-9,3000e-9)
#    #for irun in range(len(trun)):
#    for irun in range(5):
#        run=trun[irun]+1e-9
#        wrun=7e-9*(irun+1)
#        arun=0.2*(irun+1)
#        #print 'run',run
#        seq.extend([(run+0,qchip.gates['Q1readout'])
#                ,(run+0,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
#                ,(run+1e-6,qchip.gates['Q7readout'])
#                ,(run+2e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun,"amp":0.5}))
#                ,(None,qchip.gates['Q1readoutdrv'].modify({"amp":0.5}))
#            ,(None,qchip.gates['Q0rabi_ef'])
#            ,(None,qchip.gates['Q1rabi_gh'].modify({"amp":0.5}))
#            ,(None,qchip.gates['Q0readoutdrv'].modify({"amp":0.5}))
#            ])
#        #        seq.extend([(run+0,qchip.gates['Q1rabi_gh'])
#        #    ,(None,qchip.gates['Q1rabi_gh'].modify({"twidth":wrun}))
#        #    ,(None,qchip.gates['Q1rabi_gh1'])
#        #    ,(None,qchip.gates['Q1rabi_gh2'])
#        #    ])
#        #g1=qchip.gates['Q1rabi'].modify({"twidth":wrun,"amp":arun})
#        #print 'g1',g1
#        #seq.extend([(run+20e-6,g1)])#,(None,qchip.gates['Q1readoutdrv'].modify({"twidth":wrun})),(None,qchip.gates['Q1rabi_ef'].modify({"twidth":wrun})),(None,qchip.gates['Q0readoutdrv'])])
#        #seq.extend([(run+20e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun}))])
#        #seq.extend([(run+20e-6,qchip.gates['Q1rabi'].modify({"twidth":wrun})),(None,qchip.gates['Q1readoutdrv'].modify({"twidth":wrun})),(None,qchip.gates['Q1rabi_ef'].modify({"twidth":wrun})),(None,qchip.gates['Q0readoutdrv'])])
#        #g0=qchip.gates['Q1rabi']
#        #print 'g1',[p.dest for p in g1.pulses],[p.dest for p in g0.pulses],g1.chip,g0.chip
#    #seq=[(20e-6,qchip.gates['Q1readout'])]#,(20e-6,qchip.Q1.rabi),(None,qchip.Q1.readoutdrv),(None,qchip.Q1.readout)]
#    seqs=c_seqs(seq)
#    #print 'linepulsedict',seqs.linepulsedict()
#    #print [[p for p in s.gate.pulses] for s in seqs.seqlist]
#    #nvlist=[c_nvpulse(p,dt=1e-9,flo=5.5e9,mod=False) for p in s.gate.pulses for s in seqs.seqlist]
##    nvlist=[item for sublist in nv for item in sublist]
#    #print 'len set nv',len(nvlist),len(set(nvlist))
#    from ether import c_ether
#    from regmap import c_regmap
#    from cmdsim1 import c_cmdsim
#    from hfbridge import c_hfbridge
#    interface=c_ether('192.168.1.124',port=3000)
#    hf=c_hfbridge(interface=interface)
#    ((cmda,cmdv),ops)=hf.cmdgen(seqs)
#    hf.cmdgen0(seqs)
#    hf.elementmemwrite()
#    hf.cmdwrite()
#    #print cmda,cmdv,ops
#    tend=50*3000e-9
#    from cmdsim import cmdsim
#    sim1=cmdsim((cmda,cmdv),ops)
#    for chan in range(7):
#        #tv=sim1.getiqmod(chan)
#        tv=sim1.getiq(chan)
#        pyplot.subplot(7,2,chan*2+1)
#        pyplot.plot(tv[:,0],tv[:,1].real,'.')
#        pyplot.xlim([0,tend/1e-9])
#        pyplot.subplot(7,2,chan*2+2)
#        pyplot.plot(tv[:,0],tv[:,1].imag,'.')
#        pyplot.xlim([0,tend/1e-9])
#        print('chan %d max %8.2f'%(chan,max(abs(tv[:,1]))))
#    pyplot.show()
#
#
#    
#    pyplot.figure(1)
##    tend=16e-9
#    for i in range(8):
#        print('dac element %d has %d different pulses, memory length %d'%(i,len(hf.dacelements[i].mem),len(hf.dacelements[i].ramdata)))
#        ni=hf.dacelements[i].nall
#        vi=numpy.array(hf.dacelements[i].vall)
#        pyplot.subplot(8,2,2*(i+1)-1)
#        pyplot.plot(ni,vi.real,'.')
#        pyplot.xlim([0,tend/1e-9])
#        pyplot.subplot(8,2,2*(i+1))
#        pyplot.plot(ni,vi.imag,'.')
#        pyplot.xlim([0,tend/1e-9])
#    pyplot.figure(2)
#    for i in range(3):
#        print('adc element %d has %d different pulses,memory length %d'%(i,len((hf.loelements[i]).mem),len(hf.loelements[i].ramdata)))
#        ni=hf.loelements[i].nall
#        vi=numpy.array(hf.loelements[i].vall)
#        pyplot.subplot(3,2,2*(i+1)-1)
#        pyplot.plot(ni,vi.real,'.')
#        pyplot.xlim([0,tend/1e-9])
#        pyplot.subplot(3,2,2*(i+1))
#        pyplot.plot(ni,vi.imag,'.')
#        pyplot.xlim([0,tend/1e-9])
#    pyplot.figure(3)
##    print hf.adcelements[0].mem[hf.adcelements[0].mem.keys()[0]][0]==hf.adcelements[2].mem[hf.adcelements[2].mem.keys()[0]][0]
##    n0list=[i.n0() for i in nvlist]
##    print n0list
#
#
##    print 'here',seqs.seqlist,seqs.seqlist[1].gate.pulses[0].twidth
#    #for q in qchip.qubits:
#    #    print q
#    #    print 'qdrv',qchip.qubits[q].qdrv
#    #    print 'rdrv',qchip.qubits[q].rdrv
#    #    print 'read',qchip.qubits[q].read
#    #    print
#    #for q in qchip.qubits:
#    #    for sig in ['qdrv','rdrv','read']:
#    #        print q,sig
#    #        plist=[p for p in getattr(qchip.qubits[q],sig)]
#    #        print len(plist),len(set(plist)),[(p.t0,round(p.tstart/1e-9),round(p.tend()/1e-9)) for p in plist]
##    print 'seq check'
##    for seq in qchip.qubits['Q1'].qdrv:
##        print 'seq',seq.gate.pulses
##        for pulse in seq.gate.pulses:
##            print pulse.twidth
#    (t,val)=(seqs.gettv(dt=1e-9,flo=5.5e9,mod=False)).tvval()
##    (t,val)=(seqs.gettv(dt=1e-9)).tvval()
#    #plist=[[q.paradict for q in p.gate.pulses] for p in qchip.qubits['Q1'].qdrv]
##    for p in plist:
##        for q in plist:
##            print p==q
##    #print seqs
##    #print t,val
###    .val()
###    t,val=
##
##
###    tv=qchip.Q1.readout.getval(dt=1e-9)
###    print 'tv',tv
###    for pulse in qchip.Q1.rabi:
###        print 'loop',pulse,pulse.env
###        (t,v) =pulse.getval(dt=1e-9)
#    pyplot.subplot(211)
#    pyplot.plot(t,val.real,'.')
#    pyplot.subplot(212)
#    pyplot.plot(t,val.imag,'.')

