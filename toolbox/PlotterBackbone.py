import os
import numpy as np

import time
#from QubicUtil import repack1D_cent2edge, measureTime_to_string

#...!...!..................
def roys_fontset(plt):
    print('load Roys fontest')
    #plt.rcParams['axes.spines.right'] = False
    #plt.rcParams['axes.spines.top'] = False
    plt.rcParams['pdf.fonttype'] = 42
    plt.rcParams['ps.fonttype'] = 42

    tick_major = 6
    tick_minor = 4
    plt.rcParams["xtick.major.size"] = tick_major
    plt.rcParams["xtick.minor.size"] = tick_minor
    plt.rcParams["ytick.major.size"] = tick_major
    plt.rcParams["ytick.minor.size"] = tick_minor

    font_small = 12
    font_medium = 13
    font_large = 14
    plt.rc('font', size=font_small)          # controls default text sizes
    plt.rc('axes', titlesize=font_medium)    # fontsize of the axes title
    plt.rc('axes', labelsize=font_medium)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=font_small)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=font_small)    # fontsize of the tick labels
    plt.rc('legend', fontsize=font_small)    # legend fontsize
    plt.rc('figure', titlesize=font_large)   # fontsize of the figure title

#............................
#............................
#............................
class PlotterBackbone(object):
    def __init__(self, args):
        self.jobName=args.prjName
        self.venue=args.formatVenue        
        
        import matplotlib as mpl
        if args.noXterm:
            if args.verb>0: print('disable Xterm')
            mpl.use('Agg')  # to plot w/o X-server
        else:
            mpl.use('TkAgg')
        import matplotlib.pyplot as plt

        if args.verb>0: print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.args=args
        self.figL=[]
        self.outPath=args.outPath+'/'
        assert os.path.exists(self.outPath)
        if 'paper' in self.venue:
            roys_fontset(plt)

    # alternative constructors
    @classmethod #........................
    def experiment(cls, exper):
        #print('cstr:exper-plot')
        args=exper.clargs
        obj=cls([args,exper.getShortName()])
        return obj
  
    @classmethod #........................
    def simple(cls, args,short_name):
        print('cstr:simple-plot')
        obj=cls( [args,short_name])
        return obj
  
    #............................
    def figId2name(self, fid):
        figName='%s_f%d'%(self.jobName,fid)
        return figName

    #............................
    def clear(self):
        self.figL=[]
        self.plt.close('all')
    #............................
    def display_all(self, png=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        
        for fid in self.figL:
            self.plt.figure(fid)
            self.plt.tight_layout()
            figName=self.outPath+self.figId2name(fid)
            if png: figName+='.png'
            else: figName+='.pdf'
            print('Graphics saving to ',figName)
            self.plt.savefig(figName)
        self.plt.show()

# figId=self.smart_append(figId)
#...!...!....................
    def smart_append(self,id): # increment id if re-used
        while id in self.figL: id+=1
        self.figL.append(id)
        return id


# simu-speciffic
#...!...!....................
    def simu_all(self,bigD,metaD,xFrac=1.0, figId=3):
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white',figsize=(16,7))
        nrow,ncol=7,2
        _, axA = self.plt.subplots(nrow,ncol, sharex='col', sharey='row',
                        gridspec_kw={'hspace': 0, 'wspace': 0.04},num=figId)

        if xFrac<1.0:
            xV=bigD['chan_0']
            nxb=int(xV.shape[0]*xFrac)
            x1=xV[nxb,0].real
            x2=xV[nxb*2,0].real
            print('SIm_all: narrow x-range to: [%.2e, %.2e], nxb=%d, xFatc=%.2g'%(x1,x2,nxb,xFrac))
            
        for chan in range(7):
            tv=bigD['chan_%d'%(chan)]
            #print('SM chan',chan,tv.shape,tv.dtype,tv[-3:,0])
            xV=tv[:,0].real
                        
            ax=axA[chan,0]
            ax.plot(xV,tv[:,1].real,'.-',linewidth=0.5,markersize=2.)
            ax.set(ylabel='chan_%d'%(chan))
            if xFrac<1.0: ax.grid()
            
            ax=axA[chan,1]
            ax.plot(xV,tv[:,1].imag,'-',linewidth=0.5)
            if xFrac<1.0: ax.grid()
                        

        # just beautiffications ....
        for ax in axA.flat:
            ax.label_outer()
        tit=metaD['configTask']['name_full']
        ax.text(0.05,0.6,tit,transform=ax.transAxes,color='m')
        
        for i in range(ncol):
            ax=axA[nrow-1,i]
            ax.set_xlabel('time (ns)')
            if xFrac<1.0: ax.set_xlim(x1,x2)
            
        tit2=metaD['configTask']['task_name']
        axA[0,0].set(title='I (in-phase), '+tit2)
        axA[0,1].set(title='Q (out of phase), '+tit2)

        
#...!...!..................
    def rawIQdata(self,rawIQ,metaD,figId=5):
        tc=metaD['configTask']
        dsc=tc['data_shapes']
        #print('Plr:rawIQ',rawIQ.shape)
        [NQ,NM,IQ2]=dsc['rawIQ_dims'][-3:]

        #print('vv',self.venue)
        amplFact=1.e6
        
        flatIQ=rawIQ.reshape(-1,NQ,NM,IQ2)/amplFact  # clones data
        #print('Plr:flatIQ',flatIQ.shape,'NQ,NM,IQ2',NQ,NM,IQ2)       

        inX,inY=15,4*NQ; ncol=3
        isPaper='paper' in self.venue
        if isPaper:
            inX,inY=3.5,3*NQ; ncol=1
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white',figsize=(inX,inY))

        # - - - - -   qubit loop  - - - - - - - -
        for iQid in range(NQ):

            if NM==2:
                heraIQ=flatIQ[:,iQid,0]
                measIQ=flatIQ[:,iQid,1]
                #bothIQ=heraIQ+measIQ WRONG -you want stack arrays instead of adding value
                bothIQ=flatIQ[:,iQid,:].reshape(-1,IQ2)
                #print('Xshapes:',heraIQ.shape,measIQ.shape,bothIQ.shape)
            else:
                heraIQ=None
                measIQ=flatIQ[:,iQid,0]
                bothIQ=measIQ
            cmapL=['Greys','GnBu','YlOrBr']
            dataL=[bothIQ,heraIQ,measIQ]            

            tit1=' %s:  %s'%(dsc['phys_qubit_ids'][iQid],tc['short_name'])
            tit2=' '+tc['name_full']
            titL=[tit2,'herald'+tit1,'measured'+tit1]

            # .........  plotting .......
            nrow,ncol=NQ,ncol
            for i in range(ncol):            
                ax=self.plt.subplot(nrow, ncol, 1+i+iQid*ncol)
                ax.set_aspect(1.0)

                binOpt='log'
                if i!=0: binOpt=zMx/30
                if NM==1 and i==1 :
                    ax.text(0.2,0.2,'no herald data',transform=ax.transAxes)
                    continue  # skip Herald if not measured
                img=ax.hexbin(dataL[i][:,0],dataL[i][:,1],gridsize=30,cmap=cmapL[i],bins=binOpt,mincnt=1)
                 
                [ label.set_rotation(30) for label in ax.get_xticklabels()]


                #................... end of 'paper' style
                if isPaper:
                    ax.set_ylim(-2.500,1.100)
                    ax.set_xlim(-1.600,2.00)
                    ax.set(ylabel='Imag (a.u.)',xlabel='Real (a.u.)')
                    continue
                
                fig.colorbar(img, ax=ax,label='counts')

                ax.set(ylabel='Imag /%.0f'%amplFact,xlabel='Real /%.0f'%amplFact)
                if i==0 :
                    ax0=ax
                    txt2='fix123'#measureTime_to_string(metaD)
                    ax.text(0.55,0.01,txt2,transform=ax.transAxes,color='m')
                else:
                    ax.get_shared_x_axes().join(ax, ax0)
                    ax.get_shared_y_axes().join(ax, ax0)

                ax.set_title(titL[i])
                ax.grid()

                if i==0:
                    hexA=img.get_array()
                    zMx=np.max(hexA)
                    ax.text(0.05,0.01,'zMax=%d'%zMx,transform=ax.transAxes,color='red')

        return
       
