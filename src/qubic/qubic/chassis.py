"""@package chassis
"""
import json
import pkgutil
import numpy
import datetime
import time
import sys
sys.path.append('../../')
from qubic.bids.ether import c_ether
from qubic.bids.regmap import c_regmap
from qubic.qubic.cmdsim1 import c_cmdsim
from qubic.qubic import init
from qubic.qubic import commands
from matplotlib import pyplot
class c_elememory():
    """@class c_elememory
    Class define processing element memory
    """
    def __init__(self,elemindex,regmap,fullscale=19894,debug=0):
        """
        elemindex(int): Index of the element
        regmap(c_regmap): Register map of the chassis

        """
        self.elemindex=elemindex
        self.debug=debug
        self.regname='elementmem_%x'%elemindex
        self.bufsize=2**(regmap.regs[self.regname].addr_width)
        self.memcomplex=numpy.zeros(self.bufsize,dtype='complex64')
        self.fullscale=fullscale
        self.resetbuf()
    def debuginfo(self,level,*args,**kwargs):
        if self.debug>=level:
            print(*args,**kwargs)
    def resetbuf(self):
        self.lastpoint=0
        self.pulses=[]
        self.qubits=[]
        self.basepulses=[]
    def combinereim(self):
        memscale=self.fullscale*self.memcomplex
        memreim32re=memscale.real.round(10).astype(int)
        memreim32im=memscale.imag.round(10).astype(int)
#        self.memreim32=numpy.ones(len(self.memcomplex))*(1000*2**16+1000)#
        self.memreim32=memreim32re*2**16+memreim32im
        if self.lastpoint>0 and 0:
            pyplot.subplot(131)
            pyplot.plot(self.memcomplex.real)
            pyplot.plot(self.memcomplex.imag)
            pyplot.subplot(132)
            pyplot.plot(memscale.real)
            pyplot.plot(memscale.imag)
            pyplot.subplot(133)
            pyplot.plot(memreim32re)
            pyplot.plot(memreim32im)
            pyplot.show()


    def insertpulse(self,pulse,overlapcheck=True):
        success=False
        havespace=(self.lastpoint+pulse.nlength)<=self.bufsize
        self.debuginfo(4,'havespace',self.elemindex,havespace,'bufsize',self.bufsize,'lastpoint',self.lastpoint,'nlength',pulse.nlength,pulse.gate.name)
        if havespace:
            for p2 in [pp for pp in pulse.circpulse if pp.start is None]:
                overlap=False
                if overlapcheck:
                    for p1 in self.pulses:
                        if overlap:
                            pass
                        else:
                            self.debuginfo(4,'p1,2.circuit',p1.gate.circuit,p2.gate.circuit,'tstart',p1.tstart,p2.tstart,'tend',p1.tend,p2.tend)
                            overlap=p1.gate.circuit==p2.gate.circuit and ((p1.tstart<=p2.tstart and p1.tend>p2.tstart) or (p2.tstart<=p1.tstart and p2.tend>p1.tstart))
                            #overlap=(p1.trig_t<=p2.trig_t and p1.tend>p2.trig_t) or (p2.trig_t<=p1.trig_t and p2.tend>p1.trig_t)
                        self.debuginfo(4,'debug overlap',overlap,'p1',(p1.tstart,p1.tend),'p2',(p2.tstart,p2.tend),'gate=',p1.gate.name,p2.gate.name,p1.gate.circuit==p2.gate.circuit,overlap,'pulsein',pulse in self.basepulses)
                if not overlap:
                    if pulse not in self.basepulses:
                        self.memcomplex[self.lastpoint:self.lastpoint+pulse.nlength]=pulse.nv
                        self.basepulses.append(pulse)
                    if self.elemindex not in pulse.start:
                        pulse.start[self.elemindex]=self.lastpoint
                        self.lastpoint+=pulse.nlength
                        self.debuginfo(4,'increaselastpoint',self.elemindex,self.lastpoint,pulse.gate.name)
                    p2.start=pulse.start[self.elemindex]
                    p2.length=pulse.nlength
                    self.debuginfo(4,'insertpulse success',p2,p2.gate.name,p2.t0,p2.tstart,p2.tend)
                    p2.element=self.elemindex
                    self.pulses.append(p2)
        success=havespace and all([p.start is not None for p in pulse.circpulse])
#        if success:
#            print('debug insertpulse',pulse.nv[0:32])
#            print('debug self.memcomplex',self.memcomplex[0:32])
        return success

class c_chassis():
    """@class c_chassis
    Class define chassis
    """
    def __init__(self,ip,port=3000,regmappath=None,wavegrppath=None,fsample=1e9,sim=False,sqinit=True,debug=False):
        """
        Args:
        ip(str): ip address
        ,port=3000: port for the udp connection
        ,regmappath=None
        ,wavegrppath=None
        ,fsample=1e9
        ,sim=False
        ,sqinit=True
        ,debug=False
        """
        self.ip=ip
        self.fsample=fsample
        self.dt=1.0/fsample
        self.fpgadacclkratio=4
        self.sim=sim
        self.debug=debug
        if __name__ is not None and "." in __name__:
            pkg=__package__
        else:
            pkg='chassis'
        if regmappath:
            with open(regmappath) as jfile:
                registers=json.load(jfile)
        else:
            registers=json.loads(pkgutil.get_data(pkg,'regmap.json'))
        if wavegrppath:
            with open(wavegrppath) as jfile:
                wavegrp=json.load(jfile)
        else:
            wavegrp=json.loads(pkgutil.get_data(pkg,'wavegrp.json'))
        self.regmap=c_regmap(c_ether(ip, port), min3=True,memgatewaybug=False,registers=registers,wavegrp=wavegrp)
        self.ndws=[]
        self.cmdlength=2**self.regmap.regs['command'].addr_width
        self.MEMFS=19894#int(numpy.floor(32767/1.67))
        MEMFSRI=19894#int(numpy.floor(32767/1.67/numpy.sqrt(2)))
        self.fsmem={'elementmem_%x'%index:numpy.ones((2**self.regmap.regs['elementmem_%x'%index].addr_width))*(((MEMFSRI)<<16)+(MEMFSRI)) for index in range(12)}
#        self.dacelemindex=range(8)
#        self.dloelemindex=[8,9,10]
        self.dacelem={r:c_elememory(r,self.regmap,self.MEMFS,debug=self.debug) for r in range(8)}
        self.debuginfo(4,'dacelem init',self.dacelem)
        self.dloelem={r:c_elememory(r,self.regmap,self.MEMFS,debug=self.debug) for r in [8,9,10]}
        self.elements=list(self.dacelem.values())+list(self.dloelem.values())
        self.elem2bufreadregname={8:'accout_0',9:'accout_1',10:'accout_2'}
        self.bufreadregname2elem={v:k for k,v in self.elem2bufreadregname.items()}
#        self.bufreadregname2row={'accout_0':0,'accout_1':1,'accout_2':2}
#        self.bufreadrow2regname={v:k for k,v in self.bufreadregname2row.items()}
        self.readbufsize=2**self.regmap.regs['accout_0'].addr_width
        self.readbufsizecomplex=2**(self.regmap.regs['accout_0'].addr_width-1)
        if sqinit:
            self.debuginfo(4,'chassis init')
            self.write(getattr(init,'sqinit'))
        self.elembufsize=2**self.regmap.regs['elementmem_0'].addr_width
        self.acqbufregs=['buf_monout_0','buf_monout_1']
    def debuginfo(self,level,*args,**kwargs):
        if self.debug>=level:
            print(*args,**kwargs)
    def membufreset(self):
        for elem in self.elements:
            elem.resetbuf()
    def memwrite(self,clear=True):
        """
        Write element memory
        clear=True: clear memory by write all 0 before write the new memory
        """
        #        index=0
        for elem in self.elements:
            elem.combinereim()
            if elem.lastpoint>0 or clear:
                self.write(((elem.regname,elem.memreim32[0:elem.bufsize if clear else elem.lastpoint]),))
            numpy.set_printoptions(threshold=sys.maxsize,linewidth=200)
            self.debuginfo(4,elem.regname,elem.memreim32[0:elem.bufsize if clear else elem.lastpoint])
    def cmdwrite(self,cmds,clear=True,write=True):
        """
        Process and Write command buffer
        Args:
        cmds(list):  Command to be written
        clear=True: Clear command buffer by write  all 0 before write  the new commands
        write=True: Really write to the UDP port. If False, only generate simulation commands
        """
        #        if clear:
        #self.write((('command',self.zerocmd),))
        cmdlist=[]
        cmdsall=numpy.zeros(self.cmdlength,dtype=object)
        cmdlength=len(cmds)
        for index,cmd in enumerate(cmds):
            if abs(cmd['freq'])>self.fsample/2:
                self.debuginfo(1,"Warning: pulse carrier frequency %8.3f exceed chassis DAC 1st Nyquist "%(cmd['freq']))
            cmd128=commands.cmdgen128(**cmd,debug=self.debug)
            cmdsall[index]=cmd128
#                cmdlist.append()
        if write or 1:
            self.write((('command',cmdsall[0:(self.cmdlength if clear else cmdlength)]),))
        return cmdsall[0:cmdlength]
    def accbuf(self,readregnames,nbuf,npreread=0):
        """
        Read accumulated buffer

        Args:
        readregnames(str): The register name  of  the buffer to be read
        nbuf: number  of buffer  to be  read
        npreread: Read npreread buffer before read the value, tpyically used if  the first one or two buffer is not cleared
        """
        buf={}
        buf={n:numpy.zeros((nbuf,self.readbufsizecomplex)).astype(complex) for n in readregnames}
        for npreread in range(npreread):
            self.startacq()
            self.read(readregnames,resetafter=False)
        for nget in range(nbuf):
            if (nget%10)==0:
                self.debuginfo(3,'reading acc buf: %3d of %3d'%(nget+1,nbuf))
            self.read(readregnames,resetafter=False)
            for readregname in readregnames:
                val=numpy.array(self.regmap.getregval(readregname))
                valcomplex=val[0::2]+1j*val[1::2]
                buf[readregname][nget,:]=valcomplex
                #pyplot.plot(valcomplex.real,valcomplex.imag)
                #pyplot.show()
        return buf

    def accbufdata(self,readelemcmdcnt,nsample,square=True,npreread=0):
        """
        Read accumulated buffer

        Args:
        readelemcmdcnt(dict): Read element  command count, contain the number of read for each read process element
        nsample(int): Total number of sample or shot for each read command to repeat, this is used to collect  enough data  to average
        square=True: If True, only take the nsample from the result to return  and dispose the others as trash. If False, return as much data as possible  from the buffer read
        nprered=0: parameer pass to accbuf
        """
        readelemcmdcntdict={self.elem2bufreadregname[e]:readelemcmdcnt[e] for e in self.dloelem if e in readelemcmdcnt}
        readelemcmdcntlist=numpy.array(list(readelemcmdcntdict.values()))
        self.debuginfo(4,'debug accbufdata',readelemcmdcnt,readelemcmdcntlist,readelemcmdcntdict)
        if len(readelemcmdcntlist)>0:
            if not numpy.all((readelemcmdcntlist==readelemcmdcntlist[0])):
                self.debuginfo(2,'Warning: not all qubits read same length',readelemcmdcntdict,'will take more time to fill the buffers')
        else:
            self.debuginfo(3,'Warning: No read in circuit?')
        trash={w:(self.readbufsizecomplex%(readelemcmdcnt[self.bufreadregname2elem[w]])) for w in readelemcmdcntdict}
        sampleperbuf={w:(self.readbufsizecomplex//(readelemcmdcnt[self.bufreadregname2elem[w]])) for w in readelemcmdcntdict}
        sampleperbufvalue=[i for i in sampleperbuf.values()]
        bufdict={}
        if len(sampleperbufvalue)>0:
            minsampleperbuf=min(sampleperbufvalue)
            nbuf=int(numpy.ceil(nsample/minsampleperbuf))
            sampletotal={n:nbuf*sampleperbuf[n] for n in readelemcmdcntdict.keys()}
            buf=self.accbuf(readelemcmdcntdict.keys(),nbuf=nbuf,npreread=npreread)
            self.debuginfo(4,'debug accbufdata',buf)
            for name in readelemcmdcntdict:
                trashvalue=trash[name]
                self.debuginfo(4,readelemcmdcntdict[name])
                self.debuginfo(4,list(buf.keys()))
                self.debuginfo(4,[a.shape for a in buf.values()])
                buf0=buf[name][:,0:((readelemcmdcntdict[name]*minsampleperbuf) if square else (-trashvalue) if trashvalue>0 else self.readbufsizecomplex)]
                buf1=buf0.reshape((-1,readelemcmdcntdict[name]))
                if square:
                    self.debuginfo(4,'before square buf shape:',buf1.shape,'trash',trashvalue)
                    buf1=buf1[0:nsample,:]
                    self.debuginfo(4,'after  square buf shape:',buf1.shape)
                bufdict[name]=buf1
        self.debuginfo(4,'Acquire %d accumulated out buffers, and get %s samples'%(nbuf,str(sampletotal)))
        self.debuginfo(4,'trash',trash)
        bufelem={self.bufreadregname2elem[k]:v for k,v in bufdict.items()}

        return bufelem


    def setperiod(self,period):
        """
        Set the period for the command buffer to restart

        Args:
        period(float): if period<1, consider this as time  in second, else consider as total number of FPGA clock cycle
        """
        if period<1: # take a while to period on one second, so must be second
            period=int(round(period/self.dt/4))
        self.write((('period_adc',period),('period_dac0',period),('period_stream',period)))
        self.debuginfo(4,'setperiod %d FPGA clock cycle'%period)
    def startacq(self):
        """
        Inform gateware to start acquire by writing to the start register
        """
        self.write((('start',0),('start',1),('start',0)))
    def write(self,namedatalist):
        """
        Call BIDS  write to write register map, if simulation,  this is  also  used to collect write commands

        Args:
        namedatalist(list): a list of register  name and its value to be write to the  FPGA
        """
        if self.sim:
            self.ndws.extend([(n,d,1) for (n,d) in namedatalist])
        self.debuginfo(4,'debug write',[(n,len(d) if isinstance(d,numpy.ndarray) else d) for n,d in namedatalist])
        return self.regmap.write(namedatalist)
    def read(self,names,offsets=None,resetafter=True):
        """
        Call BIDS  read to write register map, if simulation,  this is  also  used to collect write commands

        Args:
        names(list): List of  names to be read
        """
        if self.sim:
            self.ndws.extend([(n,0,0) for (n) in names])
        return self.regmap.read(names,offsets,resetafter=resetafter)
    def signvalue(self,vin,width=16):
        """
        Convert  the  value to a signed  value

        Args:
        vin(float) : The value to be convert
        width(int): The width of the register,  used to determin which is the sign bit
        """
        return vin-2**width if vin>>(width-1) else vin
    def adcminmax(self):
        """
        Return the adc minimal and maximum value after last read.  Used to determin the ADC value overflow
        """
        bufs=['adc0_minmax','adc1_minmax']
        self.read(bufs)
        return ([(self.signvalue(int(i[0])>>16),self.signvalue(int(i[0])&0xffff)) for i in [self.regmap.getregval(r) for r in bufs]])

    def setacqbuf(self,delay=0.2,**regval):#opsel=0,mon_navr=0,mon_dt=1,mon_slice=0,mon_sel0=0,mon_sel1=0,panzoom_reset=1):
        acqbufsettingregs=dict(opsel=2
,mon_navr=0
,mon_dt=1
,mon_slice=0
,mon_sel0=0
,mon_sel1=2
,panzoom_reset=1
)
        """
        Setup parameters  for the acquire buffer
        # opsel 0 for navg; 1 for min; 2 for max
        # mon_sel:
#           0 : xmeasin_d2,
#           1 : ymeasin_d2,
#           2 : xlo_w[0],
#           3 : ylo_w[0],
#           4 : xlo_w[1],
#           5 : ylo_w[1],
#           6 : xlo_w[2],
#           7 : ylo_w[2],
#           8 : dac0_in,
#           9 : dac1_in,
#           10 : dac2_in,
#           11 : dac3_in,
#           12 : dac4_in,
#           13 : dac5_in,
#           14 : dac6_in,
#           15 : dac7_in,
#       print 'opsel,mon_navr,mon_dt,mon_slice,mon_sel0,mon_sel1'
#       print opsel,mon_navr,mon_dt,mon_slice,mon_sel0,mon_sel1

        """
#        opts.update({k:v for k,v in regval if k in })
        wregs=[(k,v) for k,v in regval.items() if k in acqbufsettingregs]
        if wregs:
            wregs.extend([('panzoom_reset',1)])
            self.debuginfo(4,'debug set acqbuf',wregs)
            self.write(wregs)
#                (
#            ('opsel',opsel)
#            ,('mon_navr',mon_navr)
#            ,('mon_dt',mon_dt)
#            ,('mon_slice',mon_slice)
#            ,('mon_sel0',mon_sel0)
#            ,('mon_sel1',mon_sel1)
#            ,('panzoom_reset',panzoom_reset)
#            ))
        if delay:
            time.sleep(delay)
        tstep=self.dt*2**acqbufsettingregs['mon_navr']
        toffset=acqbufsettingregs['mon_dt']*self.dt
        return tstep,toffset

    def dspreset(self):    
        """
        Write  the FPGA  dsp_reset register
        """
        wregs=[('dsp_reset',0),('dsp_reset',1),('dsp_reset',0)]
        self.write(wregs)
    def acqbufdata(self):
        """
        Read acquire buffer data
        """
        self.read(self.acqbufregs,resetafter=False)
        return {r:numpy.array(self.regmap.getregval(r)).reshape((-1,1)) for r in self.acqbufregs}
    def sim1(self,mod=False,tend=None,xlim=None,**kwargs):
        """
        Run pulse command simulation to generate  plot of pulse  sent  to  each DAC channel

        Args:
        mod=False: If True, the carrier frequency is multiplied 
        tend=None: plot horizontal  axis  limit, None for  all data set
        xlim=None: Used  to set  the x lim of the plot 
        """
        simmodel=c_cmdsim(regmappath=self.regmap.registers,ndws=self.ndws,mod=mod,debug=self.debug)
        simmodel.ploteles(tend,xlim=xlim)
    def acqbufrun(self,nbuf,cmdlists,statusupdate=10,bypass=False,**kwargs):
        """
        Read accquire buffer  for n buffers

        Args:
        nbuf(int): number  of buffer to be read, notice not number of sample/shots, each  buffer contain 1024 points
        cmdlist(list): list of  command  tob e written
        statusupdate(int): Generate a confort update  every n buffers
        kwargs:  Additional keyword parameters pass down to the self.setacqbuf
        """
        ops={}
        ops.update(kwargs)
        if bypass:
            pass
        else:
            self.memwrite(clear=True)
            for index,(cmdlist,period) in enumerate(cmdlists):
                self.cmdwrite(cmdlist,clear=True,write=True)
                self.setperiod(period)
                self.dspreset()
                if index>=1:
                    exit("Warning: Are you sure to take the acqbuf for all different circuits? The results might be confusing")
        tstep,toffset=self.setacqbuf(delay=0.0,**kwargs)

        t0=datetime.datetime.now()
        acqbuf=numpy.zeros((nbuf,1024,2))
        for i in range(nbuf):
            if (i%statusupdate)==statusupdate-1:
                self.debuginfo(4,'reading acc buf: %3d of %3d'%(i+1,nbuf))
            buf=self.acqbufdata()#opsel=2,mon_sel0=int(sys.argv[1]),mon_sel1=int(sys.argv[2]))
            for ibuf,bufname in enumerate(self.acqbufregs):
                acqbuf[i,:,ibuf]=buf[self.acqbufregs[ibuf]][:,0]
        self.t=toffset+tstep*numpy.arange(len(acqbuf[0]))*self.fpgadacclkratio
        t1=datetime.datetime.now()
        self.debuginfo(4,'read %d acqbuf'%nbuf,t1-t0)
        return acqbuf

    def writeonlyrun(self,cmdlists):
        self.memwrite(clear=True)
        if len(cmdlists)==1:
            print(cmdlists)
            cmdlist,period =cmdlists[0]
            self.cmdwrite(cmdlist,clear=True,write=True)
            self.setperiod(period)
        else:
            exit('writeonlyrun only take one cmdlist, got %d'%len(cmdlists))

    def accbufrun(self,cmdlists,nsample,**kwargs):
        """
        Read  accumulated buffer

        Args:
        cmdlist(list): list of  command  tob e written
        nsample(int): number  of sample to be read, notice not number of buffers, each  buffer contain serveral points
        kwargs:  Additional keyword parameters pass down to the self.setacqbuf

        """
        ops=dict(square=True,npreread=2,tend=None,xlim=None)
        ops.update(kwargs)
        self.memwrite(clear=True)

        accval={}#numpy.empty((nsample,0))
        totalelemcmdcnt={}
        numpy.set_printoptions(threshold=sys.maxsize,linewidth=200)
        cmdlistslength=len(cmdlists)
        for index,(cmdlist,period) in enumerate(cmdlists):
            elemcmdcnt,readelemcnt=commands.cmdelemcnt(cmdlist,self.dloelem)
            totalelemcmdcnt={k:elemcmdcnt[k]+(totalelemcmdcnt[k] if k in totalelemcmdcnt else 0) for k in elemcmdcnt}
            self.cmdwrite(cmdlist,clear=True,write=True)
            self.debuginfo(3,'Run commands %d of %d pages, setperiod %8.3e second'%(index+1,cmdlistslength,period))
            self.setperiod(period)
            self.dspreset()
            #self.startacq()
            self.debuginfo(4,'elemcmdcnt',elemcmdcnt)
            if self.sim:
                self.debuginfo(4,'simulating')
                self.sim1(mod=True,**ops)#tend=period if ops['tend'] is None else ops['tend'],**kwargs)
            self.debuginfo(4,'debug newval',readelemcnt,nsample,ops['square'],ops['npreread'])
            newval=self.accbufdata(readelemcnt,nsample,square=ops['square'],npreread=ops['npreread'])
            self.debuginfo(4,'newval',newval)
            for v in newval:
                if v not in accval:
                    accval[v]=newval[v]
                else:
                    accval[v]=numpy.append(accval[v],newval[v],1)
        readmaprev={}
        for rid in accval.keys():
            qread=self.elements[rid].qubits
            if len(qread)==1:
                readmaprev[rid]=qread[0]
            else:
                self.debuginfo(4,'element',rid,'has more than one qubits',qread)
        self.debuginfo(4,'debug readmaprev',readmaprev)
        #print('accval keys',list(accval.keys()))
        #print('readmaprev keys',list(readmaprev.keys()))
        accval={readmaprev[k]:v for k,v in accval.items()}
        return accval

if __name__=="__main__":
    c=c_chassis(ip="192.168.1.124")
    #c.setperiod(80e-3)
    print(c.adcminmax())
    a=c.acqbufrun(nbuf=1,mon_sel0=int(sys.argv[1]),mon_sel1=int(sys.argv[2]),opsel=2,mon_navr=0,mon_dt=1,mon_slice=0,panzoom_reset=1,cmdlists=[])
#    ntrace=10000
#    c.setacqbuf
#    #def setacqbuf(self,delay=0.0,**regval):#opsel=0,mon_navr=0,mon_dt=1,mon_slice=0,mon_sel0=0,mon_sel1=0,panzoom_reset=1):
#    a=numpy.zeros((ntrace,1024,2))
#    for i in range(ntrace):
#        acqbuf=c.acqbuf()#opsel=2,mon_sel0=int(sys.argv[1]),mon_sel1=int(sys.argv[2]))
#        a[i,:,0:1]=acqbuf[c.acqbufregs[0]]
#        a[i,:,1:2]=acqbuf[c.acqbufregs[1]]
    amean2=a.mean(axis=0)
    print(amean2.shape)
    pyplot.subplot(211)
    pyplot.plot(amean2[:,0])
    pyplot.subplot(212)
    pyplot.plot(amean2[:,1])
    pyplot.show()
#    print(a[0:3,0:4,0:2])
#    print(acqbuf['buf_monout_0'])
#    for i in range(10):
#        pyplot.plot(a[i,:,0],a[i,:,1])
#        pyplot.show()

#        def acqmonout(self,avrfactor,opsel,mon_navr,mon_dt,mon_sel0,mon_sel1):
#        for avr in range(avrfactor):
#            print('%d of %d'%(avr,avrfactor))
#            for mon_slice in range(4):
#                [monout_1,monout_0]=self.hf.buf_monout(opsel=opsel,mon_navr=mon_navr,mon_dt=int(mon_dt/1.0e-9/4),mon_slice=mon_slice,mon_sel0=mon_sel0,mon_sel1=mon_sel1,panzoom_reset=1,delay=0.3)
#                mon40=monout_0 if mon_slice==0 else numpy.append(mon40,monout_0,1)
#                mon41=monout_1 if mon_slice==0 else numpy.append(mon41,monout_1,1)
#            mon40=mon40.reshape([-1,1])
#            mon41=mon41.reshape([-1,1])
#            mon40sum=mon40 if avr==0 else mon40sum+mon40
#            mon41sum=mon41 if avr==0 else mon41sum+mon41
#        buf0avr=1.0*mon40sum/avrfactor
#        buf1avr=1.0*mon41sum/avrfactor
#        t=numpy.arange(len(buf1avr))*2**mon_navr*1.0e-9+mon_dt
#        return [t,buf0avr,buf1avr]
#
