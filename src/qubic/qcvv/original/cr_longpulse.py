import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip

class c_cr(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def crseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,tcr=32e-9,qubitid_c='Q5',qubitid_t='Q4',pcr=0,txgate=0e-9,qubitidread=['Q5','Q4','Q3'],fixed_ramp=False):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modcr={}
		modxgate={}
		run=0
		for ctrl in ['I','X180']:
			for meas in ['Y-90','X90','I']:
				# Readout
				therald=run
				self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])

				# Control state preparation (control qubit: I or X180)
				tini=run+delay1
				if ctrl=='X180':
					self.seqs.add(tini,         	 self.qchip.gates[qubitid_c+'X180'])
					tcrstart=self.seqs.tend()
				else:
					tcrstart=tini
				ti180=tcrstart-tini

				# CR gate (drive the control qubit at the frequency of the target frequency)
				if fixed_ramp:
					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=ti180,padd=pcr)[0]))
				else:
					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=ti180,padd=pcr)[0],env=[{'env_func': 'cos_edge_square', 'paradict': {'ramp_fraction': 0.25,'ramp_length':None}}]))
				if tcr>1e-14:
					self.seqs.add(tcrstart,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))

				# CNOT X gate (target qubit)
				if txgate>1e-14:
					modxgate.update(dict(twidth=txgate,pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=ti180+tcr)[0]))
					self.seqs.add(self.seqs.tend(),self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))

				# Projection (target qubit: Y-90, X90 or I)
				if meas=='I':
					pmeas=0
				else:
					pmeas=self.qchip.gates[qubitid_t+meas].pcalc(dt=ti180+tcr+txgate)[0]
					self.seqs.add(self.seqs.tend(),self.qchip.gates[qubitid_t+meas].modify(dict(pcarrier=pmeas)))

				# Readout
				treaddrv=self.seqs.tend()
				self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

				run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		#self.bufwidth=[self.seqs.countdest('Q5.read'),self.seqs.countdest('Q4.read'),self.seqs.countdest('Q3.read')]
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processcr(self,dt,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		#cr_result=self.process3(data,lengthperrow=max(self.bufwidth),training=False,loadname=loadname)
		cr_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,cr_result['separation'],cr_result['iqafterherald'],cr_result['population_norm']]

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	#tcrlist=numpy.arange(0,2048,40)*1e-9
	#tcrlist=numpy.arange(0,700,48)*1e-9
	#tcrlist=numpy.arange(500,700,8)*1e-9
	#tcrlist=numpy.arange(0,480,8)*1e-9
	#tcrlist=numpy.arange(140,240,2)*1e-9
	tcrlist=numpy.arange(250,350,2)*1e-9
	#tcrlist=numpy.arange(1248,2000,16)*1e-9
	cr=c_cr()
	parser,cmdlinestr=cr.cmdoptions()
	clargs=parser.parse_args()
	pnts0_traj=[]
	pnts1_traj=[]
	pcr=3.925
	txgate=0*32e-9
	for tcr in tcrlist:
		print('tcr',tcr)
		cr.initseqs()
		if clargs.sim:
			cr.setsim()
		cr.crseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,tcr=tcr,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,pcr=pcr,txgate=txgate,qubitidread=clargs.qubitidread,fixed_ramp=clargs.fixed_ramp)
		cr.run()
		data=cr.rabiacq(clargs.nget)
		fprocess=cr.savejsondata(filename=clargs.filename,extype='cr',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			cr.sim()
		#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=cr.processrabi(dt=clargs.elementstep,filename=fprocess,dumpname=fprocess[:-4],plot=clargs.plot)
		[rawdata,separation,iqafterherald,population_norm]=cr.processcr(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
		#pnt_traj=1-2*population_norm
		pnt_traj=population_norm
		pnts0_traj.append(pnt_traj[0:3])
		pnts1_traj.append(pnt_traj[3:6])
		result=[tcr]
		result.extend(list(pnt_traj.reshape((1,-1))[0]))
		print(result)
		with open(clargs.filename+'_cross_resonance_long'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.dat','a') as f:
			numpy.savetxt(f,numpy.array(result).reshape((1,-1)))
		#print 'period, separation, amp',period,separation,amp
		#cr.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
		#cr.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
		if clargs.plot:
			pyplot.grid()
			pyplot.show()
#		print cr.hf.adcminmax()
	crdata=numpy.loadtxt(clargs.filename+'_cross_resonance_long'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.dat')
	if clargs.readcorr:
		for ii in range(6):
			population_norm=crdata[:,ii+1].T
			print('population_norm before correction',population_norm)
			population_norm=cr.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
			print('population_norm after correction',population_norm)
			pnt_traj=1-2*population_norm
			crdata[:,ii+1]=pnt_traj.T
	for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
		pyplot.figure()
		pyplot.plot(crdata[:,0]*1e9,crdata[:,index+1],label='Control |0>',color='b')
		pyplot.plot(crdata[:,0]*1e9,crdata[:,index+4],label='Control |1>',color='r')
		pyplot.legend()
		pyplot.xlabel('Pulse Length (ns)')
		pyplot.ylabel(target_projection)
		#pyplot.ylim(-1,1)
		pyplot.ylim(-1.1,1.1)
		pyplot.savefig(clargs.filename+'_cross_resonance_long_'+str(index)+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')
	distance=numpy.sqrt((crdata[:,1]-crdata[:,4])**2+(crdata[:,2]-crdata[:,5])**2+(crdata[:,3]-crdata[:,6])**2)/2.0
	pyplot.figure()
	pyplot.plot(crdata[:,0]*1e9,distance)
	pyplot.xlabel('Pulse Length (ns)')
	pyplot.ylabel(r'$|\vec{R}|$')
	#pyplot.ylim(0,1)
	pyplot.ylim(-0.05,1.05)
	pyplot.savefig(clargs.filename+'_cross_resonance_long_R'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')
	print('distance',distance)
	bloch=qutip.Bloch()
	#bloch.add_points(numpy.array(pnts0_traj).T)
	#bloch.add_points(numpy.array(pnts1_traj).T)
	bloch.add_points(crdata[:,1:4].T)
	bloch.add_points(crdata[:,4:7].T)
	#bloch.show()
	bloch.save(clargs.filename+'_cross_resonance_long_T'+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp+'.png')
	#pyplot.show()

# python cr_longpulse.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr
# python cr_longpulse.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr --fixed_ramp
