import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip

class c_crZIphase(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def crZIphaseseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,tcr=32e-9,qubitid_c='Q5',qubitid_t='Q4',pcr=0,txgate=0e-9,qubitidread=['Q5','Q4','Q3'],qubitdrvamp=None):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.qubitid_list=[qubitid_c,qubitid_t]
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modcr={}
		modxgate={}
		modYn90_c={}
		modYn90_t={}
		if qubitdrvamp is not None:
			modcr.update(dict(amp=qubitdrvamp))
		run=0
		for control_phase in numpy.arange(0,numpy.pi*2,numpy.pi*2/elementlength):
			# Readout
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delay1
			tini=run

			# Control Y90
			self.seqs.add(run,self.qchip.gates[qubitid_c+'Y90'])
			run=self.seqs.tend()

			# CR gate (drive the control qubit at the frequency of the target frequency)
			modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0]))
			self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
			run=self.seqs.tend()

			# CNOT X gate (target qubit)
			if txgate>1e-14:
				modxgate.update(dict(twidth=txgate,pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=run-tini)[0]))
				self.seqs.add(run,self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))
			run=self.seqs.tend()

			# Control Y-90
			modYn90_c.update(dict(pcarrier=self.qchip.gates[qubitid_c+'Y-90'].pcalc(dt=run-tini,padd=control_phase)[0]))
			self.seqs.add(run,self.qchip.gates[qubitid_c+'Y-90'].modify(modYn90_c))
			run=self.seqs.tend()

			# Target Y-90
			modYn90_t.update(dict(pcarrier=self.qchip.gates[qubitid_t+'Y-90'].pcalc(dt=run-tini)[0]))
			self.seqs.add(run,self.qchip.gates[qubitid_t+'Y-90'].modify(modYn90_t))
			run=self.seqs.tend()

			# Readout
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_c+'read'])
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delaybetweenelement

		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]	
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2'] # Should be imported from a JSON file with the mapping between qubitidread and accout in the future
		self.accout_dict=dict(zip(qubitidread,accout_list))
	def processcrZIphase(self,filename,loaddataset_list=None):
		c=self.loadjsondata(filename)
		print('self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict)
		print('accout in use: ',self.accout_dict[self.qubitid_list[0]],self.accout_dict[self.qubitid_list[1]])
		data_list=[c[self.accout_dict[self.qubitid_list[0]]],c[self.accout_dict[self.qubitid_list[1]]]]
		crZIphase_result=self.process2q(data_list=data_list,qubitid_list=self.qubitid_list,lengthperrow_list=[self.bufwidth_dict[qid] for qid in self.qubitid_list],loaddataset_list=loaddataset_list)
		return [crZIphase_result['meas_binned'],crZIphase_result['meas_sum']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	crZIphase=c_crZIphase()
	parser,cmdlinestr=crZIphase.cmdoptions()
	parser.add_argument('--tcr',help='pulse length for CR gate',dest='tcr',type=float,default=192e-9)
	parser.add_argument('--pcr',help='starting phase for CR gate',dest='pcr',type=float,default=3.925)
	parser.add_argument('--txgate',help='X rotation for CNOT gate',dest='txgate',type=float,default=0*32e-9)
	clargs=parser.parse_args()
	if clargs.sim:
		crZIphase.setsim()
	#crZIphase.initseqs()
	crZIphase.crZIphaseseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,tcr=clargs.tcr,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,pcr=clargs.pcr,txgate=clargs.txgate,qubitidread=clargs.qubitidread,qubitdrvamp=clargs.qubitdrvamp)
	crZIphase.run()
	data=crZIphase.acqdata(clargs.nget)
	fprocess=crZIphase.savejsondata(filename=clargs.filename,extype='crZIphase',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	if clargs.sim:
		crZIphase.sim()
	[meas_binned,meas_sum]=crZIphase.processcrZIphase(filename=fprocess,loaddataset_list=None)
	print('meas_binned',meas_binned)
	print('meas_sum',meas_sum)
	#outcomes=[str(bin(i))[2:].zfill(len(rb2q.qubitid_list)) for i in range(2**len(rb2q.qubitid_list))]
	#trueq_result=[{o:int(count) for o,count in zip(outcomes,meas_sum[:,col])} for col in range(meas_sum.shape[1])]
	#if clargs.readcorr:
	#	population_norm=crZIphase.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
	#	print('corrected population_norm',population_norm)
	#crZIphase.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	print(crZIphase.hf.adcminmax())
	for irow in range(meas_binned.shape[0]):
		pyplot.plot(numpy.arange(0,numpy.pi*2,numpy.pi*2/clargs.elementlength),meas_binned[irow])
	pyplot.xlabel('Control Qubit Phase (rad)')
	pyplot.ylabel('Population')
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
# python crZIphase.py --plot -n 100 --qubitid_c Q6 --qubitid_t Q5 --tcr 192e-9 --pcr 3.925
