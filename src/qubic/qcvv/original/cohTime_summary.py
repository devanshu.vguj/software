#!/usr/bin/env python3
'''
 generates   coherence-time plots 

'''
import numpy

import math
import sys
import os, time
from datetime import datetime
import argparse

#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-X", "--noXterm", dest='noXterm', action='store_true',
                        default=False, help="disable X-term for batch mode")
    parser.add_argument('-s',"--startDate",  default='20210610_173640',
                        help=" input file statics_[startDate].dat   ")

    parser.add_argument("-o", "--outPath", default='out/',help="plots and tables saved at outPath/[startDate]/")

    args = parser.parse_args()
    args.outPath += '/'
    for arg in vars(args):
        print('myArg:', arg, getattr(args, arg))
    #assert  os.path.exists(args.dataName) 
    assert  os.path.exists(args.outPath)

    # create final output dir
    args.outPath+=args.startDate+'/'
    if not os.path.exists(args.outPath):
        os.mkdir(args.outPath)
        print('M: created',args.outPath)
    os.path.exists(args.outPath)
    
    import matplotlib as mpl
    if args.noXterm:
        mpl.use('Agg')  # to plot w/o X-server
        print('Graphics disabled')
    else:
        mpl.use('TkAgg') #  will pop-up canvas on terminal
        print('Graphics started')

    return args

#...!...!..................
def build_index(htmlF,modTime):
    fd=open(htmlF,'w')
    
    # datetime object containing current date and time
    now = datetime.now()

    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    fd.write('Coherence plots updated on  %s <br>\n'%dt_string)

    fd.write('Monitor last update %s <br>\n'%modTime)
    fd.write('Monitor started on %s <br>\n'%args.startDate)

    fd.write('<hr> T1 time trends \n')
    for x in [1,16]:  # T1
        fd.write(' <p> <img src="staticsplot%d.png" /></p>\n'%(x))

    fd.write('<hr> T2-ramsey time trends \n')
    for x in [3,15,18,22]:  
        fd.write(' <p> <img src="staticsplot%d.png" /></p>\n'%(x))

    fd.write('<hr><p> T2-spinecho time trends </p>\n')
    for x in [5,20]:  
        fd.write(' <p> <img src="staticsplot%d.png" /></p>\n'%(x))


    fd.write('<hr> per-qubit distributions \n')

    fd.write('<table> \n')    
    for j in range(8):
        if j%2==0: fd.write('<tr> \n')
        x=7+j
        fd.write(' <td><img src="staticsplot%d.png" /></td>\n'%(x))

    fd.write('</table> \n')
    
    #for x in [1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17]:
    #for x in [18,20,22]:
    #    fd.write(' <p>%d <img src="staticsplot%d.png" /></p>\n'%(x,x))

    

    fd.close()
    
# =================================
# =================================
#  M A I N
# =================================
# =================================
if __name__=="__main__":

    args=get_parser()
    from matplotlib import pyplot,dates  # must be called after mpl.use(.)
    t1=time.time()
    
    # d: [qid,t1fit,t2star,t2e,fiterrt1meas[1],fiterrramsey[1],fiterrspinecho[1],foffsetramsey,timefloat_t1meas,timefloat_ramsey,timefloat_spinecho]
    # r: [qid,fidelity,fidelity_std,SPAM]

    title_d=['$T_1$','$T_2r$','$T_2e$']
    title_r=['Fidelity','Fidelity Std','SPAM']

    d=numpy.empty((0,11),float)
    r=numpy.empty((0,4),float)
    filename='./statics_%s.dat'%args.startDate
    print('M: reading ',filename)

    modTimesinceEpoc = os.path.getmtime(filename)
    # Convert seconds since epoch to readable timestamp
    modificationTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(modTimesinceEpoc))

    assert  os.path.exists(filename) 
    d_=numpy.loadtxt(filename)
    filename_rb=filename[:8]+'rb_'+filename[8:]
    if os.path.isfile(filename_rb):
        r_=numpy.loadtxt(filename_rb)
        assert d_.shape[0]==r_.shape[0],'Please match the number of ROWs between Coherence Time Data and RB Data!'
    else:
        r_=numpy.zeros((d_.shape[0],4))
    d=numpy.vstack((d,d_))
    r=numpy.vstack((r,r_))
    t2=time.time()
    
    qubitid=[int(i) for i in sorted(set(d[:,0]))]
    print('M: read elaT=%.1f sec, qubitid:'%(t2-t1),qubitid,d.shape[0]//len(qubitid),' meas/qubit ')
    row_subplot=2
    col_subplot=math.ceil(len(qubitid)/2)

    for ifig,i in enumerate(qubitid):
            index=numpy.where(d[:,0]==i)
            di=d[index]
            ri=r[index]
            for j,k,t in zip(range(1,4),range(4,7),range(8,11)):
                    dv=di[:,j]
                    de=di[:,j+3]
                    dt=di[:,j+7]
                    yerr_l=dv-1/(1/dv+de)
                    yerr_h=1/(1/dv-de)-dv
                    fig=pyplot.figure(2*j-1)
                    ax1=pyplot.subplot(row_subplot,col_subplot,ifig+1)
                    pyplot.errorbar(range(len(dv)),dv*1e6,marker='.',ls='None',yerr=[yerr_l*1e6,yerr_h*1e6])
                    pyplot.xlabel('No.')
                    pyplot.ylabel('%s time ($\mu$s)'%(title_d[j-1]))
                    pyplot.text(0.1,0.1,'mean:%.1f$\mu$s'%(dv.mean()*1e6),transform=ax1.transAxes)
                    pyplot.title('Q%d'%i)
                    pyplot.suptitle(title_d[j-1])
                    

                    fig_t=pyplot.figure(len(qubitid)+7+2*j-1)
                    fig_t.set_size_inches(15., 6)
                    ax1_t=pyplot.subplot(row_subplot,col_subplot,ifig+1)
                    ax1_t.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
                    ax1_t.plot_date(dt,dv*1e6,ls='-',marker='.')
                    fig_t.autofmt_xdate(rotation=30)
                    
                    pyplot.ylabel('%s time ($\mu$s)'%(title_d[j-1]))
                    pyplot.title('Q%d'%i)
                    pyplot.suptitle(title_d[j-1])
                    

                    pyplot.figure(2*j)
                    ax2=pyplot.subplot(row_subplot,col_subplot,ifig+1)
                    rv=ri[:,j]
                    if j==1:
                            pyplot.errorbar(range(len(rv)),rv,marker='.',ls='None',yerr=ri[:,2])
                    else:
                            pyplot.plot(rv,'.')
                    pyplot.xlabel('No.')
                    pyplot.ylabel(title_r[j-1])
                    pyplot.text(0.1,0.1,'mean:%.4f'%(rv.mean()),transform=ax2.transAxes)
                    pyplot.title('Q%d'%i)
                    pyplot.suptitle(title_r[j-1])
                    

            fig=pyplot.figure(len(qubitid)+7)
            fig.set_size_inches(15., 6)
            ax3=pyplot.subplot(row_subplot,col_subplot,ifig+1)
            df=di[:,7]/1e3
            pyplot.plot(df,'.')
            pyplot.xlabel('No.')
            pyplot.ylabel('Offset Freq (kHz)')
            pyplot.text(0.1,0.1,'mean:%.0fkHz'%(df.mean()),transform=ax3.transAxes)
            pyplot.title('Q%d'%i)
            pyplot.suptitle('Ramsey Offset Frequency')


            fig_tf=pyplot.figure(len(qubitid)+7+7)
            fig_tf.set_size_inches(15., 6)
            ax3_t=pyplot.subplot(row_subplot,col_subplot,ifig+1)
            dt=di[:,9]
            ax3_t.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
            ax3_t.plot_date(dt,df,ls='-',marker='.')
            fig_tf.autofmt_xdate(rotation=30)
            pyplot.ylabel('Offset Freq (kHz)')
            pyplot.title('Q%d'%i)
            pyplot.suptitle('Ramsey Offset Frequency')
            
    # save once for all qubits (ifig)
    for j in range(1,4):
        fig=pyplot.figure(2*j-1)
        fig.set_size_inches(15., 6)
        pyplot.tight_layout()

        pyplot.savefig(args.outPath+'staticsplot'+str(2*j-1)+'.png')
        pyplot.figure(len(qubitid)+7+2*j-1)
        pyplot.savefig(args.outPath+'staticsplot'+str(len(qubitid)+7+2*j-1)+'.png')
        fig=pyplot.figure(2*j)
        fig.set_size_inches(15., 6)
        pyplot.savefig(args.outPath+'staticsplot'+str(2*j)+'.png')
        
    # save once for all 3 j's
    pyplot.figure(len(qubitid)+7)
    pyplot.savefig(args.outPath+'staticsplot'+str(len(qubitid)+7)+'.png')
    pyplot.figure(len(qubitid)+7+7)
    pyplot.savefig(args.outPath+'staticsplot'+str(len(qubitid)+7+7)+'.png')
    
    t3=time.time()
    print('M: per-qubit plots done, elaT=%.1f sec'%( t3-t2))       

    colors=['skyblue','orange','green']
    for ifig,i in enumerate(qubitid):
            index=numpy.where(d[:,0]==i)
            di=d[index]
            for j in range(1,4):
                    fig=pyplot.figure(ifig+7)
                    dv=di[:,j]
                    dv=dv/1e-6
                    tmean=dv.mean()
                    tstd=dv.std()
                    pyplot.hist(dv,histtype='bar',alpha=0.3,facecolor=colors[j-1],label='%s:%.1f$\pm$%.1f$\mu$s'%(title_d[j-1],tmean,tstd))
                    pyplot.axvline(tmean,linestyle='--',linewidth=1,color=colors[j-1])#,label='%s (%.1f)'%(title[j-1],tmean))
            pyplot.title('Q%d Coherence Statistics'%i)
            pyplot.legend()
            pyplot.xlabel('Coherence Time ($\mu$s)')
            pyplot.ylabel('Count')
            fig.set_size_inches(6., 4)
            pyplot.savefig(args.outPath+'staticsplot'+str(ifig+7)+'.png')

    t4=time.time()
    print('M: time plots done, elaT=%.1f sec'%( t4-t3))
    outF=args.outPath+'index.htm'
    build_index(outF,modificationTime)
    print('M: saved',outF)
    pyplot.show()
