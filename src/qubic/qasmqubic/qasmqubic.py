import time
import datetime
import os
import sys
sys.path.append('../..')
import antlr4
import qubic.qasmqubic
from qubic.qasmqubic.antlr4qasm3.qasm3Parser import qasm3Parser
from qubic.qasmqubic.antlr4qasm3.qasm3Lexer import qasm3Lexer
from qubic.qasmqubic.antlr4qasm3.qasm3Listener import qasm3Listener
from qubic.qasmqubic.quanVisitor import quanVisitor
from qubic.qasmqubic.preVisitor import preVisitor
#if __name__ is not None and "." in __name__:
#else:
#    from antlr4qasm3.qasm3Parser import qasm3Parser
#    from antlr4qasm3.qasm3Lexer import qasm3Lexer
#    from antlr4qasm3.qasm3Listener import qasm3Listener
#    from quanVisitor import quanVisitor
#    from preVisitor import preVisitor
import numpy
import pprint

class  c_logicqubit():
    def __init__(self,iden,index):
        self.phase=0
        self.readphase=0
        self.phyqubit=None
        self.iden=iden
        self.index=index
    def assignphyqubit(self,qubitname):#,tgatelength):
        self.phyqubit=qubitname
class c_localgate():
    def __init__(self,name,pulses):
        #print(pulses)
        self.name=name
        #print(self)
        self.pulses=pulses
        self.tgatelength=pulses[0]['tgatelength']
        for p in self.pulses:
            if 'patchphase' not in p:
                p['patchphase']=0
        pass
def ispoweroftwo(n):
    n=int(n)
    return ((n&(n-1))==0)and n!=0
class c_qasmqubic():
    def __init__(self,qasmstr,qubitsmap,preprocess=True,phunit='rad',**kwargs):
        self.opts=dict(debug=0,gatemap=dict(sx='X90',X='X90',x='X90',meas='read',cx='CNOT',cnot='CNOT',CNOT='CNOT',rz='virtualz',Z='virtualz',z='virtualz',p='virtualz',barrier='barrier',delay='delay'))
        self.opts.update(kwargs)
        self.visitor=self.parseqasmcircuit(qasmstr,preprocess,debug=self.opts['debug'])
        if phunit=='rad':
            self.phunittorad=1.0
        elif phunit=='deg':
            self.phunittorad=numpy.pi/180.0
        else:
            print('phunit should be rad or deg, %s is not valid'%str(phunit))
        self.logicqubits,self.phyqubits=self.qregphyqubit(qubitsmap)
        self.circuitgates=[]
#        self.gates={}
#        for gate in localgate:
#            self.gates[gate]=c_localgate(name=gate,pulses=localgate[gate])
#            self.patchpulseingate(self.gates[gate])
    def qregphyqubit(self,qubitsmap):
        logicqubits={}
        phyqubits={}
        for qreg in self.visitor.qubits:
            qregiden=qreg['Identifier']
            if qreg['designator']:
                for qindex in range(qreg['designator']):
                    logicqubits[(qregiden,qindex)]=c_logicqubit(qregiden,qindex)
            else:
                logicqubits[(qregiden,None)]=c_logicqubit(qregiden,None)
        for qreg in qubitsmap:
            if isinstance(qubitsmap[qreg],list):
                for qindex in range(len(qubitsmap[qreg])):
                    if (qreg,qindex) in logicqubits:
                        logicqubits[(qreg,qindex)].assignphyqubit(qubitsmap[qreg][qindex])
                        phyqubits[qubitsmap[qreg][qindex]]=logicqubits[(qreg,qindex)]
                    else:
                        print('qubit index (%s, %d) not in qasm'%(qreg,qindex))
#                        exit(1)
            else:
                qindex=None
                logicqubits[(qreg,qindex)].assignphyqubit(qubitsmap[qreg])#,qubitsmap[qreg][index][0]['tgatelength'])
                phyqubits[qubitsmap[qreg]]=logicqubits[(qreg,qindex)]
        return logicqubits,phyqubits
    #    print('debug logicqubits',self.logicqubits)
    #    print('debug phyqubits',self.phyqubits)

    def parseqasmcircuit(self,qasmstr,preprocess=True,debug=0):
        t0=time.time()#datetime.datetime.now()#.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
        pre=preprocess
        for process in ['preprocess','process'] if preprocess else ['process']:
            inputs  = antlr4.InputStream(qasmstr) #sys.stdin.read())
            lexer  = qasm3Lexer(inputs)
            tokens = antlr4.CommonTokenStream(lexer)
            parser = qasm3Parser(tokens)
        #    print('here 21',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
            parser._errHandler = antlr4.BailErrorStrategy()
        #    print('here 22',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
            tree   = parser.program()
        #    print('here 23',datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f'))
            if pre:
                previsitor=preVisitor(tree,debug=debug)
                for old, new in previsitor.getreplace().items():
                    qasmstr=qasmstr.replace(old,new)
                pre=False
            else:
                visitor=quanVisitor()
                #print('tree string',tree.toStringTree(recog=parser))
                visitor.visit(tree)
        t1=time.time()#datetime.datetime.now()#strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
        if ispoweroftwo(self.opts['circindex']):
            print('parse qasm circuit %d circuit'%self.opts['circindex'],'str length: %d '%len(qasmstr), 'take time: %.2f s'%(t1-t0))
        return visitor
    def qasmqubic(self):
        gatecnt=0
        for g in self.visitor.gates:
            logicqubitdest=[]
            for iden in g['indexIdentifierList']:
                if iden['rangelist'] is None:
                    for rangelist in self.logicqubits:
                        logicqubitdest.append(self.logicqubits[rangelist])
                else:
                    for rangelist in iden['rangelist']:
                        if (iden['Identifier'],rangelist) in self.logicqubits:
                            logicqubitdest.append(self.logicqubits[(iden['Identifier'],rangelist)])
            phyqubit=[q.phyqubit for q in logicqubitdest]
            if 'quantumGateName' in g:
                gatename=g['quantumGateName']
            elif 'timingInstructionName' in g:
                gatename=g['timingInstructionName']
            else:
                print('Neither quantum instruction nor timing statement',g)
            #print(gatename)
            if gatename in self.opts['gatemap']:
                if self.opts['gatemap'][gatename]=='virtualz':
                    phadd=g['expressionList'][0]
                    phygate=dict(name='virtualz',qubit=phyqubit,para=dict(phase=phadd*self.phunittorad))
                elif self.opts['gatemap'][gatename]=='delay':
                    phygate=dict(name='delay',qubit=phyqubit,para=dict(delay=g['designator']))
                else:
                    phygate=dict(name=self.opts['gatemap'][gatename],qubit=phyqubit)
            elif gatename[0]=='A':  # custom gate for Jan, must be present in calib.json. should not be here
                phygate=dict(name=gatename,qubit=phyqubit)
            else:
                print('c_qasmqubic():unknown gate %s'%gatename)
                exit(1)
                phygate=None#''


            #if gatename=='barrier':
            #    phygate=dict(name='barrier',qubit=phyqubit)
            #elif gatename=='sx' or gatename=='X' or gatename== 'x':
            #    phygate=dict(name='X90',qubit=phyqubit)
            #elif gatename[0]=='A':  # custom gate for Jan, must be present in calib.json
            #    phygate=dict(name=gatename,qubit=phyqubit)
            #elif gatename=='meas':
            #    phygate=dict(name='read',qubit=phyqubit)
            #elif gatename=='cx' or gatename=='cnot' or gatename=='CNOT':
            #    phygate=dict(name='CNOT',qubit=phyqubit)
            #elif gatename=='rz' or gatename=='Z':
            #    phadd=g['expressionList'][0]
            #    phygate=dict(name='virtualz',qubit=phyqubit,para=dict(phase=phadd*self.phunittorad))
            #elif gatename=='delay':
            #    phygate=dict(name='delay',qubit=phyqubit,para=dict(delay=g['designator']))
            #else:
            #    print('unknown gate %s'%gatename)
            #    exit(1)
            #    phygate=None#''
            self.circuitgates.append(phygate)
#            if phygate:
#                if phygate.name!='delay':
#                    tgatelength=self.gates[phygate.name].pulses[0]['tgatelength'] if phygate.name in self.gates else 0
##                    print('debug tgatelength',tgatelength,phygate.name, phygate.name in self.gates)
#                    phygate.setlength(tgatelength)
#                self.circuittree.append(phygate)
#                for q in phygate.qubit:
#                    if q in self.circuitgates:
#                        if self.circuitgates[q]:
#                            phygate.required.append(self.circuitgates[q][-1])
#            for dest in [q.phyqubit for q in logicqubitdest]:
#                #                print('debug dest',dest)
#                if dest not in self.circuitgates:
#                    self.circuitgates[dest]=[]
#                if phygate:
#                    self.circuitgates[dest].append(phygate)
#                    phygate.cnt=gatecnt
#                    #print(phygate.name,phygate.cnt)
#                    gatecnt=gatecnt+1;
##            tgatelength=self.gates[phygate][0]['tgatelength'] if phygate in self.gates else 0
#            pcarrier=[p['pcarrier'] for p in self.gates[phygate]] if phygate in self.gates else 0#[index]['tgatelength'] if phygate in localgate else 0
        return self.circuitgates


if __name__ == '__main__':
    import argparse
#    from regmap import c_regmap
#    from ether import c_ether
#    parser = argparse.ArgumentParser()
#    parser.add_argument('--ip',help='ip address',dest='ip',type=str,default=os.environ['QUBICIP'] if 'QUBICIP' in os.environ else '192.168.1.125')
#    parser.add_argument('--port',help='port',dest='port',type=int,default=os.environ['QUBICPORT'] if 'QUBICPORT' in os.environ else 3000)
#    parser.add_argument('--regmappath',help='regmap path',dest='regmappath',type=str,default='regmap.json')
#    parser.add_argument('--wavegrppath',help='wavegrp path',dest='wavegrppath',type=str,default='wavegrp.json')
#    parser.add_argument('-p','--plot',help='plot',dest='plot',default=False,action='store_true')
#    parser.add_argument('--run',help='run',dest='run',default=False,action='store_true')
#    parser.add_argument('-qf','--qasmfile',help='qasmfile',dest='qasmfile',type=str,default='')
#    parser.add_argument('--chipcfg',help='chip cfg json path',dest='chipcfg',type=str,default='qubitcfg_X6Y8.json')
#    clargs=parser.parse_args()
    #f=open(clargs.qasmfile)
    #qasmstr=f.read()
    #f.close()
    qasmstr='''OPENQASM 3.0;
qreg q[4];
creg meas[4];
cnot q[1],q[0];
delay [ 30us ] q;//[1],q[0];
barrier q;
measure q[1] -> meas[1];
measure q[2] -> meas[2];
Z(-1.5899150599321608) q[1];
X q[1];
X q[2];
barrier q;
'''
    import sys
    sys.path.append('..')
    from qubic.expression import c_expression
    ftestvar=c_expression(expression='ftest',variable=['ftest'])
    localgate={'Q4X90': [{'element': 0, 'dest': 1, 'pcarrier': 0.0, 'fcarrier': 40000000, 'fcarriername': 'Q4.freq', 't0': 0.0, 'tgatelength': 3.2e-08, 'patch': 32, 'cond': 0, 'start': 32, 'length': 32,'patchmem':100}],
            'Q5X90': [{'element': 1, 'dest': 2, 'pcarrier': 0.0, 'fcarrier': 229320599.58293822, 'fcarriername': 'Q5.freq', 't0': 0.0, 'tgatelength': 3.2e-08, 'patch': 32, 'cond': 0, 'start': 32, 'length': 32,'patchmem':100}],
            'Q6X90': [{'element': 2, 'dest': 3, 'pcarrier': 0.0, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 0.0, 'tgatelength': 3.2e-08, 'patch': 32, 'cond': 0, 'start': 32, 'length': 32,'patchmem':100}],
            'Q5Q4CNOT': [{'element': 1, 'dest': 2, 'pcarrier': -2.1682519310360795, 'fcarrier': ftestvar, 'fcarriername': 'Q5.freq', 't0': 1.76e-07, 'tgatelength': 2.4e-07, 'patch': 32, 'cond': 0, 'start': 96, 'length': 4,'patchmem':100}, {'element': 1, 'dest': 2, 'pcarrier': 6.925847668783883, 'fcarrier': 49176766.56551969, 'fcarriername': 'Q4.freq', 't0': 0.0, 'tgatelength': 2.4e-07, 'patch': 32, 'cond': 0, 'start': 132, 'length': 176,'patchmem':100}, {'element': 0, 'dest': 1, 'pcarrier': -0.6426623616042966, 'fcarrier': 49176766.56551969, 'fcarriername': 'Q4.freq', 't0': 1.76e-07, 'tgatelength': 2.4e-07, 'patch': 32, 'cond': 0, 'start': 96, 'length': 64,'patchmem':100}],
            'Q5Q6CNOT': [{'element': 1, 'dest': 2, 'pcarrier': -1.763042275980836, 'fcarrier': 229320599.58293822, 'fcarriername': 'Q5.freq', 't0': 2.56e-07, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 340, 'length': 4,'patchmem':100}, {'element': 1, 'dest': 2, 'pcarrier': 10.851787351763816, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 0.0, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 376, 'length': 256,'patchmem':100}, {'element': 2, 'dest': 3, 'pcarrier': -4.568602044584231, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 2.56e-07, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 96, 'length': 32,'patchmem':100}],
            'Q7Q6CNOT': [{'element': 1, 'dest': 2, 'pcarrier': -1.763042275980836, 'fcarrier': 229320599.58293822, 'fcarriername': 'Q5.freq', 't0': 0, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 340, 'length': 4,'patchmem':100}, {'element': 1, 'dest': 2, 'pcarrier': 10.851787351763816, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 0.0, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 376, 'length': 256,'patchmem':100}, {'element': 2, 'dest': 3, 'pcarrier': -4.568602044584231, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 2.56e-07, 'tgatelength': 2.8800000000000004e-07, 'patch': 32, 'cond': 0, 'start': 96, 'length': 32,'patchmem':100}],
            'Q4read': [{'element': 3, 'dest': 0, 'pcarrier': 0.0, 'fcarrier': 70173527.13527481, 'fcarriername': 'Q4.readfreq', 't0': 0.0, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}, {'element': 8, 'dest': 1, 'pcarrier': 0.0, 'fcarrier': 70173527.13527481, 'fcarriername': 'Q4.readfreq', 't0': 6.4e-07, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}],
            'Q5read': [{'element': 4, 'dest': 0, 'pcarrier': 0.0, 'fcarrier': 127945211.28656544, 'fcarriername': 'Q5.readfreq', 't0': 0.0, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}, {'element': 9, 'dest': 2, 'pcarrier': 0.0, 'fcarrier': 127945211.28656544, 'fcarriername': 'Q5.readfreq', 't0': 6.4e-07, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}],
            'Q6read': [{'element': 5, 'dest': 0, 'pcarrier': 0.0, 'fcarrier': 189185416.3703824, 'fcarriername': 'Q6.readfreq', 't0': 0.0, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}, {'element': 10, 'dest': 3, 'pcarrier': 0.0, 'fcarrier': 189185416.3703824, 'fcarriername': 'Q6.readfreq', 't0': 6.4e-07, 'tgatelength': 2.64e-06, 'patch': 32, 'cond': 0, 'start': 32, 'length': 2000,'patchmem':100}],
            'Q4X180': [{'element': 0, 'dest': 1, 'pcarrier': 0.0, 'fcarrier': 49176766.56551969, 'fcarriername': 'Q4.freq', 't0': 0.0, 'tgatelength': 6.4e-08, 'patch': 32, 'cond': 0, 'start': 192, 'length': 64,'patchmem':100}],
            'Q5X180': [{'element': 1, 'dest': 2, 'pcarrier': 0.0, 'fcarrier': 229320599.58293822, 'fcarriername': 'Q5.freq', 't0': 0.0, 'tgatelength': 6.4e-08, 'patch': 32, 'cond': 0, 'start': 664, 'length': 64,'patchmem':100}],
            'Q6X180': [{'element': 2, 'dest': 3, 'pcarrier': 0.0, 'fcarrier': 185074785.6167343, 'fcarriername': 'Q6.freq', 't0': 0.0, 'tgatelength': 6.4e-08, 'patch': 32, 'cond': 0, 'start': 160, 'length': 64,'patchmem':100}]}


    qubitsmap={'q':['Q4','Q5','Q6','Q7']}#,'gamma':'QQQ'}
    qasmqubic=c_qasmqubic(qasmstr,qubitsmap,preprocess=True,phunit='rad')
#    qasmqubic=c_qasmqubic(qasmstr,qubitsmap,localgate)
    circuitgates=qasmqubic.qasmqubic()
    for gate in circuitgates:
        print(gate)
#    lastgates=c_circuitnode()
#    lastgates.required=[circuitgates[q][-1] for q in circuitgates if circuitgates[q]]
#    lastgates.print()
    print(qasmstr)

#    cmdlistfun,lastpoint=qasmqubic.qubictime(circuitgates)
#    print('\n'.join([str(i) for i in cmdlistfun(ftest=222222)]))
#    print('\n'.join([str(i) for i in cmdlistfun(ftest=333333)]))
