#import datetime
#import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
#import time
#import init
#import pprint
#from experiment import c_experiment
from rabi_w import c_rabi
from scipy.optimize import minimize,fmin,fminbound

def rabiwscanqdrvamp(x,fread,fqubit,readoutdrvamp,delayread,nget,period_target,delay1,delaybetweenelement,elementlength,elementstep,qubitid,qubitidread=['Q7','Q6','Q5'],qubitcfg='qubitcfg.json',ip='192.168.1.124',wiremapmodule='wiremap_X6Y8_20210629'):
	qubitdrvamp=x
	rabi=c_rabi(qubitcfg=qubitcfg,ip=ip,wiremapmodule=wiremapmodule)
	rabi.rabiseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength,elementstep=elementstep,rdc=0,fqubit=fqubit,fread=fread,qubitdrvamp=qubitdrvamp,readoutdrvamp=readoutdrvamp,qubitid=qubitid,qubitidread=qubitidread)
	rabi.run()
	data=rabi.rabiacq(nget)
	fprocess=rabi.savejsondata(filename='',extype='tmp',cmdlinestr='',data=data,timeinfo=False)
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset='',plot=False,isfitdecay=False)
	result=abs(period*1e9-period_target)
	print("########readoutdrvamp,fread,qubitdrvamp,fqubit########",readoutdrvamp,fread,qubitdrvamp,fqubit)
	print("################result,amp,separation#################",result,amp,separation)
	sys.stdout.flush()
	return result

if __name__=="__main__":
	qubitdrvamp=fminbound(func=rabiwscanqdrvamp,x1=0.2,x2=0.9,xtol=0.005,disp=3,args=(154e6,168.4e6,0.40,680e-9,20,128,12e-6,600e-6,80,4e-9,'Q6',['Q7','Q6','Q5'],'qubitcfg_chip57.json','192.168.1.123','wiremap_X6Y8_20210629'))
	print('qubitdrvamp',qubitdrvamp)
