import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vna import c_vna
from yokogawa import yokogawa
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	parser.set_defaults(plot=False)
	yoko=yokogawa()
	yoko.setoutput('off')
	yoko.setrange(1e-3)
	yoko.setoutput('on')
	clargs=parser.parse_args()
	print('plot?',clargs.plot)
	vna=c_vna(**clargs.__dict__)
	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			vna.sim()
		vna.run(bypass=clargs.bypass)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vna.processvna(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	if clargs.plot:
		pyplot.ion()
		fig=pyplot.figure(1)
#	level=-1e-3
#	while True:
	norm=33676490.666547835
	levelrange=numpy.arange(-1e-3,1e-3,0.01e-3)
	vnavalue=numpy.zeros((len(levelrange),clargs.elementlength))
	for index,level in enumerate(levelrange):
		try:
			data=vna.vnadata(clargs.nget)
			fprocess=vna.savejsondata(filename=clargs.filename,extype='vnaloop',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
			c=vna.processvna(fprocess)
			vnavalue[index]=abs(c)/norm
			print('adc minmax',vna.hf.adcminmax())
			if clargs.plot:
				fig.clf()
				vna.vnaplot(c)
				pyplot.draw()
			yoko.setlevel(level)
			level=-1e-3 if level > 0.95e-3 else level+0.1e-3
			print(yoko.readstatus())
			pyplot.pause(1e-6)
		except KeyboardInterrupt:
			pyplot.close('all')
			break
	result={'level':levelrange.tolist(),'vna':vnavalue.tolist(),'freq':vna.freq.tolist()}
	yoko.setlevel(0.0)
#	print(result)
	fprocess=vna.savejsondata(filename=clargs.filename,extype='vnaloop_scanyokogawa',cmdlinestr=cmdlinestr,data=result)
	pyplot.pcolormesh(numpy.array(result['level'])/1.0e-6,numpy.array(result['freq'])/1e9+8.0,20*numpy.log10(numpy.array(result['vna'])).transpose(),cmap='RdBu_r')
	pyplot.colorbar(label='QubiC vna - S21')
	pyplot.xlabel('YokoDC - Current (uA)')
	pyplot.ylabel('Frequency (GHz)')
	pyplot.savefig(fprocess+'.pdf')
	if clargs.plot:
		pyplot.show()

