import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import pprint
import experiment
import copy
from itertools import product

class c_ilrb(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def ilrbseqs(self,elementlength=80,elementstep=2e-6,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,targetgate=None,restart=True):
		if restart:
			self.initseqs()
		ilrbgates=[]
		c_list=self.rand_clif_1q_target(elementlength+1,targetgate)
		for i in range(elementlength):
			c_list_id=self.g_list_add_inverse(c_list[:(3*i+3)])
			ilrbgates.append(c_list_id)
			print('c_list_id',c_list_id)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0
		for gates in ilrbgates:
			print('gates',gates)
			self.readall(trdrv=run,delayread=delayread,qubitsamps={"Q7":({},{}),"Q6":({},{}),"Q5":({},{})})
			pini=0
			tgate=self.seqs.tend()+delay1
			tini=tgate
			padd=0
			for gate in gates:
				gatemodi={}
				if gate=='I':
					pass
				else:
					if 'Z' in gate:
						padd=padd-float(gate[1:])/180*numpy.pi
					else:
						pnew=self.qchip.gates['Q7'+gate].pcalc(dt=tgate-tini,padd=padd)[0]
						if tgate-tini:
							gatemodi.update(dict(pcarrier=pnew))
						self.seqs.add(tgate,self.qchip.gates['Q7'+gate].modify(gatemodi))
					tgate=self.seqs.tend()
			self.readall(trdrv=self.seqs.tend(),delayread=delayread,qubitsamps={"Q7":({},{}),"Q6":({},{}),"Q5":({},{})})
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def processilrb(self,dt,filename,loadname):
		c=self.loadjsondata(filename)
		ilrb_result=self.process3(c['accout_0'],lengthperrow=self.hf.getbufwidthdict()['accout_0'],training=False,loadname=loadname)
		#print 'separation',ilrb_result['separation']
		return [c['accout_0'],ilrb_result['separation'],ilrb_result['iqafterherald'],ilrb_result['population_norm']]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	ilrb=c_ilrb(**clargs.__dict__)
	for nilrb in range(clargs.nmeas):
		print('measurement: %3d of %3d'%(nilrb+1,clargs.nmeas))
		ilrb.ilrbseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,targetgate=clargs.gate)
		ilrb.run(bypass=clargs.bypass or not clargs.processfile=='')
		if clargs.processfile=='':
			if clargs.sim:
				ilrb.sim()
			data=ilrb.acqdata(clargs.nget)
			fprocess=ilrb.savejsondata(filename=clargs.filename,extype='ilrb',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
		else:
			fprocess=clargs.processfile
		[rawdata,separation,iqafterherald,population_norm]=ilrb.processilrb(dt=clargs.elementstep,filename=fprocess,loadname=clargs.dataset)
		print('dataset',clargs.dataset)
		population_norm_tmp=population_norm.reshape((1,-1))
		population_norm_array=population_norm_tmp if nilrb==0 else numpy.append(population_norm_array,population_norm_tmp,axis=0)
		print('separation',separation)
		#ilrb.plotrawdata(rawdata)
		#ilrb.plotafterheraldingtest(iqafterherald)
		#ilrb.plotpopulation_norm(population_norm)
	population_norm_array=population_norm_array[2:,:]
	recovery_population_array=1-population_norm_array
	recovery_population_mean=numpy.mean(recovery_population_array,axis=0)
	numpy.savetxt('ilrb_recovery_population_array'+fprocess,recovery_population_array)
	ilrb.plotrb(recovery_population_array,recovery_population_mean,figname=fprocess)  # plotting is the same as rb
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
