import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import experiment
from t1meas import c_t1meas
from ramsey import c_ramsey
from spinecho import c_spinecho
import re
import matplotlib.dates

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--filestarttime',help='file start time',dest='filestarttime',type=str,default='')
	clargs=parser.parse_args()

	qid=int(re.findall('\d+',clargs.qubitid)[0])
	print('qubitid',qid)
	elementlength_t1meas=60#80
	elementstep_t1meas=8e-6#1e-6#4e-6
	elementlength_ramsey=80
	elementstep_ramsey=2e-6#4e-8#2e-6
	elementlength_spinecho=60#80
	elementstep_spinecho=8e-6#2e-7#2e-6


	t1meas=c_t1meas(**clargs.__dict__)
	t1meas.t1measseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=elementlength_t1meas,elementstep=elementstep_t1meas,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	t1meas.run(bypass=clargs.bypass or not clargs.processfile=='')
	timenow_t1meas=datetime.datetime.now()
	timestamp_t1meas=datetime.datetime.strftime(timenow_t1meas,'%Y%m%d_%H%M%S_%f')
	timefloat_t1meas=matplotlib.dates.date2num(timenow_t1meas)
	filet1meas='statics_t1meas_%s_%s.dat'%(clargs.qubitid,timestamp_t1meas)
	f=open(filet1meas,'w')
	json.dump(t1meas.acqdata(clargs.nget),f)
	f.close()
	[rawdata,separation,iqafterherald,population_norm,t1fit,fiterrt1meas]=t1meas.processt1meas(dt=elementstep_t1meas,filename=filet1meas,loaddataset=clargs.dataset,plot=True)
	print('REPORT T1 ',clargs.qubitid,timestamp_t1meas,t1fit,fiterrt1meas,filet1meas)
	print('t1fit',t1fit)
	print('fiterrt1meas',fiterrt1meas)


	ramsey=c_ramsey(**clargs.__dict__)
	ramsey.ramseyseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,elementlength=elementlength_ramsey,elementstep=elementstep_ramsey,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,framsey=clargs.framsey,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	ramsey.run(bypass=clargs.bypass or not clargs.processfile=='')
	timenow_ramsey=datetime.datetime.now()
	timestamp_ramsey=datetime.datetime.strftime(timenow_ramsey,'%Y%m%d_%H%M%S_%f')
	timefloat_ramsey=matplotlib.dates.date2num(timenow_ramsey)
	fileramsey='statics_ramsey_%s_%s.dat'%(clargs.qubitid,timestamp_ramsey)
	f=open(fileramsey,'w')
	json.dump(ramsey.acqdata(clargs.nget),f)
	f.close()
	[rawdata,separation,iqafterherald,population_norm,t2star,foffsetramsey,fiterrramsey]=ramsey.processramsey(dt=elementstep_ramsey,filename=fileramsey,loaddataset=clargs.dataset,plot=True)
	print('REPORT T2r ',clargs.qubitid,timestamp_ramsey,t2star,fiterrramsey,foffsetramsey,fileramsey)
	print('t2star',t2star)
	print('fiterrramsey',fiterrramsey)
	print('foffsetramsey',foffsetramsey)


	spinecho=c_spinecho(**clargs.__dict__)
	spinecho.spinechoseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=elementlength_spinecho,elementstep=elementstep_spinecho,framsey=0,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	spinecho.run(bypass=clargs.bypass or not clargs.processfile=='')
	timenow_spinecho=datetime.datetime.now()
	timestamp_spinecho=datetime.datetime.strftime(timenow_spinecho,'%Y%m%d_%H%M%S_%f')
	timefloat_spinecho=matplotlib.dates.date2num(timenow_spinecho)
	filespinecho='statics_spinecho_%s_%s.dat'%(clargs.qubitid,timestamp_spinecho)
	f=open(filespinecho,'w')
	json.dump(spinecho.acqdata(clargs.nget),f)
	f.close()
	[rawdata,separation,iqafterherald,population_norm,t2e,fiterrspinecho]=spinecho.processspinecho(dt=elementstep_spinecho,filename=filespinecho,loaddataset=clargs.dataset,plot=True)
	print('REPORT T2e ',clargs.qubitid,timestamp_spinecho,t2e,fiterrspinecho,filespinecho)
	print('t2e',t2e)
	print('fiterrspinecho',fiterrspinecho)


	#filestarttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	#scanfilename='statics_%s.dat'%(filestarttime)
	scanfilename='statics_%s.dat'%(clargs.filestarttime)
	with open(scanfilename,'a') as f:
		f.write('\n')
		numpy.savetxt(f,[qid,t1fit,t2star,t2e,fiterrt1meas[1],fiterrramsey[1],fiterrspinecho[1],foffsetramsey,timefloat_t1meas,timefloat_ramsey,timefloat_spinecho],newline=' ')
