import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
#from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
import time
if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	with open('qubitcfg.json') as jfile:
		chipcfg=json.load(jfile)
	qchip=c_qchip(chipcfg)
	run=0;
	delayread=524e-9#32e-9#528e-9#32e-9
	delay1=600e-9#900e-9

	seq=[]
	seq.extend([(0,				qchip.gates['Q1trig'].modify({"twidth":20e-9}))])
	for irun in range(20):
		run=run+800e-9#1500e-9
		rrun0=12e-9+20e-9*irun
		rrun1=48e-9#80e-9
		seq.extend([
			(run+40e-9,				qchip.gates['Q7readoutdrv'].modify({"amp":1.0,"twidth":rrun0}))
			,(run+40e-9+delayread,			qchip.gates['Q7readout'].modify({"twidth":rrun0,"amp":1.0}))
			,(run+40e-9+delay1+20e-9,		qchip.gates['Q7readoutdrv'].modify({"amp":1.0,"twidth":rrun1}))
			,(run+40e-9+delay1+20e-9+delayread,	qchip.gates['Q7readout'].modify({"twidth":rrun1}))
			])

	init=(('digiloopback',0)
			,('valid',0xff)
			)
	seqs=c_seqs(seq)
	hf=c_hfbridge(interface=interface)
	hf.run(seqs=seqs,init=init)
#	hf.cmdclear()
#	hf.elementmemclear()
#	hf.cmdgen0(seqs)
#	hf.write(init)
#	hf.elementmemwrite()
#	hf.cmdwrite()
#	hf.setperiod(numpy.ceil(seqs.tend()/1.0e-9/4.0))
	buf_monout=hf.buf_monout(opsel=2,mon_navr=9,mon_dt=0,mon_slice=0,mon_sel0=0,mon_sel1=2,panzoom_reset=0)
	pyplot.subplot(211)
	pyplot.plot(buf_monout[0])
	pyplot.subplot(212)
	pyplot.plot(buf_monout[1])
	pyplot.show()
