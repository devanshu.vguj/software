import os
import datetime
from matplotlib import pyplot
import numpy
from scipy import signal
import sys
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.analysis.s11peaks import s11peaks
class c_vnaqubit(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,readqubitid=None,repeat=1,ef=False,**kwargs):
        readqubitid=qubitid if readqubitid is None else (readqubitid if  isinstance(readqubitid,list) or isinstance(readqubitid,tuple)  else [readqubitid])
        self.opts['readqubitid']=readqubitid
        if 'c_expression' not in sys.modules:
            from qubic.qubic.expression import c_expression
        f=c_expression('f','f')
        seqs=[]
        seq=[]
        if ef:
            seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append({'name': 'X90', 'qubit': qubitid})
        for r in range(repeat):
            seq.append({'name': 'rabi', 'qubit': qubitid,'modi' : [{'twidth':4e-6,'fcarrier':f,'amp':1.0}]})
        seq.append({'name': 'barrier', 'qubit': readqubitid})
        for r in readqubitid:    
            seq.append({'name': 'read', 'qubit': r})
        seqs.append(seq)
        #print(seqs)
        return seqs


    def seqs(self,fx,**kwargs):
        self.opts.update(kwargs)
        self.opts['heraldcmds']=None
        self.opts['delaybetweenelement']=100e-6
        self.fx=fx
        self.opts['seqs']=self.baseseqs(**self.opts)
        self.opts['gmixs']=None
        self.opts['para']=[dict(f=f) for f in fx]
        print(self.opts['qubitid'])
        print(self.opts['seqs'])
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
    def run(self,nsample):
        self.debug(4,'debug c_vnaqubit run, gmixs',self.opts['gmixs'])
        experiment.c_experiment.accbufrun(self,nsample,includegmm=False)
        self.sep=numpy.zeros(len(self.fx))

        self.s11={}
        self.p0={}
        for r in self.opts['readqubitid']:
            self.s11[r]=(self.accresult['accval'][r].mean(axis=0))
#            self.p0[r]=self.accresult['countsum']['psingle'][r]['0']
            for fi,f in enumerate(self.fx):
                print(r)
                gmmi=self.gmmcount(accval={r:numpy.expand_dims(self.accval[r][:,fi],axis=1)})
                self.sep[fi]=gmmi['gmixs'][r].separation()[('0','1')]


    def fit(self):
        pass
    def s11peaks(self,pslimit=0):
        self.peaks={}
        for r,s11 in self.s11.items():
            #    print('pslimit in vnaqubit s11peak',r,pslimit)
            up=numpy.unwrap(numpy.angle(s11))
            self.peaks[r]=s11peaks(self.fx,up-up.mean(),pslimit=pslimit)
        return self.peaks
    def peaksplot(self,readqubit,ax):
        ax.plot(self.fx[0:len(self.peaks[readqubit]['phdiff'])],self.peaks[readqubit]['phdiff'])
        maxprominences=0
        for r,p in self.peaks.items():
            if len(p['prominences'])>0:
                maxprominences=max(maxprominences,max(p['prominences']))
        if len(self.peaks[readqubit]['peakindex'])>0:
            ax.plot(self.fx[self.peaks[readqubit]['peakindex']],self.peaks[readqubit]['phdiff'][self.peaks[readqubit]['peakindex']],'x')
            for i,(left,right) in enumerate(self.peaks[readqubit]['fscan']):
                ax.hlines(xmin=left,xmax=right,y=self.peaks[readqubit]['phdiff'].std(),color='r',linewidth=3)
            ax2=ax.twinx()
            ax2.plot(self.fx[self.peaks[readqubit]['peakindex']],self.peaks[readqubit]['prominences'],'r*')
            ax2.set_ylim([0,1.1*maxprominences])
            ax2.set_ylabel('Prominences')
        return ax 

    def iqapplot(self,readqubit,fig):
        #for r in self.s11:
        #    print('iqapplot',r)
        plot.iqapplot(x=self.fx,iq=self.s11[readqubit],fig=fig,label=readqubit)
        return fig
    def iqplot(self,readqubit,fig,**kwargs):
        #        for r in self.s11:
        accvaliq=self.accresult['accval'][readqubit]
        plot.iqplot(iq=accvaliq,fig=fig,label=readqubit)
        plot.gmmplot(gmixs=self.accresult['gmixs'][readqubit],fig=fig,**kwargs)
        fig.set_title('drive %s, read %s'%(self.opts['qubitid'],readqubit))
        return fig 



if __name__=="__main__":
    import sys
    sys.path.append('../..')
    qubitid=sys.argv[1]
    readqubitid=['Q6','Q7']
    readqubitid=['Q0','Q1','Q2']
    readqubitid=['Q3','Q4','Q5']
    readqubitid=[qubitid]
    vnaqubit=c_vnaqubit(qubitid=qubitid,calirepo='../../../../qchip',debug=False)
    loq=vnaqubit.opts['wiremap'].loq
    bw=vnaqubit.opts['chassis'].fsample

#    fx=numpy.linspace(5.48e9,5.6e9,1000)
#    fx=numpy.linspace(4.91e9,5.91e9,2000)
#    loq=5.65e9
#    bw=100e6
    fx=numpy.linspace(loq-bw/2,loq+bw/2,1000)
#    fx=numpy.linspace(5.06e9,5.08e9,1000)
#    fx=numpy.linspace(5070700000.0,5070800000,10)
    bw=1e9
#    fx=5.070698628e9+numpy.linspace(-bw/2,bw/2,10)
#    fx=numpy.linspace(5e9,5.4e9,1000)
#    fx=numpy.linspace(5.2e9,5.21e9,1000)
    vnaqubit.seqs(fx,ef=False,readqubitid=None)#readqubitid)
    vnaqubit.run(200)
    pslimit=0
#    for i in range(2):
#        print('pslimit in vnaqubit',pslimit)
#        vnaqubit.s11peaks(pslimit)
#        fig3=pyplot.figure(figsize=(15,8))
#        ax3=fig3.subplots(1,len(readqubitid))
#        for ir,rq in enumerate(readqubitid):
#            axi=ax3[ir] if len(readqubitid)>1 else ax3
#            vnaqubit.peaksplot(rq,axi)
#            axi.set_title(rq)
#
#        if i==0 and 0:
#            pyplot.show()
#            pslimit=float(input('pslimit\n'))
#    print(list(vnaqubit.peaks[qubitid].keys()))
#    print([(k,v) for k,v in vnaqubit.peaks[qubitid].items() if k in ['fscan','ftot']])
##    fig1=pyplot.figure(figsize=(15,8))
##    ax1=fig1.subplots(len(readqubitid),1)
#    for ir,rq in enumerate(readqubitid):
##    sub1=fig1.subplots()
#        figrq=pyplot.figure(rq)
#        axrq=figrq.subplots(3,2)
#        figrq.suptitle('drive %s, read %s'%(vnaqubit.opts['qubitid'],rq))
#        figrq.legend()
#        vnaqubit.iqapplot(rq,axrq.reshape(axrq.size))#ax1[ir])
    fig2=pyplot.figure(figsize=(15,8))
    ax2=fig2.subplots(1,len(readqubitid))
#    figp0=pyplot.figure('figp0')
#    axp0=figp0.subplots()#8,8)
#    figp0.suptitle('P(0)')
    for ir,rq in enumerate(readqubitid):
        ax2ir=ax2[ir] if len(readqubitid)>1 else ax2
        vnaqubit.iqplot(rq,ax2ir)
        ax2ir.set_title(rq)
#        ax4=axp0#[revqubit[qubitid],revqubit[read]]
#        ax4.set_title('read %s drive %s'%(rq,qubitid),fontsize=5)
#        ax4.plot(vnaqubit.fx,vnaqubit.p0[rq])
##    pyplot.savefig('fig1.pdf')
#    pyplot.show()
    pyplot.figure('sep')
    print(vnaqubit.sep)
    pyplot.plot(vnaqubit.fx,vnaqubit.sep)
    pyplot.show()
