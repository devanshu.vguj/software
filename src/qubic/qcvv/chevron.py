import json
import os
import datetime
from matplotlib import pyplot
import numpy
from scipy import signal
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit

class c_chevron(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,trabi,fq,ef=False,efrotback=False,**kwargs):
        self.debug(4,'chevron seq ef',ef)
        self.debug(4,'chevron kwargs',kwargs)
        self.ef=ef
        from qubic.qubic.expression import c_expression
        f=c_expression('f','f')
        seqs=[]
        for f in fq:
            for twidth in trabi:
                seq=[]
                if ef:
                    seq.append({'name': 'X90', 'qubit': qubitid})
                    seq.append({'name': 'X90', 'qubit': qubitid})
                rabimodi={}
                rabimodi.update({'twidth':twidth,'fcarrier':f})
                if 'amp' in kwargs:
                    if kwargs['amp'] is not None:
                        rabimodi.update({'amp':kwargs['amp']})
                #seq.append({'name': 'rabi_ef' if ef else 'rabi' , 'qubit': qubitid,'modi' : [rabimodi]})#{'twidth':twidth,'fcarrier':f}]})
                seq.append({'name': 'rabi_ef' if ef else 'rabi' , 'qubit': qubitid,'modi' : [rabimodi]})
                #seq.append({'name': 'rabi_ef' if ef else 'rabi' , 'qubit': qubitid,'modi' : [{'twidth':twidth,'fcarrier':f}]})
                if efrotback:
                    seq.append({'name': 'X90', 'qubit': qubitid})
                    seq.append({'name': 'X90', 'qubit': qubitid})
                seq.append({'name': 'read', 'qubit': qubitid})
                seqs.append(seq)
        self.debug(4,'debug chevron seqs',seqs)
        return seqs

    def seqs(self,**kwargs):
        self.opts.update(dict(elementlength=40,elementstep=4e-9,fstart=5.5e9,fstop=5.6e9,fsteps=40))
        self.opts.update(kwargs)
        self.trabi=numpy.arange(0,self.opts['elementlength']*self.opts['elementstep'],self.opts['elementstep'])
        self.fq,fstep=numpy.linspace(self.opts['fstart'],self.opts['fstop'],self.opts['fsteps'],retstep=True)

        self.opts['seqs']=self.baseseqs(trabi=self.trabi,fq=self.fq,**self.opts)
        self.compile()

        self.y=numpy.zeros(len(self.fq)+1)
        self.y[:-1]=self.fq-fstep/2
        self.y[-1]=self.fq[-1]+fstep/2
        self.x=numpy.zeros(len(self.trabi)+1)
        self.x[:-1]=self.trabi-self.opts['elementstep']/2
        self.x[-1]=self.trabi[-1]+self.opts['elementstep']/2

    def run(self,nsample,**kwargs):
        self.opts.update(kwargs)
        self.opts.update(dict(nsample=nsample))
        experiment.c_experiment.accbufrun(self,**self.opts)
        gmixlabels=self.accresult['gmixs'][self.opts['qubitid'][0]].labels
        self.result={}
        for il,l in enumerate(sorted(list(gmixlabels.keys()),key=lambda x:gmixlabels[x])):
            self.result[gmixlabels[l]]=numpy.array(self.accresult['countsum']['psingle'][self.opts['qubitid'][0]][gmixlabels[l]]).reshape((len(self.fq),len(self.trabi)))

    def plot(self,ax,label=None):
        #        gmixlabels=self.accresult['gmixs'][self.opts['qubitid']].labels
#        #ax=fig.subplots(1,len(gmixlabels))
        #if len(fig)==len(gmixlabels):
        #    for il,l in enumerate(sorted(list(gmixlabels.keys()),key=lambda x:gmixlabels[x])):
        #        self.result=numpy.array(self.accresult['countsum']['psingle'][self.opts['qubitid']][gmixlabels[l]]).reshape((len(self.fq),len(self.trabi)))
        if len(ax)>=len(self.result):
            il=0
            for k,result in self.result.items():
                pcm=ax[il].pcolormesh(self.x,self.y,result,cmap='RdBu_r',vmin=0,vmax=1)
                ax[il].set_title(k)
                il+=1
        
    #    fig.figure.colorbar(label='P(|1>)')
        #fig.title(self.opts['qubitid'])
        return pcm

    def fit(self):

        fitpara={}
        fitpara1={}
        fitpara2={}
        for k,result in self.result.items():
            if k not in fitpara1:
                fitpara1[k]=[]
            if k not in fitpara2:
                fitpara2[k]=[]
            if k not in fitpara:
                fitpara[k]=[]
            for iy,y in enumerate(self.fq):
                #    print('shape',self.trabi.shape,result[k].shape,[self.result[r].shape for r in self.result])
                try:
                    est=fit.sinestimate(x=self.x,y=result[iy,:])
                except:
                    est=None
                try:
                    fit1=fit.fitsin(x=self.trabi,y=result[iy,:],p0=est,bounds=[(-2,-numpy.inf,-2*numpy.pi,-1),(2,numpy.inf,numpy.pi*2,2)])
                    fitval1=fit1
                except:
                    fit1=None
                    fitval1=None
#                try:
                if 1:
                    y=result[iy,:]
                    est=[0,2/self.trabi[-1],0,y.mean(),self.trabi[-1]/2]
                    fit2=fit.fitexpsin(x=self.trabi,y=result[iy,:],p0=est,bounds=[(-2,-numpy.inf,-2*numpy.pi,-1,-numpy.inf),(2,numpy.inf,numpy.pi*2,2,numpy.inf)])
                    fitval2=fit2
#                except:
#                    print('fitexpsin fail')
#                    fit2=None
#                    fitval2=None
                if fit1 is not None and  fit2 is not None:
                    if fit1['rsqr'] < fit2['rsqr']:
                        fitval=fitval1 
                    else:
                        fitval=fitval2
                elif fit1 is not None:
                    fitval=fitval1
                elif fit2 is not None:
                    fitval=fitval2
                else:
                    fitval=None
                fitpara[k].append(fitval)
                fitpara1[k].append(fitval1)
                fitpara2[k].append(fitval2)
        #print(fitpara)
        self.ampfit=[f['popt'][0] for f in fitpara['0']]
        self.rsqr=[f['rsqr'] for f in fitpara['0'] if f is not None]
        self.rsqr1=[f['rsqr'] for f in fitpara1['0'] if f is not None]
        self.rsqr2=[f['rsqr'] for f in fitpara2['0'] if f is not None]
        return self.ampfit,self.rsqr
#        pyplot.figure('ampfit')
#        pyplot.plot(self.fq,self.ampfit)
                
        pass
    def ampfitplot(self,ax):
        ax.plot(self.fq,self.ampfit)
    def rsqrplot(self,ax):
        ax.plot(self.fq,self.rsqr,'r')
        ax.plot(self.fq,self.rsqr1,'g')
        ax.plot(self.fq,self.rsqr2,'b')


if __name__=="__main__":
    import sys
    sys.path.append('../../')
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(help='qubitid list',dest='qubitid')
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('-efrotback','--efrotback',help='ef rotate back',dest='efrotback',default=False,action='store_true')
    parser.add_argument('-dt','--dt',help='tstep',dest='dt',default=8e-9,type=float)
    parser.add_argument('-a','--amp',help='chevron drive amplitude, default None for use calibration file data',dest='amp',default=None,type=float)
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-g','--gmixs',help='gmixs: None: use data\n default: use the cali path\ndict: use the dict from gmixdict\n or path to gmix folder',dest='gmixs',type=str,default=None)
    parser.add_argument('-gd','--gmixsdict',help=''''gmix dict  -gd '{"Q5":"Q5_gmix.npz"}' ''',dest='gmixsdict',type=json.loads,default=None)
    parser.add_argument('-gl','--gmixslist',help=''''gmix list ('default','gmixs3')''',dest='gmixslist',type=str,default=('default','gmixs3'),nargs=2)
    parser.add_argument('-fullbw','--fullbw',help='full band width',dest='fullbw',default=False,action='store_true')

    clargs=parser.parse_args()

    print(clargs)
    t0=datetime.datetime.now()
    qubitid=clargs.qubitid
    chevron=c_chevron(qubitid=qubitid,calirepo=clargs.cpath,debug=False
            ,gmixs=clargs.gmixsdict if clargs.gmixs=='dict' else clargs.gmixslist if clargs.gmixs=='list' else clargs.gmixs )
#            ,gmixs='./')
#    fbw=80e6
#    print(freq)
#    freq=5.5637e9
#    fbw=20e6
#    freq=5.510e9
    fbw=20e6
    freq=5.04e9
    freq=5.169e9
    freq=5.13e9
    fbw=10e6
    freq=5.41e9
    fbw=1e9
    freq=5.2e9
    freq=5298741418.425535-270e6
    efrotback=clargs.efrotback
    freq=5438319445.233609
    freq=5.2e9
    fbw=200e6

#    for ief,ef in enumerate(clargs.ef):
    ef=clargs.ef
    ief=0
    qdrvfreq=chevron.opts['qchip'].getfreq(qubitid+'.freq'+('_ef' if ef else ''))
    #print('qdrvfreq',qdrvfreq)
    lofreq=chevron.opts['wiremap'].loq
    if clargs.fullbw:
        freq=lofreq
        fbw=1000e6
        fsteps=40
    else:
        freq=qdrvfreq
        fbw=100e6
        #fbw=20e6
        #fbw=100e6
        fsteps=30
    elementlength=41
    elementstep=clargs.dt
    chevron.seqs(fstart=freq-fbw/2,fstop=freq+fbw/2,elementlength=elementlength,elementstep=elementstep,fsteps=fsteps,ef=ef,efrotback=efrotback,overlapcheck=False,amp=clargs.amp)
    chevron.run(30)
    resultlen=len(chevron.result)
    fig1=pyplot.figure(figsize=((resultlen*5,5)))
    fig1.suptitle('%s_%s'%(qubitid,'chevron_ef' if ef else 'chevron'))
    ax1=fig1.subplots(1,resultlen)
    chevron.plot(ax1)
    for ax in ax1:
        ax.axhline(y=qdrvfreq,color='black')
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'chevron_ef' if chevron.ef else 'chevron'))
        pyplot.savefig(title+'.pdf')
    chevron.fit()
    #ax1.set_title(qubitid)
    #t1=datetime.datetime.now()
#    fig2=pyplot.figure(figsize=(15,8))
#    ax2=fig2.subplots()
#    chevron.iqplot(ax2,'*')
#    chevron.gmmplot(ax2)
    fig3=pyplot.figure('ampfit')
    ax3=fig3.subplots()
    chevron.ampfitplot(ax3)
    fig4=pyplot.figure('rsqr')
    ax4=fig4.subplots()
    chevron.rsqrplot(ax4)
#    ax4.set_ylim((0,10))

#    print('chevron time: ', t1-t0)
    if clargs.noplot:
        pass
    else:
        pyplot.show()
