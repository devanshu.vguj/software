import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vnaqubit import c_vnaqubit
from yokogawa import yokogawa
def fullsin(t,a,f,p,c):
	    return a*numpy.sin(2*numpy.pi*f*t+p)+c

def calcfread(current,poptfile='popt'):
	with open(poptfile) as fpopt:
		popt=json.load(fpopt)
#		[-5.31508255e+04 , 1.10160282e+03, -6.73561015e-01, -1.84681290e+08]):
	#		[8.65880615e+04,9.70585987e+02,9.04859365e-01,-3.30354939e+08]):
		#[ 8.32330774e+04  ,9.67674980e+02 , 8.55500310e-01, -3.30353165e+08]):

	#	return 75e3*numpy.cos(level/530.0e-6*numpy.pi+0.3)+77.821e6
	return fullsin(current,*popt)
#	return 75e3*numpy.cos(current/530.0e-6*numpy.pi+1.4)+77.821e6
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	parser.set_defaults(plot=False)
	yoko=yokogawa()
	yoko.setoutput('off')
	yoko.setrange(1e-3)
	yoko.setoutput('on')
	clargs=parser.parse_args()
	print('plot?',clargs.plot)
	vnaqubit=c_vnaqubit(**clargs.__dict__)
	#vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	vnaqubit.vnaqubitseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitid=clargs.qubitid,qubitdrvamp=clargs.qubitdrvamp,fread=clargs.fread,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			vnaqubit.sim()
		vnaqubit.run(bypass=clargs.bypass)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vna.processvna(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	if clargs.plot:
		pyplot.ion()
		fig=pyplot.figure(1)
#	level=-1e-3
#	while True:
	norm=1.0#33676490.666547835
	levelrange=numpy.arange(-1e-3,1e-3,0.01e-3)
	vnaqubitvalue=numpy.zeros((len(levelrange),clargs.elementlength))
	vnaqubitvaluere=numpy.zeros((len(levelrange),clargs.elementlength))
	vnaqubitvalueim=numpy.zeros((len(levelrange),clargs.elementlength))
	for index,level in enumerate(levelrange):
		try:
			freadupdate=calcfread(level)	
			vnaqubit.hf.cmd128all=[]
			vnaqubit.hf.cmdgen2(destfrdrv={clargs.qubitid+'.rdrv':freadupdate},destfread={clargs.qubitid+'.read':freadupdate})
			print('level',level,'fread',freadupdate)
			vnaqubit.hf.run(seqs=vnaqubit.seqs,memclear=False,cmdclear=False)
			data=vnaqubit.vnaqubitdata(clargs.nget)
			fprocess=vnaqubit.savejsondata(filename=clargs.filename,extype='vnaqubitloop',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
			c=vnaqubit.processvnaqubit(fprocess)
			vnaqubitvalue[index]=abs(c)/norm
			vnaqubitvaluere[index]=c.real
			vnaqubitvalueim[index]=c.imag
			print('adc minmax',vnaqubit.hf.adcminmax())
			if clargs.plot:
				fig.clf()
				vnaqubit.vnaqubitplot(c)
				pyplot.draw()
			yoko.setlevel(level)
			level=-1e-3 if level > 0.95e-3 else level+0.1e-3
			print(yoko.readstatus())
			pyplot.pause(1e-6)
		except KeyboardInterrupt:
			pyplot.close('all')
			break
	result={'level':levelrange.tolist(),'vnaqubit':vnaqubitvalue.tolist(),'freq':vnaqubit.freq.tolist(),'vnaqubitre':vnaqubitvaluere.tolist(),'vnaqubitim':vnaqubitvalueim.tolist()}
	yoko.setlevel(0.0)
#	print(result)
	fprocess=vnaqubit.savejsondata(filename=clargs.filename,extype='vnaqubitloop_scanyokogawa',cmdlinestr=cmdlinestr,data=result)
	if clargs.plot:
		pyplot.figure(2)
		pyplot.pcolormesh(numpy.array(result['level'])/1.0e-6,numpy.array(result['freq'])/1e9+5.5,(numpy.array(result['vnaqubit'])).transpose(),cmap='RdBu_r')
		pyplot.colorbar(label='QubiC vnaqubit')
		pyplot.xlabel('YokoDC - Current (uA)')
		pyplot.ylabel('Qubit drive frequency (GHz)')
		pyplot.savefig(fprocess+'.pdf')
		pyplot.show()

