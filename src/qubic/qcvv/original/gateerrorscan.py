import datetime
import argparse
import sys
from squbic import *
sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import experiment
class c_gateerrorscan(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',sim=False,initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,sim=sim,initcfg=initcfg,**kwargs)
		pass
	def readall(self,trdrv,delayread,readoutdrvamp0,readoutdrvamp1,readoutdrvamp2):
		tread=trdrv+delayread
		self.seqs.add(trdrv,             self.qchip.gates['Q7readoutdrv'].modify({"amp":readoutdrvamp0}))
		self.seqs.add(trdrv,         	   self.qchip.gates['Q5readoutdrv'].modify({"amp":readoutdrvamp1}))
		self.seqs.add(trdrv,             self.qchip.gates['Q6readoutdrv'].modify({"amp":readoutdrvamp2}))
		self.seqs.add(tread,         self.qchip.gates['Q7readout'])
		self.seqs.add(tread,         self.qchip.gates['Q5readout'])
		self.seqs.add(tread,         self.qchip.gates['Q6readout'])
		
	def gateerrorscanseqs(self,elementlength=80,elementstep=2e-6,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,readoutdrvamp=0.24,qubitdrvamp=0.3,readwidth=1000e-9,gatetype='X90'):
		readoutdrvamp0=readoutdrvamp
		readoutdrvamp1=readoutdrvamp if readoutdrvamp<0.33 else (1-readoutdrvamp)/2.0
		readoutdrvamp2=readoutdrvamp if readoutdrvamp<0.33 else (1-readoutdrvamp)/2.0

		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		run=0

		for i in range(elementlength):
			self.readall(trdrv=run,delayread=delayread,readoutdrvamp0=readoutdrvamp0,readoutdrvamp1=readoutdrvamp1,readoutdrvamp2=readoutdrvamp2)
			#print run,'heralding'
			pini=0
			t90=self.seqs.tend()+delay1
			tini=t90
			self.seqs.add(t90,self.qchip.gates['Q7'+('X90' if 'X' in gatetype else 'Y90')])
			#print t90,'init90'
			tgate=self.seqs.tend()
			for igate in range(i):
				for rgate in range(2 if '90' in gatetype else 1):
					gate=gatetype
					gatemodi={}
					if gate=='I':
						pass
					else:
						gateobj=self.qchip.gates['Q7'+gate]
						pgate=gateobj.pulses[0].pcarrier
						ptime=2*numpy.pi*self.qchip.getfreq("Q7.freq")*(tgate-tini)
						if ptime:
							gatemodi.update(dict(pcarrier=pgate+ptime))
						gatemodi.update(dict(amp=qubitdrvamp))
						self.seqs.add(tgate,self.qchip.gates['Q7'+gate].modify(gatemodi))
						#print tgate,gatemodi
						tgate=self.seqs.tend()
			tread=self.seqs.tend()
			self.readall(trdrv=tread,delayread=delayread,readoutdrvamp0=readoutdrvamp0,readoutdrvamp1=readoutdrvamp1,readoutdrvamp2=readoutdrvamp2)
			#print tread,'reading'
			run=self.seqs.tend()+delaybetweenelement

		self.seqs.setperiod(period=run)
		self.bufwidth=[self.seqs.countdest('Q7.read'),self.seqs.countdest('Q6.read'),self.seqs.countdest('Q5.read')]
	def processgateerrorscan(self,dt,filename,loadname):
		f=open(filename)
		g=json.load(f)
		f.close()
		c={}
		for k in g.keys():
			#c[k]=numpy.array([re+1j*im for (re,im) in g[k]])
			gka=numpy.array(g[k])
			c[k]=gka[:,0]+1j*gka[:,1]
			#print c[k].shape
		gateerrorscan_result=self.process3(c['accout_0'],lengthperrow=self.bufwidth[0],trainging=False,loadname=loadname)
		print('separation',gateerrorscan_result['separation'])
		slope=self.fitgateerrorscan(dt,abs(gateerrorscan_result['population_norm']-0.5))
		return [c['accout_0'],gateerrorscan_result['separation'],gateerrorscan_result['iqafterherald'],gateerrorscan_result['population_norm'],slope]

if __name__=="__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-r','--readoutdrvamp',help='readout drv amp',dest='readoutdrvamp',type=float,default=0.25)
	parser.add_argument('-q','--qubitdrvamp',help='qubit drv amp',dest='qubitdrvamp',type=float,default=0.29)
	parser.add_argument('-b','--bypasswritecommand',help='bypass write command',dest='bypass',default=False,action='store_true')
	parser.add_argument('-n','--nget',help='number of buffer to get',dest='nget',type=int,default=10)
	parser.add_argument('-L','--limit',help='plot limit',dest='limit',type=float,default=0)
	parser.add_argument('-f','--filename',help='filename ',dest='filename',type=str,default='')
	parser.add_argument('--elementlength',help='number of element point in test',dest='elementlength',type=int,default=20)
	parser.add_argument('--elementstep',help='time per rabi test point',dest='elementstep',type=float,default=2e-6)
	parser.add_argument('--swappre',help='swap pre process',dest='swappre',default=True,action='store_true')
	parser.add_argument('--swapdata',help='swap data process',dest='swapdata',default=True,action='store_true')
	parser.add_argument('-p','--processfile',help='processfile',dest='processfile',type=str,default='')
	parser.add_argument('--gatetype',help='gate type',dest='gatetype',type=str,default='X90')
	clargs=parser.parse_args()
	gateerrorscan=c_gateerrorscan(**clargs.__dict__)
	
	gateerrorscan.gateerrorscanseqs(elementlength=clargs.elementlength,elementstep=clargs.elementstep,delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,readwidth=clargs.readwidth,gatetype=clargs.gatetype)
	if clargs.processfile=='':
		gateerrorscan.run(load=not clargs.bypass)
#	numpy.set_printoptions(precision=None, threshold=numpy.nan, edgeitems=None, linewidth=200)
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
#		gateerrorscan.hf.sim1()
		datafilename='%s_gateerrorscan_%s_%4.2f_q%4.2f_%s.dat'%(clargs.filename,clargs.gatetype,clargs.readoutdrvamp,clargs.qubitdrvamp,timestamp)
		f=open(datafilename,'w')
		json.dump(gateerrorscan.acqdata(clargs.nget),f)
		f.close()
		print('datafile',datafilename)
		fprocess=datafilename
	else:
		fprocess=clargs.processfile

	[rawdata,separation,iqafterherald,population_norm,slope]=gateerrorscan.processgateerrorscan(dt=1,filename=fprocess,loadname=clargs.dataset)
	print('slope',datafilename,slope)
	#gateerrorscan.plotrawdata(rawdata)
	#print 't2e',t2e
	#print iqafterherald.shape
	#gateerrorscan.plotafterheraldingtest(iqafterherald)
#	gateerrorscan.plotpopulation_norm(population_norm)
#	numpy.savetxt('fitdata',population_norm)
#	pyplot.grid()
#	pyplot.show()
