import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
class c_chical(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def chicalseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,readwidth=None,fstart=-500e6,fstop=500e6,rdc=0,qubitdrvamp=None,fqubit=None,gate=None):
		df=1.0*(fstop-fstart)/(elementlength-1)
		self.freq=numpy.arange(fstart,fstop+df,df)[:elementlength] # avoid rounding error in python, make sure that self.freq length is elementlength
		print(len(self.freq))
		modq7rdrv={}
		modq7read={}
		modq7qdrv={}
		if readoutdrvamp:
			modq7rdrv.update(dict(amp=min(1-rdc,readoutdrvamp)))
		if qubitdrvamp:
			modq7qdrv.update(dict(amp=qubitdrvamp))
		if fqubit:
			modq7qdrv.update(dict(fcarrier=fqubit))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for frun in self.freq:
			modq7rdrv.update(dict(fcarrier=frun))
			modq7read.update(dict(fcarrier=frun))
			if gate:
				self.seqs.add(trun,self.qchip.gates['Q7'+gate].modify(modq7qdrv))
				self.readall(trdrv=self.seqs.tend(),delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read)})
			else:
				self.readall(trdrv=trun,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read)})
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
	def chicaldata(self,nget):
		data=self.acqdata(nget)
		return data
	def processchical(self,filename):
		c=self.loadjsondata(filename)['accout_0']
		cavr=c.reshape((-1,len(self.freq))).mean(0)
		return cavr
	def chicalplot(self,freq,c,label):
		pyplot.subplot(211)
		#pyplot.semilogy(self.freq,abs(c))
		pyplot.semilogy(freq,abs(c),label=label)
		pyplot.tight_layout()
		pyplot.legend()
		pyplot.subplot(212)
		#pyplot.plot(self.freq,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.plot(freq,signal.detrend(numpy.unwrap(numpy.angle(c))),label=label)
		pyplot.tight_layout()
		pyplot.legend()
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	chical_elementlength=1024  # elementlength for chical scan
	parser.set_defaults(elementlength=chical_elementlength)
	clargs=parser.parse_args()
	chical=c_chical(**clargs.__dict__)
	chical.chicalseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=None,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitdrvamp=clargs.qubitdrvamp,fqubit=clargs.fqubit,gate=clargs.gate)
	if clargs.processfile=='':
		if clargs.sim:
			chical.sim()
		chical.run(bypass=clargs.bypass)
		data=chical.chicaldata(clargs.nget)
		fprocess=chical.savejsondata(filename=clargs.filename,extype='chical',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=chical.processchical(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	c=chical.processchical(fprocess)
#	chical.plotrawdata(rawdata)
#	chical.plotafterheraldingtest(iqafterherald)
#	chical.plotpopulation_norm(population_norm)
	if clargs.plot:
		if clargs.gate:
			chical.chicalplot(chical.freq,c,'|1>')
		else:
			chical.chicalplot(chical.freq,c,'|0>')
		pyplot.grid()
		pyplot.show()
	if clargs.gate:
		numpy.savetxt('chical_'+clargs.gate+'.dat',(chical.freq,c))
	else:
		numpy.savetxt('chical_none.dat',(chical.freq,c))
	if clargs.gate:
		data0=numpy.loadtxt('chical_none.dat',dtype=complex,converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(chical_elementlength)))
		t0=data0[0]
		d0=data0[1]
		data1=numpy.loadtxt('chical_'+clargs.gate+'.dat',dtype=complex,converters=dict((k,lambda s: complex(s.decode().replace('+-', '-'))) for k in range(chical_elementlength)))
		t1=data1[0]
		d1=data1[1]
		chical.chicalplot(t0,d0,'|0>')
		chical.chicalplot(t1,d1,'|1>')
		pyplot.savefig('chical.png',dpi=600)
		pyplot.show()
		# python chical.py --plot -n 100 -f0 40.5e6 -f1 44.5e6 -r 0.9; python chical.py --plot -n 100 -f0 40.5e6 -f1 44.5e6 -r 0.9 -g X180
