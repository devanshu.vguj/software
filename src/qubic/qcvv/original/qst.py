import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip
import qtrl

class c_qst(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_list=None
		self.tomography_list=None
		pass
	def qstseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid_list=['Q6','Q5'],input_states=['I','I'],qubitidread=['Q7','Q6','Q5'],tomography_list=['I','X90','X180','Y90'],tcr=0e-9,pcr=0,txgate=0e-9,ZIphase=0):
		self.qubitid_list=qubitid_list
		self.tomography_list=tomography_list
		#self.tomography_list=['I','X90','X180','Y90']
		#self.tomography_list=['I','X90','Y90']
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		qubitid0=qubitid_list[0]
		qubitid1=qubitid_list[1]
		if len(input_states)==2:
			input0=input_states[0]
			input1=input_states[1]
		modinput1={}
		modtomo0={}
		modtomo1={}
		modcr={}
		modxgate={}
		run=0
		for tomo0 in self.tomography_list:
			for tomo1 in self.tomography_list:
				# Readout
				therald=run
				self.seqs.add(therald,self.qchip.gates[qubitid0+'read'])
				self.seqs.add(therald,self.qchip.gates[qubitid1+'read'])
				run=self.seqs.tend()+delay1
				tini=run

				# Init state preparation
				if len(input_states)==2:
					if input0!='I':
						self.seqs.add(run,         self.qchip.gates[qubitid0+input0])
						run=self.seqs.tend()

					if input1!='I':
						modinput1.update(dict(pcarrier=self.qchip.gates[qubitid1+input1].pcalc(dt=run-tini)))
						self.seqs.add(run,         self.qchip.gates[qubitid1+input1].modify(modinput1))
						run=self.seqs.tend()

				elif input_states==['Bell']:
					self.seqs.add(run,self.qchip.gates[qubitid0+'Y90'])
					run=self.seqs.tend()

					modcr.update(dict(twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid0+qubitid1+')'].pcalc(dt=run-tini,padd=pcr)[0]))
					self.seqs.add(run,self.qchip.gates['CR('+qubitid0+qubitid1+')'].modify(modcr))
					run=self.seqs.tend()

					if txgate>1e-14:
						modxgate.update(dict(twidth=txgate,pcarrier=self.qchip.gates['CNOT('+qubitid0+qubitid1+').'+qubitid1+'X'].pcalc(dt=run-tini)[0]))
						self.seqs.add(run,self.qchip.gates['CNOT('+qubitid0+qubitid1+').'+qubitid1+'X'].modify(modxgate))
					run=self.seqs.tend()
				else:
					print('What init state do you want?')

				# Quantum state tomography gate
				if tomo0!='I':
					modtomo0.update(dict(pcarrier=self.qchip.gates[qubitid0+tomo0].pcalc(dt=run-tini,padd=ZIphase if input_states==['Bell'] else 0)))
					self.seqs.add(run,         self.qchip.gates[qubitid0+tomo0].modify(modtomo0))
					run=self.seqs.tend()

				if tomo1!='I':
					modtomo1.update(dict(pcarrier=self.qchip.gates[qubitid1+tomo1].pcalc(dt=run-tini)))
					self.seqs.add(run,         self.qchip.gates[qubitid1+tomo1].modify(modtomo1))
					run=self.seqs.tend()

				# Readout
				treaddrv=self.seqs.tend()
				self.seqs.add(treaddrv,self.qchip.gates[qubitid0+'read'])
				self.seqs.add(treaddrv,self.qchip.gates[qubitid1+'read'])

				run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		accout_list=['accout_0','accout_1','accout_2'] # Should be imported from a JSON file with the mapping between qubitidread and accout in the future
		self.accout_dict=dict(zip(qubitidread,accout_list))

	def processqst(self,filename,loaddataset_list=None):
		c=self.loadjsondata(filename)
		print('self.bufwidth_dict',self.bufwidth_dict,'   self.accout_dict',self.accout_dict)
		print('accout in use: ',self.accout_dict[self.qubitid_list[0]],self.accout_dict[self.qubitid_list[1]])
		data_list=[c[self.accout_dict[self.qubitid_list[0]]],c[self.accout_dict[self.qubitid_list[1]]]]
		qst_result=self.process2q(data_list=data_list,qubitid_list=self.qubitid_list,lengthperrow_list=[self.bufwidth_dict[qid] for qid in self.qubitid_list],loaddataset_list=loaddataset_list)
		return qst_result['meas_binned']

	def processdm(self,measurement_avgs,plot=True):
		measurement_avgs=measurement_avgs.reshape(-1)
		dm=qtrl.standard_tomography(measurement_avgs=measurement_avgs,n_qubits=len(self.qubitid_list),tomography_list=self.tomography_list)
		#dm=qtrl.project_and_normalize_density_matrix(dm)
		if plot:
			qtrl.plot_dm(dm,rounding=3,scale=5)
		return dm

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qst=c_qst()
	parser,cmdlinestr=qst.cmdoptions()
	parser.add_argument('--tcr',help='pulse length for CR gate',dest='tcr',type=float,default=192e-9)
	parser.add_argument('--pcr',help='starting phase for CR gate',dest='pcr',type=float,default=3.925)
	parser.add_argument('--txgate',help='X rotation for CNOT gate',dest='txgate',type=float,default=0*32e-9)
	parser.add_argument('--ZIphase',help='ZIphase for CR gate',dest='ZIphase',type=float,default=5.1)
	clargs=parser.parse_args()
	input_states_list=[['I','I'],['X180','I'],['I','X180'],['X180','X180'],['X90','X90'],['I','X90'],['X90','I'],['X180','X90'],['X90','X180'],['Bell']]
	#input_states_list=[['I','I']]
	#input_states_list=[['X180','I']]
	#input_states_list=[['I','X180']]
	#input_states_list=[['X180','X180']]
	input_states_list=[['Bell']]
	for input_states in input_states_list:
		print('input_states',input_states)
		qst.initseqs()
		if clargs.sim:
			qst.setsim()
		qst.qstseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_list=clargs.qubitid_list,input_states=input_states,qubitidread=clargs.qubitidread,tomography_list=clargs.tomography_list,tcr=clargs.tcr,pcr=clargs.pcr,txgate=clargs.txgate,ZIphase=clargs.ZIphase)
		qst.run()
		data=qst.rabiacq(clargs.nget)
		fprocess=qst.savejsondata(filename=clargs.filename,extype='qst',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			qst.sim()
		meas_binned=qst.processqst(filename=fprocess,loaddataset_list=None)
		if clargs.readcorr:
			meas_binned=qst.readoutcorrection(qubitid_list=clargs.qubitid_list,meas_binned=meas_binned,corr_matrix=numpy.load(clargs.corrmx))
			print('corrected meas_binned',meas_binned,meas_binned.shape)
		dm=qst.processdm(measurement_avgs=meas_binned)
		print('dm',dm)
		#with open('cross_resonance_'+timestamp+'.dat','a') as f:
		#	numpy.savetxt(f,numpy.array(result).reshape((1,-1)))
		if clargs.plot:
			pyplot.grid()
			pyplot.show()
		print(qst.hf.adcminmax())
	pyplot.show()
# python qst.py -n 600 --qubitid_list Q6 Q5 --tcr 192e-9 --pcr 3.925 --txgate 0e-9 --ZIphase 6.07 --readcorr
