import vxi11
import argparse
def BNC845(ip="192.168.1.23",freq=6.75e9,power=2.2,on=True,query=False):
    instr=vxi11.Instrument(ip)
    print(instr.ask("*IDN?\n"))
    if not query:
        instr.write("FREQ %s %s\r\n"%(freq,'Hz'))
    print('Freq: ','%8.2f'%float(instr.ask("FREQ?\n")),'Hz')
    if not query:
        instr.write("POW %s %s\r\n"%(power,'dBm'))
    print('Power: ','%8.2f'%float(instr.ask("POW?\n")),'dBm')
    if not query:
        instr.write("OUTP %s \r\n"%('ON' if on else 'OFF'))
    print('BNC845 status','is ON' if int(instr.ask("OUTP?\n")) else 'is OFF')


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--ip',help='ip',dest='ip',type=str,default='192.168.1.23')
    parser.add_argument('-f','--freq',help='freq',dest='freq',type=float,default=6.781e9)
    parser.add_argument('-p','--power',help='power',dest='power',type=float,default=1.2)
    parser.add_argument('-o','--on',help='on/off (default=off)',dest='on',action='store_true')
    parser.add_argument('-q','--query',help='query',dest='query',action='store_true',default=False)
    clargs=parser.parse_args()
    BNC845(ip=clargs.ip,freq=clargs.freq,power=clargs.power,on=clargs.on,query=clargs.query)
