import numpy
import sys
f=sys.argv[1] #'gmixfastreset_rabi_nget50_qubitidQ5_20200225_233211_851475.joblib'
gmix=numpy.load(f)
p01=gmix['means']
##p01=numpy.array([[0,0],[0,0]])
p0=p01[0][0]+1j*p01[0][1]
p1=p01[1][0]+1j*p01[1][1]
angle=numpy.angle(p0-p1)
center=p0+(p1-p0)*1.0/2.0
print(p0,p1,angle,center)

