import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit
from qubic.qcvv.rabi import c_rabi
#from qubic.qubic.expression import c_expression
#def rabiseq(qubitid,**kwargs):
#    opts=dict(frabi=None,trabi=None,arabi=None,fread=None,tread=None,aread=None,pread=None)
class c_blob(c_rabi):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,frabi,fread,aread,trabi=32e-9,arabis=0.5):
        #frabipara=c_expression('frabipara','frabipara')
        #freadpara=c_expression('freadpara','freadpara')
        seqs=[]
        seq=[]
        for arabi in  arabis:
            seq.extend(self.rabiseq(qubitid=qubitid,arabi=arabi,frabi=frabi,fread=fread,trabi=trabi))
            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': 10e-6}})
        seqs.append(seq)

        return seqs

    def seqs(self,**kwargs):
        self.opts.update(kwargs)
        self.aread=0.5#numpy.linspace(0,1,0.1)
        self.opts['seqs']=[]
        fq=self.opts['qchip'].getfreq(self.opts['qubitid'][0]+'.freq')
        fr=self.opts['qchip'].getfreq(self.opts['qubitid'][0]+'.readfreq')
        ar=self.opts['qchip'].gates[self.opts['qubitid'][0]+'read'].paralist[0]['amp']
        bw=10e6
        self.frabi=numpy.linspace(fq-20e6,fq+20e6,8)
        #self.fread=numpy.linspace(fr-3e6,fr+3e6,200)
        self.fread=numpy.linspace(fr-bw/2.0,fr+bw/2.0,401)
        self.arabis=numpy.linspace(0,1,5)
        for fread  in self.fread:
            self.opts['seqs'].extend(self.baseseqs(qubitid=self.opts['qubitid'][0],frabi=None,fread=fread,aread=self.aread,arabis=self.arabis))
        self.compile(delaybetweenelement=1e-6,heraldcmds=None)

    def run(self,nsample):
        experiment.c_experiment.accbufrun(self,nsample=nsample)
        nfrabi=len(self.frabi)
        nfread=len(self.fread)
        narabi=len(self.arabis)
        self.accvals=[]
        for  k,v in self.accval.items():
            vs=v.reshape((-1,nfread,narabi))
            for ifread  in range(nfread):
                self.accvals.append({k:vs[:,ifread,:]})
        self.qubicrun.ops['gmix']=None
        self.sept={}
        self.bics={}
        self.gmmresults=[]
        for ifread,fread in enumerate(self.fread):
            gmmresult=self.gmmcount(self.accvals[ifread])
            self.gmmresults.append(gmmresult)
            for k,v in gmmresult['gmixs'][self.opts['qubitid'][0]].separation().items():
                if k not in self.sept:
                    self.sept[k]=[]
                    self.bics[k]=[]
                self.sept[k].append(v)
                self.bics[k].append(gmmresult['gmixs'][self.opts['qubitid'][0]].bic_val())
#        self.psingle=self.accresult['countsum']['psingle']
        gmixs=self.accresult['gmixs'][self.opts['qubitid'][0]]
        self.debug(4,'mean',gmixs.means_)
        self.debug(4,'covariances',gmixs.covariances_)
#        self.result['separation']=gmixs.separation()#numpy.sqrt(numpy.sum(numpy.diff(gmixs.means_.T)**2)/numpy.sqrt(numpy.product(gmixs.covariances_)))


    def fit(self):
        est=fit.sinestimate(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'])
        popt,pcov,self.yfit=fit.fitsin(x=self.x,y=self.psingle[self.opts['qubitid'][0]]['1'],p0=est)
        self.result.update(dict(amp=popt[0],period=1.0/popt[1],err=pcov))
        return self.result

    def psingleplot(self,fig):
        return plot.psingleplot(x=self.x,psingle=self.psingle,fig=fig,marker='.',linestyle=None,linewidth=0)
    def plottyfit(self,fig):
        fig.plot(self.x,self.yfit)
        return fig

    def iqplot(self,fig):
        index=len(self.fread)//2#numpy.nanargmin(numpy.abs(self.fread-fr))
        accvaliq=self.accvals[index][self.opts['qubitid'][0]]
#        self.accresult['accval'][self.opts['qubitid'][0]]
        plot.gmmplot(gmixs=self.gmmresults[index]['gmixs'][self.opts['qubitid'][0]],fig=fig)
        return plot.iqplot(iq=accvaliq,fig=fig)
    def savegmm(self):
        for q in self.accresult['gmixs']:
            numpy.savez('%s_gmix.npz'%q,**self.accresult['gmixs'][q].modelpara())

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(help='qubitid list',dest='qubitid')#,nargs='+')
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=1000,type=int)
    clargs=parser.parse_args()
    blob=c_blob(qubitid=clargs.qubitid,calirepo=clargs.cpath,debug=False,gmixs=None)
    blob.seqs(overlapcheck=False)#elementlength=10)
    blob.run(clargs.nshot)
#    blob.fit()
#    blob.savegmm()
    #print('result',blob.result)
    fr=blob.opts['qchip'].getfreq(blob.opts['qubitid'][0]+'.readfreq')

    fig,(sub1,sub2)=pyplot.subplots(2,1)
    fig.suptitle(clargs.qubitid)
    #print(blob.fread,blob.sept.values())
    for k,v in blob.sept.items():
        sub1.plot(blob.fread,v,label=k)
        sub1.set_ylabel('separation')
        sub1r=sub1.twinx()
        sub1r.plot(blob.fread,blob.bics[k],'r')
        sub1r.set_ylabel("bic",color='r')
    sub1.axvline(x=fr)
#    blob.psingleplot(sub1)
#    blob.plottyfit(sub1)
    blob.iqplot(sub2)
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'blob'))
        pyplot.savefig(title+'.pdf')
#pyplot.savefig('fig1.pdf')
    if clargs.noplot:
        pass
    else:
        pyplot.show()
