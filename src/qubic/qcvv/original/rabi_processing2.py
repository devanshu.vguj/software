import numpy
import sys
import argparse
import time
import read
from matplotlib import pyplot,patches
from sklearn import mixture
from scipy.optimize import curve_fit

if __name__=="__main__":
	parser=argparse.ArgumentParser()
	parser.add_argument('-sp','--swappre',help='swap label 0 and label 1 of Gaussian Mixture components during pre-processing',dest='swap_pre',type=int,default=0)
	parser.add_argument('-sr','--swaprabi',help='swap label 0 and label 1 of Gaussian Mixture components during rabi-processing',dest='swap_rabi',type=int,default=0)
	parser.add_argument('-f','--filename',help='filename',dest='filename',type=str,default='')
	parser.add_argument('-l','--rabilength',help='rabi length',dest='rabilength',type=int,default=80)
	clargs=parser.parse_args()

	# ----------read data----------
	rabilength=clargs.rabilength
	[d1,d2,d3]=read.readrabi(clargs.filename)
	d1raw_all=d1
	d1raw_herald=d1.reshape((-1,2))[:,0]
	d1raw_regular=d1.reshape((-1,2))[:,1]
	lim=1.1*max(max(abs(d1raw_all.real)),max(abs(d1raw_all.imag)))

	# ----------plot raw data (all, heralding, regular)----------
	pyplot.figure(1,figsize=(30,8))
	ax1=pyplot.subplot(131)
	ax1.set_aspect('equal')
	ax1.set_xlim([-lim,lim])
	ax1.set_ylim([-lim,lim])
	ax1.set_title('all')
	pyplot.hexbin(d1raw_all.real,d1raw_all.imag,gridsize=30,cmap='Greys',bins='log')
	ax2=pyplot.subplot(132)
	ax2.set_aspect('equal')
	ax2.set_xlim([-lim,lim])
	ax2.set_ylim([-lim,lim])
	ax2.set_title('heralding')
	pyplot.hexbin(d1raw_herald.real,d1raw_herald.imag,gridsize=30,cmap='Greys',bins='log')
	ax3=pyplot.subplot(133)
	ax3.set_aspect('equal')
	ax3.set_xlim([-lim,lim])
	ax3.set_ylim([-lim,lim])
	ax3.set_title('regular')
	pyplot.hexbin(d1raw_regular.real,d1raw_regular.imag,gridsize=30,cmap='Greys',bins='log')

	# ----------pre processing (select data which pass the heralding test)----------
	d1raw_all_iq=numpy.vstack((d1raw_all.real,d1raw_all.imag)).T
	d1raw_herald_iq=numpy.vstack((d1raw_herald.real,d1raw_herald.imag)).T
	d1raw_regular_iq=numpy.vstack((d1raw_regular.real,d1raw_regular.imag)).T

	n_gaussians=2
	gmix_raw=mixture.GaussianMixture(n_components=n_gaussians,covariance_type='spherical')
	gmix_raw.fit(d1raw_all_iq)
	order_raw=numpy.argsort(gmix_raw.means_[:,0])
	gmix_raw.means_=gmix_raw.means_[order_raw]
	gmix_raw.covariances_=gmix_raw.covariances_[order_raw]

	prediction_raw_all=gmix_raw.predict(d1raw_all_iq)
	predictionproba_raw_all=gmix_raw.predict_proba(d1raw_all_iq)
	print('raw all iq',d1raw_all_iq)
	print('raw all prediction',prediction_raw_all)
	#print 'raw all prediction proba',predictionproba_raw_all
	prediction_raw_herald=gmix_raw.predict(d1raw_herald_iq)
	prediction_raw_regular=gmix_raw.predict(d1raw_regular_iq)
	if (clargs.swap_pre):
		prediction_raw_all=1-prediction_raw_all
		prediction_raw_herald=1-prediction_raw_herald
		prediction_raw_regular=1-prediction_raw_regular

	prediction_raw_herald_2D=prediction_raw_herald.reshape((-1,rabilength))
	d1raw_regular_2D=d1raw_regular.reshape((-1,rabilength))
	index_2D=numpy.array([range(d1raw_regular_2D.shape[1])]*d1raw_regular_2D.shape[0])
	index_2D_T=index_2D.T
	prediction_raw_herald_2D_T=prediction_raw_herald_2D.T
	d1raw_regular_2D_T=d1raw_regular_2D.T
	condition_T=prediction_raw_herald_2D_T==0
	resultcomp_T=numpy.extract(condition_T,d1raw_regular_2D_T)
	index_T=numpy.extract(condition_T,index_2D_T)
	result=numpy.vstack((index_T,resultcomp_T.real,resultcomp_T.imag)).T

	# ----------plot data after the heralding test----------
	pyplot.figure(2)
	cx=pyplot.subplot(111)
	cx.set_aspect('equal')
	cx.set_xlim([-lim,lim])
	cx.set_ylim([-lim,lim])
	cx.set_title('taking off points according to the heralding')
	pyplot.hexbin(result[:,1],result[:,2],gridsize=30,cmap='Greys',bins='log')
	#pyplot.savefig(clargs.filename+'_iq.png')

	# ----------rabi processing----------
	gmix=mixture.GaussianMixture(n_components=n_gaussians,covariance_type='spherical')
	gmix.fit(result[:,1:])
	order=numpy.argsort(gmix.means_[:,0])
	gmix.means_=gmix.means_[order]
	gmix.covariances_=gmix.covariances_[order]

	prediction=gmix.predict(result[:,1:])
	predictionproba=gmix.predict_proba(result[:,1:])
	print('iq',result[:,1:])
	print('prediction',prediction)
	if (clargs.swap_rabi):
		prediction=1-prediction

	separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.T)**2)/numpy.sqrt(numpy.product(gmix.covariances_)))
	#separation_fidelity=0.5+numpy.erf(numpy.sqrt(separation/8.0))/2.0
	#separation_absolute=numpy.sqrt(numpy.sum((numpy.diff(gmix.means_.T))**2))
	print('separation',separation)

	unique,counts=numpy.unique(result[:,0],return_counts=True)
	if numpy.array_equal(unique,numpy.arange(rabilength))==False: 
		print('!!!Error!!!')
	population=[]
	strobe=numpy.add.accumulate(counts)
	strobe_start=numpy.append([0],strobe[:-1])
	strobe_stop=strobe
	for i in range(rabilength):
		population.append(numpy.sum(prediction[strobe_start[i]:strobe_stop[i]]))
	population_norm=1.0*numpy.array(population)/counts

	def func(x,w,a):
		return (a*numpy.sin(w*x))
	xdata=numpy.arange(len(population_norm))
	popt,pcov=curve_fit(func,xdata,population_norm)

	pyplot.figure(3)
	pyplot.plot(population_norm,'.-')
	pyplot.plot(xdata,func(xdata,*popt),'r--')#,label='fit: w=%5.3f' % tuple(popt))
	pyplot.legend()
	pyplot.ylim(0,1)
	pyplot.title(clargs.filename)
	#pyplot.savefig(clargs.filename+'.png')
	pyplot.show()
