import matplotlib
matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from experiment import c_experiment
from rabi_w import c_rabi
import vxi11

if __name__=="__main__":
	instr=vxi11.Instrument('192.168.1.252')
	print(instr.ask("*IDN?\n"))
	instr.write(":CONF:FREQ 1,4,(@3)\n")
	flag_processrabi=True#False
	counter=0
	lim_i_list=[]
	lim_q_list=[]
	lim_i_low_list=[]
	lim_i_high_list=[]
	lim_q_low_list=[]
	lim_q_high_list=[]
	flag_twpaon=True
	if flag_twpaon:
		lim_i_low_list_custom=[-35000,-70000,-25000]
		lim_i_high_list_custom=[-5000,-20000,-5000]
		lim_q_low_list_custom=[50000,5000,10000]
		lim_q_high_list_custom=[70000,55000,30000]
	else:
		lim_i_low_list_custom=[-10000,20000,-30000]
		lim_i_high_list_custom=[20000,35000,-10000]
		lim_q_low_list_custom=[-40000,-40000,-30000]
		lim_q_high_list_custom=[-20000,-15000,-10000]
	flag_customlim=False
	k1=0.95
	k2=1.05
	while True:
		try:
			print('read LO freq',instr.ask(":READ:FREQ?\n"))
			time.sleep(1)
			#for index,qubitid in enumerate(['Q5','Q6','Q7']):
			#for index,qubitid in enumerate(['Q0','Q6','Q1']):
			for index,qubitid in enumerate(['Q5','Q6']):
				rabi=c_rabi()
				parser,cmdlinestr=rabi.cmdoptions()
				parser.set_defaults(elementstep=4e-9)
				clargs=parser.parse_args()
				rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid=qubitid,qubitidread=clargs.qubitidread)
				rabi.run(bypass=clargs.bypass)
				#pyplot.ion()
				#fig=pyplot.figure(1)
				data=rabi.rabiacq(clargs.nget)
				fprocess=rabi.savejsondata(filename=clargs.filename,extype=qubitid+'_rabi',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if flag_processrabi:
					[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=False)
					print(' qubitid:',qubitid,' period:',period,' separation:',separation,' amp:',amp)
					print(rabi.hf.adcminmax())
					#rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
					#rabi.plotafterheraldingtest(iqafterherald)
					#rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
					timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
					rabi.plotpopulation_norm(population_norm=population_norm,figname=qubitid+'_'+timestamp)
					pyplot.close('all')
					#fig.clf()
					#if clargs.plot:
					#	rabi.plotpopulation_norm(population_norm=population_norm)
					#pyplot.draw()
					#pyplot.pause(1e-6)
				else:
					lengthperrow=rabi.bufwidth_dict[qubitid]
					c=rabi.loadjsondata(fprocess)
					cdata=c[list(c.keys())[0]]
					cavr=cdata.reshape((-1,lengthperrow)).mean(0)
					print('real part',cavr.real,'std',cavr.real.std())
					print('imag part',cavr.imag,'std',cavr.imag.std())
					print(' qubitid:',qubitid,' mean I: ',numpy.mean(cavr.real),' mean Q:',numpy.mean(cavr.imag))
					if counter<3:
						lim_i_list.append(numpy.mean(cavr.real))
						lim_q_list.append(numpy.mean(cavr.imag))
						lim_i_low_list.append(k1*numpy.mean(cavr.real) if numpy.mean(cavr.real)>0 else k2*numpy.mean(cavr.real))
						lim_i_high_list.append(k2*numpy.mean(cavr.real) if numpy.mean(cavr.real)>0 else k1*numpy.mean(cavr.real))
						lim_q_low_list.append(k1*numpy.mean(cavr.imag) if numpy.mean(cavr.imag)>0 else k2*numpy.mean(cavr.imag))
						lim_q_high_list.append(k2*numpy.mean(cavr.imag) if numpy.mean(cavr.imag)>0 else k1*numpy.mean(cavr.imag))
					counter=counter+1
					print(rabi.hf.adcminmax())
					timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
					figname='RabiIQ_'+qubitid+'_'+timestamp
					fig=pyplot.figure(figname)
					pyplot.subplot(211)
					pyplot.title(figname)
					pyplot.plot(cavr.real,'r.',label='I')
					if flag_customlim:
						pyplot.ylim(lim_i_low_list_custom[index],lim_i_high_list_custom[index])
					else:
						pyplot.ylim(lim_i_low_list[index],lim_i_high_list[index])
					pyplot.legend()
					pyplot.subplot(212)
					pyplot.plot(cavr.imag,'b.',label='Q')
					if flag_customlim:
						pyplot.ylim(lim_q_low_list_custom[index],lim_q_high_list_custom[index])
					else:
						pyplot.ylim(lim_q_low_list[index],lim_q_high_list[index])
					pyplot.legend()
					#pyplot.savefig(figname+'.png')
					pyplot.close('all')
		except KeyboardInterrupt:
			#pyplot.close('all')
			break
