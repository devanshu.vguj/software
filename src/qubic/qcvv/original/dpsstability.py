import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
class c_dps(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def dpsseqs(self,delaybetweenelement=600e-6,elementlength=1024,readoutdrvamp=None,qubitid='Q3'):
		self.phase=numpy.linspace(0,2*numpy.pi,elementlength)
		modrdrv={}
		modread={}
		self.qubitid=qubitid
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		trun=0
		for prun in self.phase:
			modrdrv.update(dict(pcarrier=prun))
			self.seqs.add(trun,self.qchip.gates[self.qubitid+'read'].modify([modrdrv,modread]))
			trun=self.seqs.tend()+delaybetweenelement
		print('period',trun)
		self.seqs.setperiod(period=trun)
	def dpsdata(self,nget):
		data=self.acqdata(nget)
		return data
	def processdps(self,filename):
		c_tmp=self.loadjsondata(filename)
		c=c_tmp[list(c_tmp.keys())[0]]
		cavr=c.reshape((-1,len(self.phase))).mean(0)
		return cavr
	def dpsplot(self,c):
		pyplot.figure(1)
		pyplot.subplot(211)
		pyplot.plot(self.phase,(abs(c)-numpy.mean(abs(c)))/numpy.mean(abs(c)))
		print(self.qubitid+' Vrms,Vmean,Vrms/Vmean',numpy.std(abs(c)),numpy.mean(abs(c)),numpy.std(abs(c))/numpy.mean(abs(c)))
		#pyplot.plot(self.phase,abs(c))
		print(self.qubitid+' Vpp,Vmean,Vpp/Vmean',max(abs(c))-min(abs(c)),numpy.mean(abs(c)),(max(abs(c))-min(abs(c)))/numpy.mean(abs(c)))
		pyplot.ylabel('Amplitude',fontsize=13)
		pyplot.tight_layout()
		pyplot.subplot(212)
		pyplot.plot(self.phase,signal.detrend(numpy.unwrap(numpy.angle(c))))
		pyplot.ylabel('Phase (rad)',fontsize=13)
		pyplot.xlabel('Phase Shift (rad)',fontsize=13)
		print(self.qubitid+' phase (rad)',max(signal.detrend(numpy.unwrap(numpy.angle(c))))-min(signal.detrend(numpy.unwrap(numpy.angle(c)))))
		#pyplot.plot(self.phase,numpy.unwrap(numpy.angle(c)))
		pyplot.tight_layout()
		pyplot.figure(2)
		ax1=pyplot.subplot(111)
		ax1.set_aspect('equal')
		lim=max(1.1*max(max(abs(c.real)),max(abs(c.imag))),0.1)
		ax1.set_xlim([-lim,lim])
		ax1.set_ylim([-lim,lim])
		ax1.set_xlabel('I (a.u.)',fontsize=13)
		ax1.set_ylabel('Q (a.u.)',fontsize=13)
		pyplot.plot(c.real,c.imag,'.')
	def recorddps(self,readfilename,writefilename):
		c_tmp=self.loadjsondata(readfilename)
		c=c_tmp[list(c_tmp.keys())[0]]
		with open(writefilename,'a') as f:
			numpy.savetxt(f,[c[0]])
	def recordplot(self,filename,plot=True):
		d=numpy.loadtxt(filename,dtype=numpy.complex128)
		pyplot.figure()
		pyplot.subplot(211)
		pyplot.plot(abs(d))
		pyplot.ylabel('Amplitude Response',fontsize=13)
		pyplot.tight_layout()
		print('Amplitude Vmean,Vrms,Vrms/Vmean',numpy.mean(abs(d)),numpy.std(abs(d)),numpy.std(abs(d))/numpy.mean(abs(d)))
		pyplot.subplot(212)
		pyplot.plot(numpy.angle(d))
		pyplot.xlabel('No.',fontsize=13)
		pyplot.ylabel('Phase Response (rad)',fontsize=13)
		pyplot.tight_layout()
		print('Phase Vmean,Vrms',numpy.mean(numpy.angle(d)),numpy.std(numpy.angle(d)))
		if plot:
			pyplot.show()

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	dps=c_dps(**clargs.__dict__)
	dps.dpsseqs(delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,qubitid=clargs.qubitid)
	dps.run(bypass=clargs.bypass)
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	data0_filename='dps_stability_'+timestamp+'.dat'
	for i in range(60*24*7): # 10800
		data=dps.dpsdata(clargs.nget)
		fprocess=dps.savejsondata(filename=clargs.filename,extype='dps',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		dps.recorddps(fprocess,data0_filename)
		time.sleep(60) # 10
	dps.recordplot(data0_filename)
	#c=dps.processdps(fprocess)
	#print('adc min max',dps.hf.adcminmax())
	#if clargs.plot:
	#	dps.dpsplot(c)
	#	pyplot.grid()
	#	pyplot.show()
	# source loopback.sh; python dpsstability.py --qubitid Q3 -n 1
