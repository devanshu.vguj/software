import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from experiment import c_experiment
from rabi_w import c_rabi
from scipy.optimize import minimize,fmin,fminbound
def rabiwscanfreadforseparation(x0,fqubit,readoutdrvamp,qubitdrvamp,delayread,nget,delay1,delaybetweenelement,elementlength,elementstep,qubitid,qubitidread=['Q7','Q6','Q5'],qubitcfg='qubitcfg.json',ip='192.168.1.124'):
	dt=elementstep
	print('fqubit,readoutdrvamp,qubitdrvamp',fqubit,readoutdrvamp,qubitdrvamp)
	print('delayread,nget',delayread,nget)

	fread=x0
	#fqubit=-85e6
	#readoutdrvamp=0.35
	#qubitdrvamp=0.25
	fqubit=fqubit
	readoutdrvamp=readoutdrvamp
	qubitdrvamp=qubitdrvamp

	rabi=c_rabi(qubitcfg=qubitcfg,ip=ip)
	rabi.rabiseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,elementlength=elementlength,elementstep=elementstep,rdc=0,fqubit=fqubit,fread=fread,qubitdrvamp=qubitdrvamp,readoutdrvamp=readoutdrvamp,qubitid=qubitid,qubitidread=qubitidread)
	rabi.run()
	data=rabi.rabiacq(nget)
	cmdlinestr='readoutdrvamp'+str(readoutdrvamp)+'_fread'+str(fread)+'_qubitdrvamp'+str(qubitdrvamp)+'_fqubit'+str(fqubit)+'_qubitid'+qubitid
	fprocess=rabi.savejsondata(filename='',extype='rabi',cmdlinestr=cmdlinestr,data=data)
	print('save data to ',fprocess)
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=dt,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset='',plot=False,isfitdecay=False)
	#result=abs(0.5-amp)
	result=-separation
	print("########readoutdrvamp,fread,qubitdrvamp,fqubit########",readoutdrvamp,fread,qubitdrvamp,fqubit)
	print("################result,amp,separation#################",result,amp,separation)
	#rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	#rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	sys.stdout.flush()
	return result
if __name__=="__main__":
	#minimum=fmin(rabiwscanfreadforseparation,159.4e6,ftol=0.1)
	#fread=fminbound(func=rabiwscanfreadforseparation,x1=156e6,x2=162e6,xtol=1e3,disp=1)
	fread=fminbound(func=rabiwscanfreadforseparation,x1=156e6,x2=162e6,xtol=1e3,disp=1,args=(-85e6,0.35,0.25,944e-9,50,'Q5'))
	print('fread',fread)
