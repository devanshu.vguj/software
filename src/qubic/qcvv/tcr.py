import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit


class c_tcr(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,tcrlist,amp=None,repeat=1,**kwargs):
        seqs=[]
        for twidth in tcrlist:
            crmodi={}
            crmodi.update(dict(amp=amp) if amp is not None else {})
            crmodi.update(dict(twidth=twidth))#{'twidth':twidth})
            for init in [0,1]:
                for tomogate in ['X90','Y-90',None]:
                    seq=[]
                    if init==1:
                        seq.append({'name': 'X90', 'qubit': [qubitid[0]]})
                        seq.append({'name': 'X90', 'qubit': [qubitid[0]]})
                    for r in range(repeat):
                        seq.append({'name': 'CR', 'qubit': qubitid,'modi' : [crmodi]})
                    for qubit in qubitid:
                        if tomogate is not None:
                            seq.append({'name': tomogate, 'qubit': [qubit]})
                        seq.append({'name': 'read', 'qubit': [qubit]})
                    seqs.append(seq)
        return seqs
    def seqs(self,tcrlist,**kwargs):
        self.opts.update(kwargs)
        self.opts['seqs']=self.baseseqs(tcrlist=tcrlist,**self.opts)
        self.compile()
        self.tcrlist=tcrlist

    def run(self,nsample,**kwargs):
        self.opts.update(kwargs)
        self.opts.update(nsample=nsample)
        experiment.c_experiment.accbufrun(self,**self.opts)
        self.v6={}
        self.d3={}
        self.r={}
        for q,lv in self.accresult['countsum']['psingle'].items():
            self.v6[q]={}
            self.d3[q]={}
            self.r[q]={}
            for l,v in lv.items():
                v6=v.reshape((-1,6))
                d3=v6[:,3:6]-v6[:,0:3]
                self.debug(5,'d3 shape',d3.shape)
                self.d3[q][l]=d3
                self.v6[q][l]=v6
                self.r[q][l]=(v6[:,0]-v6[:,3])**2+(v6[:,1]-v6[:,4])**2+(v6[:,2]-v6[:,5])**2
    def plot(self,plotfit=False):
        ylim=(-2,2)
        for q in self.opts['qubitid']:
            v61=self.v6[q]['1']
            d31=self.d3[q]['1']
            self.debug(5,d31)
            self.debug(5,d31.shape)
            r1=self.r[q]['1']
            pyplot.figure(q)
            pyplot.suptitle(q)
            pyplot.subplot(411)
            pyplot.plot(self.tcrlist,r1)
            #pyplot.subplot(415)
            if plotfit:
                pyplot.plot(self.tcrlist,self.yfit[q],'r')
            pyplot.subplot(412)
            pyplot.plot(self.tcrlist,d31[:,0])
            pyplot.ylim(ylim)
            pyplot.subplot(413)
            pyplot.plot(self.tcrlist,d31[:,1])
            pyplot.ylim(ylim)
            pyplot.subplot(414)
            pyplot.plot(self.tcrlist,d31[:,2])
            pyplot.ylim(ylim)
    
    def fit(self):
        self.yfit={}
        #print('qubitid',self.opts['qubitid'])
        for q in self.opts['qubitid']:
            #print('q',q)
            r1=self.r[q]['1']
            try:
                est=fit.sinestimate(x=self.tcrlist,y=r1)
            except:
                est=None
            fitpara=fit.fitsin(x=self.tcrlist,y=r1,p0=est)
            self.yfit[q]=fitpara['yfit']
        #    print('fitpara',q,fitpara)
            self.result.update({q:dict(amp=fitpara['popt'][0],period=1.0/fitpara['popt'][1] if fitpara['popt'][1]!=0 else 0,p0=fitpara['popt'][2],err=fitpara['pcov'],firstmaxt=((numpy.pi/2-fitpara['popt'][2])%(2*numpy.pi))/(2*numpy.pi*fitpara['popt'][1]))})
        return self.result
    def scanacr(self,nsample,repeat,tcr,ampchange=None,**kwargs):
        #print('scanacr',nsample,repeat,tcr,ampchange)
        self.opts.update(kwargs)
        self.opts['seqs']=[]
        #print(ampchange)
        self.ampchange=ampchange
        for amp in self.ampchange:
            #print(amp,repeat,tcr)
#            self.seqs(tcrlist=[tcr],amp=amp,repeat=repeat))
            self.opts['seqs'].extend(self.baseseqs(self.opts['qubitid'],tcrlist=[tcr],amp=amp,repeat=repeat))
        self.compile()
        self.run(nsample,combineorder=self.opts['qubitid'])
#        self.psingletgt1=self.accresult['countsum']['psingle'][self.opts['qubitid'][1]]['1']
        y=self.r[self.opts['qubitid'][1]]['1']
        x=self.ampchange
        est=[(-min(y)+max(y))/(max(x)-min(x))**2,x[int(len(x)/2)],1]
        fitpara=fit.fitquadratic(x,y,p0=est)
        if self.opts['plot']:
            pyplot.figure('tcr optimize all')
            pyplot.plot(x,y)
            pyplot.plot(x,fitpara['yfit'])
            pyplot.figure('tcr optimize each')
            pyplot.clf()
            pyplot.plot(x,y)
            pyplot.plot(x,fitpara['yfit'])
            pyplot.pause(0.1)

        return fitpara['popt'][1] if fitpara['rsqr']<1 else None
#        self.fit()
#            
#                self.opts['seqs']=self.baseseqs(tcrlist=tcrlist,**self.opts)
        #self.compile()
#        self.tcrlist=tcrlist
    def optimizesetup(self,**kwargs):
        opts=dict(valkey='1',repeat=4,initspan=2.0/3.0,ngatemult=2,ngateadd=1,xtol=0.01,xsteps=50)
        opts.update(kwargs)
        self.opts.update(opts)
        self.ampchange=[]
        self.vals={}
    def optimize(self,tcr,nsample,nsteps=4,acenter=None,aspan=None,**kwargs):
        self.opts.update(kwargs)
        if self.opts['plot']:
            pyplot.figure('tcr optimize all')
            pyplot.ion()
            pyplot.clf()
            pyplot.show()
            pyplot.figure('tcr optimize each')
            pyplot.ion()
            pyplot.clf()
            pyplot.show()
            pyplot.pause(0.1)
        self.optimizesetup(**kwargs)
        self.nsample=nsample
        acenter=self.opts['qchip'].gates[''.join(self.opts['qubitid'])+'CR'].pulses[0].amp if acenter is None else acenter
        aspan=acenter*2.0/3.0 if aspan is None else aspan
#        acenter=0.5
#        aspan=1
        self.opts['repeat']=self.opts['ngateadd']
        x1=max(acenter-aspan/2,0.01)
        x2=min(acenter+aspan/2,1.0)
        for istep,step in enumerate(range(nsteps)):
            scan=True
            failcnt=0
            while scan and failcnt<5:
                acenter=self.scanacr(nsample=self.nsample,tcr=tcr,repeat=self.opts['repeat'],ampchange=numpy.linspace(x1,x2,self.opts['xsteps']))
                if acenter is not None:
                    if acenter > x1 and acenter < x2:
                        aspan=aspan/2
                        x1=max(acenter-aspan/2,80e-9)
                        x2=acenter+aspan/2
                        self.opts['xtol']=self.opts['xtol']/2
                        self.opts['repeat']=self.opts['ngatemult']*2**istep+self.opts['ngateadd']
                        scan=False
                    else:
                        failcnt+=1
                        scan=True
                else:
                    failcnt+=1
                    scan=True
                print('x1','x2',acenter,x1,x2,failcnt)
        if self.opts['plot']:
            pyplot.ioff()
        updatedict={}
        updatedict.update({('Gates',''.join(self.opts['qubitid'])+'CR',0,'amp'):acenter})
        updatedict.update({('Gates',''.join(self.opts['qubitid'])+'CR',0,'twidth'):tcr})
        updatedict.update({('Gates',''.join(self.opts['qubitid'])+'CNOT',2,'t0'):tcr-8e-9})
        updatedict.update({('Gates',''.join(self.opts['qubitid'])+'CNOT',3,'t0'):tcr-8e-9})
        updatedict.update({('Gates',''.join(self.opts['qubitid'])+'CNOT',4,'t0'):tcr})
        return updatedict


if __name__=="__main__":
    t0=datetime.datetime.now()
    qubitid=['Q1','Q2']
    qubitid=['Q0','Q1']
    qubitid=['Q3','Q2']
#    qubitid=['Q6','Q7']
    qubitid=['Q5','Q4']
    qubitid=['Q4','Q3']
    qubitid=['Q6','Q5']
    qubitid=['Q6','Q5']
    qubitid=['Q5','Q4']
    qubitid=['Q3','Q2']
    qubitid=['Q4','Q3']
    qubitid=['Q6','Q5']
    qubitid=['Q2','Q1']
    qubitid=['Q3','Q2']
    qubitid=['Q5','Q0']
    qubitid=['Q6','Q5']
    qubitid=['Q1','Q0']
    tcr=c_tcr(qubitid=qubitid,calirepo='../../../../qchip',debug=2)
    tcrlist=numpy.arange(80e-9,320e-9,8e-9)
    repeat=1
    if 0:
        ampcr=numpy.arange(0.1,1.0,0.2)
       # ampcr=[0.8]#numpy.arange(0.1,1.0,0.2)
        rcrtgt=numpy.zeros(len(ampcr))
        rcrctl=numpy.zeros(len(ampcr))
#        pyplot.figure('cr')
        pyplot.ion()
        for ia,amp in enumerate(ampcr):
            tcr.seqs(tcrlist=tcrlist,repeat=repeat,amp=amp)
            tcr.run(100,combineorder=qubitid)
            result=tcr.fit()
            rcrtgt[ia]=result[qubitid[1]]['amp']
            rcrctl[ia]=result[qubitid[0]]['amp']
            tcr.plot(plotfit=True)
            pyplot.draw()
            print(ia,amp,rcrtgt[ia],result[qubitid[1]]['firstmaxt'],max(tcr.r[qubitid[1]]['1']))
            pyplot.pause(0.1)
        pyplot.ioff()
#        pyplot.figure('rcr')
#        pyplot.plot(ampcr,rcrtgt)
#        pyplot.plot(ampcr,rcrctl)
        print(ampcr,rcrtgt,rcrctl)
        pyplot.show()
#    print('tcr',result)
#    pyplot.show()
    if 1:
        tcrsel=300e-9#max(numpy.ceil(result[qubitid[1]]['firstmaxt']/16e-9)*16e-9,256e-9)
        print(tcrsel)
        updatedict=tcr.optimize(nsample=100,tcr=tcrsel,plot=True,nsteps=2,acenter=0.286,aspan=0.3)
        print(updatedict)
        tcr.opts['qchip'].updatecfg(updatedict,tcr.opts['qubitcfgfile'])
    if 0:
        updatedict=tcr.optimize(nsample=100,tcr=200e-9,plot=True,nsteps=5,acenter=0.7,aspan=0.4)
        print(updatedict)
        tcr.opts['qchip'].updatecfg(updatedict,tcr.opts['qubitcfgfile'])
    if 0:
        tcr.scanacr(nsample=100,repeat=1,ampchange=numpy.linspace(0,1,40),tcr=288e-9,plot=True)
#        tcr.scanacr(nsample=100,repeat=2,ampchange=numpy.linspace(0,1,40),tcr=256e-9,plot=True)
#    else:
    if 0:
        acrs=numpy.linspace(0.3,1.0,10)
        tcrmins=numpy.zeros(len(acrs))
        pyplot.ion()
        fig=pyplot.figure('each')
        for iacr,amp in enumerate(acrs):
            tcr.seqs(tcrlist=tcrlist,repeat=1,amp=amp)
            tcr.run(50,combineorder=qubitid)
            result=tcr.fit()
            print('tcramp',amp,result)
            pyplot.figure('each')
            tcr.plot(plotfit=True)
#            pyplot.show()
            tcrmins[iacr]=numpy.ceil(result[qubitid[1]]['period']/2/16e-9)*16e-9
        pyplot.ioff()
        fig=pyplot.figure()
        pyplot.plot(acrs,tcrmins)
        pyplot.show()
        print('tcrmin',tcrmins)
        tcrsel=float(input('tcr'))
        updatedict=tcr.optimize(nsample=100,tcr=tcrsel,plot=True,nsteps=2,acenter=0.7,aspan=0.4)
        print(updatedict)
        tcr.opts['qchip'].updatecfg(updatedict,tcr.opts['qubitcfgfile'])

        t1=datetime.datetime.now()
        print('tcr time: ', t1-t0)
        sys.stdout.flush()
    #pyplot.show()
