import sys
sys.path.append('../..')
from qubic.bids.register import c_register
from qubic.bids.mem_gateway import c_mem_gateway
from qubic.bids.ether import c_ether
import time
import json
try:
    basestring
except NameError:
    basestring = str
class c_regmap(c_mem_gateway):
    DWIDTH=32
#    def __init__(self,interface,regmappath='regmap.json',wavegrppath='wavegrp.json',min3=False,memgatewaybug=True,run=True):
    def __init__(self,interface,registers,wavegrp,min3=False,memgatewaybug=True,run=True):
        c_mem_gateway.__init__(self, interface=interface,min3=min3,memgatewaybug=memgatewaybug,run=run)
        #print regmappath
        #with open(regmappath) as jsonfile:
        self.regs={}
        self.regindex={}
        #jsonfile=open(regmappath)
        self.registers=registers#json.load(jsonfile)
        #jsonfile.close()
        for name in self.registers:
            #print name,self.registers[name]
            self.regs[name]=c_register(**dict({'name':name,'DWIDTH':self.DWIDTH},**self.registers[name]))
            #???#
            for addr in self.regs[name].readaddr():
                self.regindex[addr]=self.regs[name]
    #   print [hex(i) for i in self.regindex]
        #with open(wavegrppath) as jsonfile:
        #jsonfile=open(wavegrppath)
        self.wavegrp=wavegrp #json.load(jsonfile)
        #jsonfile.close()
    def wavecheckreadallreset(self,wavelist,resetafter=True,sleeptime=0.1):
        if not resetafter:
            for wavename in wavelist:
                status_reg=self.wavegrp[wavename]['status']['name']
                status_bit=self.wavegrp[wavename]['status']['bit']
                reset_reg=self.wavegrp[wavename]['reset']['name']
                #print('3rd step!!!!!!!!!!!!reset_reg!!!!!!',wavename,reset_reg)###
                #print 'reset',wavename
                self.write(((reset_reg,0),(reset_reg,0),(reset_reg,0)))
                self.read(status_reg)
                status=(self.regs[status_reg].getvalue()[0]>>status_bit)&0x1
                #print('1st step*******status_reg,self.regs[status_reg].getvalue()[0],status_bit,status**********',status_reg,self.regs[status_reg].getvalue()[0],status_bit,status)###
        timeout=500
        for wavename in wavelist:
            status_reg=self.wavegrp[wavename]['status']['name']
            status_bit=self.wavegrp[wavename]['status']['bit']
            self.read(status_reg)
            status=(self.regs[status_reg].getvalue()[0]>>status_bit)&0x1
            #print('1st step*******status_reg,self.regs[status_reg].getvalue()[0],status_bit,status**********',status_reg,self.regs[status_reg].getvalue()[0],status_bit,status)###
            index=0
            while (not status) and index<timeout:
                #print [hex(i) for i in self.read(((status_reg),))]
                self.read(status_reg)
                status=(self.regs[status_reg].getvalue()[0]>>status_bit)&0x1
                index=index+1
                #print('2nd step@@@@@@@@@@@@@@@@@@in loop status, index',status,index)###
                #print 'c_regmap wavecheckreadallreset index',index
                #self.read((('keep'),))
                #v3=self.regs['keep'].getvalue()[0]
                #self.read((('trace_status2'),))
                #v2=self.regs['trace_status2'].getvalue()[0]
                #v2=self.read(status_reg)[0]
#            print 'wavecheckreadreset',status_bit,status_reg,format(v,'08x'),status,'trace_status2:',format(v2,'08x'),'keep:',format(v3,'08x')
                if sleeptime:
                    time.sleep(sleeptime)
            if index==timeout:
                print('timeout on ',wavename)
#            print status_reg,status
            self.readregs((wavename,))
        if resetafter:
            for wavename in wavelist:
                reset_reg=self.wavegrp[wavename]['reset']['name']
                #print('3rd step!!!!!!!!!!!!reset_reg!!!!!!',wavename,reset_reg)###
                #print 'reset',wavename
                self.write(((reset_reg,0),(reset_reg,0),(reset_reg,0)))
        result=[]
        for wavename in wavelist:
            result.append(self.regs[wavename].getvalue())
        return result

    def wavecheckreadreset(self,wavename,length=None):
        status_reg=self.wavegrp[wavename]['status']['name']
        status_bit=self.wavegrp[wavename]['status']['bit']
        reset_reg=self.wavegrp[wavename]['reset']['name']
        self.read(status_reg)
        status=(self.regs[status_reg].getvalue()[0]>>status_bit)&0x1
        index=0
        while (not status) and index<10:
            #print [hex(i) for i in self.read(((status_reg),))]
            self.read(status_reg)
            status=(self.regs[status_reg].getvalue()[0]>>status_bit)&0x1
            index=index+1
            #print index
            #self.read((('keep'),))
            #v3=self.regs['keep'].getvalue()[0]
            #self.read((('trace_status2'),))
            #v2=self.regs['trace_status2'].getvalue()[0]
            #v2=self.read(status_reg)[0]
#            print 'wavecheckreadreset',status_bit,status_reg,format(v,'08x'),status,'trace_status2:',format(v2,'08x'),'keep:',format(v3,'08x')
            time.sleep(0.1)
#        print status_reg,status
        self.readregs((wavename,))
        self.write(((reset_reg,0),))
        return self.regs[wavename].getvalue()

    def readandwrite(self,opnamedatalist,addr=None):
        #        print opnamedatalist
        alist=[]
        dlist=[]
        wlist=[]
        for (o,n,d) in opnamedatalist:
            if o=='write':
                (a,d,w)=self.writeadw(((n,d),))
            elif o=='read':
                (a,d,w)=self.readadw(((n),))
            alist.extend(a)
            dlist.extend(d)
            wlist.extend(w)
        return self.rawadw(alist,dlist,wlist)
    def write(self,namedatalist):
        #(alist,dlist,wlist)=self.writeadw(namedatalist)
#        print 'write',alist,dlist,wlist
#        print len(namedatalist)
        nvlist=[(r if isinstance(r,basestring) else r.name,v) for r,v in namedatalist]
    #    rprint=[(r if isinstance(r,basestring) else r.name, v if (isinstance(v,int) or isinstance(v,float)) else v[0:min(10,len(v))]) for r,v in namedatalist]
    #    print('debug regmap write',rprint)
        #print 'regmap write',nvlist
        return self.writeregs(nvlist)
    def writeregs(self,namedatalist):
        adlist=[]
        for item in namedatalist:
            name=item[0]
            data=item[1]
            offset=(None if len(item)==2 else item[2])
            adlist.append(self.regs[name].write(data,offset))
            ###if name=='period_dac0':###
                ###print '((((((((((((name==period_dac0))))))))))))',data###
            ###if name=='start':###
                ###print '((((((((((((name==start))))))))))))',data###
        #print adlist
        (alist,dlist)=zip(*sum(adlist,[]))
        wlist=len(alist)*[1]
        return self.rawadw(alist,dlist,wlist)
    def readregs(self,names,offsets=None):
        #print names
        reg=[self.regs[name] for name in names]
#        print(type(reg),type(offsets))
#        print(reg)
        alist=sum([list(ba.readaddr(offset)) for (ba,offset) in zip(reg, len(reg)*[None]if offsets==None else offsets)],[])
        dlist=len(alist)*[0]
        wlist=len(alist)*[0]
        return self.rawadw(alist,dlist,wlist)
    def read(self,names,offsets=None,resetafter=True):
        #    print('debuf regmap read',names)
        if (isinstance(names,str) or isinstance(names,basestring)):
                names=[names]
        namelists=[]
        reglist=[]
        wavelist=[]
        reglist = [n for n in names if n not in self.wavegrp]
        wavelist = [n for n in names if n in self.wavegrp]
        if reglist:
            self.readregs(reglist)
        if wavelist:
            #print('0 step in read^^^^^^^^',wavelist)###
            self.wavecheckreadallreset(wavelist,resetafter=resetafter)

        #for name in names:
        #    if name in self.wavegrp:
        #        if reglist:
        #            namelists.append(reglist)
        #            reglist=[]
        #        namelists.append(name)
        #    else:
        #        reglist.append(name)
        #if reglist:
        #    namelists.append(reglist)
        #for regwave in namelists:
        #    if not isinstance(regwave,list):
        #        if regwave in self.wavegrp:
        #            self.wavecheckreadreset(regwave)
        #    else:
        #        self.readregs(regwave)
        #result=[]
        #for name in names:
        #    print name
        #    result.append({name:self.getregval(name)})
#
#        print 'read',alist,dlist,wlist
        #return result
    def rawadw(self,alist,dlist,wlist):
        result=self.readwrite(adwlist=list(zip(alist,dlist,wlist)))
        value=self.parse_readvalue(result)
        #print 'rawadw',len(alist),len(result),len(value)
        self.updatereadvalue(value)
        return value
    def updatereadvalue(self,value):
        #        result=[]
        for v in value:
            cmd=(v>>56)
            addr=((v>>32)&0xffffff)
            val=v&0xffffffff
            #has=self.regindex.has_key(addr)
            #print 'has',has,hex(addr)
            if addr in self.regindex and cmd==0x10:
                self.regindex[addr].setvalue(val,addr)
            #    result.append((self.regindex[addr].name,self.regindex[addr].getvalue()))
        #return result
#        print result
    def getregval(self,name):
        return self.regs[name].getvalue()

if __name__=="__main__":
    #interface=c_ether('192.168.1.122',port=3000)
    #regmap=c_regmap(interface, min3=True,memgatewaybug=False)
    #regmap.read(['full','adc0_minmax'])
    #print [regmap.getregval(i) for i in ['full','adc0_minmax']]
    #print regmap.wavecheckreadreset('buf_monout_0')
    #print regmap.read('buf_monout_0')
    #print regmap.write((('valid',0),))
    #print [hex(i) for i in regmap.write((('command',[(4<<96)+(3<<64)+(2<<32)+1,(5<<96)+(6<<64)+(7<<32)+8]),))]
    interface=c_ether('192.168.1.124',port=3000)
    jsonfile=open('../qubic/regmap.json')
    registers=json.load(jsonfile)
    jsonfile.close()
    jsonfile=open('../qubic/wavegrp.json')
    wavegrp=json.load(jsonfile)
    jsonfile.close()
    regmap=c_regmap(interface, registers=registers,wavegrp=wavegrp,min3=True,memgatewaybug=False)
    regmap.read(['period_dac0'])
    print([regmap.getregval(i) for i in ['period_dac0']])
    regmap.write((('period_dac0',1000000000),))
    #regmap.read(['full','adc0_minmax'])
    #print([regmap.getregval(i) for i in ['full','adc0_minmax']])
    #print(regmap.wavecheckreadreset('buf_monout_0'))
    #print(regmap.read('buf_monout_0'))
    #print(regmap.write((('valid',0),)))
    #print([hex(i) for i in regmap.write((('command',[(4<<96)+(3<<64)+(2<<32)+1,(5<<96)+(6<<64)+(7<<32)+8])))])
