import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment 
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit

class c_ramsey(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        #,chassis=None,wiremap=None,qchip=None
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,framsey=None,logx=False,ef=False,**kwargs):
        opts=dict(elementlength=80,elementstep=1.8e-6)
        opts.update(**{k:v for k,v in kwargs.items() if k in opts})
        print("EFEFEFEFEFEFEFEF",ef)
        if framsey is None:
            gramsey={'name': 'X90_ef' if ef else 'X90', 'qubit': qubitid}
        else:
            gramsey={'name': 'X90_ef' if ef else 'X90', 'qubit': qubitid,'modi' : [{'fcarrier':framsey}]}

        seqs=[]
        if logx:
            t=numpy.logspace(0,numpy.log10(opts['elementlength']*opts['elementstep']/4e-9),num=opts['elementlength']).astype(int)*4e-9
        else:
            t=numpy.arange(opts['elementlength'])*opts['elementstep']
        for tdelay in t:
            seq=[]
            if ef :
                seq.append({'name': 'X90', 'qubit': qubitid})
                seq.append({'name': 'X90', 'qubit': qubitid})
            seq.append(gramsey)
            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': tdelay}})
            seq.append(gramsey)
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
#        print(seqs)
        return t,seqs

    def framsey(self,framseydelta):
        return None if framseydelta is None else (self.opts['qchip'].getfreq(self.opts['qubitid'][0]+('.freq_ef' if self.ef else '.freq') )+framseydelta)
    def run(self,nsample):
        experiment.c_experiment.accbufrun(self,nsample=nsample)
        self.psingle=self.accresult['countsum']['psingle']


    def fit(self):
        #try:
        state='2' if self.ef else '1'
        estsine=fit.sinestimate(x=self.t,y=self.psingle[self.opts['qubitid'][0]][state],p=0)#numpy.pi if self.ef else 0)
        est=list(estsine)
        est.append(self.t[-1])
        self.debug(4,'est fit',est,estsine)
        #except:
        #    est=None
        #    print('error 0')
        fitpara1=fit.fitexpsin(x=self.t,y=self.psingle[self.opts['qubitid'][0]][state],p0=est,bounds=(([0,-numpy.inf,0,-1,1e-9],[2,numpy.inf,2*numpy.pi,2,numpy.inf])))
        popt1,pcov1,yfit1,rsqr1=[fitpara1['popt'],fitpara1['pcov'],fitpara1['yfit'],fitpara1['rsqr']]
        err1=numpy.sqrt(numpy.diag(pcov1))
        fitpara2=fit.fitexp(x=self.t,y=self.psingle[self.opts['qubitid'][0]][state],p0=[1,self.t[-1],0.5],bounds=(([0,1e-9,0],[2,numpy.inf,2])))
        popt2,pcov2,yfit2,rsqr2=[fitpara2['popt'],fitpara2['pcov'],fitpara2['yfit'],fitpara2['rsqr']]
        err2=numpy.sqrt(numpy.diag(pcov2))
        goodfit=0.8
        print('expsin %8.3f'%rsqr1,'exp %8.3f'%rsqr2)
        if rsqr1 >=goodfit and rsqr2 >=goodfit :
            xy=numpy.zeros((len(self.t),2))
            xy[:,0]=self.t
            xy[:,1]=self.psingle[self.opts['qubitid'][0]][state]
            numpy.savetxt('ramseyerror.dat',xy)
            self.debug(0,"Error: No good fit for ramsey %8.3f %8.3f"%(rsqr1,rsqr2))
            [amp,foffset,foffseterr,t2ramsey,t2ramseyerr,self.yfit,rsqr]=[0,0,0,0,0,[0],1]
        elif rsqr1 < goodfit and rsqr2 <goodfit :
            [amp,foffset,foffseterr,t2ramsey,t2ramseyerr,self.yfit,rsqr]=[popt1[0],popt1[1],err1[1],popt1[4],err1[4],yfit1,rsqr1] if rsqr1<rsqr2*0.9 else        [popt2[0],0,0,popt2[1],err2[1],yfit2,rsqr2]
        elif rsqr1 < goodfit :
            [amp,foffset,foffseterr,t2ramsey,t2ramseyerr,self.yfit,rsqr]=[popt1[0],popt1[1],err1[1],popt1[4],err1[4],yfit1,rsqr1] 
        elif rsqr2 < goodfit :
            [amp,foffset,foffseterr,t2ramsey,t2ramseyerr,self.yfit,rsqr]=[popt2[0],0,0,popt2[1],err2[1],yfit2,rsqr2]
        else:
            self.debug(0,'Error: fitting error??')
        self.result.update(dict(amp=amp,foffset=foffset,foffseterr=foffseterr,t2ramsey=t2ramsey,t2ramseyerr=t2ramseyerr,rsqr=rsqr))
        self.debug(4,'expsin',popt1,err1)
        self.debug(4,'exp',popt2,err2)
        self.debug(4,'select','expsin' if err1[0]<err2[0] else 'exp')
#        pyplot.plot(self.psingle[self.opts['qubitid'][0]]['1'])
#        pyplot.plot(self.t,self.yfit)
#        pyplot.show()
        return self.result


    def seqs(self,framseydelta=None,**kwargs):
        self.opts['ef']=False
        self.opts.update(kwargs)
        self.ef=self.opts['ef']
        self.debug(5,'ramsey seqs',self.opts)
        self.t,self.opts['seqs']=self.baseseqs(framsey=self.framsey(framseydelta),**self.opts)
        self.compile()

    def plot(self,fig):
        plot.psingleplot(x=self.t,psingle=self.accresult['countsum']['psingle'],fig=fig,marker='*',linewidth=0)
        return fig

    def plottyfit(self,fig,**kwargs):
        self.debug(5,self.t, self.yfit)
        if self.result['rsqr']<1:
            fig.plot(self.t,self.yfit,**kwargs)
        return fig
    def optimize(self,nsample,**kwargs):
        self.opts.update(kwargs)
        if self.opts['plot']:
            pyplot.ion()
            fig=pyplot.figure('ramsey optimize')
            pyplot.clf()
            ax=fig.subplots(3,1)
            #pyplot.show()
            pyplot.pause(0.1)
        framseydelta=0
        foffsets=[]
        for i in range(3):
            self.seqs(framseydelta=framseydelta,**self.opts)
            self.debug(4,'framseydelta',framseydelta)
            if i==0:
                run=True
            elif foffsets[0]==0:
                run=False
            else:
                run=True
            if run:
                self.run(nsample)
                self.fit()
                foffset=self.result['foffset']
            foffsets.append(foffset)
            framseydelta=foffsets[0] if i==0 else -foffsets[0]

            if self.opts['plot']:
                print('ramsey optimize plot')
#                pyplot.figure('ramsey optimize')
                self.plot(fig=ax[i])
                self.plottyfit(fig=ax[i])
            #    ax[i].draw(renderer=fig.canvas.renderer)
                pyplot.draw()
            #    pyplot.show()
                pyplot.pause(0.1)
                #input('next')
        self.debug(4,'foffsets',foffsets)
        freq=self.opts['qchip'].getfreq(self.opts['qubitid'][0]+('.freq_ef' if self.ef else '.freq'))
        freqnew= freq+foffsets[0] if abs(foffsets[1])<abs(foffsets[2]) else freq-foffsets[0]
        updatedict={('Qubits',self.opts['qubitid'][0],'freq_ef' if self.ef else 'freq'):freqnew}
        print('old %10.9e new %10.9e delta %10.9e'%(freq,freqnew,freqnew-freq))
        if self.opts['plot']:
            pyplot.ioff()
        return updatedict


    def gmmplot(self,fig):
        for kq,gmix in self.accresult['gmixs'].items():
            plot.gmmplot(gmixs=gmix,fig=fig)
        return fig

    def iqplot(self,fig):
        accvaliq=self.accresult['accval'][self.opts['qubitid'][0]]
        return plot.iqplot(iq=accvaliq,fig=fig)


if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('-log','--log',help='use log',dest='log',default=False,action='store_true')
    parser.add_argument(help='qubitid list',dest='qubitid')
    parser.add_argument('-o','--optimize',help='optimize',dest='optimize',default=False,action='store_true')
    parser.add_argument('-dt','--dt',help='tstep',dest='dt',default=0.1e-6,type=float)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=100,type=int)
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-g','--gmixs',help='gmixs: None: use data\n default: use the cali path\ndict: use the dict from gmixdict\n or path to gmix folder',dest='gmixs',type=str,default='default')
    clargs=parser.parse_args()
    ramsey=c_ramsey(qubitid=clargs.qubitid,calirepo=clargs.cpath,gmixs=clargs.gmixs,debug=1,ef=clargs.ef)
    if clargs.optimize:
        updatedict=ramsey.optimize(clargs.nshot,elementstep=clargs.dt,elementlength=80,logx=False,plot=True)
        print(updatedict)
        ramsey.opts['qchip'].updatecfg(updatedict,ramsey.opts['qubitcfgfile'])
    else:
        ramsey.seqs(framseydelta=0,elementstep=clargs.dt,elementlength=80,logx=clargs.log,ef=clargs.ef)
        ramsey.run(clargs.nshot)
        print(ramsey.fit())
        fig1=pyplot.figure(figsize=(15,15))
        sub=fig1.subplots(1,1)
        ramsey.plot(fig=sub)
        ramsey.plottyfit(fig=sub,label='fit')
        sub.set_title(clargs.qubitid)
        sub.legend()
    #ramsey.plottyfit(fig=sub)
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'ramsey_ef' if ramsey.ef else 'ramsey'))
        pyplot.savefig(title+'.pdf')
        #pyplot.savefig('fig1.pdf')
    figiq=pyplot.figure("iq")
    subiq=figiq.subplots()
    ramsey.iqplot(subiq)
    ramsey.gmmplot(subiq)
    if clargs.noplot:
        pass
    else:
        pyplot.show()

