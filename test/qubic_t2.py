import sys
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
def modulation(length=1024,scale=32767,usepad=True):
	if usepad:
		length = length-4
	t=1.0*numpy.arange(length)
	m1=-abs(t-length/2.0)**2
	m1=m1/max(abs(m1))
	m1=m1-min(m1)
	pad=numpy.zeros(4)
	m2=numpy.exp(-(((t-length/2.0))/(length/5.0))**2)
	m2=m2/max(m2)
	m3=numpy.ones(length)/2.0
	m4=length/2.0-abs(t-length/2.0)
	m4=m4/max(m4)
	if usepad:
		result=(numpy.concatenate((pad,m3,pad,m3,pad,m3,pad,m3))*scale).astype(int)
	else:
		result=(numpy.concatenate((m3,m3,m3,m3))*scale).astype(int)
	return  result
def to32bits(arrayin):
	return arrayin
def cmdgen(trig_t,element,dest,start,length,phini,freq):
	ph_binary = int(phini/360.0*2**14)
	#freq_binary=int(1e-9*freq*2**24)
	freq_binary=int(1.0*freq*2**22/250e6)
	cmd={trig_t:((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)}
	print(trig_t,element,dest,start,length,ph_binary,freq_binary,hex(cmd[trig_t]))
	return cmd
def cmdadd(cmddict,newcmddict):
	commonkeys=set(cmddict.keys())&set(newcmddict.keys())
	if not commonkeys:
		cmddict.update(newcmddict)
	else:
		print
		print("ERROR: commonkeys ",commonkeys)
		print
	return cmddict
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg = c_mem_gateway(interface, min3=True,memgatewaybug=False)
	tslice=4
	nwave=4
	nel=8
	#amp_list=[]
	comp_list=[]
	#x_list=[]
	#y_list=[]
	freq_start=float(sys.argv[1])*1e6
	freq_stop=float(sys.argv[2])*1e6
	freq_step=float(sys.argv[3])*1e6
	freq_list=numpy.arange(freq_start,freq_stop,freq_step)
	alist=[]
	dlist=[]
	alist.extend(range(0x38000,0x40000))
	dlist.extend((0x40000-0x38000)*[19897])  # amplitude to internal lo for demodulating
	#dlist.extend((0x40000-0x38000)*[2000])  # amplitude to internal lo for demodulating
	alist.extend(range(0x41000,0x50000))
	dlist.extend((0x50000-0x41000)*[19897])  # amplitude to internal lo for demodulating
	#dlist.extend((0x50000-0x41000)*[2000])  # amplitude to internal lo for demodulating
	alist.append(0x50000)
	dlist.append(0xff)
	period=300000/4
	alist.extend((0x7,0x8,16))  # period_adc,period_dac0,period_stream
	dlist.extend((period,period,period))
	bufsel=(0b010<<12)+(0b010<<9)+(0b010<<6)+0b010
	alist.append(0x50001)
	dlist.append(bufsel)
	digiloopback=1
	alist.append(0x50004)
	dlist.append(digiloopback)
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	for frequency in freq_list:
		alist=[]
		dlist=[]
		#pulselength=20
		cmddict={}
		#cmdadd(cmddict,cmdgen(trig_t=14,element=2,dest=3,start=25,length=pulselength,phini=0,freq= 66.47885e6))
		#cmdadd(cmddict,cmdgen(trig_t=56,element=4,dest=0,start=0x800/4,length=pulselength*4,phini=0,freq= 210e6))
		cmdadd(cmddict,cmdgen(trig_t=0,element=0,dest=3,start=0,length=1,phini=30,freq=frequency))
		#cmdadd(cmddict,cmdgen(trig_t=100,element=0,dest=0,start=0,length=500,phini=30-40e-9*frequency*360,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=100,element=0,dest=0,start=0,length=500,phini=30-24e-9*frequency*360,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=106,element=8,dest=1,start=0,length=500,phini=30,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=110,element=1,dest=1,start=0,length=500,phini=30,freq=frequency))
		cmdlist=[v for k,v in sorted(cmddict.items())]
		for i in range(len(cmdlist)):
			alist.extend([0x40000+2*i,0x40400+2*i,0x40400+2*i+1])
			dlist.extend([(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff])
			print([hex(j) for j in [0x40000+2*i,0x40400+2*i,0x40400+2*i+1]])
			print([hex(j) for j in [(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff]])
		result=mg.readwrite(alist=alist,dlist=dlist,write=1)

		alist=[(0x50002)]
		dlist=[(1)]
		result=mg.readwrite(alist=alist,dlist=dlist,write=1)
		time.sleep(1.5)  # 0x50005 full (need to be done later)
		alist=[]
		dlist=[]
		alist.extend(range(0x42000,0x42000+0x1000))
		dlist=numpy.zeros(len(alist))
		result=mg.readwrite(alist=alist,dlist=dlist,write=0)
		numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
		xy8=numpy.array([signvalue(i&0xffffffff,32) for i in mg.parse_readvalue(result)])
		#amp=[]
		#for i in xy8.reshape([-1,2]):
		#	amp.append((i[0]**2+i[1]**2)**0.5)
		xy82=xy8.reshape([-1,2])
		xy8comp=xy82[:,0]+1j*xy82[:,1]
		print('xy8comp',xy8comp)
		print('abs(xy8comp.mean())',abs(xy8comp.mean()))
		comp_list.append(xy8comp.mean())
		#amp=numpy.mean(amp)
		#print 'amp',amp
		#amp_list.append(amp)
		xy8=numpy.transpose(xy8.reshape([-1,2]))
		x=xy8[0]
		y=xy8[1]
		#x_list.append(x.mean())
		#y_list.append(y.mean())
		#pyplot.hexbin(x,y,bins=50,cmap='Greys',extent=[0,250000,0,250000])
		#pyplot.hexbin(x,y,bins=50,cmap='Greys')
	comp_array=numpy.array(comp_list)
	pyplot.figure(1)
	#pyplot.plot(freq_list,amp_list,'o')
	pyplot.subplot(211)
	pyplot.plot(freq_list,abs(comp_array),'o')
	#pyplot.plot(freq_list,amp_list,'bo')
	pyplot.subplot(212)
	pyplot.plot(freq_list,numpy.angle(comp_array),'o')
	pyplot.figure(2)
	#pyplot.plot(x_list,y_list,'o')
	pyplot.plot(comp_array.real,comp_array.imag,'o')
	pyplot.show()
	#numpy.savetxt('freq_comp.txt',numpy.column_stack((freq_list,abs(comp_array))))
