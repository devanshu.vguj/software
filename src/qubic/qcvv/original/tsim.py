import numpy
from matplotlib import pyplot
import trueq
import re
import json
import sys
from scipy.optimize import curve_fit

config = trueq.Config("""
        Name: MyDevice
        N_Systems: 2
        Mode: ZXZXZ
        Dimension: 2
        Gate Z(phi):
            Hamiltonian:
                - ["Z", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate X(phi):
            Hamiltonian:
                - ["X", phi]
            Involving: {'(5,)': (), '(6,)': ()}
        Gate CNOT:
            Matrix:
                - [1, 0, 0, 0]
                - [0, 1, 0, 0]
                - [0, 0, 0, 1]
                - [0, 0, 1, 0]
            Involving:
                (6, 5) : ()
        """
    )

def z(p1deg,p2deg):
	return numpy.kron(config.Z(p1deg).mat,config.Z(p2deg).mat)

def x(p1deg=90,p2deg=90):
	return numpy.kron(config.X(p1deg).mat,config.X(p2deg).mat)

def cnot():
	return config.CNOT().mat

def sini(s00=1,s01=0,s10=0,s11=0):
	return numpy.array([[s00,s01,s10,s11]]).T

def op1(z1p1deg,z1p2deg,z2p1deg,z2p2deg,z3p1deg,z3p2deg):
	return cnot()@z(z3p1deg,z3p2deg)@x()@z(z2p1deg,z2p2deg)@x()@z(z1p1deg,z1p2deg)

def op2(pdeg,p1deg,p2deg):
	return x()@z(pdeg,pdeg)@z(p1deg,p2deg)

def prob(s):
	return numpy.square(abs(s))

def fullxymeas(pdeg,p1deg,p2deg,z1p1deg,z1p2deg,z2p1deg,z2p2deg,z3p1deg,z3p2deg,s00,s01,s10,s11):
	'''Qubit Map:(control,target)'''
	return numpy.array([prob(op2(p,p1deg,p2deg)@op1(z1p1deg,z1p2deg,z2p1deg,z2p2deg,z3p1deg,z3p2deg)@sini(s00,s01,s10,s11)) for p in pdeg])

def fullxymeas_flat(pdeg,p1deg,p2deg,z1p1deg,z1p2deg,z2p1deg,z2p2deg,z3p1deg,z3p2deg,s00,s01,s10,s11):
	return fullxymeas(pdeg[0:int(len(pdeg)/4)],p1deg,p2deg,z1p1deg,z1p2deg,z2p1deg,z2p2deg,z3p1deg,z3p2deg,s00,s01,s10,s11).flatten('F')


if __name__=="__main__":
	f=open(sys.argv[1])
	s=f.read().replace("'",'"')
	f.close()
	measphdeg=[]
	Meas=[]
	m00=[]
	m01=[]
	m10=[]
	m11=[]
	for l in s.split('\n')[:-1]:
		m1=re.match("cnotphdeg (\d+) measphdeg (\d+) Sim (\{[\s\S]+\}) Meas (\{[\s\S]*\})",l)
		m2=re.match("measphdeg( )(\d+) Sim (\{[\s\S]+\}) Meas (\{[\s\S]*\})",l)
		if m1:
			m=m1
		if m2:
			m=m2
		if m:
			g=m.groups()
			meas=json.loads(g[3])
			m00.append(meas['00'] if '00' in meas.keys() else 0)
			m01.append(meas['01'] if '01' in meas.keys() else 0)
			m10.append(meas['10'] if '10' in meas.keys() else 0)
			m11.append(meas['11'] if '11' in meas.keys() else 0)
			measphdeg.append(float(g[1]))
		else:
			print('not match',l)
	xall=measphdeg
	xfit=measphdeg[:-1]
	z1p1deg=-180
	z1p2deg=-22.73120305953306
	z2p1deg=100.31578875780873
	z2p2deg=68.67682384718997
	z3p1deg=-127.12950576043217
	z3p2deg=-8.661934773590986
	s00=1
	s01=0
	s10=0
	s11=0
	xdata=numpy.tile(numpy.array(xfit),4)
	'''TrueQ Meas Qubit Map: (Q5,Q6), Q5 for target, Q6 for control'''
	ydata=numpy.array([m00[:-1],m10[:-1],m01[:-1],m11[:-1]]).flatten()
	popt,pcov=curve_fit(lambda pdeg,p1deg,p2deg:fullxymeas_flat(pdeg,p1deg,p2deg,z1p1deg=z1p1deg,z1p2deg=z1p2deg,z2p1deg=z2p1deg,z2p2deg=z2p2deg,z3p1deg=z3p1deg,z3p2deg=z3p2deg,s00=s00,s01=s01,s10=s10,s11=s11),xdata,ydata,bounds=(([-360,-360],[360,360])))
	print('popt',popt)
	results=fullxymeas_flat(xdata,*popt,z1p1deg=z1p1deg,z1p2deg=z1p2deg,z2p1deg=z2p1deg,z2p2deg=z2p2deg,z3p1deg=z3p1deg,z3p2deg=z3p2deg,s00=s00,s01=s01,s10=s10,s11=s11).reshape((4,-1))
	print('results',results)
	pyplot.figure()
	pyplot.plot(xfit,results[0],label='Fit00',color='b',linestyle='--')
	pyplot.plot(xfit,results[2],label='Fit01',color='orange',linestyle='--') #results[2] to Fit01
	pyplot.plot(xfit,results[1],label='Fit10',color='g',linestyle='--')      #results[1] to Fit10
	pyplot.plot(xfit,results[3],label='Fit11',color='r',linestyle='--')
	pyplot.plot(xall,m00,label='Meas00',color='b',marker='*',linestyle='None')
	pyplot.plot(xall,m01,label='Meas01',color='orange',marker='*',linestyle='None')
	pyplot.plot(xall,m10,label='Meas10',color='g',marker='*',linestyle='None')
	pyplot.plot(xall,m11,label='Meas11',color='r',marker='*',linestyle='None')
	pyplot.ylim([-0.1,1.1])
	pyplot.axvline(0,linestyle='--',color='k')
	pyplot.axvline(270,linestyle='--',color='k')
	pyplot.axvline(360,linestyle='--',color='k')
	pyplot.legend()
	pyplot.xlabel('Rotation Angle (deg)')
	pyplot.ylabel('Population')
	pyplot.title('Full XY-Plane Measurement')
	pyplot.show()
