# run in python3

import trueq as tq
import numpy
import datetime
import sys
from matplotlib import pyplot

timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')

config = tq.Config(
    """
    Name: MyDevice
    N_Systems: 2
    Mode: ZXZXZ
    Dimension: 2
    Gate Z(phi):
        Hamiltonian:
            - ["Z", phi]
        Involving: {'(5,)': (), '(6,)': ()}
    Gate X(phi):
        Hamiltonian:
            - ["X", phi]
        Involving: {'(5,)': (), '(6,)': ()}
    Gate CNOT:
        Matrix:
            - [1, 0, 0, 0]
            - [0, 1, 0, 0]
            - [0, 0, 0, 1]
            - [0, 0, 1, 0]
        Involving:
            (6, 5) : ()
    """
)

transpiler = tq.compilation.get_transpiler(config)
if sys.argv[1]=='1q':
	qubitid_list = ['Q5']
else:
	qubitid_list = ['Q6', 'Q5']
circuit_length = [4, 32, 128]
n_circuits = 20
circuits = tq.make_srb([[int(x[1:]) for x in qubitid_list]], circuit_length, n_circuits)
transpiled_circuit = transpiler.compile(circuits)
circuits.save('circuits_'+timestamp+'.bin')

with open('circuits_'+timestamp+'.txt','a') as f:
	f.write(' '.join(qubitid_list)+'\n')
	f.write(' '.join(map(str,circuit_length))+'\n')
	f.write(str(n_circuits)+'\n')
	f.close()

#for i in range(n_circuits):
#	for j in range(len(circuit_length)):
#		index=j*n_circuits+i
for index in range(len(transpiled_circuit)):
	for k in range(len(transpiled_circuit[index])):
		gate_list=[]
		if len(list(transpiled_circuit[index][k].meas.keys()))==0:
			if len(qubitid_list)==1:
				if len(list(transpiled_circuit[index][k].gates.keys()))==1:
					qubitid_tmp='Q'+str(list(transpiled_circuit[index][k].gates.keys())[0][0])
					name_tmp=str(list(transpiled_circuit[index][k].gates.values())[0].name.upper())
					phi_tmp=str(list(transpiled_circuit[index][k].gates.values())[0].parameters['phi'])
					gate_list.append(qubitid_tmp+name_tmp+phi_tmp)
				elif len(list(transpiled_circuit[index][k].gates.keys()))==0:
					qubitid_tmp=qubitid_list[0]
					name_tmp='pass'
					gate_list.append(qubitid_tmp+name_tmp)
				else:
					print('unexpected condition: len(list(transpiled_circuit[index][k].gates.keys()))',len(list(transpiled_circuit[index][k].gates.keys())))
			elif len(qubitid_list)==2:
				if len(list(transpiled_circuit[index][k].gates.keys()))==2:
					for m in range(len(qubitid_list)):
						qubitid_tmp='Q'+str(list(transpiled_circuit[index][k].gates.keys())[m][0])
						name_tmp=str(list(transpiled_circuit[index][k].gates.values())[m].name.upper())
						phi_tmp=str(list(transpiled_circuit[index][k].gates.values())[m].parameters['phi'])
						gate_list.append(qubitid_tmp+name_tmp+phi_tmp)
					gate_list=gate_list if 'Q'+str(list(transpiled_circuit[index][k].gates.keys())[0][0])==qubitid_list[0] else gate_list[::-1]
				elif len(list(transpiled_circuit[index][k].gates.keys()))==1:
					if len(list(transpiled_circuit[index][k].gates.keys())[0])==1:
						qubitid0_tmp='Q'+str(list(transpiled_circuit[index][k].gates.keys())[0][0])
						name0_tmp=str(list(transpiled_circuit[index][k].gates.values())[0].name.upper())
						phi0_tmp=str(list(transpiled_circuit[index][k].gates.values())[0].parameters['phi'])
						qubitid1_tmp=[x for x in qubitid_list if x!=qubitid0_tmp][0]
						name1_tmp='pass'
						gate_list_tmp=[qubitid0_tmp+name0_tmp+phi0_tmp,qubitid1_tmp+name1_tmp]
						gate_list.extend(gate_list_tmp if qubitid0_tmp==qubitid_list[0] else gate_list_tmp[::-1])
					elif len(list(transpiled_circuit[index][k].gates.keys())[0])==2:
						qubitid0_tmp='Q'+str(list(transpiled_circuit[index][k].gates.keys())[0][0])
						qubitid1_tmp='Q'+str(list(transpiled_circuit[index][k].gates.keys())[0][1])
						name_tmp=str(list(transpiled_circuit[index][k].gates.values())[0].name.upper())
						gate_list.extend([qubitid0_tmp+qubitid1_tmp+name_tmp]*2)
					else:
						print('unexpected condition: len(list(transpiled_circuit[index][k].gates.keys())[0])',len(list(transpiled_circuit[index][k].gates.keys())[0]))
				elif len(list(transpiled_circuit[index][k].gates.keys()))==0:
					for m in range(len(qubitid_list)):
						qubitid_tmp=qubitid_list[m]
						name_tmp='pass'
						gate_list.append(qubitid_tmp+name_tmp)
				else:
					print('unexpected condition: len(list(transpiled_circuit[index][k].gates.keys()))',len(list(transpiled_circuit[index][k].gates.keys())))
			else:
				print('unexpected condition: current software does not support 3 or more qubits')
		else:
			for m in range(len(qubitid_list)):
				qubitid_tmp='Q'+str(list(transpiled_circuit[index][k].meas.keys())[m][0])
				name_tmp='meas'
				gate_list.append(qubitid_tmp+name_tmp)
			gate_list=gate_list if 'Q'+str(list(transpiled_circuit[index][k].meas.keys())[0][0])==qubitid_list[0] else gate_list[::-1]
		with open('circuits_'+timestamp+'.txt','a') as f:
			f.write(' '.join(gate_list)+'\n')
			f.close()


#for i in range(n_circuits):
#	for j in range(len(circuit_length)):
#		index=j*n_circuits+i
for index in range(len(transpiled_circuit)):
	print(transpiled_circuit[index])
circuits=numpy.loadtxt('circuits_'+timestamp+'.txt',dtype=str,skiprows=3).tolist()
if len(qubitid_list)==1:
	idx_list=[idx+1 for idx,val in enumerate(circuits) if val=='Q5meas']
else:
	idx_list=[idx+1 for idx,val in enumerate(circuits) if val==['Q6meas','Q5meas']]
print('idx_list',idx_list)
rb_gates=[circuits[i: j] for i, j in zip([0]+idx_list, idx_list+([len(circuits)] if idx_list[-1]!=len(circuits) else []))]
print(rb_gates)

#sim = tq.Simulator().add_stochastic_pauli(px=0.01)
#sim.run(circuits)
#circuits.fit().summarize()
#circuits.plot.raw()
#pyplot.show()
