import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.fit import fit


class c_spinecho(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,qubitid,ef=False,**kwargs):
        opts=dict(elementlength=80,elementstep=4e-6)
        opts.update(**{k:v for k,v in kwargs.items() if k in opts})
        self.ef=ef

        X90={'name': 'X90', 'qubit': qubitid}
        X90_ef={'name': 'X90_ef', 'qubit': qubitid}

        seqs=[]
        t=numpy.arange(opts['elementlength'])*opts['elementstep']
        for tdelay in t/2:
            seq=[]
            if ef:
                seq.append(X90)
                seq.append(X90)
            seq.append(X90_ef if ef else X90)
            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': tdelay}})
            seq.append(X90_ef if ef else X90)
            seq.append(X90_ef if ef else X90)
            seq.append({'name': 'delay', 'qubit': qubitid, 'para': {'delay': tdelay}})
            seq.append(X90_ef if ef else X90)
            seq.append({'name': 'read', 'qubit': qubitid})
            seqs.append(seq)
        return t,seqs


    def seqs(self,fspinechodelta=None,**kwargs):
        self.opts.update(kwargs)
        self.t,self.opts['seqs']=self.baseseqs(**self.opts)
        self.compile()
    def run(self,nsample):
        experiment.c_experiment.accbufrun(self,nsample=nsample)
        self.psingle=self.accresult['countsum']['psingle']

    def plot(self,ax):
        plot.psingleplot(x=self.t,psingle=self.psingle,fig=ax)
        return ax 
    def fit(self):
        est=[-1,self.t[-1]/2,1]
        fitpara=fit.fitexp(x=self.t,y=self.psingle[self.opts['qubitid'][0]]['2' if self.ef else '1' ],p0=est,bounds=(([-numpy.inf,0,0],[0,numpy.inf,1])))
        popt,pcov,self.yfit,rsqr=[fitpara['popt'],fitpara['pcov'],fitpara['yfit'],fitpara['rsqr']]
        err=numpy.sqrt(numpy.diag(pcov))
        self.result.update(dict(amp=popt[0],l=popt[1],c=popt[2],t2spinecho=popt[1],t2spinechoerr=err[1]))
        return self.result

    def plottyfit(self,ax):
        ax.plot(self.t,self.yfit,label='fit 1')
        return ax


if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(help='qubitid list',dest='qubitid')
    parser.add_argument('-ef','--ef',help='use ef',dest='ef',default=False,action='store_true')
    parser.add_argument('-dt','--dt',help='tstep',dest='dt',default=1e-6,type=float)
    parser.add_argument('-n','--nshot',help='number of shot',dest='nshot',default=400,type=int)
    parser.add_argument('-s','--savefig',help='save figure plot',dest='savefig',default=False,action='store_true')
    parser.add_argument('-np','--noplot',help='no plot',dest='noplot',default=False,action='store_true')
    parser.add_argument('-c','--calipath',help='path to calibration repo',dest='cpath',default='../../../../qchip',type=str)
    parser.add_argument('-g','--gmixs',help='gmixs: None: use data\n default: use the cali path\ndict: use the dict from gmixdict\n or path to gmix folder',dest='gmixs',type=str,default='default')
    clargs=parser.parse_args()
    
    spinecho=c_spinecho(qubitid=clargs.qubitid,calirepo=clargs.cpath,gmixs=clargs.gmixs)
    spinecho.seqs(elementstep=clargs.dt,elementlength=40,ef=clargs.ef)
    spinecho.run(clargs.nshot)
    spinecho.fit()
    print(spinecho.result)
    fig1=pyplot.figure(figsize=(15,15))
    ax=fig1.subplots(1,1)
    spinecho.plot(ax=ax)
    spinecho.plottyfit(ax=ax)
    ax.set_xlabel('Qubit drive pulse length (s)')
    ax.set_ylabel('Probability')
    ax.set_title('%s_spinecho'%clargs.qubitid)
    if clargs.savefig:
        tstr=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S')
        title=('%s_%s'%(clargs.qubitid,'spinecho_ef' if spinecho.ef else 'spinecho'))
        pyplot.savefig(title+'.pdf')
        #pyplot.savefig('fig1.pdf')
    if clargs.noplot:
        pass
    else:
        pyplot.show()
