import os
import yaml
import pickle
import json
from .qasmrun import qasmrun
zxzxz01={
'Name': 'MyDevice',
'N_Systems': 2,
'Mode': 'ZXZXZ',
'Dimension': 2,
'Gates': [
    {'z': {'Hamiltonian': [['Z', 'phi']], 'Involving': {'(0,)': '()', '(1,)': '()','(2,)': '()'}}},
    {'x': {'Hamiltonian': [['X', 90]], 'Involving': {'(0,)': '()', '(1,)': '()', '(2,)': '()'}}},
    {'cnot': {'Involving': {'(1, 0)': '()','(1,2)':'()'},
            'Matrix': [[1, 0, 0, 0],
                          [0, 1, 0, 0],
                       [0, 0, 0, 1],
                       [0, 0, 1, 0]]
            }
    }
    ],
}


def trueqqasm(nsample,trueqcircfilename,heraldfilename,qubitsmap,**kwargs):
    ops={}
    ops['combineorder']=None
#    qstr=[]
    ops['runpickle']=False
    ops['configdict']=zxzxz01
    ops.update(kwargs)
    if os.path.isfile(trueqcircfilename+'.qasm.pbz2'):
        ops['runpickle']=True
        ops['snapshotfilename']=trueqcircfilename+'.qasm.pbz2'
        qasms=''

    elif os.path.isfile(trueqcircfilename+'.qasm'):
        with open(trueqcircfilename+'.qasm') as jfile:
            #        qstr.append(jfile.read())
            qasms=json.load(jfile)
    elif os.path.isfile(trueqcircfilename):
        import trueq
        circuits=trueq.load(trueqcircfilename)
        config=trueq.Config.from_yaml(yaml.dump(ops['configdict']))
        trueq.interface.QASM.set_config(config)
        transpiler=trueq.Compiler.from_config(config)
        transpiled_circuits=transpiler.compile(circuits)
        qasms=[c.to_qasm() for c in transpiled_circuits]
        with open(os.path.basename(trueqcircfilename)+'.qasm','w') as jfile:
            json.dump(qasms,jfile)
    else:
        print('TrueQ qasm, no such file:',trueqcircfilename)
    print(heraldfilename)
    if heraldfilename is None:
        heraldingqasm=None
        ops['heraldingsymbol']=None
    elif isinstance(heraldfilename,str):
        if os.path.exists(heraldfilename):
            heraldingqasm=[]
            for fname in [heraldfilename]:#,'gmm1.qasm']:
                f=open(fname)
                heraldingqasm.append(f.read())
                f.close()
        else:
            print('%s is not a file'%heraldfilename)
    elif isinstance(heraldfilename,list):
        heraldingqasm=heraldfilename
    else:
        exit('Heralding options not understand')
    qasmresult=qasmrun(nsample=nsample,qasms=qasms,qubitsmap=qubitsmap,heraldingqasm=heraldingqasm,**ops)#combineorder=ops['combineorder'])
    return qasmresult


if __name__=="__main__":
    import sys
    nsample=1000
    trueqcircfilename=sys.argv[1]
    heraldfilename=sys.argv[2]
    qubitsmap={'q':['Q0','Q1','Q2','Q3','Q4','Q5','Q6','Q7']}#,'Q6','Q7']}#,'gamma':'QQQ'}
    qubitsmap={'q':['Q5','Q6']}#,'Q6','Q7']}#,'gamma':'QQQ'}
    qasmresult=trueqqasm(nsample,trueqcircfilename,heraldfilename,qubitsmap)
    circuits,fidelity=trueqsrbprocess(trueqcircfilename,qasmresult,sim=True)
    print(fidelity)
    from matplotlib import pyplot
    circuits.plot.raw()
    pyplot.show()
