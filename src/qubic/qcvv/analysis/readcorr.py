import sys
import datetime
from sklearn import mixture
import numpy
import itertools
from qubic.qcvv.analysis.blob import c_blob
from matplotlib import pyplot
class readcorr():
    def __init__(self,corrfiledict=None,combineorder=None,countsum=None,rcalpath=None):
        if countsum is None:
            self.combineorder=combineorder
            self.corrfiledict=corrfiledict
        else:
            self.combineorder=countsum['combineorder'] if combineorder is None else combineorder
            pathtorcal='.' if rcalpath is None else rcalpath
            self.corrfiledict={q:'/'.join([pathtorcal,'%s_rcal.npz'%(q)]) for q in self.combineorder} if corrfiledict is None else corrfiledict

        self.m={}
        self.minv=None  # matrix inverse
        self.labels=None
        self.err=False
        pass
    def save(self,pmeas,labels=['0','1']):
        if len(self.combineorder)==1:
            self.labels=labels
            m=numpy.zeros((len(self.labels),len(self.labels)))
            #print(pmeas)
            for ii,i in enumerate(self.labels):
                for jj,j in enumerate(self.labels):
                    #print('pmeas',pmeas)
                    #m[jj,ii]=pmeas[(i,)][jj]
                    m[jj,ii]=pmeas[(j,)][ii]
            numpy.savez(self.corrfiledict[self.combineorder[0]],**dict(labels=self.labels,matrix=m))
        else:
            print('Error:multi qubit vector rcal not ready yet')
    def corr(self,pmeas,clip=False):
        pcorr={}
        if self.minv is None:
            self.load()
            if not self.err:
                self.combine()
        if not self.err:
            #print('len(list(self.labels))',len(self.labels),self.labels)
            #print('len(list(pmeas.keys())',len(list(pmeas.keys())),list(pmeas.keys()))
            if len(self.labels)==len(list(pmeas.keys())):
                #print(list(pmeas.keys()))
                pmeasmatrix=numpy.matrix(numpy.zeros((len(self.labels),len(pmeas[self.labels[0]]))))
                #print(self.labels,pmeas)
                for il,l in enumerate(self.labels):
                    pmeasmatrix[il]=pmeas[l]
                #print(self.minv)
                #print(pmeasmatrix)
                pcorrmatrix=numpy.array(self.minv*pmeasmatrix)
                if clip:
                    pcorrmatrix=numpy.clip(pcorrmatrix,0.0,1.0)
                for il,l in enumerate(self.labels):
                    pcorr[l]=pcorrmatrix[il]
            else:
                print('Readout correction error: labels length and measurement key length not match')
        else:
            print("Readout correction: load rcal error, NO readout correction result available")
        return pcorr

    def load(self):
        try:
            for q in self.corrfiledict:
                self.m[q]=numpy.load(self.corrfiledict[q],allow_pickle=True)
                #print('at load',self.m[q]['labels'])
        except:
            self.err=True
    def combine(self):
        try:
            for iq,q in enumerate(self.combineorder):
                if iq==0:
                    m=self.m[q]['matrix']
                    l=[self.m[q]['labels']]

                else:
                    m=numpy.kron(m,self.m[q]['matrix'])
                    l.append(self.m[q]['labels'])
            self.minv=numpy.matrix(numpy.linalg.inv(m))
            #self.labels=l[0] if len(l)==1 else itertools.product(*l)
            self.labels=[i for i in itertools.product(*l)]
                    
        except:
            self.err=True

    #    print(m)
    #    print(numpy.linalg.inv(m))
    #    print(m.shape)
    #    print(type(m))
#def rcal(result,combineorder,rcaldict=None,labels=['0','1']):
#    rcalcombine=numpy.matrix([1],dtype='complex')
#    psinglepermeasrcal={}
#    for iq,q in enumerate(combineorder):
#        psinglepermeasrcal[q]=[]
#        for im,m in enumerate(result['psinglepermeas'][q]):
#            vecin=numpy.zeros(len(rcaldict[q].item()['labels']))
#            for ilabel,label in enumerate(rcaldict[q].item()['labels']):
#                vecin[ilabel]=result['psinglepermeas'][q][im][label]
#            vecout=numpy.matmul(rcaldict[q].item()['matrix'],vecin)
#            print('vecout',vecout)
#            print(rcaldict[q].item()['labels'])
#            print({l:il for il,l in enumerate(rcaldict[q].item()['labels'])})
#            psinglepermeasrcal[q].append({l:vecout[il] for il,l in enumerate(rcaldict[q].item()['labels'])})
#        rcalcombine=rcaldict[q].item()['matrix'] if iq==0 else numpy.kron(rcalcombine,rcaldict[q].item()['matrix'])
#        combinekcode=rcaldict[q].item()['labels'] if iq==0 else itertools.product(combinekcode,rcaldict[q].item()['labels'])
#    #combinekcode=[i for i in itertools.product(*[predict[p].labels.keys() for p in combineorder])]
#    pcombinepermeasrcal=[]
#    for im,m in enumerate(result['pcombinepermeas']):
#        print(im,m)
#        vecin=numpy.array([m[k] for k in combinekcode])
#        vecout=numpy.matmul(rcalcombine,vecin)
#        pcombinepermeasrcal.append({k:vecout[ik] for ik,k in enumerate(combinekcode)})
#    print(rcalcombine.shape)
#    print(result['pcombinepermeas'])
#    print(pcombinepermeasrcal)
#
#if __name__=="__main__":
#    import sys
#    gmix=GaussianMixtureLabeled(fromdict=numpy.load(sys.argv[1]))
##    print(gmix.means_)
##    print(dir(gmix))
##    print(gmix.covariances_)
#    print(gmix.separation())
#
