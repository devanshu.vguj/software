import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
#envset,qubicrun,heralding
from fractions import Fraction

class c_cwdrv(experiment.c_experiment):
    def __init__(self,qubitid,calirepo,**kwargs):
        experiment.c_experiment.__init__(self,qubitid,calirepo,**kwargs)
    def baseseqs(self,**kwargs):
        opts=dict(qubitid='Q0',dest=None,twidthlist=[],pcarrierlist=[],amp=None,fcarrier=None)
        opts.update({k:v for k,v in kwargs.items() if k in opts})
        print('baseseqs',kwargs)
        seqs=[]
        for index,twidth in enumerate(opts['twidthlist']):
            seq=[]
            envmodi=[dict(env_func='square',paradict=dict(phase=opts['pcarrierlist'][index],amplitude=1.0))]
            drvmodi=dict(env=envmodi)
            drvmodi.update(dict(twidth=twidth))
            drvmodi.update({} if opts['fcarrier'] is None else dict(fcarrier=opts['fcarrier']))
            drvmodi.update({} if opts['amp'] is None else dict(amp=opts['amp']))
            drvmodi.update({} if opts['dest'] is None else dict(dest='%s.%s'%(opts['qubitid'][0],opts['dest'])))
            seq.append(dict(name='rabi',qubit=opts['qubitid'][0],modi=[drvmodi]))
        print(seq)
        seqs.append(seq)
        return seqs

    def seqs(self,dest,freq=None,amp=None,**kwargs):
        self.opts.update(kwargs)
        self.opts['delaybetweenelement']=0e-6
        self.opts['dest']=dest
        destname='%s.%s'%(self.opts['qubitid'][0],dest)
        lo=self.opts['wiremap'].lofreq[destname]
        print(destname)
        fif=freq-lo
        fifreal,twidth=self.fintcalc(fif)
        self.opts['fcarrier']=fifreal+lo
        self.opts['twidthlist']=[twidth]
        self.opts['pcarrierlist']=[0]
        self.opts['seqs']=self.baseseqs(**self.opts)#**self.opts)
        print(self.opts['seqs'])
        self.compile()#cmdlists=qubicrun.compile(**self.opts)
    def fintcalc(self,freq):
        bufwidth=12
        maxmem=2**bufwidth
        freqwidth=24
        dt=1e-9
        dp=freq*dt
        dp4096=int(round(dp*maxmem))
        fint=dp4096*2**freqwidth/maxmem
        twidth=maxmem*dt
        freal=fint/2**freqwidth/dt

        return freal, twidth


    def run(self):
        self.opts['chassis'].writeonlyrun(cmdlists=self.qubicrun.cmdlists)#self.opts['seqs'])

if __name__=="__main__":
    #cwdrv=c_cwdrv(qubitid='Q1',calirepo='../../../../qchip',debug=0,sim=False)
    cwdrv=c_cwdrv(qubitid='Q5',calirepo='../../../../qchip',debug=0,sim=False)
    fif=230e6
    cwdrv.seqs(dest='rdrv',freq=5.919e9,amp=1.0)#fif+5.41e9)
    cwdrv.compile(heraldcmds=None,mark0=False,starttime=0)
    cwdrv.run()
