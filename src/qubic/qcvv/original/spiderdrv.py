import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment

class c_spiderdrv(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg_spidernet.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def spiderdrvseqs(self,delaybetweenelement=600e-6,qubitdrvamp=None,fqubit=None,qubitdrvtwidth=None,qubitdrvpcarrier=None,qubitdrvt0=None):
		mod={}
		if qubitdrvamp is not None:
			mod.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			mod.update(dict(fcarrier=fqubit))
		if qubitdrvtwidth is not None:
			mod.update(dict(twidth=qubitdrvtwidth))
		if qubitdrvpcarrier is not None:
			mod.update(dict(pcarrier=qubitdrvpcarrier))
		if qubitdrvt0 is not None:
			mod.update(dict(t0=qubitdrvt0))
		run=32e-9 # 0
		self.seqs.add(388e-9+12e-9,self.qchip.gates['M0mark'])#.modify(dict(twidth=12e-9)))
		#self.seqs.add(run+2e-9,self.qchip.gates['A1drv'].modify(mod)) #  DC+100M,'Q3.qdrv':3,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['A2drv'].modify(mod)) #  DC+200M,'Q3.qdrv':3,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['B1drv'].modify(mod)) #  6G+100M,'Q2.qdrv':2,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['B2drv'].modify(mod)) #  6G+200M,'Q2.qdrv':2,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['C1drv'].modify(mod)) # 12G+100M,'Q1.qdrv':1,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['C2drv'].modify(mod)) # 12G+200M,'Q1.qdrv':1,"square","amp":1.0,"twidth":1e-06
		#self.seqs.add(run,self.qchip.gates['A1A2drv']) # "t0":(0,8e-9),"amp":(0.5,0.5)
		#self.seqs.add(run,self.qchip.gates['B1B2drv']) # "t0":(0,8e-9),"amp":(0.5,0.5)
		#self.seqs.add(run,self.qchip.gates['C1C2drv']) # "t0":(0,8e-9),"amp":(0.5,0.5)
		self.seqs.add(run,self.qchip.gates['A1B1C1drv'].modify([dict(t0=2e-9,twidth=32e-9),dict(t0=0e-9,twidth=4e-9,pcarrier=numpy.pi),dict(t0=0e-9,twidth=32e-9,pcarrier=numpy.pi/2)])) # "t0":(0,8e-9,16e-9),"amp":(0.5,0.5,0.5)
		#self.seqs.add(run,self.qchip.gates['A1B1C1drv'].modify([mod,mod,mod])) # "t0":(0,8e-9,16e-9),"amp":(0.5,0.5,0.5)
		#self.seqs.add(run,self.qchip.gates['A2B2C2drv']) # "t0":(0,8e-9,16e-9),"amp":(0.5,0.5,0.5)
		run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--qubitdrvpcarrier',help='qubit drive pcarrier',dest='qubitdrvpcarrier',type=float,default=None)
	parser.add_argument('--qubitdrvt0',help='qubit drive t0',dest='qubitdrvt0',type=float,default=None)
	clargs=parser.parse_args()
	spiderdrv=c_spiderdrv(**clargs.__dict__)
	if clargs.sim:
		spiderdrv.setsim()
	spiderdrv.spiderdrvseqs(delaybetweenelement=clargs.delaybetweenelement,qubitdrvamp=clargs.qubitdrvamp,fqubit=clargs.fqubit,qubitdrvtwidth=clargs.qubitdrvtwidth,qubitdrvpcarrier=clargs.qubitdrvpcarrier,qubitdrvt0=clargs.qubitdrvt0)
	spiderdrv.run(bypass=clargs.bypass)
	if clargs.sim:
		spiderdrv.sim()
	# python spiderdrv.py --qubitdrvamp 0.6789 --fqubit 111.11e6 --qwidth 88e-9 --qubitdrvpcarrier 1.23 --qubitdrvt0 22e-9 
