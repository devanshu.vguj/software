import numpy
import matplotlib
from matplotlib import pyplot
import sys
sys.path.append('../../')
from qubic.qcvv.analysis import gmm
from qubic.qcvv.analysis import blob
fname=sys.argv[1]
gmixs=gmm.GaussianMixtureLabeled(fromdict=numpy.load(fname))
sep=gmixs.separation()
print(sep)
fig=pyplot.figure()
ax=fig.subplots()
colors=['red','green','black']
sepcolor={k:colors[ik] for ik,k in enumerate(sorted(list(sep.keys())))}
for l,b in gmixs.blobs.items():
    ax.add_patch(b.ellipse())
    for k,s in sep.items():
        if l in k:
            x,y=b.ellipsexy(k=s)
            ax.plot(x,y,color=sepcolor[k])
    ax.plot(b.x0,b.y0,color='b',marker='$%s$'%l,markersize=20)
blob.scalemax(ax)
pyplot.show()
