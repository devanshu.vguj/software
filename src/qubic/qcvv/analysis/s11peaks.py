import numpy
import scipy.signal
import sys
def arraypeak(x,y,pslimit=0,**kwargs):
    #    peakindex=scipy.signal.find_peaks_cwt(y,numpy.arange(1,20))#,min_snr=0.5)#,noise_perc=20)
#    peakindex=scipy.signal.argrelextrema(y,comparator=numpy.greater)[0]
    peakindex,_=scipy.signal.find_peaks(y)#,comparator=numpy.greater)[0]
    prominences,left_bases,rightbases=scipy.signal.peak_prominences(y,peakindex)
#    print('pslimit0',peakindex,prominences)
    #print('pslimit in arraypeak',pslimit)
    peakindex=numpy.array([peakindex[i] for i,ps in enumerate(prominences) if ps>pslimit])
    prominences=numpy.array([prominences[i] for i,ps in enumerate(prominences) if ps>pslimit])
#    print('pslimit1',pslimit,peakindex,prominences)
    sys.stdout.flush()
    pw,pwh,lip,rip=scipy.signal.peak_widths(y,peakindex)
    pwint=numpy.array([numpy.ceil(i) for i in pw]).astype(int)
    #print(peakindex,pwint)
    p2min=peakindex-pwint
    p2min=[max(0,i) for i in p2min]
    p2max=peakindex+pwint
    p2max=[min(len(x)-1,i) for i in p2max]
    xmin=x[p2min]#xc-pwhz
    xmax=x[p2max]#xc+pwhz
#    print(xmin,xmax)
#    print('combineoverlap',combineoverlap(xmin,xmax))
    return peakindex,prominences, xmin,xmax
    pass
def combineoverlap(xmin,xmax):
    scan=[]
    start=xmin[0]
    stop=xmax[0]
    tot=0
    if len(xmin)>1:
        for i in range(1,len(xmin)):
            if xmin[i]<=stop:
                stop=xmax[i]
#            print('overlap')
                if i==len(xmin)-1:
                    scan.append((start,stop))
            else:
                scan.append((start,stop))
#            print('diff','%8.3e'%(stop-start))
                tot+=(stop-start)
                start=xmin[i]
                stop=xmax[i]
                if i==len(xmin)-1:
                    scan.append((xmin[i],xmax[i]))
                    tot+=(xmax[i]-xmin[i])
    else:
        scan.append((start,stop))
        tot+=(stop-start)
    #print('combineoverlap',xmin,xmax,scan,tot)
    return scan,tot

def s11peaks(fx,y,pslimit=0):
    #print('pslimit in s11peak',pslimit)
    #phdiff=abs(numpy.diff(numpy.unwrap(numpy.angle(s11)),1))
#    up=numpy.unwrap(numpy.angle(s11))
#    phdiff=abs(up-up.mean())
    phdiff=y
    peakindex,prominences,fxmin,fxmax=arraypeak(fx,phdiff,pslimit)
    if len(peakindex)>0:
        fxc=fx[peakindex]
        fscan,ftot=combineoverlap(fxmin,fxmax)
    else:
        fxc=[]
        fscan=[]
        ftot=0

    return dict(peakindex=peakindex,phdiff=phdiff,fxc=fxc,fscan=fscan,ftot=ftot,prominences=prominences)
#
#    if 0:
#        p1,p1p=arraypeak(fx,phdiff,prominence=2*phdiff.std())#height=p.mean()+3*p.std()))
#        pp1=sorted(p1p['prominences'],reverse=True)
#        if (len(pp1)<=npeak):
#            ppn=pp1[-1]
#            print('not enough peaks, only %d, need %d'%(len(pp1),npeak))
#        else:
#            ppn=pp1[npeak]
#        #peakindex ,_=scipy.signal.find_peaks(phdiff,prominence=prominence if prominence is not None  else ppn)#height=p.mean()+3*p.std()))
#        peakindex ,_=arraypeak(fx,phdiff,prominence=prominence if prominence is not None  else ppn)#height=p.mean()+3*p.std()))
#        print('ppn,prominence',ppn,prominence)
#        #self.p=p
#        #self.p2=p2
#        fxc=fx[peakindex]
#        pw,pwh,lip,rip=scipy.signal.peak_widths(phdiff,peakindex)
#        pwint=numpy.array([numpy.ceil(i) for i in pw]).astype(int)
#        #print('pwint',pwint)
#        #print('p2',p2)
#        p2min=peakindex-pwint*2
#        p2min=[max(0,i) for i in p2min]
#        p2max=peakindex+pwint*2
#        p2max=[min(len(fx),i) for i in p2max]
#        #pwhz=[max(i,minbw/2) for i in pw*fxstep]
#        #print('fx 800-820',self.fx[800:820])
#        fxmin=fx[p2min]#fxc-pwhz
#        fxmax=fx[p2max]#fxc+pwhz
#        #print('pw,p2min,p2max',pw,p2min,p2max,fxmin,fxmax)
#        fscan=[]
#        fstart=fxmin[0]
#        fstop=fxmax[0]
#        ftot=0
#        for i in range(1,len(peakindex)):
#            fstartthis=fxmin[i]
#            fstopthis=fxmax[i]
#            if fxmin[i]<fstop:
#                fstop=fxmax[i]
#                print('overlap')
#                if i==len(peakindex)-1:
#                    fscan.append((fstart,fstop))
#            else:
#                fscan.append((fstart,fstop))
#                print('diff','%8.3e'%(fstop-fstart))
#                ftot+=(fstop-fstart)
#                fstart=fxmin[i]
#                fstop=fxmax[i]
#                if i==len(peakindex)-1:
#                    fscan.append((fxmin[i],fxmax[i]))
#                    ftot+=(fxmax[i]-fxmin[i])
