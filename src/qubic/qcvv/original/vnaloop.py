import datetime
import argparse
import sys
from squbic import *
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
from scipy import signal
import experiment
from vna import c_vna
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=2e-6)
	parser.set_defaults(elementlength=1024)
	clargs=parser.parse_args()
	vna=c_vna(**clargs.__dict__)
	vna.vnaseqs(delayread=clargs.delayread,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fstart=clargs.fstart,fstop=clargs.fstop,rdc=0,qubitidread=clargs.qubitidread)
	if clargs.processfile=='':
		if clargs.sim:
			vna.sim()
		vna.run(bypass=clargs.bypass)
	else:
		fprocess=clargs.processfile
	#[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=vna.processvna(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
	pyplot.ion()
	fig=pyplot.figure(1)
	while True:
		try:
			data=vna.vnadata(clargs.nget)
			fprocess=vna.savejsondata(filename=clargs.filename,extype='vnaloop',cmdlinestr=cmdlinestr,data=data,timeinfo=False)
			c=vna.processvna(fprocess)
			print('adc minmax',vna.hf.adcminmax())
			fig.clf()
			if clargs.plot:
				vna.vnaplot(c)
			pyplot.draw()
			pyplot.pause(1e-6)
		except KeyboardInterrupt:
			pyplot.close('all')
			break
