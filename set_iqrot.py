from register import c_register
from mem_gateway import c_mem_gateway
from ether import c_ether
import time
import json
import sys
try:
	basestring
except NameError:
	basestring = str
from regmap import c_regmap


if __name__=="__main__":
	#interface=c_ether('192.168.1.122',port=3000)
	#regmap=c_regmap(interface, min3=True,memgatewaybug=False)
	#regmap.read(['full','adc0_minmax'])
	#print [regmap.getregval(i) for i in ['full','adc0_minmax']]
	#print regmap.wavecheckreadreset('buf_monout_0')
	#print regmap.read('buf_monout_0')
	#print regmap.write((('valid',0),))
	#print [hex(i) for i in regmap.write((('command',[(4<<96)+(3<<64)+(2<<32)+1,(5<<96)+(6<<64)+(7<<32)+8]),))]
	interface=c_ether('192.168.1.124',port=3000)
	regmap=c_regmap(interface, min3=True,memgatewaybug=False)
	regmap.read(['full','adc0_minmax'])
	print([regmap.getregval(i) for i in ['full','adc0_minmax']])
	print(regmap.write((('period_dac0',int(75000)),)))
	print(regmap.write((('iqrot',int(sys.argv[1])),)))
