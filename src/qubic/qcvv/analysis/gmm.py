import sys
import datetime
from sklearn import mixture
import numpy
import itertools
from qubic.qcvv.analysis.blob import c_blob
from matplotlib import pyplot
class gmmpredictresult(numpy.ndarray):
    def __array_finalize__(self, obj):
        if obj is None: return
        self.labels= getattr(obj, 'labels', {})
        self.labelsrev= getattr(obj, 'labelsrev', {})
    def setlabels(self,labels):
        self.labels=labels
        self.labelsrev={v:k for k,v in labels.items()}

class GaussianMixtureLabeled(mixture.GaussianMixture):
    def __init__(self,fromdict=None,**kwargs):
        if fromdict:
            means=fromdict['means']
            covariance_type=fromdict['covariance_type']
            #print('debug fromdict',means,covariance_type)
            mixture.GaussianMixture.__init__(self,n_components=len(means),covariance_type=covariance_type)#,reg_covar=1e-5)
            self.precisions_cholesky_=numpy.linalg.cholesky(numpy.linalg.inv(fromdict['covariances']))
            self.weights_=fromdict['weights']
            self.means_=means
            self.covariances_=fromdict['covariances']
            self.createtimestamp=fromdict['createtimestamp']
            self.labels={int(k):v for k,v in fromdict['labels']}
            self.labelsrev={v:k for k,v in self.labels.items()}
            self.blobs=self.gmixblobs()
        else:
            self.createtimestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
            mixture.GaussianMixture.__init__(self,**kwargs)
            self.labels=[]
            if self.n_components==None:
                self.labels=[]
            #    self.explorebestfit()
            else:
                self.labels={k:None for k in range(self.n_components)}
            self.blobs=None
    def gmixblobs(self):
        blobs={}
        for ilabel,label in self.labels.items():
            blob=c_blob(mean=self.means_[ilabel],covar=self.covariances_[ilabel])
            blobs[label]=blob
        return blobs
    def bic_val(self):
        return self._bic
    def separation(self):
        if self.blobs is None:
            self.blobs=self.gmixblobs()
        distij={}
        for i,j in itertools.combinations(self.blobs,2):
            distij[tuple(sorted([i,j]))]=self.blobs[i].dist(self.blobs[j])
        return distij
    def angle(self):
        if self.blobs is None:
            self.blobs=self.gmixblobs()
        angle={}
        for m,n in itertools.combinations(self.blobs,2):
            diff=(self.blobs[m].x0-self.blobs[n].x0)+1j*(self.blobs[m].y0-self.blobs[n].y0)
            angle[tuple(sorted([m,n]))]=numpy.angle(diff)%(numpy.pi*2)
        return angle

    def modelpara(self):
        #print('modelpara save',self.means_,self.covariances_)
        return dict(means=self.means_,weights=self.weights_,covariances=self.covariances_,covariance_type=self.covariance_type,labels=numpy.array(list(self.labels.items())),createtimestamp=self.createtimestamp)

    def fit_complex(self,datac,**args):
        # datac complex data
        # other args for mixture.GaussianMixter passed in as **args
        datac1d=datac.reshape((-1,1))
        dataiq=numpy.append(datac1d.real,datac1d.imag,1)
        try:
            gmix=self.fit(dataiq)#n_gaussians=n_gaussians,**args)
            self._bic=self.bic(dataiq)#n_gaussians=n_gaussians,**args)
        except:
            pyplot.ioff()
            pyplot.figure('debug dataiq')
            print(dataiq)
            sys.stdout.flush()
            pyplot.plot(dataiq[:,0],dataiq[:,1],'.')
            pyplot.show()
            gmix=None
        return gmix
    def setlabels(self,data,label,mode='mintomax',reset=False):
        #print('setlabels mode',mode)
        if data is None:
            #print(self.labels)
            unassignedlabel=[i for i in self.labels if self.labels[i] is None]
            if len(unassignedlabel)==1:
                self.labels[unassignedlabel[0]]=label
            else:
                print('only the last label can be assigned without data predict',len(unassignedlabel),unassignedlabel)
        else:
            #            print('setlabel status',reset,mode,[self.labels[i] for i in range(self.n_components)])
            setstatusvalue={i:numpy.sum(self.predict_complex(data)==i,axis=0) for i in range(self.n_components) if (self.labels[i] is None) or reset or mode=='mintomax'}
            #print('debug setlabels: setstatusvalue',setstatusvalue)
#        s=numpy.argsort(numpy.array(setstatusvalue))
            s=[k for k in sorted(setstatusvalue, key=lambda x:setstatusvalue[x])]
#        print('debug setlabels for gmix model using mode',mode, ' to value ', s)
            #print('sort predict',s)
            if mode=='max':
                self.labels[s[-1]]=label
#                print('setting',s[-1],label)
            elif mode=='mintomax':
                if len(label)==len(s):
                    for index,sortindex in enumerate(s):
                        self.labels[index]=label[sortindex]
                else:
                    print(setstatusvalue)
                    exit('error, not match in size,label length %d, s size %d'%(len(label),len(s)))
            else:
                exit('GMM setlabels mode should be max|mintomax, got %s'%mode)
        self.labelsrev={v:k for k,v in self.labels.items()}
    def predict_complex(self,datac):
        if 0:
            import pickle
            f=open('datac.pickle','wb')
            pickle.dump(datac,f)
            f.close()
        shapein=datac.shape
        datac1d=datac.reshape((-1,1))
        dataiq=numpy.append(datac1d.real,datac1d.imag,1)
        predict1d=self.predict(dataiq)
        predict=predict1d.reshape(shapein)
        rp=predict.view(gmmpredictresult)
        rp.setlabels({k:v for k,v in self.labels.items()})
        return rp
def countsum(predict,heraldingsymbol=None,combineorder=None,debug=0):
    #print('predict',predict,predict['Q5'].shape)
#    print('predict in gmm',list(predict.keys()),[a.shape for i,a in predict.items()])
#    print('combineorder',combineorder)
    if debug>=1:
        print('Info: countsum,heraldingsymbol',heraldingsymbol)
    if 0:
        import pickle
        f=open('data.pickle','wb')
        pickle.dump(predict,f)
        f.close()
    #print('enter countsum',len(predict.keys()),[p.shape for p in predict.values()])
    if combineorder is None:
        combineorder=sorted(list(predict.keys()))
    readelemcmdcnt=numpy.array([p.shape[1] for p in predict.values()])
    #print(readelemcmdcnt)
    heraldable=not numpy.any(readelemcmdcnt&1)
    #print('readelemcmdcnt&1',readelemcmdcnt,readelemcmdcnt&1)
    combinable=numpy.all((readelemcmdcnt==readelemcmdcnt[0]))
    herald=False
    #print('predict.labels',[p.labels for p in predict.values()])
    heraldingsymboldict={}
    if heraldable:
        if heraldingsymbol is not None:
            if readelemcmdcnt[0]%2==0:
                nherald=readelemcmdcnt[0]//2
                herald=True
            else:
                print('error, need even readelem cmd cnt')
                exit(1)
            if isinstance(heraldingsymbol,dict):
                for p in predict.keys():
                    if isinstance(heraldingsymbol[p],list):
                        if len(heraldingsymbol[p])==nherald:
                            heraldingsymboldict=heraldsymbol
                            herald=True
                        else:
                            print('heralding dict member ',p," doesn't match")
                    elif heraldingsymbol[p] in predict[p].labels:
                        heraldingsymboldict[p]=numpy.array(nherald*[heraldingsymbol[p]])
                        herald=True
            else:
                for p in predict.keys():
                    if debug >=1:
                        print('heralding symbol ',heraldingsymbol,'n herald columns',nherald)
                    if heraldingsymbol in predict[p].labels.values():
                        heraldingsymboldict[p]=numpy.array(nherald*[heraldingsymbol])
                        herald=True
                    else:
                        print('heralding symbol %s not in labels %s'%(heraldingsymbol,str(predict[p].labels)))
                        exit(1)
        else:
            print('heralding symbol is None')
    else:
        print('not heraldable')
#    print('debug heraldingsymboldict',heraldingsymboldict,'herald',herald)
#    print('debug herald,predict.keys',herald,list(predict.keys()))
    combineorder=[p for p in combineorder if p in predict]
    if herald:
    #    predicth01=[predict[p][:,0::2] for p in combineorder]
    #    print('predicthlabelreverse',[predict[p].labelsrev for p in combineorder])
    #    print('predicthlabel',[[predict[p].labelsrev[l] for l in heraldingsymboldict[p]] for p in combineorder])
        predicth={p:predict[p][:,0::2]==[predict[p].labelsrev[l] for l in heraldingsymboldict[p]] for p in combineorder}
        predictm={p:predict[p][:,1::2] for p in combineorder}
    else:
        predicth=None
        predictm={p:predict[p] for p in combineorder}
#    print(predictm)
#    print(list(predictm.keys())[0])
#    print(predictm[list(predictm.keys())[0]])
    nmeas=predictm[list(predictm.keys())[0]].shape[1]
    #print('nmeas',nmeas)
    if not combinable:
        print('Warning, not comibnable, because the meas for each qubit is not the same length',readelemcmdcnt)

    nqubit=len(predict.keys())
    vsingle={}
    psingle={}
    vcombine={}
    pcombine={}
    vcombinepermeas=[{} for i in range(nmeas)]
    pcombinepermeas=[{} for i in range(nmeas)]

    for q,p in predictm.items():
        vsingle[q]={}
        for l in predict[q].labels.values():
            if herald:
                vsingle[q][l]=numpy.array(numpy.sum(predicth[q]*numpy.array([v==p.labelsrev[l] for v in p]),axis=0))
            else:
                vsingle[q][l]=numpy.array(numpy.sum(numpy.array([v==p.labelsrev[l] for v in p]),axis=0))

#    for l in predict[combineorder[0]].labels.values():
#        if herald:
#            vsingle[l]={i:numpy.array(numpy.sum(predicth[i]*numpy.array([v==p.labelsrev[l] for v in p]),axis=0)) for i,p in predictm.items()}
#        else:
#            print([p.labelsrev for i,p in predictm.items()])
#            vsingle[l]={i:numpy.array(numpy.sum(numpy.array([v==p.labelsrev[l] for v in p]),axis=0)) for i,p in predictm.items()}
#    totalsingle=numpy.sum(numpy.array(list([v for m,vq in vsingle.items() for k,v in vq.items()])),axis=0)
    totalsingle={q:numpy.sum(numpy.array(list([v for l,v in vsingle[q].items()])),axis=0) for q in vsingle}
    for q,lv in vsingle.items():
        psingle[q]={}
        for l,v in lv.items():
            psingle[q][l]=vsingle[q][l]/totalsingle[q]
#    for label in predict[combineorder[0]].labels.values():
#        psingle[label]={}
#        for qubitindex,vsing in vsingle[label].items():
#            psingle[label][qubitindex]=vsing/totalsingle[qubitindex]
    vsinglepermeas={}
    psinglepermeas={}
    for q,lv in vsingle.items():
        vsinglepermeas[q]=nmeas*[{}]
        psinglepermeas[q]=nmeas*[{}]
        for l,iv in lv.items():
            for i,v in enumerate(iv):
                vsinglepermeas[q][i][l]=v
                psinglepermeas[q][i][l]=v/totalsingle[q][i]

    labels=[predict[p].labels for p in combineorder]
    labelsrev=[predict[p].labelsrev for p in combineorder]
#    print('predicth.shape',predicth[0].shape,predicth[1].shape)
    if combinable:
        combinekcode=[i for i in itertools.product(*[predict[p].labels.keys() for p in combineorder])]
        #print('debug in gmm',combinekcode)
        combinescode=[[predict[combineorder[i]].labels[p] for i,p in enumerate(s)] for s in combinekcode]
        if herald:
            combineh=numpy.array(list(map(tuple,numpy.array([predicth[p] for p in combineorder]).transpose())))
        combinem=numpy.array(list(map(tuple,numpy.array([predictm[p] for p in combineorder]).transpose())))

        for icode,code in enumerate(combinekcode):
            #scode=''.join(combinescode[icode])
            scode=tuple(combinescode[icode])
            if herald:
                vcombine[scode]=numpy.sum((numpy.all(combineh,axis=2)*numpy.all(combinem==code,axis=2)).transpose(),axis=0)
            else:
                vcombine[scode]=numpy.sum((numpy.all(combinem==code,axis=2)).transpose(),axis=0)
        totalcombine=numpy.sum(numpy.array(list(vcombine.values())),axis=0)
        for icode,code in enumerate(combinekcode):
            #scode=''.join(combinescode[icode])
            scode=tuple(combinescode[icode])
            pcombine[scode]=vcombine[scode]/totalcombine
    #    print(combinekcode)
    #    print(combinescode)
        for imeas in range(nmeas):
            for icode,code in enumerate(combinekcode):
                #scode=''.join(combinescode[icode])
                scode=tuple(combinescode[icode])
#                print('scode',scode,combinescode[icode])
                vcombinepermeas[imeas][scode]=vcombine[scode][imeas]
                pcombinepermeas[imeas][scode]=pcombine[scode][imeas]
    else:
        vcombine=None
        totalcombine=None
    result=dict(vsingle=vsingle,psingle=psingle,vsinglepermeas=vsinglepermeas,psinglepermeas=psinglepermeas,totalsingle=totalsingle,vcombine=vcombine,totalcombine=totalcombine,vcombinepermeas=vcombinepermeas,pcombinepermeas=pcombinepermeas,combineorder=combineorder,predicth=predicth,pcombine=pcombine)
#    print('pcombine in gmm',result['pcombine'])
    return result

if __name__=="__main__":
    import sys
    gmix=GaussianMixtureLabeled(fromdict=numpy.load(sys.argv[1]))
#    print(gmix.means_)
#    print(dir(gmix))
#    print(gmix.covariances_)
    print(gmix.separation())

