import numpy
from matplotlib import pyplot
def memgen(qchip,memcfg,dt,bufsize,fullscale,gates=None):
	#		dt=self.dt
	#bufsize=self.elembufsize
	#fullscale=self.MEMFS
	elemdict=memcfg.elemdict
	patchdict=memcfg.patchdict
	destdict=memcfg.destdict
	gates=memcfg.gates if gates is None else gates
	print('memcfg gates:',memcfg.gates)
	gatecmd={}
	start={}
	buf={}
	for gate in gates:
		gatecmd[gate]=[]
		tgatelength=qchip.gates[gate].tlength
		for pulse in qchip.gates[gate].pulses:
			dest=pulse.dest
			memreg='elementmem_%x'%elemdict[dest]
			patchmem=patchdict[dest]
			if memreg not in start.keys():
				start[memreg]=0
				buf[memreg]=numpy.zeros(bufsize)
			gcmd={'element':elemdict[dest],'dest':destdict[dest],'pcarrier':pulse.pcarrier,'fcarrier':pulse.fcarrier,'fcarriername':pulse.paradict['fcarrier'],'t0':pulse.t0,'tgatelength':tgatelength,'patchmem':patchmem,'cond':pulse.paradict['cond'] if 'cond' in pulse.paradict else 0}#,'pulse':pulse}
			buf[memreg][start[memreg]:start[memreg]+patchmem]=numpy.zeros(patchmem)
			start[memreg]=start[memreg]+patchdict[dest]
			gcmd.update({'start':start[memreg]})
			tv=pulse.val(dt,flo=0,mod=False)
			nv=tv.nvval(dt)
			real=((fullscale*nv[1].real).astype(int))
			imag=((fullscale*nv[1].imag).astype(int))
			buf[memreg][start[memreg]+nv[0]-nv[0][0]]=real*2**16+imag;#(((2**16*nv[1].real).astype(int)))#+(2**16*nv[1].imag.astype(int))
#			print('debug fullscale',fullscale)
#			print('debug pulse',pulse.paradict)
#			pyplot.subplot(121)
#			pyplot.plot(real)
#			pyplot.plot(imag)
#			pyplot.subplot(122)
#			#pyplot.plot(nv[1].real)
#			#pyplot.plot(nv[1].imag)
#			pyplot.plot(nv[1].real)
#			pyplot.plot(nv[1].imag)
#			pyplot.title(gate)
#			pyplot.show()
			length=len(nv[0])
			gcmd.update(dict(length=length,patchmem=patchmem))
			gatecmd[gate].append(gcmd)
			start[memreg]=start[memreg]+length
	usedsize=start
	memresult=dict(buf=buf,gatecmd=gatecmd,usedsize=usedsize)
	return memresult
def readmaprev(memcfg,qubits):
	r={memcfg.elemdict[q+'.read']:q for q in [qs for ql in qubits for qs in ql]}
	#print('debug readmaprev',r)
	return r
