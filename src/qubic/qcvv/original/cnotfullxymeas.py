import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import re
from simp2q import c_simp2q
import trueq
import qubic_trueq
import importlib


def cnotfullxymeas(qubitid_c=None,qubitid_t=None,z1p1deg=246,z1p2deg=147,z2p1deg=314,z2p2deg=234,z3p1deg=336,z3p2deg=37,step=10,delaybetweenelement=600e-6,nget=2,readcorr=True):
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()
	simp2q=c_simp2q(**clargs.__dict__)

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	[qid_c,qid_t]=[int(qubitid_c[1:]),int(qubitid_t[1:])]
	delayafterheralding=12e-6
	delaybetweenelement=delaybetweenelement
	firsttime=True

	measurement_results={}
	cnotphdeg=0
	keycnotphdeg=int(cnotphdeg)
	measurement_results[keycnotphdeg]={}
	for measphdeg in numpy.arange(0,360+step,step):
		keymeasphdeg=int(measphdeg)
		tt_list=[]
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(z1p2deg),(qid_c,):config.Z(z1p1deg)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.X(90),(qid_c,):config.X(90)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(z2p2deg),(qid_c,):config.Z(z2p1deg)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.X(90),(qid_c,):config.X(90)}))
		tt_list.append(trueq.Cycle({(qid_t,):config.Z(z3p2deg),(qid_c,):config.Z(z3p1deg)}))
		tt_list.append(trueq.Cycle({(qid_c,qid_t):config.CNOT()}))
		if (measphdeg<360):
			tt_list.append(trueq.Cycle({(qid_t,):config.Z(measphdeg),(qid_c,):config.Z(measphdeg)}))
			tt_list.append(trueq.Cycle({(qid_t,):config.X(90),(qid_c,):config.X(90)}))
		tt_list.append(trueq.Cycle({(qid_t,):trueq.Meas(),(qid_c,):trueq.Meas()}))
		newcirc=trueq.Circuit(tt_list) 
		transpiled_circuit=trueq.CircuitCollection(newcirc)

		trueq_result=[]
		measbinned_result=[]
		simulation_results=[]
		index=0
		lastcmdarraylength=0
		for circuit in transpiled_circuit:
			index=index+1
			[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0,cnotphdeg=cnotphdeg)
			gates=[simp2q.qchip.gates[qubitid_c+qubitid_t+'CNOT']]+[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
			elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			lastcmdarraylength=cmdarraylength
			data=simp2q.simp2qacq(nget)
			fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
			[trueq_return,meas_binned]=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset,readcorr=readcorr,corr_matrix=numpy.load(clargs.corrmx))
			trueq_result.extend(trueq_return)
			measbinned_result.extend([{'00':meas_binned[0][0],'01':meas_binned[1][0],'10':meas_binned[2][0],'11':meas_binned[3][0]}])
			firsttime=False

		s=trueq.Simulator()
		s.run(newcirc,n_shots=4096)
		contval=newcirc.results.to_dict()['content']['value']
		contvalsum=sum(contval.values())
		contvalfrac={k:v*1.0/contvalsum for k,v in contval.items()}
		simulation_results.append(contvalfrac)
		measurement_results[keycnotphdeg][keymeasphdeg]=measbinned_result  #trueq_result
		resultfilename='cnotfullxymeas'+'_control'+qubitid_c+'_target'+qubitid_t+'_z1p1deg'+str(z1p1deg)+'_z1p2deg'+str(z1p2deg)+'_z2p1deg'+str(z2p1deg)+'_z2p2deg'+str(z2p2deg)+'_z3p1deg'+str(z3p1deg)+'_z3p2deg'+str(z3p2deg)+'_'+timestamp+'.dat'
		for i in range(len(measurement_results[keycnotphdeg][keymeasphdeg])):
			print2file=open(resultfilename,'a')
			print('cnotphdeg',cnotphdeg,'measphdeg',measphdeg,'Sim',{k: v for k, v in sorted(simulation_results[i].items(), key=lambda item: item[1], reverse=True)},'Meas',{k: v for k, v in sorted(measurement_results[keycnotphdeg][keymeasphdeg][i].items(), key=lambda item: item[1], reverse=True)},file=print2file)
			print2file.close()
	json.dumps(measurement_results)
	with open('measurement_results.json','w+') as f:
		json.dump(measurement_results,f)
	return resultfilename

if __name__=="__main__":
	cnotfullxymeas(qubitid_c='Q6',qubitid_t='Q5',z1p1deg=246,z1p2deg=147,z2p1deg=314,z2p2deg=234,z3p1deg=336,z3p2deg=37,step=10,delaybetweenelement=600e-6,nget=2,readcorr=True)
