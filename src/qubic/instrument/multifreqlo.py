import struct
import serial
import sys
import time
import datetime
import numpy
sys.path.append('../../')
from qubic.instrument import devcom
import os
from lmx2595 import lmx2595

def multifreqlo(chan,freq=None,deviceid='multifreqlo_v1',sndev=devcom.sndev,logfilename='multifreqlolog.log'):
    dev=devcom.devcom(sndev)
    print('devcom',dev)
    loginfo,regs=multifreqlochanfreq(chan=chan,freq=freq,serialport=dev[deviceid],device=deviceid)
    multifreqlolog(loginfo,logfilename,elog=False)
    return regs
def hexbytestolong(b,bytesperword=3):
    dl=[]
    if len(b)%bytesperword==0 and bytesperword<=4:
        for index in range(0,len(b),bytesperword):
            longbytes=(4-bytesperword)*b'\x00'+b[index:index+bytesperword]
            dl.append(struct.unpack('>L',longbytes)[0])

    else:
        print('length error')
        dl=[i for i in struct.unpack('>%dB'%(len(b)),b)]
    return dl



def printhexbytes(b,bytesperword=1):
    d=hexbytestolong(b,bytesperword=bytesperword)
    return ([hex(i) for i in d])
def multifreqlochanfreq(chan,serialport,device,oscin=100e6,freq=None):
    ser=serial.Serial(serialport,9600)
#    input('serial connected, enter to continune')
#    print('run0')
#    loginfo0,regs0=mfl(chan=chan,freq=None,ser=ser,device=device)
#    time.sleep(1)
#    print('run1')
    loginfo1,regs1=mfl(chan=chan,freq=freq,ser=ser,device=device)
#    time.sleep(1)
#    print('run2')
#    loginfo2,regs2=mfl(chan=chan,freq=None,ser=ser,device=device)
    return loginfo1,regs1#mfl(chan=chan,ser=ser,device=device,oscin=oscin,freq=freq)

def mfl(chan,ser,device,oscin=100e6,freq=None):
    ser.flushOutput()
    ser.flushInput()
    time.sleep(0.1)
#    print(printhexbytes(ser.read(ser.in_waiting)))
    ser.write(str.encode('c%d'%int(chan)))
    while ser.in_waiting==0:
        ser.write(str.encode('c%d'%int(chan)))
        time.sleep(0.1)
    p2=ser.read(ser.in_waiting)
#    print('after chan',printhexbytes(p2))
#    print('after chan',p2)

    c1=lmx2595(oscin=oscin)
    if freq is not None:
        regs=c1.regfreq(oscin=oscin,rfouta=freq)
        #print(regs)
        regbyte=b''.join([int(k).to_bytes(3,'big') for k in regs])
        ser.write(str.encode('d'))
        for ib,b in enumerate(regbyte):
            ser.write([b])
            if (ib%10==0):
                time.sleep(0.1)
        ser.write(str.encode('b'))
        while ser.in_waiting!=0:
            print(printhexbytes(ser.read(ser.in_waiting)))
        timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
        log='%s %s change chan %s to : %s'%(timestamp,device,chan,freq)
    else:
        ser.write(str.encode('r'))
        pr=b''
        time.sleep(0.5)
        while ser.in_waiting!=0:
            pr+=ser.read(ser.in_waiting)
            time.sleep(0.1)
        log='read channel %s '%chan
        regs=hexbytestolong(pr,bytesperword=3)
        print('t1cspro ','\n'.join(c1.ticsproformat(regs)))
        print('read out length',len(pr))

    print(log)
    return log,regs
def multifreqlolog(loginfo,logfilename,elog=False):
    f=open(logfilename,'a+')
    f.write(str(loginfo))
    f.close()

#    os.system(r"echo %s |elog -h 192.168.1.24 -p 8080  -l QubiC -a Author=scriptupdate -a type=Routine"%(log.rstrip('\r\n')))
    if elog:
        rewriteelog(log.rstrip('\r\n'),msg_id=1335)#None)

if __name__=="__main__":
    #    multifreqlo(chan=int(sys.argv[1]),freq=float(sys.argv[2]),deviceid='multifreqlo_v2every')#testevery')
    #multifreqlo(chan=int(sys.argv[1]),freq=float(sys.argv[2]),deviceid='testevery')
    multifreqlo(chan=int(sys.argv[1]),freq=4.98e9,deviceid='a4spi_04')#testevery')
