import numpy
from matplotlib import pyplot
import math
import sys
import os

# d: [qid,t1fit,t2star,t2e,fiterrt1meas[1],fiterrramsey[1],fiterrspinecho[1],foffsetramsey]
# r: [qid,fidelity,fidelity_std,SPAM]

title_d=['$T_1$','$T_2r$','$T_2e$']
title_r=['Fidelity','Fidelity Std','SPAM']

#rb_valid=False # ???
#thld=0.99

d=numpy.empty((0,8),float)
r=numpy.empty((0,4),float)
for filename in sys.argv[1:]:
	d_=numpy.loadtxt(filename)
	filename_rb=filename[:8]+'rb_'+filename[8:]
	if os.path.isfile(filename_rb):
		r_=numpy.loadtxt(filename_rb)
		assert d_.shape[0]==r_.shape[0],'Please match the number of ROWs between Coherence Time Data and RB Data!'
	else:
		r_=numpy.zeros((d_.shape[0],4))
	d=numpy.vstack((d,d_))
	r=numpy.vstack((r,r_))

qubitid=[int(i) for i in sorted(set(d[:,0]))]
print('qubitid:',qubitid)
row_subplot=2
col_subplot=math.ceil(len(qubitid)/2)

for ifig,i in enumerate(qubitid):
	#index=sorted(list(set(numpy.where(r[:,0]==i)[0])&set(numpy.where(r[:,1]>thld)[0]))) if rb_valid else numpy.where(d[:,0]==i)
	index=numpy.where(d[:,0]==i)
	di=d[index]
	ri=r[index]
	for j,k in zip(range(1,4),range(4,7)):
		pyplot.figure(2*j-1)
		ax1=pyplot.subplot(row_subplot,col_subplot,ifig+1)
		dv=di[:,j]
		de=di[:,j+3]
		yerr_l=dv-1/(1/dv+de)
		yerr_h=1/(1/dv-de)-dv
		pyplot.errorbar(range(len(dv)),dv*1e6,marker='.',ls='None',yerr=[yerr_l*1e6,yerr_h*1e6])
		pyplot.xlabel('No.')
		pyplot.ylabel('%s time ($\mu$s)'%(title_d[j-1]))
		pyplot.text(0.1,0.1,'mean:%.1f$\mu$s'%(dv.mean()*1e6),transform=ax1.transAxes)
		pyplot.title('Q%d'%i)
		pyplot.suptitle(title_d[j-1])
		pyplot.savefig('staticsplot'+str(2*j-1)+'.png')

		pyplot.figure(2*j)
		ax2=pyplot.subplot(row_subplot,col_subplot,ifig+1)
		rv=ri[:,j]
		if j==1:
			pyplot.errorbar(range(len(rv)),rv,marker='.',ls='None',yerr=ri[:,2])
		else:
			pyplot.plot(rv,'.')
		pyplot.xlabel('No.')
		pyplot.ylabel(title_r[j-1])
		pyplot.text(0.1,0.1,'mean:%.4f'%(rv.mean()),transform=ax2.transAxes)
		pyplot.title('Q%d'%i)
		pyplot.suptitle(title_r[j-1])
		pyplot.savefig('staticsplot'+str(2*j)+'.png')
	pyplot.figure(len(qubitid)+7)
	ax3=pyplot.subplot(row_subplot,col_subplot,ifig+1)
	df=di[:,7]/1e3
	pyplot.plot(df,'.')
	pyplot.xlabel('No.')
	pyplot.ylabel('Offset Freq (kHz)')
	pyplot.text(0.1,0.1,'mean:%.0fkHz'%(df.mean()),transform=ax3.transAxes)
	pyplot.title('Q%d'%i)
	pyplot.suptitle('Ramsey Offset Frequency')
	pyplot.savefig('staticsplot'+str(len(qubitid)+7)+'.png')

colors=['skyblue','orange','green']
for ifig,i in enumerate(qubitid):
	#index=sorted(list(set(numpy.where(r[:,0]==i)[0])&set(numpy.where(r[:,1]>thld)[0]))) if rb_valid else numpy.where(d[:,0]==i)
	index=numpy.where(d[:,0]==i)
	di=d[index]
	for j in range(1,4):
		pyplot.figure(ifig+7)
		dv=di[:,j]
		dv=dv/1e-6
		tmean=dv.mean()
		tstd=dv.std()
		pyplot.hist(dv,histtype='bar',alpha=0.3,facecolor=colors[j-1],label='%s:%.1f$\pm$%.1f$\mu$s'%(title_d[j-1],tmean,tstd))
		pyplot.axvline(tmean,linestyle='--',linewidth=1,color=colors[j-1])#,label='%s (%.1f)'%(title[j-1],tmean))
	pyplot.title('Q%d Coherence Statistics'%i)
	pyplot.legend()
	pyplot.xlabel('Coherence Time ($\mu$s)')
	pyplot.ylabel('Count')
	pyplot.savefig('staticsplot'+str(ifig+7)+'.png')

pyplot.show()
