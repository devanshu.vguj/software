template='''import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment
class c_%(experiment)s(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def %(experiment)sseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0):
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modq7rdrv={}
		if readoutdrvamp:
			modq7rdrv.update(dict(amp=readoutdrvamp0))

		run=0;
		for irun in range(elementlength):
			self.readall(trdrv=run,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
			#add sequence here
			#trabi=self.seqs.tend()+delay1
			#self.seqs.add(trabi,         	 self.qchip.gates['Q7rabi'].modify(modq7rabi))

			self.readall(trdrv=self.seqs.tend(),delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def acq%(experiment)s(self,nget):
		return nget
	def process%(experiment)s(self,dt,filename):
		c=self.loadjsondata(filename)
		#add process here
		return []
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	%(experiment)s=c_%(experiment)s(**clargs.__dict__)
	%(experiment)s.%(experiment)sseqs(delayread=668e-9,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit)
	%(experiment)s.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			%(experiment)s.sim()
		data=%(experiment)s.acq%(experiment)s(clargs.nget)
		fprocess=%(experiment)s.savejsondata(filename=clargs.filename,extype='%(experiment)s',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=%(experiment)s.process%(experiment)s(dt=clargs.elementstep,filename=fprocess,swapdata=clargs.swapdata,swappre=clargs.swappre)
#	print 'period',period
#	print 'separation',separation
#	print 'amp',amp
#	%(experiment)s.plotrawdata(rawdata)
#	%(experiment)s.plotafterheraldingtest(iqafterherald)
#	%(experiment)s.plotpopulation_norm(population_norm)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
		'''
