import time
import os
import datetime
from matplotlib import pyplot
import numpy
import sys
sys.path.append('../..')
from qubic.qubic import experiment
from qubic.qcvv.plot import plot
from qubic.qcvv.vna import c_vna
from qubic.instrument.RFsource_BNC845 import BNC845

if __name__=="__main__":
    vna=c_vna(qubitid='vna',calirepo='../../../../qchip',debug=True)
    lor=vna.opts['wiremap'].lor
    bw=vna.opts['chassis'].fsample
    fcenter=lor
    fbw=bw
    amp=0.4
    nfreq=1000
    vna.qubicrun.setvat(vatdevice='readvat',val=0)
    fcenter=6.32e9
    fbw=0.05e9
    fcenter=6.3260e9
    fbw=0.001e9
    fx,fxstep=numpy.linspace(fcenter-fbw/2,fcenter+fbw/2,nfreq,retstep=True)
    vna.seqs(fx,t0=600e-9,amp=amp)
    fig1=pyplot.figure("s11",figsize=(15,8))
    axs1=fig1.subplots(2,1)
#    fig2=pyplot.figure("gain",figsize=(15,8))
#    axs2=fig2.subplots(2,1)
    plist=numpy.arange(-11,16,4)
    plist=numpy.arange(13,16,1)
    flist=numpy.arange(7.1e9,8.91e9,200e6)
    flist=numpy.arange(7.7e9,7.91e9,20e6)
    fig3=pyplot.figure("all",figsize=(15,8))
    axs3=fig3.subplots(len(plist),len(flist))
    data=numpy.zeros((len(plist),len(flist),nfreq),dtype=numpy.complex64)
    pyplot.ion() 
    for twpa in [0,1]:
        if twpa==0:
            BNC845(on=False)
#            BNC845(freq=8.5e9,power=9,on=True,query=False)
            runshot=100
            vna.run(runshot)
            print('adcminmax',vna.opts['chassis'].adcminmax())
            s11ref=vna.s11
            vna.ampplot(axs1[0])
            vna.phplot(axs1[1])
            vna.qubicrun.setvat(vatdevice='readvat',val=0)
        else:
            runshot=20
            for indexp,ptwpa in enumerate(plist):
                for indexf,ftwpa in enumerate(flist): 
                    BNC845(freq=ftwpa,power=ptwpa,on=True,query=False)
                    print(ftwpa,ptwpa)
                    vna.run(runshot)
                    minmax=vna.opts['chassis'].adcminmax()
                    if numpy.max(minmax)==32767 or numpy.min(minmax)==-32768:
                        color='red'
                    else:
                        color='blue'
                    print('adcminmax',minmax)
                    gain=vna.s11/s11ref
                    data[indexp,indexf]=gain
                    #fig3=pyplot.figure('live_%s_%s'%(str(ftwpa),str(ptwpa)),figsize=(4,3))
                    #axs3=fig3.subplots(2,1)
                    axs3[indexp,indexf].plot(fx,abs(gain),color=color)
                    axs3[indexp,indexf].set_ylim(-1,10)
#                    axs3[1].plot(fx,numpy.angle(gain))
                    pyplot.draw()
                    pyplot.pause(0.1)
#                    axs2[0].plot(fx,abs(gain))
#                    axs2[1].plot(fx,numpy.angle(gain))
                    time.sleep(0.5)

    pyplot.ioff()
    pyplot.savefig("gain.pdf")
    pyplot.show()

