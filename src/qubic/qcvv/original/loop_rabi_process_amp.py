from matplotlib import pyplot,dates
import datetime

#qubitid_list=['Q5','Q6']
#filenames=['looprabidata_20200212_1224','looprabidata_20200212_1455']  # attn1 5dB
#filenames=['looprabidata_20200213_1340','looprabidata_20200213_1425']  # attn1 5dB qdrv=0
#filenames=['looprabidata_20200214_0014','looprabidata_20200214_0113','looprabidata_20200214_0738','looprabidata_20200214_0801']  # attn1 5dB
#qubitid_list=['Q5','Q6','Q7']
#filenames=['looprabidata_20200214_1104']  # attn1 5dB
#filenames=['looprabidata_20200217_0005']  # attn1 5dB
#filenames=['looprabidata_20200217_1558']  # attn1 10dB
#filenames=['looprabidata_20200218_0043']  # attn1 10dB
#qubitid_list=['Q0','Q6','Q1']
#filenames=['looprabidata_20200218_2356','looprabidata_20200219_0834']  # fixed attn amp
#filenames=['looprabidata_20200220_0012']  # BPF fixedattn     amp fixedattn amp   (low readdrv, twpaon for Q6 6.781GHz 1.2dBm)
#filenames=['looprabidata_20200220_0131']  # BPF fixedattn     amp fixedattn amp   (low readdrv, twpaon for Q1 6.765GHz 1.2dBm)
qubitid_list=['Q5','Q6']
filenames=['looprabidata_20200226_1018']

for qubitid in qubitid_list:
	time=[]
	amp=[]
	for filename in filenames:
		data=open(filename,'r').read().splitlines()
		for i in range(len(data)):
			if 'save data to  _'+qubitid in data[i]:
				time_tmp=dates.date2num(datetime.datetime.strptime(data[i][-26:-4],'%Y%m%d_%H%M%S_%f'))
				time.append(time_tmp)
			if ' qubitid: '+qubitid in data[i]:
				amp.append(float(data[i].split(' ')[-1]))
	if len(time)!=len(amp):
		time=time[:len(amp)]

	fig=pyplot.figure()
	ax=fig.add_subplot(111)
	#ax.set_xticks(time)
	ax.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
	ax.plot_date(time,amp,ls='-',marker='.')
	ax.set_title(qubitid)
	ax.set_ylabel('Rabi Amplitude')
	#ax.grid(True)
	fig.autofmt_xdate(rotation=30)
	fig.tight_layout()
	#fig.show()
	#pyplot.show()

flag_locount=True#False
if flag_locount:
	lofreq=[]
	for filename in filenames:
		data=open(filename,'r').read().splitlines()
		for i in range(len(data)):
			if 'read LO freq' in data[i]:
				lofreq.append(float(data[i].split(' ')[-1]))
	if len(lofreq)!=len(time):
		lofreq=lofreq[:len(time)]
	fig=pyplot.figure()
	ax=fig.add_subplot(111)
	ax.xaxis.set_major_formatter(dates.DateFormatter('%m/%d/%Y %H:%M'))
	ax.plot_date(time,lofreq,ls='-',marker='.')
	ax.set_title('LO frequency')
	ax.set_ylabel('LO freq (Hz)')
	fig.autofmt_xdate(rotation=30)
	fig.tight_layout()
	#fig.show()
else:
	pass

pyplot.show()

