import socket
import sys
sys.path.append('../../')
from qubic.instrument  import devcom
from qubic.instrument  import switch
from qubic.instrument  import stepvat
from qubic.instrument  import multifreqlo
import json
import struct

class c_ttyserver:
    def __init__(self,port=10000):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('',port))
        print('starting up on %s port %s' % self.sock.getsockname(),file=sys.stderr)
        self.devcomid=devcom.devcom(devcom.sndev)
    def loop(self):
        while True:
            print('waiting for a connection',file=sys.stderr)
            data,client_address = self.sock.recvfrom(1024)
            datadict=json.loads(data)
            print(datadict)
            print('data',data,'from',client_address)
            print ('received "%s"' % data,file=sys.stderr)
            if datadict:
                txmsg=self.dataloop(datadict,client_address)
                self.sock.sendto(txmsg,client_address)
    def dataloop(self,datadict,client_address):
        if datadict['cmd'] == 'idn':
            txmsg=str.encode(json.dumps(self.devcomid))
        elif datadict['cmd'] == 'write':
            deviceid=datadict['deviceid']
            if deviceid in self.devcomid:
                if datadict['deviceid']=='switch8':
                    port=datadict['data']
                    serialport=self.devcomid[datadict['deviceid']]
                    loginfo=switch.switchport(port=port,serialport=serialport,device=deviceid)
                    loginfo=' '.join([str(client_address),loginfo])
                    switch.switchlog(loginfo=loginfo,logfilename='ttylog.log')
                    datadict.update(dict(status=True,result=loginfo))
                    txmsg=str.encode(json.dumps(datadict))
                elif datadict['deviceid'] in ['attn1','attn2','attn3','attn4']:
                    atten=datadict['data']
                    serialport=self.devcomid[datadict['deviceid']]
                    loginfo=stepvat.atten(atten=atten,serialport=serialport,device=deviceid)
                    loginfo=' '.join([str(client_address),loginfo])
                    stepvat.log(loginfo=loginfo,logfilename='ttylog.log')
                    datadict.update(dict(status=True,result=loginfo))
                    txmsg=str.encode(json.dumps(datadict))
                elif datadict['deviceid'] in ['multifreqlo_v1','testevery','multifreqlo_v2every','a4spi_01','a4spi_02','a4spi_03','a4spi_04']:
                    chan=datadict['chan']
                    freq=datadict['freq']
                    serialport=self.devcomid[datadict['deviceid']]
                    loginfo,regs=multifreqlo.multifreqlochanfreq(chan=chan,freq=freq,serialport=serialport,device=deviceid)
                    #loginfo='temp log info'
                    datadict.update(dict(status=True,result=loginfo))
                    txmsg=str.encode(json.dumps(datadict))
                else:
                    datadict.update(dict(status=False,result="No such device on ttyserver"))
                    txmsg=str.encode(json.dumps(datadict))
            else:
                datadict.update(dict(status=False,client_address=client_address,result='No such device on ttyserver'))
                txmsg=str.encode(json.dumps(datadict))
        else:
            datadict.update(dict(status=False,client_address=client_address,result='Unknown command for ttyserver'))
            txmsg=str.encode(json.dumps(datadict))
        return txmsg
if __name__=="__main__":
    ttyserver=c_ttyserver(10000)
    ttyserver.loop()
