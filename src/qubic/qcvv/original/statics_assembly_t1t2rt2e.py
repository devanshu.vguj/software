import sys
f=open(sys.argv[1])
pdfs=f.read()
f.close()
linetemplate=r'''\includegraphics[width=0.14\textwidth,page=1]{./alldata/%s}'''
tex=r'''\documentclass[10pt,letterpaper,landscape]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[margin=0pt]{geometry}
\begin{document}
\setlength{\parindent}{0cm}
%s
\end{document}'''
index=1
mtypes=['ramsey','t1meas','spinecho']
lines={}
for pdf in pdfs.split('\n'):
	if len(pdf):
		for mtype in mtypes:
			if mtype not in lines:
				lines[mtype]={}
			for qid in ['Q'+str(i) for i in range(1,7)]:
				if qid not in lines[mtype]:
					lines[mtype][qid]=[]
				if mtype in pdf:
					if qid in pdf:

						line=linetemplate%(pdf)
						lines[mtype][qid].append(line)
s=[]
for mtype in mtypes:
	for qid in ['Q'+str(i) for i in range(1,7)]:
		s.append(mtype)
		s.append(qid)
		s.append(r'\\')
		index=0
		for line in lines[mtype][qid]:
			s.append(line)
			index=index+1
			if index%7==0:
				s.append(r'\\')
		s.append(r'\newpage')
print(tex%('\n'.join(s)))

