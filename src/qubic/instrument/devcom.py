import usb
import serial.tools.list_ports
import serial
import sys
import time
import datetime

sndev={'B14E4CB55151363448202020FF172513':'attn1'
    ,'C1715E095151363448202020FF18241D':'attn2'
    ,'85828FEF5151363448202020FF172C4A':'attn3'
    ,'849DA7495151363448202020FF183419':'attn4'
    ,'956B5CCE514D355936202020FF07271E':'multifreqlo_v2every'
    ,'AK08KYQC':'switch8'
    ,'A90834I1':'multifreqlo_v1'
    ,'DEE6DD8F5151363448202020FF18401B':'testevery'
    ,'DF15D71A515146544E4B2020FF0C400F':'a4spi_02'
    ,'5AC2A410515146544E4B2020FF0B1C4C':'a4spi_03'
    ,'37C79493515146544E4B2020FF093846':'a4spi_04'
    }

def devcom(sndev,idVendor=0x2341,idProduct=0x0058,debug=0):
    devs=usb.core.find(idVendor=idVendor,idProduct=idProduct,find_all=True)
#    arduino_sn=[]
#    for dev in devs:
#        arduino_sn.append(usb.util.get_string(dev,dev.iSerialNumber))
#        print('***',usb.util.get_string(dev,dev.iSerialNumber))
    coms=serial.tools.list_ports.comports()
    devcom={}
    for com in coms:
        comsn=com.serial_number
        comdev=com.device
        if debug:
            print('com.device',com.device)
            print('comsn',comsn)
        if comsn in sndev.keys():
            devcom[sndev[comsn]]=comdev
            if debug:
                print(sndev[comsn],comsn)
    return devcom

if __name__=="__main__":
    for k,v in devcom(sndev,debug=1).items():
        print(k,v)
