import os
try:
	noXterm=os.environ['QUBICnoXTERM']
	print('noXterm',noXterm,'Using matplotlib / pylab without a DISPLAY')
	import matplotlib
	matplotlib.use('Agg')
except:
	print('yesXterm','Using matplotlib / pylab with a DISPLAY')
	import matplotlib
	matplotlib.use('TkAgg')

import datetime
from matplotlib import pyplot
import numpy
from ..qubic import experiment


class c_rabi(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		self.qubitid_x=None
		pass
	def rabiseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid='Q5',qubitidread=['Q7','Q6','Q5'],xtalk_compensate=False,amp_multiplier=0,phase_offset=0,qubitid_x='Q7'):
		self.qubitid=qubitid
		self.qubitid_x=qubitid_x
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modrdrv={}
		modread={}
		modrabi={}
		modrabi_simul={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if qubitdrvamp is not None:
			modrabi.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modrabi.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		if preadout is not None:
			modread.update(dict(pcarrier=preadout))

		run=0
		for irun in range(elementlength):
			therald=run
			self.seqs.add(therald,self.qchip.gates[self.qubitid+'read'].modify([modrdrv,modread]))
			trabi=self.seqs.tend()+delay1
			wrabi=elementstep*irun
			if wrabi!=0:
				modrabi.update(dict(twidth=wrabi))
				self.seqs.add(trabi,         	 self.qchip.gates[self.qubitid+'rabi'].modify(modrabi))
				if xtalk_compensate:
					pcarrier_simul_tmp=self.qchip.gates[self.qubitid+'rabi'].pcalc(padd=phase_offset)[0]
					amp_simul_tmp=self.qchip.gates[self.qubitid+'rabi'].paralist[0]['amp']*amp_multiplier
					modrabi_simul.update(dict(twidth=wrabi,dest=self.qubitid_x+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
					self.seqs.add(trabi,         self.qchip.gates[self.qubitid+'rabi'].modify(modrabi_simul))
				treaddrv=self.seqs.tend()
			else:
				treaddrv=self.seqs.tend()+delay1

			self.seqs.add(treaddrv,self.qchip.gates[self.qubitid+'read'].modify([modrdrv,modread]))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabiacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabi(self,dt,filename,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		c=self.loadjsondata(filename)
		return self.processrabidata(dt=dt,data=c[list(c.keys())[0]],dumpdataset=dumpdataset,loaddataset=loaddataset,plot=plot,isfitdecay=isfitdecay)
	def processrabidata(self,dt,data,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		self.rabi_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		if isfitdecay:
			[amp,period,tdecay,fiterr]=self.fitrabidecay(dt=dt,data=self.rabi_result['population_norm'],plot=plot)
		else:
			[amp,period,fiterr]=self.fitrabi(dx=dt,data=self.rabi_result['population_norm'],plot=plot)
		return [data,self.rabi_result['separation'],self.rabi_result['iqafterherald'],self.rabi_result['population_norm'],amp,period,fiterr]
	def plotrabicol(self,bufwidth):
		meas=self.rabi_result['meas'].reshape((-1,bufwidth))
		herald=self.rabi_result['herald'].reshape((-1,bufwidth))
		lim=1.1*max(max(abs(meas.reshape((-1,1)).real)),max(abs(meas.reshape((-1,1)).imag)))
		for i in range(bufwidth):
			iqafterheraldcol=numpy.extract(herald[:,i],meas[:,i])
			pyplot.clf()
			ax=pyplot.subplot(111)
			ax.set_aspect('equal')
			ax.set_xlim([-lim,lim])
			ax.set_ylim([-lim,lim])
			pyplot.grid()
			pyplot.hexbin(iqafterheraldcol.real,iqafterheraldcol.imag,gridsize=30,cmap='Greys',bins='log')
			pyplot.savefig('animation_%02d.png'%i)


def main():
	parser,cmdlinestr=experiment.cmdoptions()
	print('cmdlinestr',cmdlinestr)
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	rabi=c_rabi(**clargs.__dict__)
	if clargs.sim:
		rabi.setsim()
	rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread,xtalk_compensate=clargs.xtalk_compensate,amp_multiplier=clargs.amp_multiplier,phase_offset=clargs.phase_offset,qubitid_x=clargs.qubitid_x)
	if clargs.processfile=='':
		rabi.run(bypass=clargs.bypass)
		data=rabi.rabiacq(clargs.nget)
		fprocess=rabi.savejsondata(filename=clargs.filename,extype='rabi',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			rabi.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot,isfitdecay=False)
	print('period',period)
	print('separation',separation)
	print('amp',amp)
	print(rabi.hf.adcminmax())
	if clargs.xtalk_compensate:
		print('gateamp_corr',128e-9/period)
	if clargs.readcorr:
		population_norm=rabi.readoutcorrection(qubitid_list=[clargs.qubitid],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
		print('corrected population_norm',population_norm)
	rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	#rabi.plotafterheraldingtest(iqafterherald)
	rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
		#rabi.plotrabicol(bufwidth=80)   # uncomment this line and "return" line in experience.process3 when dealing with the animation
	if clargs.removefile:
		os.remove(fprocess)
if __name__=="__main__":
	main()#sys.argv[1:])
# python rabi_w.py --plot -n 50 --qubitid Q5 --qubitidread Q5
# python rabi_w.py --plot -n 50 --qubitid Q5 --qubitidread Q5 --readcorr
# python rabi_w.py --plot -n 50 --qubitid Q5 --readcorr -el 40 -es 8e-9 --xtalk_compensate --qubitid_x Q7 --amp_multiplier 0.5 --phase_offset 3.5416  --qubitidread??
# python rabi_w.py --plot -n 50 --qubitid Q6 --readcorr -el 40 -es 4e-9 --xtalk_compensate --qubitid_x Q7 --amp_multiplier 1.625 --phase_offset 5.8916  --qubitidread??
