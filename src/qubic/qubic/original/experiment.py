print('debug experiment __name__',__name__)
import datetime
import argparse
import sys
import json
#from squbic import *
from matplotlib import pyplot
import numpy
#from ether import c_ether
#from mem_gateway import c_mem_gateway
import time
from qubic.qubic import init
#	from ..qcvv import qtrl
from qubic.qubic import squbic
from qubic.qubic import hfbridge
import pprint
from sklearn import mixture
from joblib import dump,load
from scipy.optimize import curve_fit
import os
import copy
from itertools import product
import random
from matplotlib.colors import LogNorm
import functools

def line(t,a,b):
	return a*t+b
def fullsin(t,a,f,p,c):
	return a*numpy.sin(2*numpy.pi*f*t+p)+c
# rabi fit
def sin_square(t, a, f, p, c = 0): # set default constant offset to zero
	return a * numpy.sin(2*numpy.pi*f * t + p)**2 + c
# t1 and spin-echo fit
def expfit(t, a, l, c = 0): # set default constant offset to zero
	return a * numpy.exp(-l * t) + c
# ramsey fit
def exp_sin(t, a, l, f, p, c): # input, amplitude, decay parameter, angular freq, offset phase, constant offset
	return a * numpy.exp(-l * t) * numpy.sin(2*numpy.pi*f * t + p) + c # ideally c = 0.5
def rb_func(x,p,a): # fit this to a power law
	return a*p**x+0.5

class c_experiment:
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',gmmdatasetspath='gmmdatasets.json',wiremapmodule='wiremap',simpseqs=False,**kwargs):
		self.simpseqs=simpseqs
		print('debug wavegrp',wavegrppath)
		self.hf=hfbridge.c_hfbridge(ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,wiremapmodule=wiremapmodule)
		print('qubitcfg file name',qubitcfg)
		with open(qubitcfg) as jfile:
			chipcfg=json.load(jfile)
		self.qchip=squbic.c_qchip(chipcfg)
		self.initseqs()
		self.init=getattr(init,initcfg)
		self.dt=dt
		with open(gmmdatasetspath) as gfile:
			self.gmmdatasets=json.load(gfile)
	def run(self,preread=False,bypass=False,memclear=True,cmdclear=True,memscale=None,cmdclearlength=0,memwrite=True,cmdwrite=True,periodwrite=True):
		print('compiling')
		if self.simpseqs:
			pass
		else:
			self.hf.cmdgen0(seqs=self.seqs,bypass=bypass)
			self.hf.cmdgen1(bypass=bypass)
			self.hf.cmdgen2(bypass=bypass)
		print('run')
		if not bypass:
			self.hf.run(seqs=self.seqs,init=self.init,preread=preread,memclear=memclear,cmdclear=cmdclear,memscale=memscale,cmdclearlength=cmdclearlength,memwrite=memwrite,cmdwrite=cmdwrite,periodwrite=periodwrite)
	def initseqs(self):
		if self.simpseqs:
			self.seqs=squbic.c_simpseqs()
		else:
			self.seqs=squbic.c_seqs()
		self.hf.initelem()
	def setsim(self):
		self.hf.sim=True
	def sim(self,mod=True):
		self.hf.sim1(mod=mod)
	def add(self,t,gate):
		self.seqs.add(t,gate)
	def acqdata(self,ngetdata=1):
		bufsize=2**12
		#chan=[i for i in range(len(self.bufwidth)) if self.bufwidth[i]]#,reverse=True)
		#print 'bufwidth',self.bufwidth
		#print 'chan',chan
		bufreadwidth=self.hf.getbufreadwidth()
		trash=[(bufsize)%(2*w) for w in self.hf.getbufreadwidth()]  # divided by 2 for the I Q
		print('bufsize',bufsize,'bufreadwidth',bufreadwidth)
		print('trash',trash)
#		bufa=numpy.empty((len(chan)
		for nget in range(ngetdata):
			#print('#########################',nget)
			if (nget%10)==0:
				print('%3d of %3d'%(nget,ngetdata))
			newbuf = self.hf.accbuf()
			accbuf = newbuf if nget==0 else numpy.append(accbuf,newbuf,0)
		print('accbuf shape',accbuf.shape)
		r,c=accbuf.shape
		bufresult={}
		for i in range(len(trash)):
			if bufreadwidth[i]>0:
				print('debug acqdata',i,bufsize,accbuf.shape)
				bufsquare=accbuf[:,i].reshape((-1,bufsize))
				print(i,trash[i])
				bufuse=bufsquare if trash[i]==0 else bufsquare[:,0:-trash[i]]
				bufuseiq = bufuse.reshape((-1,2))
				bufcomp = bufuseiq[:,0]+1j*bufuseiq[:,1]
				bufresult[self.hf.bufread[i]]=[[c.real,c.imag] for c in bufcomp]
		return bufresult

	def process3(self,data,qubitid,lengthperrow,training=False,dumpdataset='',loaddataset='',partition=False,n_gaussians=2):
		#print('experiment process3',training,loaddataset)
		if training:
			d1=data.reshape((-1,1))
			d1iq=numpy.append(d1.real,d1.imag,1)
			gmix_raw=self.gmmfitc(d1,n_gaussians=n_gaussians)
			d1predict=gmix_raw.predict(d1iq).reshape((-1,2))
			swap=self.gmmswapbyherald(d1predict[:,0])
			ph_raw=d1predict[:,0].reshape((-1,int(lengthperrow/2)))==(1 if swap else 0)
			pm_raw=d1predict[:,1].reshape((-1,int(lengthperrow/2)))==(0 if swap else 1)
			count_raw=ph_raw.sum(0)
			population_norm_raw=1.0*(ph_raw*pm_raw).sum(0)/count_raw
			if partition:
				[amp,period,fiterr]=self.fitrabi(dx=1,data=population_norm_raw,plot=False)
				partial=int((lengthperrow/2)%numpy.round(period))
				ph_raw_partial=ph_raw[:,0:-partial]
				meas_raw_partial=((d1.reshape((-1,2))[:,1]).reshape((-1,int(lengthperrow/2))))[:,0:-partial]
			else:
				ph_raw_partial=ph_raw[:,:]
				meas_raw_partial=((d1.reshape((-1,2))[:,1]).reshape((-1,int(lengthperrow/2))))[:,:]
			meas_training=numpy.extract(ph_raw_partial,meas_raw_partial).reshape((-1,1))
			means_init=gmix_raw.means_[::-1] if swap else gmix_raw.means_
			gmix=self.gmmfitc(meas_training,means_init=means_init,n_gaussians=n_gaussians)
			dumpdatasetname='gmix'+dumpdataset+'.joblib'
			dump(gmix,dumpdatasetname)
			self.gmmdatasets[qubitid]=dumpdatasetname
			fwrite=open('gmmdatasets.json','w')
			json.dump(self.gmmdatasets,fwrite,indent=4)
			fwrite.close()
		else:
			if loaddataset:
				gmix=load(loaddataset)
			else:
				gmix=load(self.gmmdatasets[qubitid])
		result=self.getgmmresult(data,qubitid,lengthperrow,gmix=gmix,n_gaussians=n_gaussians)
		return dict((k,result[k]) for k in ["separation","population_norm","iqafterherald","population_norm2","population_norm0","0","1"])
#		predict=gmix.predict(d1iq).reshape((-1,2))
#		ph=predict[:,0].reshape((-1,lengthperrow/2))==0
#		pm=predict[:,1].reshape((-1,lengthperrow/2))==1
#		count=ph.sum(0)
#		population_norm=1.0*(ph*pm).sum(0)/count # array multiplication using * (elementwise operation)
#		print 'predict.shape',predict.shape,
#		print '1.0*(ph*pm).sum(0)',1.0*(ph*pm).sum(0),
#		print 'count',count,
#		print 'population_norm',population_norm
#		iqafterherald=numpy.extract(ph,d1.reshape((-1,2))[:,1].reshape((-1,lengthperrow/2))).reshape((-1,1))
#		separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.T)**2)/numpy.sqrt(numpy.product(gmix.covariances_)))  # separation of trained rabi model blobs
		#return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1)}
		#return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1),"herald":ph.reshape((-1,1)),"meas":d1.reshape((-1,2))[:,1]}    # uncomment this line and rabi.plotrabicol line (in rabi_w.py) when dealing with the animation

	def process_trueq(self,data,qubitid,lengthperrow,loaddataset=''):
		if loaddataset:
			gmix=load(loaddataset)
		else:
			gmix=load(self.gmmdatasets[qubitid])
		result=self.getgmmresult(data,qubitid,lengthperrow,gmix=gmix)
		return dict((k,result[k]) for k in ["separation","population_norm","0","1"])

	def getgmmresult(self,data,qubitid,lengthperrow,gmix,n_gaussians=2):
		print('getgmmresult',data.shape,type(data))
		d1=data.reshape((-1,1))
		d1iq=numpy.append(d1.real,d1.imag,1)
		predict=gmix.predict(d1iq).reshape((-1,2))
		print('lengthperrow',lengthperrow)
		ph=predict[:,0].reshape((-1,int(lengthperrow/2)))==0
		pm=predict[:,1].reshape((-1,int(lengthperrow/2)))==1

		count=ph.sum(0)
		population_norm=1.0*(ph*pm).sum(0)/count # array multiplication using * (elementwise operation)
		if (n_gaussians==3):
			pg=predict[:,1].reshape((-1,int(lengthperrow/2)))==0
			population_norm0=1.0*(ph*pg).sum(0)/count # array multiplication using * (elementwise operation)
			pf=predict[:,1].reshape((-1,int(lengthperrow/2)))==2
			population_norm2=1.0*(ph*pf).sum(0)/count # array multiplication using * (elementwise operation)
		else:
			pf=0
			population_norm2=0
			population_norm0=0
		print('predict.shape',predict.shape,end='')
		print('1.0*(ph*pm).sum(0)',1.0*(ph*pm).sum(0),end='')
		print('count',count,end='')
		print('population_norm',population_norm)
		print('|0> in the heralding:',1.0*ph.sum(0)/numpy.shape(ph)[0])
		iqafterherald=numpy.extract(ph,d1.reshape((-1,2))[:,1].reshape((-1,int(lengthperrow/2)))).reshape((-1,1))
		separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.T)**2)/numpy.sqrt(numpy.product(gmix.covariances_)))  # separation of trained rabi model blobs
		count_1=(ph*pm).sum(0)
		count_0=count-count_1
		return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1),"0":count_0,"1":count_1,"population_norm2":population_norm2,"population_norm0":population_norm0}
		#return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1),"herald":ph.reshape((-1,1)),"meas":d1.reshape((-1,2))[:,1]}    # uncomment this line and rabi.plotrabicol line (in rabi_w.py) when dealing with the animation 

	def process4(self,data,qubitid,lengthperrow,training=False,dumpdataset='',loaddataset='',partition=False):
		nreset=1
		d1=data.reshape((-1,1))
		d1iq=numpy.append(d1.real,d1.imag,1)
		lengthperrow=int(lengthperrow/(nreset+2.0)*2.0)
		if 1:
			if loaddataset:
				gmix=load(loaddataset)
			else:
				gmix=load(self.gmmdatasets[qubitid])
		predict3=gmix.predict(d1iq).reshape((-1,nreset+2))
		predict=predict3[:,[0,-1]]
		ph=predict[:,0].reshape((-1,int(lengthperrow/2)))==0
		pm=predict[:,1].reshape((-1,int(lengthperrow/2)))==1
		count=ph.sum(0)
		population_norm=1.0*(ph*pm).sum(0)/count # array multiplication using * (elementwise operation)
		print('predict.shape',predict.shape,end='')
		print('1.0*(ph*pm).sum(0)',1.0*(ph*pm).sum(0),end='')
		print('count',count,end='')
		print('population_norm',population_norm)
		iqafterherald=numpy.extract(ph,d1.reshape((-1,nreset+2))[:,0].reshape((-1,int(lengthperrow/2)))).reshape((-1,1))
		separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.T)**2)/numpy.sqrt(numpy.product(gmix.covariances_)))  # separation of trained rabi model blobs
		return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1)}
		#return {"separation":separation,"population_norm": population_norm,"iqafterherald":numpy.append(iqafterherald.real,iqafterherald.imag,1),"herald":ph.reshape((-1,1)),"meas":d1.reshape((-1,2))[:,1]}    # uncomment this line and rabi.plotrabicol line (in rabi_w.py) when dealing with the animation

	def process2q(self,data_list,qubitid_list,lengthperrow_list,loaddataset_list=None):
		ph_list=[]
		pm_list=[]
		for i,qid in enumerate(qubitid_list):
			d=data_list[i].reshape((-1,1))
			diq=numpy.append(d.real,d.imag,1)
			if loaddataset_list:
				gmix=load(loaddataset_list[i])
			else:
				gmix=load(self.gmmdatasets[qid])
			predict=gmix.predict(diq).reshape((-1,2))
			ph=predict[:,0].reshape((-1,int(lengthperrow_list[i]/2)))==0
			pm=predict[:,1].reshape((-1,int(lengthperrow_list[i]/2)))==1
			ph_list.append(ph)
			pm_list.append(pm)
		ph_list=ph_list[::-1]
		pm_list=pm_list[::-1]
		herald=functools.reduce(lambda x,y:x*y,ph_list) # array multiplication using * (elementwise operation)
#		meas_unbinned=qtrl.binary_arrays_to_bins(pm_list)
		meas_unbinned=numpy.sum(numpy.einsum('i..., i->i...', x, system_levels ** numpy.arange(numpy.array(pm_list).shape[0])), 0)
		meas_sum=numpy.sum([(meas_unbinned==i)*herald for i in range(2**len(qubitid_list))],1)
		meas_sum[0,:]=herald.sum(0)-meas_sum[1:,:].sum(0)
		meas_binned=1.0*meas_sum/herald.sum(0)
		print('|0> in the heralding:',1.0*herald.sum(0)/numpy.shape(herald)[0])
		#print('herald',herald)
		#print('meas_unbinned',meas_unbinned)
		print('meas_sum',meas_sum)
		print('herald.sum(0)',herald.sum(0))
		print('meas_binned',meas_binned,meas_binned.shape)
		#meas_binned=meas_binned.reshape(-1)
		return {"meas_binned":meas_binned,"meas_sum":meas_sum}

	def readoutcorrection(self,qubitid_list,meas_binned,corr_matrix):
		""" Work for single qubit or two qubits.
		1. qubitid_list can be ['Q6','Q5'] or ['Q6']
		2. corr_matrix=numpy.load('corrmatrix.npz')
		3. meas_binned is numpy.vstack((1-population_norm,population_norm)) for single qubit or meas_binned from process2q for two qubits"""
		corr_matrix=numpy.reshape(numpy.vstack([corr_matrix[qubitid] for qubitid in qubitid_list]),[-1,1,2,2])
		r=numpy.matrix([1],dtype='complex')
		for l in corr_matrix:
			result=numpy.kron(r,l)
		corr_matrix=numpy.array(r.real.astype(float))

	#	corr_matrix=numpy.array(qtrl.tensor(*corr_matrix).real.astype(float))
		#print('***corr_matrix***',corr_matrix,corr_matrix.shape)
		#print('***meas_binned***',meas_binned,meas_binned.shape)
		binned_data=numpy.linalg.inv(corr_matrix)@meas_binned
		#print('***binned_data***',binned_data,binned_data.shape)
		#binned_data=numpy.linalg.solve(corr_matrix,meas_binned)
		return binned_data

	def process3_1read(self,data,lengthperrow,loadname=''):
		d1=data.reshape((-1,1))
		d1iq=numpy.append(d1.real,d1.imag,1)
		gmix=load(loadname)
		predict=gmix.predict(d1iq).reshape((-1,1))
		pm=predict.reshape((-1,lengthperrow))==1
		count=pm.shape[0]
		population_norm=1.0*pm.sum(0)/count
		separation=numpy.sqrt(numpy.sum(numpy.diff(gmix.means_.T)**2)/numpy.sqrt(numpy.product(gmix.covariances_)))  # separation of trained rabi model blobs
		return {"separation":separation,"population_norm": population_norm}

	def gmmswapbymean(self,means,p0,p1):
		swap=(p0==1 and p1==0) if abs(numpy.angle(means[0][0]+1j*means[0][1]))<abs(numpy.angle(means[1][0]+1j*means[1][1])) else (p0==0 and p1==1)
		return swap
	def gmmswapbyherald(self,herald_predict):
		swap=True if numpy.sum(herald_predict)>len(herald_predict)/2.0 else False
		print('|0> in the heralding:',1.0*numpy.sum(herald_predict)/len(herald_predict) if swap else 1.0-1.0*numpy.sum(herald_predict)/len(herald_predict))
		return swap

	def gmmpredictc(self,gmm,datac):
		dataiq=numpy.append(datac.real,datac.imag,1)
		return gmm.predict(dataiq)
	def gmmpredictpc(self,gmm,datac):
		dataiq=numpy.append(datac.real,datac.imag,1)
		return gmm.predict_proba(dataiq)
	def gmmfitc(self,datac,n_gaussians=2,means_init=None):
		iq=numpy.append(datac.real,datac.imag,1)
		return self.gmmfit(data=iq,means_init=means_init,n_gaussians=n_gaussians)
	def gmmfit(self,data,means_init,n_gaussians=2):
		gmix=mixture.GaussianMixture(n_components=n_gaussians,covariance_type='spherical',means_init=means_init)
		gmix.fit(data)
#		order=numpy.argsort(gmix.means_[:,0])
#		gmix.means_=gmix.means_[order]
#		gmix.covariances_=gmix.covariances_[order]
		return gmix

	def plotrawdata(self,d1,figname='',limit=True,hexbin=True):
		d1raw_all=d1
		d1raw_herald=d1.reshape((-1,2))[:,0]
		d1raw_regular=d1.reshape((-1,2))[:,1]
		lim=max(1.1*max(max(abs(d1raw_all.real)),max(abs(d1raw_all.imag))),0.1)

		pyplot.figure(figname,figsize=(30,8))
		ax1=pyplot.subplot(131)
		ax1.set_aspect('equal')
		if limit:
			ax1.set_xlim([-lim,lim])
			ax1.set_ylim([-lim,lim])
		ax1.set_title(figname+'all')
		pyplot.grid()
		if hexbin:
			pyplot.hexbin(d1raw_all.real,d1raw_all.imag,gridsize=30,cmap='Greys',bins='log')
		else:
			pyplot.plot(d1raw_all.real,d1raw_all.imag,'*')
		ax2=pyplot.subplot(132)
		ax2.set_aspect('equal')
		if limit:
			ax2.set_xlim([-lim,lim])
			ax2.set_ylim([-lim,lim])
		ax2.set_title(figname+'heralding')
		pyplot.grid()
		if hexbin:
			pyplot.hexbin(d1raw_herald.real,d1raw_herald.imag,gridsize=100,cmap='Greys',bins='log')
		else:
			pyplot.plot(d1raw_herald.real,d1raw_herald.imag,'*')
		ax3=pyplot.subplot(133)
		ax3.set_aspect('equal')
		if limit:
			ax3.set_xlim([-lim,lim])
			ax3.set_ylim([-lim,lim])
		ax3.set_title(figname+'regular')
		pyplot.grid()
		if hexbin:
			#pyplot.hexbin(d1raw_regular.real,d1raw_regular.imag,gridsize=30,cmap='Greys',bins='log')
			pyplot.hexbin(d1raw_regular.real,d1raw_regular.imag,cmap='Greys',norm=LogNorm(),alpha=0.4)
		else:
			pyplot.plot(d1raw_regular.real,d1raw_regular.imag,'*')

		pyplot.savefig(figname+'.pdf')

	def plotiqtest(self,d0,d1,d2,figname='',limit=True):
		d0raw_all=d0
		d0raw_herald=d0.reshape((-1,2))[:,0]
		d0raw_regular=d0.reshape((-1,2))[:,1]
		lim0=max(1.1*max(max(abs(d0raw_all.real)),max(abs(d0raw_all.imag))),0.1)
		d1raw_all=d1
		d1raw_herald=d1.reshape((-1,2))[:,0]
		d1raw_regular=d1.reshape((-1,2))[:,1]
		lim1=max(1.1*max(max(abs(d1raw_all.real)),max(abs(d1raw_all.imag))),0.1)
		d2raw_all=d2
		d2raw_herald=d2.reshape((-1,2))[:,0]
		d2raw_regular=d2.reshape((-1,2))[:,1]
		lim2=max(1.1*max(max(abs(d2raw_all.real)),max(abs(d2raw_all.imag))),0.1)
		lim=max(lim0,lim1,lim2)
		pyplot.figure(figname)
		ax1=pyplot.subplot(111)
		ax1.set_aspect('equal')
		if limit:
			ax1.set_xlim([-lim,lim])
			ax1.set_ylim([-lim,lim])
		ax1.set_title(figname+'all')
		pyplot.grid()
		pyplot.plot(d0raw_all.real,d0raw_all.imag,'.',label='Q7')
		pyplot.plot(d1raw_all.real,d1raw_all.imag,'.',label='Q6')
		pyplot.plot(d2raw_all.real,d2raw_all.imag,'.',label='Q5')
		pyplot.legend()

	def plotafterheraldingtest(self,result):
		#print 'plotafterheraldingtest',result.shape
		pyplot.figure()
		cx=pyplot.subplot(111)
		cx.set_aspect('equal')
#		cx.set_xlim([-lim,lim])
#		cx.set_ylim([-lim,lim])
		cx.set_title('taking off points according to the heralding')
		pyplot.hexbin(result[:,0],result[:,1],gridsize=30,cmap='Greys',bins='log')
		#pyplot.savefig(clargs.filename+'_iq.png')

	def plotpopulation_norm(self,population_norm,figname='',population_norm2=None,population_norm0=None):
		if figname:
			pyplot.figure(figname)
		else:
			pyplot.figure()
		if population_norm2 is not None:
			pyplot.plot(population_norm2,'.-',color='b')
		if population_norm0 is not None:
			pyplot.plot(population_norm0,'.-',color='g')
		if (population_norm2 is not None) or (population_norm0 is not None):
			pyplot.plot(population_norm,'.-',color='r')
		else:
			pyplot.plot(population_norm,'.',color='r')
		pyplot.ylim(-0.05,1.05)
		pyplot.axhline(0.5, linestyle='--', color='k')
		pyplot.grid()
		pyplot.title(figname)
		#pyplot.savefig(figname+'.png')
		pyplot.savefig(figname+'.pdf')

	def plotrb(self,recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=''):
		x=el_clif_list_pair
		pyplot.figure(1)
		pyplot.plot(x,recovery_population_array.T,'.')
		pyplot.xlabel('Number of Clifford Pairs')
		pyplot.ylabel('Recovery Population')
		#pyplot.yscale('log')
		pyplot.ylim(0,1)
		#pyplot.ylim(0.1,1)
		pyplot.tight_layout()
		pyplot.savefig('rbraw_recovery'+figname+'.png',dpi=600)
		pyplot.figure(2)
		yerr=recovery_population_std
		pyplot.errorbar(x,recovery_population_mean,yerr=yerr)
		pyplot.xlabel('Number of Clifford Pairs')
		pyplot.ylabel('Averaged Recovery Population')
		#pyplot.yscale('log')
		pyplot.ylim(0,1)
		#pyplot.ylim(0.1,1)
		pyplot.tight_layout()
		pyplot.savefig('rbraw_average_recovery'+figname+'.png',dpi=600)

	def I(self,n=2):
		return numpy.asmatrix(numpy.eye(n,dtype='complex'))

	def rot_x(self,theta):
		c,s=numpy.cos(theta/2.),numpy.sin(theta/2.)
		#return numpy.matrix([[c,-1j*s],[-1j*s,c]])
		return numpy.matrix([[c,-1j*s],[-1j*s,c]]).round(15)

	def rot_y(self,theta):
		c,s=numpy.cos(theta/2.),numpy.sin(theta/2.)
		#return numpy.matrix([[c,-s],[s,c]])
		return numpy.matrix([[c,-s],[s,c]]).round(15)

	def rot_z(self,theta):
		exp_z,exp_z_inv=numpy.exp(1j*theta/2.),numpy.exp(-1j*theta/2.)
		#return numpy.matrix([[exp_z_inv,0],[0,exp_z]])
		return numpy.matrix([[exp_z_inv,0],[0,exp_z]]).round(15)

	def rho(self,theta,phi):
		c_theta,s_theta=numpy.cos(theta),numpy.sin(theta)
		c_phi,s_phi=numpy.cos(phi),numpy.sin(phi)
		#return 0.5*numpy.matrix([[1+c_theta,c_phi*s_theta-1j*s_phi*s_theta],[c_phi*s_theta+1j*s_phi*s_theta,1-c_theta]])
		return 0.5*numpy.matrix([[1+c_theta,c_phi*s_theta-1j*s_phi*s_theta],[c_phi*s_theta+1j*s_phi*s_theta,1-c_theta]]).round(15)

	#def tuple_rho(self,rho,digits=15):
	#	return tuple(numpy.array(rho.round(digits)).flatten())

	def unitary_rot(self,rot,rho):
		return rot*rho*numpy.linalg.inv(rot)

	def g_list_simplify(self,g_list):
		"""http://www.vcpc.univie.ac.at/~ian/hotlist/qc/talks/bloch-sphere-rotations.pdf
		https://en.wikipedia.org/wiki/IEEE_754,For Binary64, DecimalDigits=15.95"""
#		digits=15  # For Binary64, DecimalDigits=15.95
#		simp_dict={self.tuple_rho(self.rho(0,0),digits):'I',
#				self.tuple_rho(self.rho(numpy.pi,0),digits):random.choice(['X180','Y180']),
#				self.tuple_rho(self.rho(numpy.pi/2,0),digits):'Y90',
#				self.tuple_rho(self.rho(numpy.pi/2,numpy.pi),digits):'Y270',
#				self.tuple_rho(self.rho(numpy.pi/2,numpy.pi/2),digits):'X270',
#				self.tuple_rho(self.rho(numpy.pi/2,numpy.pi/2*3),digits):'X90'}
		simp_dict={tuple(numpy.array(self.rho(0,0)).flatten()):'I',  #random.choice(['I','Z90','Z180','Z270']),
				tuple(numpy.array(self.rho(numpy.pi,0)).flatten()):random.choice(['X180','Y180']),
				tuple(numpy.array(self.rho(numpy.pi/2,0)).flatten()):'Y90',
				tuple(numpy.array(self.rho(numpy.pi/2,numpy.pi)).flatten()):'Y270',
				tuple(numpy.array(self.rho(numpy.pi/2,numpy.pi/2)).flatten()):'X270',
				tuple(numpy.array(self.rho(numpy.pi/2,numpy.pi/2*3)).flatten()):'X90'}
		gate_dict={'I':self.I(),
				'X90':self.rot_x(numpy.pi/2),
				'Y90':self.rot_y(numpy.pi/2),
				'Z90':self.rot_z(numpy.pi/2),
				'X180':self.rot_x(numpy.pi),
				'Y180':self.rot_y(numpy.pi),
				'Z180':self.rot_z(numpy.pi),
				'X270':self.rot_x(numpy.pi/2*3),
				'Y270':self.rot_y(numpy.pi/2*3),
				'Z270':self.rot_z(numpy.pi/2*3)}
		#rho0=self.rho(0,0)
		#rho=reduce(lambda rho, rot: self.unitary_rot(rot,rho), [rho0]+map(gate_dict.get,g_list))
		rho=self.rho(0,0)
		for g in g_list:
			rho=self.unitary_rot(gate_dict[g],rho)
		g_simplified=simp_dict[tuple(numpy.array(rho.round(15)).flatten())]
		#g_simplified=simp_dict[self.tuple_rho(rho,digits)]
		#print 'g_simplified',g_simplified
		return g_simplified

	def g_list_add_inverse(self,g_list):
		g_simplified=self.g_list_simplify(g_list)
		g_inv=g_simplified
		if '90' in g_simplified:
			g_inv=g_simplified[0]+'270'
		if '270' in g_simplified:
			g_inv=g_simplified[0]+'90'
		#g_inv=copy.deepcopy(g_list_simplified)[::-1]
		#for i,g in enumerate(g_inv):
		#	if '90' in g:
		#		g_inv[i]=g[0]+'270'
		#	if '270' in g:
		#		g_inv[i]=g[0]+'90'
		g_list.append(g_inv)
		#print 'g_inv',g_inv
		#print 'g_full',g_list
		return g_list

	def rand_clif_1q(self,n):
		cset1=['I','Y90','Y180','Y270']
		cset2=['I','X90','X180','X270','Z90','Z270']
		clif_list=list(product(cset1,cset2))
		c_list=list(numpy.array([list(clif_list[a]) for a in numpy.random.choice(range(len(clif_list)),n)]).flatten())
		return c_list

	def rand_clif_1q_target(self,n,targetgate):
		c_list=self.rand_clif_1q(n)
		for i in range(n):
			c_list.insert(3*i+2,targetgate)
		return c_list

	def acqmonout(self,avrfactor,opsel,mon_navr,mon_dt,mon_sel0,mon_sel1):
		for avr in range(avrfactor):
			print('%d of %d'%(avr,avrfactor))
			for mon_slice in range(4):
				[monout_1,monout_0]=self.hf.buf_monout(opsel=opsel,mon_navr=mon_navr,mon_dt=int(mon_dt/1.0e-9/4),mon_slice=mon_slice,mon_sel0=mon_sel0,mon_sel1=mon_sel1,panzoom_reset=1,delay=0.3)
				mon40=monout_0 if mon_slice==0 else numpy.append(mon40,monout_0,1)
				mon41=monout_1 if mon_slice==0 else numpy.append(mon41,monout_1,1)
			mon40=mon40.reshape([-1,1])
			mon41=mon41.reshape([-1,1])
			mon40sum=mon40 if avr==0 else mon40sum+mon40
			mon41sum=mon41 if avr==0 else mon41sum+mon41
		buf0avr=1.0*mon40sum/avrfactor
		buf1avr=1.0*mon41sum/avrfactor
		t=numpy.arange(len(buf1avr))*2**mon_navr*1.0e-9+mon_dt
		return [t,buf0avr,buf1avr]
	def plotmonout(self,t,buf0,buf1):
		pyplot.subplot(211)
		pyplot.plot(t,buf0,'.-')
		pyplot.xlim([t.min(),t.max()])
		pyplot.subplot(212)
		pyplot.plot(t,buf1,'.-')
		pyplot.xlim([t.min(),t.max()])
	def fitrabi(self,dx,data,plot=True,figname='fitrabi'):
		tdata = dx* numpy.arange(len(data)) # unit: seconds
		spec=abs(numpy.fft.rfft(data-data.mean()))
		fest=numpy.argmax(spec)*1.0/(dx*len(data))
		aest=(data.max()-data.min())/2.0
		oest=data.mean()
		#fmin=0.5*fest
		fmin=0.3*fest
		fmax=1.2*fest
		print('est period limit',1.0/fmin,1.0/fmax)
		print('fest',fest,'aest',aest)
		#popt, pcov = curve_fit(fullsin, tdata, data, bounds = (([0.8*aest, fmin, -numpy.pi, oest*0.8], [1.2*aest, fmax, numpy.pi, oest*1.2])))
		popt, pcov = curve_fit(fullsin, tdata, data, bounds = (([0.2*aest, fmin, -2*numpy.pi, oest*0.3], [1.2*aest, fmax, 2*numpy.pi, oest*1.2])))
		#aest=(data.max()-data.min())/2.0
		#cest=data.mean()
		#pest=0
		#popt, pcov = curve_fit(fullsin, tdata, data, p0=[aest,fest,pest,cest]);#bounds = (([0.2*aest, fmin, -2*numpy.pi, oest*0.3], [1.2*aest, fmax, 2*numpy.pi, oest*1.2])))
		fiterr= numpy.sqrt(numpy.diag(pcov))
		period= 1.0/popt[1] 
		amp= popt[0] 
		#amp= abs(popt[0])
		if plot:
			pyplot.figure(figname)
			pyplot.plot(tdata,fullsin(tdata,*popt))
			pyplot.plot(tdata,data,'*')
			pyplot.ylim(0,1)
			pyplot.savefig(figname+'.pdf')
		return [amp,period,fiterr]
	def fitrabidecay(self,dt,data,plot=True,figname='fitrabidecay'):
		tdata = dt * numpy.arange(len(data)) # unit: seconds
		spec=abs(numpy.fft.rfft(data-data.mean()))
#		pyplot.plot(spec)
		df=1.0/(dt*len(data))
		aest=(data.max()-data.min())/2.0
		oest=data.mean()
		indexmax=numpy.argmax(spec)
		festmin = (0 if indexmax<3 else indexmax)*df *0.8
		festmax = indexmax*df*1.5
		#print('fitrabidecay festmin/max',festmin,festmax)
		popt, pcov = curve_fit(exp_sin, tdata, data, bounds = (([0.8*aest, 5e3,festmin, -numpy.pi, oest*0.8], [1.2*aest, numpy.inf, festmax, numpy.pi, oest*1.2])))
		amp = popt[0]
		tdecay = 1/popt[1]
		period = 1/popt[2]
		fiterr= numpy.sqrt(numpy.diag(pcov))
		print('popt: a, l, f, p, c',popt)
		#print('tdecay',tdecay)
		if plot:
			pyplot.figure(figname)
			pyplot.plot(tdata,exp_sin(tdata,*popt))
			pyplot.plot(tdata,data,'*')
			pyplot.ylim(0,1)
			pyplot.savefig(figname+'.pdf')
		return [amp,period,tdecay,fiterr]
	def fitt1meas(self,dt,data,plot=True,figname='fitt1meas'):
		tdata = dt * numpy.arange(len(data)) # unit: seconds
		popt, pcov = curve_fit(expfit, tdata, data, bounds=((0,numpy.inf)))
		t1fit = 1./popt[1]
		fiterr= numpy.sqrt(numpy.diag(pcov))
		if plot:
			pyplot.figure(figname)
			pyplot.plot(tdata,expfit(tdata,*popt))
			pyplot.plot(tdata,data,'*')
			pyplot.ylim(0,1)
			pyplot.savefig(figname+'.pdf')
		return [t1fit,fiterr]
	def fitramsey(self,dt,data,plot=True,figname='fitramsey'):
		tdata = dt * numpy.arange(len(data)) # unit: seconds
		spec=abs(numpy.fft.rfft(data-data.mean()))
#		pyplot.plot(spec)
		df=1.0/(dt*len(data))
		indexmax=numpy.argmax(spec)
		festmin = (0 if indexmax<3 else indexmax)*df *0.8
		festmax = indexmax*df*1.5
		print('fitramsey festmin/max',festmin,festmax)
		popt, pcov = curve_fit(exp_sin, tdata, data, bounds = (([0, 5e3,festmin, -numpy.pi, 0.4], [1.1, numpy.inf, festmax, numpy.pi, 0.6])))
		#cest=data.mean()
		#aest=(data.max()-data.min())/2.0
		#popt, pcov = curve_fit(exp_sin, tdata, data, p0=[aest,0,festmin,0,cest])#bounds = (([0, 5e3,festmin, -numpy.pi, 0.4], [1.1, numpy.inf, festmax, numpy.pi, 0.6])))
		t2star = 1./popt[1]
		foffset = popt[2]
		fiterr= numpy.sqrt(numpy.diag(pcov))
		if plot:
			pyplot.figure(figname)
			pyplot.plot(tdata,exp_sin(tdata,*popt))
			pyplot.plot(tdata,data,'*')
			pyplot.ylim(0,1)
			pyplot.savefig(figname+'.pdf')
		return [t2star,foffset,fiterr]
	def fitspinecho(self,dt,data,plot=True,figname='fitspinecho'):
		tdata = dt * numpy.arange(len(data)) # unit: seconds
		popt, pcov = curve_fit(expfit, tdata, data, bounds=(([-numpy.inf, 0, 0.4], [0, numpy.inf, 0.6])))
		t2e= 1./popt[1]
		fiterr= numpy.sqrt(numpy.diag(pcov))
		if plot:
			pyplot.figure(figname)
			pyplot.plot(tdata,expfit(tdata,*popt))
			pyplot.plot(tdata,data,'*')
			pyplot.ylim(0,1)
			pyplot.savefig(figname+'.pdf')
		return [t2e,fiterr]
	def fitgateerrorscan(self,dt,data,plot=True):
		tdata = dt * numpy.arange(len(data)) # unit: seconds
		popt, pcov = curve_fit(line, tdata, data)
		if plot:
			pyplot.plot(tdata,line(tdata,*popt))
			pyplot.plot(tdata,data,'*')
		return popt[0]
	def fitdragalphascan(self,tdata,data,gatetype,plot=True):
		popt, pcov = curve_fit(line, tdata, data)
		if plot:
			pyplot.plot(tdata,line(tdata,*popt),label=gatetype)
			pyplot.plot(tdata,data,'*',label=gatetype)
			pyplot.legend()
		return [popt[0],popt[1]]
	def fitrb(self,xdata,ydata,yerr,figname='',plot=True):
		popt, pcov = curve_fit(rb_func,xdata,ydata)
		fiterr = numpy.sqrt(numpy.diag(pcov))
		if plot:
			pyplot.figure('fitrb')
			pyplot.errorbar(xdata,ydata,yerr=yerr,marker='o')
			pyplot.plot(xdata,rb_func(xdata,*popt))
			pyplot.xlabel('Number of Clifford Pairs')
			pyplot.ylabel('Averaged Recovery Population')
			#pyplot.ylim(0.1,1)
			pyplot.ylim(0,1)
			pyplot.legend()
			pyplot.savefig('rbfit'+figname+'.png',dpi=600)
		return [popt[0],popt[1]]
	def fitrb_violin(self,recovery_population_array,recovery_population_mean,recovery_population_std,el_clif_list_pair,figname=''):
		fontsize=13
		xdata=el_clif_list_pair
		ydata=recovery_population_mean
		popt,pcov=curve_fit(rb_func,xdata,ydata)
		fiterr=numpy.sqrt(numpy.diag(pcov))
		fig,ax=pyplot.subplots()
		ax.violinplot(recovery_population_array,positions=range(len(xdata)),showmeans=True,showmedians=False)
		ax.set_xticks(range(len(xdata)))
		ax.set_xticklabels([str(item) for item in xdata])
		ax.plot(rb_func(xdata,*popt),label='Fidelity:%.4f'%popt[0])
		ax.set_ylim(-0.05,1.05)
		#ax.set_ylim(0,1)
		ax.legend()
		ax.set_xlabel('Number of Clifford Pairs',fontsize=fontsize)
		ax.set_ylabel('Recovery Population',fontsize=fontsize)
		#pyplot.savefig('rb_violin'+figname+'.png',dpi=600)
		pyplot.savefig('rb_violin'+figname+'.pdf')
		return [popt[0],popt[1]]
	def repack1D_cent2edge(self,x):
		# repack 1D from center to edges of bin, add 1 bin
		dxh=(x[1]-x[0])/2.
		n=x.shape[0]
		y=numpy.empty(n+1)
		y[:n]=x-dxh
		y[n]=x[-1]+dxh
		return y
	def sigma_AoverB(self,A,sigma_A,B,sigma_B):
		return numpy.abs(A/B)*numpy.sqrt((sigma_A/A)**2+(sigma_B/B)**2)
	def gate_fidelity(self,p_irb,p_irb_std,p_srb,p_srb_std):
		fidelity=1-3/4*(1-p_irb/p_srb)
		sigma=3/4*self.sigma_AoverB(p_irb,p_irb_std,p_srb,p_srb_std)
		return [fidelity,sigma]
	def rdrvcalc(self,readoutdrvamp,dcoffset=0):
		fullscale=1-dcoffset
		readoutdrvampin = readoutdrvamp if readoutdrvamp else 0
		readoutdrvamp0=readoutdrvampin
		readoutdrvamp1=readoutdrvampin if readoutdrvampin<0.33 else (fullscale-readoutdrvampin)/2.0
		readoutdrvamp2=readoutdrvampin if readoutdrvampin<0.33 else (fullscale-readoutdrvampin)/2.0
		return [readoutdrvamp0,readoutdrvamp1,readoutdrvamp2]
	def loadjsondata(self,filename):
		f=open(filename)
		g=json.load(f)
		f.close()
		return self.iq2comp(g)
	def iq2comp(self,g):
		c={}
		for k in g.keys():
			#c[k]=numpy.array([re+1j*im for (re,im) in g[k]])
			gka=numpy.array(g[k])
			c[k]=gka[:,0]+1j*gka[:,1]
			#print c[k].shape
		return c
	def savejsondata(self,filename,extype,cmdlinestr,data,timeinfo=True):
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		if timeinfo:
			datafilename='%s.dat'%('_'.join([filename,extype,cmdlinestr,timestamp]))
		else:
			datafilename='%s.dat'%('_'.join([filename,extype,cmdlinestr]))
		datafilename=datafilename.replace('/','_')
		datafilename='%s__%s'%(datafilename[0:100],datafilename[-100:]) if len(datafilename)>=250 else datafilename
		f=open(datafilename,'w')
		json.dump(data,f)
		f.close()
		return datafilename
	def readall(self,trdrv,qubitsamps={"Q5":({},{}),"Q4":({},{}),"Q3":({},{})}):
		for (qubit,(rdrv,read)) in qubitsamps.items():
			self.seqs.add(trdrv,self.qchip.gates[qubit+'read'].modify([rdrv,read]))

class MyArgumentParser(argparse.ArgumentParser):
	def cmdlinestring(self,exception=[]):
		cmdlinedict=self.convert_arg_line_to_args(sys.argv)
		print('cmdlinedict',cmdlinedict)
		cmdlinestr=[]
		for (k,v) in cmdlinedict.items():
			if k not in exception:
				if isinstance(v,list):
					v=''.join([str(i) for i in v])
				cmdlinestr.append('%s%s'%(k,str(v)))

		return '_'.join(cmdlinestr)
	#'_'.join(['%s%s'%(k,str(v)) for (k,v) in cmdlinedict.items() if k not in exception])

	def convert_arg_line_to_args(self, arg_line):
		print('argline',arg_line)
		namespace = argparse.Namespace()
		namespace, args = self._parse_known_args(arg_line, namespace)
		return vars(namespace)
	#return ''.join(arg_line[1:]).replace('-','_')
def cmdoptions():
	monseloptions='''0(xmeasin_d2),1(ymeasin_d2),2(xlo_w[0]),3(ylo_w[0]),4(xlo_w[1]),5(ylo_w[1]),6(xlo_w[2]),7(ylo_w[2]),8(dac0_in),9(dac1_in),10(dac2_in),11(dac3_in),12(dac4_in),13(dac5_in),14(dac6_in),15(dac7_in)'''
	#parser = argparse.ArgumentParser()
	parser = MyArgumentParser()
	parser.add_argument('-0','--mon_sel0',help='channel selection for mon_sel0; \n'+monseloptions,dest='mon_sel0',type=int,default=0)
	parser.add_argument('-1','--mon_sel1',help='channel selection for mon_sel1'+monseloptions,dest='mon_sel1',type=int,default=2)
	parser.add_argument('-a','--avrfactor',help='average factor',dest='avrfactor',type=int,default=1)
	parser.add_argument('--alphastart',help='start point of drag alpha coefficient tuning',dest='alphastart',type=float,default=0.0)
	parser.add_argument('--alphastep',help='step of drag alpha coefficient tuning',dest='alphastep',type=float,default=0.1)
	parser.add_argument('--alphastop',help='stop point of drag alpha coefficient tuning',dest='alphastop',type=float,default=3.0)
	parser.add_argument('-b','--bypasswritecommand',help='bypass write command',dest='bypass',default=False,action='store_true')
	parser.add_argument('-c','--calibration',help='calibration file',dest='califile',type=str,default='')
	parser.add_argument('-d','--delaybetweenelement',help='delaybetweneelement',dest='delaybetweenelement',type=float,default=600e-6)
	parser.add_argument('--dataset',help='rabi training dataset',dest='dataset',type=str,default='')
	parser.add_argument('-dr','--delayread',help='delayread',dest='delayread',type=float,default=680e-9) #640e-9 for X6Y8 #632e-9 for MIT-LL #680e-9 for chip 57 #944e-9 for chip 34 #712e-9 for chip 42
	parser.add_argument('--draggate',help='gate type of drag alpha coefficient tuning',dest='draggate',type=str,choices=['X90Y180','Y90X180'])
	parser.add_argument('-el','--elementlength',help='number of element point in test',dest='elementlength',type=int,default=80)
	parser.add_argument('-es','--elementstep',help='time per rabi test point',dest='elementstep',type=float,default=4e-9)
	parser.add_argument('-f','--filename',help='filename',dest='filename',type=str,default='')
	parser.add_argument('-fq','--fqubit',help='qubit drive carrier frequency',dest='fqubit',type=float,default=None)
	parser.add_argument('-fr','--fread',help='readout drive carrier frequency',dest='fread',type=float,default=None)
	parser.add_argument('--framsey',help='artificial ramsey frequency',dest='framsey',type=float,default=None)
	parser.add_argument('-f0','--fstart',help='start frequency',dest='fstart',type=float,default='-500e6')
	parser.add_argument('-f1','--fstop',help='stop frequency',dest='fstop',type=float,default='500e6')
	parser.add_argument('-g','--gate',help='gate type I, X90, Y90, X180, Y180, X270 or Y270',dest='gate', choices=['I','X90', 'Y90', 'X180','Y180','X270','Y270'],type=str)
	parser.add_argument('--meas',help='meas axis, X90 for y, Y-90 for x, I for z',dest='meas', choices=['I','X90', 'Y-90'],type=str)
	parser.add_argument('--ip',help='ip address',dest='ip',type=str,default=os.environ['QUBICIP'] if 'QUBICIP' in os.environ else '192.168.1.125')
	parser.add_argument('--port',help='port',dest='port',type=int,default=os.environ['QUBICPORT'] if 'QUBICPORT' in os.environ else 3000)
	parser.add_argument('--dt',help='dt',dest='dt',type=float,default=1.0e-9)
	parser.add_argument('-l','--limit',help='plot limit',dest='limit',type=float,default=1)
	parser.add_argument('-L','--LOshift',help='LOshift in s',dest='loshift',type=float,default=0)
	parser.add_argument('-n','--nget',help='number of buffer to get',dest='nget',type=int,default=1)
	parser.add_argument('-nm','--nmeas',help='number of measurements to be implemented (the experiment could be (interleaved) randomized benchmarking)',dest='nmeas',type=int,default=32)
	parser.add_argument('-nsd','--nsdata',help='not swap data process',dest='swapdata',action='store_false')
	parser.add_argument('-nsp','--nspre',help='not swap pre process',dest='swappre',action='store_false')
	parser.add_argument('-o','--opsel',help=' 0 for navg; 1 for min; 2 for max',dest='opsel',choices=[0,1,2],type=int,default=2)
	parser.add_argument('--plot',help='plot',dest='plot',default=False,action='store_true')
	parser.add_argument('--qubitcfg',help='qubit config filename.json',dest='qubitcfg',default=os.environ['QUBICQUBITCFG'] if 'QUBICQUBITCFG' in os.environ else 'qubitcfg_mitll.json',type=str)
	parser.add_argument('--readcorr',help='readout correction',dest='readcorr',default=False,action='store_true')
	parser.add_argument('--removefile',help='remove data file',dest='removefile',default=False,action='store_true')
	parser.add_argument('--corrmx',help='readout correction matrix',dest='corrmx',type=str,default='corrmatrix.npz')
	parser.add_argument('--fixed_ramp',help='envelope with fixed ramp length',dest='fixed_ramp',default=False,action='store_true')
	parser.add_argument('--xtalk_compensate',help='crosstalk compensation',dest='xtalk_compensate',default=False,action='store_true')
	parser.add_argument('--amp_multiplier',help='amplitude multiplier for crosstalk compensation',dest='amp_multiplier',type=float,default=0)
	parser.add_argument('--phase_offset',help='phase offset (rad) for crosstalk compensation',dest='phase_offset',type=float,default=0)
	parser.add_argument('--gateamp_corr',help='gate amplitude correction for period',dest='gateamp_corr',type=float,default=1)
	parser.add_argument('-p','--processfile',help='processfile',dest='processfile',type=str,default='')
	parser.add_argument('-pread','--preadout',help='preadout',dest='preadout',type=float,default=0.0)
	parser.add_argument('-q','--qubitdrvamp',help='qubit drv amp',dest='qubitdrvamp',type=float,default=None)
	parser.add_argument('-qw','--qwidth',help='qubit drv twidth',dest='qubitdrvtwidth',type=float,default=None)
	parser.add_argument('--qubitid',help='qubitid 0-7',dest='qubitid',type=str,default='Q6')
	parser.add_argument('--qubitid_c',help='control qubitid  0-7',dest='qubitid_c',type=str,default='Q6')
	parser.add_argument('--qubitid_t',help='target qubitid  0-7',dest='qubitid_t',type=str,default='Q5')
	parser.add_argument('--qubitid_x',help='crosstalk compensation qubitid  0-7',dest='qubitid_x',type=str,default='Q4')
	parser.add_argument('--qubitid_list',help='qubitid list',dest='qubitid_list',nargs='+',default=['Q6','Q5'])
	parser.add_argument('--qubitidread',help='qubitid readout',dest='qubitidread',nargs='+',default=['Q1','Q2','Q3'])
	parser.add_argument('--tomography_list',help='tomography list',dest='tomography_list',nargs='+',default=['I','X90','X180','Y90'])
	parser.add_argument('-r','--readoutdrvamp',help='readout drv amp',dest='readoutdrvamp',type=float,default=None)
	parser.add_argument('--readwidth',help='readout time width',dest='readwidth',type=float,default=None)
	parser.add_argument('-s','--shift',help='timeshift',dest='timeshift',type=int,default=0)
	parser.add_argument('-si','--sim',help='run cmd simulation',dest='sim',action='store_true')
	parser.add_argument('-z','--zoom',help='timezoom',dest='timezoom',type=int,default=0)
	parser.add_argument('--regmappath',help='regmap path',dest='regmappath',type=str,default=None)
	parser.add_argument('--wavegrppath',help='wavegrp path',dest='wavegrppath',type=str,default=None)
	parser.add_argument('--wiremapmodule',help='wiremap python module',dest='wiremapmodule',type=str,default=os.environ['QUBICWIREMAP'] if 'QUBICWIREMAP' in os.environ else 'wiremap')
	parser.add_argument('--initcfg',help='initial value config',dest='initcfg',type=str,default='sqinit')
	parser.add_argument('--gmmdatasetspath',help='gmm data set path',dest='gmmdatasetspath',type=str,default='gmmdatasets.json')
	parser.add_argument('--outpath',default='out/',help='path for all output files')
	parser.add_argument('--outname',default=None,help='core name of output files')
	#parser.add_argument('--simpseqs',help='use RUNC compiler simple sequence',dest='simpseqs',action='store_true',default=False)
	cmdlinestr=parser.cmdlinestring(exception=['filename','limit','swapdata','swappre','plot','processfile','sim'])
	return parser,cmdlinestr
if __name__=="__main__":
	parser = MyArgumentParser()
	parser.add_argument('-e','--experiment',help='experiment name',dest='experiment',type=str,action="store",required=True)
	clargs=parser.parse_args()
	filename=clargs.experiment+'.py'
	from template import template
	print(template%({"experiment":clargs.experiment}))

