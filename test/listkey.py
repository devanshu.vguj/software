import sys
import re
filename=sys.argv[1]
f=open(filename)
s=f.read()
f.close()

lines=[]
ippat=r"([\s\S]*)c\[c.keys\(\)\[0\]\]([\s\S]*)"
for l in s.split('\n'):
	m=re.match(ippat,l)
	if m:
		g=m.groups()
		before=g[0]
		after=g[1]
		line="%sc[list(c.keys())[0]]%s"%(g[0],g[1])
		lines.append(line)
		print(l,line)
	else:
		lines.append(l)
new='\n'.join(lines)

f=open(filename,'w')
f.write(new)
f.close()

