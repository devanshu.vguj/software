import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip
import qtrl
from scipy import linalg
from qst import c_qst
from itertools import product

class c_qpt(c_qst):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		pass
	def processqpt(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid_list=['Q5','Q4'],qubitidread=['Q5','Q4','Q3'],tomography_list=['I','X90','X180','Y90'],nget=50):
		res={}
		input_states=list(product(tomography_list, repeat=len(qubitid_list)))
		ideal_output={}
		for input_state in input_states:
			print('input_state',input_state)
			self.initseqs()
			self.qstseqs(delayread=delayread,delay1=delay1,delaybetweenelement=delaybetweenelement,qubitid_list=qubitid_list,qubitidread=qubitidread,tomography_list=tomography_list)
			self.run()
			data=self.rabiacq(nget)
			fprocess=self.savejsondata(filename=clargs.filename,extype='qpt',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			meas_binned=self.processqst(filename=fprocess,loaddataset_list=None)
			dm=self.processdm(measurement_avgs=meas_binned,plot=False)
			res[input_state]=dm

			#ideal_u_matrix=None
			#if ideal_u_matrix is not None:
			#	ideal_output[input_state] = qtrl.unitary_transform(ideal_u_matrix, qtrl.pulses_to_matrix(input_state))
			#	qtrl.plot_matrix(ideal_output[input_state], rounding=2, scale=4, title='Ideal')
			#	state_fidelity = numpy.around(numpy.real(numpy.trace(numpy.matrix(linalg.sqrtm(linalg.sqrtm(dm)))*ideal_output[input_state]*numpy.matrix(linalg.sqrtm(linalg.sqrtm(dm))))**2),4)
			#	print(f'State Fidelity: {state_fidelity}')
			#qtrl.plot_matrix(dm, rounding=2, scale=4)
			#pyplot.grid(False)
			#pyplot.tight_layout()
			#pyplot.show()

		states_out=numpy.array([res[input_state] for input_state in input_states])
		states=numpy.array([qtrl.pulses_to_matrix(input_state) for input_state in input_states])
		basis_str=list(map(''.join, product(*[["I", 'X', 'Y', 'Z']] * len(qubitid_list))))
		pm=qtrl.get_process_matrix(states, states_out)
		#pm_ideal=qtrl.get_process_matrix(states, numpy.array(list(ideal_output.values())))
		pyplot.rc('font', **{'size': 16, 'family': 'Arial'})
		#qtrl.plot_matrix(pm,x_labels=basis_str,y_labels=basis_str,scale=6*len(qubitid_list),rounding=2)
		pyplot.ylabel('Output Pauli')
		pyplot.xlabel('Input Pauli')
		#print 'Pauli Transfer Matrix, Fidelity',numpy.around(qtrl.process_fidelity(pm_ideal, pm),4)
		pyplot.tight_layout()
		pyplot.show()
		return pm,states_out,states

if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	qpt=c_qpt()
	parser,cmdlinestr=qpt.cmdoptions()
	clargs=parser.parse_args()
	pm,states_out,states=qpt.processqpt(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_list=clargs.qubitid_list,qubitidread=clargs.qubitidread,tomography_list=clargs.tomography_list,nget=clargs.nget) # Since accout_0, accout_1, accout_2 match qubitidread, elements in the qubitid_list and qubitidread should be in the same order.
