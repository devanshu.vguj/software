#import matplotlib
#matplotlib.use('Agg') ### Using matplotlib / pylab without a DISPLAY ### Comment this line if needs plot showing
import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import copy
from experiment import c_experiment
from rabi_w import c_rabi
import qutip
import math

class c_crZXamp(c_rabi):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit'):
		c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg)
		self.qubitid_c=None
		self.qubitid_t=None
		pass
	def crZXampseqs(self,delayread=712e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid_c='Q6',qubitid_t='Q5',ctrl='I',meas='I',pcr=0,txgate=0e-9,acr_list=None,tcr=144e-9,qubitidread=['Q7','Q6','Q5'],fixed_ramp=False):
		self.qubitid_c=qubitid_c
		self.qubitid_t=qubitid_t
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		modcr={}
		modxgate={}
		run=0
		for acr in acr_list:
			# Readout
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delay1
			tini=run

			# Control state preparation (control qubit: I or X180)
			if ctrl=='X180':
				self.seqs.add(run,         	 self.qchip.gates[qubitid_c+'X180'])
				run=self.seqs.tend()

			# CR gate (drive the control qubit at the frequency of the target frequency)
			if fixed_ramp:
				modcr.update(dict(amp=acr,twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0]))
			else:
				modcr.update(dict(amp=acr,twidth=tcr,pcarrier=self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].pcalc(dt=run-tini,padd=pcr)[0],env=[{'env_func': 'cos_edge_square', 'paradict': {'ramp_fraction': 0.25,'ramp_length':None}}]))
			self.seqs.add(run,self.qchip.gates['CR('+qubitid_c+qubitid_t+')'].modify(modcr))
			run=self.seqs.tend()

			# CNOT X gate (target qubit)
			if txgate>1e-14:
				modxgate.update(dict(twidth=txgate,pcarrier=self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].pcalc(dt=run-tini)[0]))
				self.seqs.add(run,self.qchip.gates['CNOT('+qubitid_c+qubitid_t+').'+qubitid_t+'X'].modify(modxgate))
				run=self.seqs.tend()

			# Projection (target qubit: Y-90, X90 or I)
			if meas=='I':
				pmeas=0
			else:
				pmeas=self.qchip.gates[qubitid_t+meas].pcalc(dt=run-tini)[0]
				self.seqs.add(run,self.qchip.gates[qubitid_t+meas].modify(dict(pcarrier=pmeas)))

			# Readout
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid_t+'read'])

			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processcrZXamp(self,dt,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		crZXamp_result=self.process3(data,qubitid=self.qubitid_t,lengthperrow=self.bufwidth_dict[self.qubitid_t],training=False,loaddataset=loaddataset)
		return [data,crZXamp_result['separation'],crZXamp_result['iqafterherald'],crZXamp_result['population_norm']]


if __name__=="__main__":
	timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	crZXamp=c_crZXamp()
	parser,cmdlinestr=crZXamp.cmdoptions()
	clargs=parser.parse_args()
	acr_list=0.45+numpy.linspace(-0.1,0.1,81)
	#parser.add_argument('--tcr',help='pulse length for CR gate',dest='tcr',type=float,default=192e-9)
	#parser.add_argument('--pcr',help='starting phase for CR gate',dest='pcr',type=float,default=3.925)
	#parser.add_argument('--txgate',help='X rotation for CNOT gate',dest='txgate',type=float,default=0e-9)
	tcr=192e-9
	pcr=3.925
	txgate=0e-9
	result_filename=clargs.filename+'_crZXamp'+'_tcr'+str(tcr)+'_pcr'+str(pcr)+'_txgate'+str(txgate)+'_'+timestamp
	if clargs.processfile=='':
		for ctrl in ['I','X180']:
			for meas in ['Y-90','X90','I']:
				crZXamp=c_crZXamp()
				parser,cmdlinestr=crZXamp.cmdoptions()
				clargs=parser.parse_args()
				#crZXamp.initseqs()
				if clargs.sim:
					crZXamp.setsim()
				crZXamp.crZXampseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,qubitid_c=clargs.qubitid_c,qubitid_t=clargs.qubitid_t,ctrl=ctrl,meas=meas,pcr=pcr,txgate=txgate,acr_list=acr_list,tcr=tcr,qubitidread=clargs.qubitidread,fixed_ramp=clargs.fixed_ramp)
				crZXamp.run()
				data=crZXamp.rabiacq(clargs.nget)
				fprocess=crZXamp.savejsondata(filename=clargs.filename,extype='crZXamp',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					crZXamp.sim()
				[rawdata,separation,iqafterherald,population_norm]=crZXamp.processcrZXamp(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
				if clargs.readcorr:
					population_norm=crZXamp.readoutcorrection(qubitid_list=[clargs.qubitid_t],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
					print('corrected population_norm',population_norm)
				if clargs.plot:
					pyplot.grid()
					pyplot.show()
				pnt_traj=1-2*population_norm
				with open(result_filename+'.dat','a') as f:
					numpy.savetxt(f,[pnt_traj])
		with open(result_filename+'.dat','a') as f:
			numpy.savetxt(f,[numpy.array(acr_list)])

		crdata=numpy.loadtxt(result_filename+'.dat')
	else:
		crdata=numpy.loadtxt(clargs.processfile)
	for index,target_projection in enumerate(['Target <X>','Target <Y>','Target <Z>']):
		pyplot.figure()
		pyplot.plot(crdata[6,:],crdata[index,:],label='Control |0>',color='b')
		pyplot.plot(crdata[6,:],crdata[index+3,:],label='Control |1>',color='r')
		pyplot.legend()
		pyplot.xlabel('CR ZX Amplitude')
		pyplot.ylabel(target_projection)
		#pyplot.ylim(-1,1)
		pyplot.ylim(-1.1,1.1)
		pyplot.savefig(result_filename+'_'+str(index)+'.png')
	distance=numpy.sqrt((crdata[0,:]-crdata[3,:])**2+(crdata[1,:]-crdata[4,:])**2+(crdata[2,:]-crdata[5,:])**2)/2.0
	pyplot.figure()
	pyplot.plot(crdata[6,:],distance)
	pyplot.xlabel('CR ZX Amplitude')
	pyplot.ylabel(r'$|\vec{R}|$')
	pyplot.ylim(-0.05,1.05)
	pyplot.savefig(result_filename+'_R'+'.png')
	print('distance',distance)
	bloch=qutip.Bloch()
	bloch.add_points(crdata[0:3,:])
	bloch.add_points(crdata[3:6,:])
	bloch.show()
	pyplot.show()
	bloch.save(result_filename+'_T'+'.png')

# python crZXamp.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr
# python crZXamp.py -n 50 --qubitid_c Q6 --qubitid_t Q5 --readcorr --fixed_ramp
