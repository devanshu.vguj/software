import datetime
import argparse
import sys
from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
from qubic_t1 import cmdadd, cmdgen
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
class c_rabi(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		self.qubitid_x=None
		pass
	def rabiseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=4e-9,readoutdrvamp=None,qubitdrvamp=None,readwidth=None,fqubit=None,fread=None,rdc=0,preadout=None,qubitid='Q5',qubitidread=['Q7','Q6','Q5'],xtalk_compensate=False,amp_multiplier=0,phase_offset=0,qubitid_x='Q7',gateamp_corr=1):
		self.qubitid=qubitid
		self.qubitid_x=qubitid_x
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=rdc)
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		modqNrdrv={}
		modqNread={}
		modqNrabi={}
		modqNrabi_simul={}
		if readoutdrvamp is not None:
			modqNrdrv.update(dict(amp=readoutdrvamp0))
		if qubitdrvamp is not None:
			modqNrabi.update(dict(amp=qubitdrvamp))
		if fqubit is not None:
			modqNrabi.update(dict(fcarrier=fqubit))
		if readwidth is not None:
			modqNrdrv.update(dict(twidth=readwidth))
			modqNread.update(dict(twidth=readwidth))
		if fread is not None:
			modqNrdrv.update(dict(fcarrier=fread))
			modqNread.update(dict(fcarrier=fread))
		if preadout is not None:
			modqNread.update(dict(pcarrier=preadout))

		run=0
		for irun in range(elementlength):

			therald=run
			theraldread=run+delayread
			self.seqs.add(therald,self.qchip.gates[qubitid+'readoutdrv'].modify(modqNrdrv))
			self.seqs.add(therald+delayread,self.qchip.gates[qubitid+'readout'].modify(modqNread))
			trabi=run+delay1
			wrabi=elementstep*irun
			if wrabi!=0:
				amp_tmp=self.qchip.gates[qubitid+'rabi'].paralist[0]['amp']/gateamp_corr
				modqNrabi.update(dict(twidth=wrabi,amp=amp_tmp))
				self.seqs.add(trabi,         	 self.qchip.gates[qubitid+'rabi'].modify(modqNrabi))
				if xtalk_compensate:
					pcarrier_simul_tmp=self.qchip.gates[qubitid+'rabi'].pcalc(padd=phase_offset)[0]
					amp_simul_tmp=self.qchip.gates[qubitid+'rabi'].paralist[0]['amp']/gateamp_corr*amp_multiplier
					modqNrabi_simul.update(dict(twidth=wrabi,dest=qubitid_x+'.qdrv',amp=amp_simul_tmp,pcarrier=pcarrier_simul_tmp))
					self.seqs.add(trabi,         self.qchip.gates[qubitid+'rabi'].modify(modqNrabi_simul))

			treaddrv=self.seqs.tend()
			treadout=treaddrv+delayread
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'readoutdrv'].modify(modqNrdrv))
			self.seqs.add(treaddrv+delayread,self.qchip.gates[qubitid+'readout'].modify(modqNread))
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
		print('fread,fqubit,readoutdrvamp,qubitdrvamp',fread,fqubit,readoutdrvamp,qubitdrvamp)
	def rabiacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processrabi(self,dt,filename,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		c=self.loadjsondata(filename)
		return self.processrabidata(dt=dt,data=c[list(c.keys())[0]],dumpdataset=dumpdataset,loaddataset=loaddataset,plot=plot,isfitdecay=isfitdecay)
	def processrabidata(self,dt,data,dumpdataset,loaddataset,plot=False,isfitdecay=True):
		self.rabi_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=loaddataset=='',dumpdataset=dumpdataset,loaddataset=loaddataset)
		if isfitdecay:
			[amp,period,tdecay,fiterr]=self.fitrabidecay(dt=dt,data=self.rabi_result['population_norm'],plot=plot)
		else:
			[amp,period,fiterr]=self.fitrabi(dx=dt,data=self.rabi_result['population_norm'],plot=plot)
		return [data,self.rabi_result['separation'],self.rabi_result['iqafterherald'],self.rabi_result['population_norm'],amp,period,fiterr]


if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	print(cmdlinestr)
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	rabi=c_rabi(**clargs.__dict__)
	if clargs.sim:
		rabi.setsim()
	rabi.rabiseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,readwidth=clargs.readwidth,readoutdrvamp=clargs.readoutdrvamp,qubitdrvamp=clargs.qubitdrvamp,elementlength=clargs.elementlength,elementstep=clargs.elementstep,fqubit=clargs.fqubit,preadout=clargs.preadout,fread=clargs.fread,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread,xtalk_compensate=clargs.xtalk_compensate,amp_multiplier=clargs.amp_multiplier,phase_offset=clargs.phase_offset,qubitid_x=clargs.qubitid_x,gateamp_corr=clargs.gateamp_corr)
	if clargs.processfile=='':
		rabi.run(bypass=clargs.bypass)
		data=rabi.rabiacq(clargs.nget)
		fprocess=rabi.savejsondata(filename=clargs.filename,extype='rabi',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			rabi.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm,amp,period,fiterr]=rabi.processrabi(dt=clargs.elementstep,filename=fprocess,dumpdataset=fprocess[:-4],loaddataset=clargs.dataset,plot=clargs.plot)
	print('period',period)
	print('separation',separation)
	print('amp',amp)
	print(rabi.hf.adcminmax())
	if clargs.xtalk_compensate:
		print('current gateamp_corr ratio',128e-9/period)
	if clargs.readcorr:
		population_norm=rabi.readoutcorrection(qubitid_list=[clargs.qubitid],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
		print('corrected population_norm',population_norm)
	rabi.plotrawdata(d1=rawdata,figname='blobs'+fprocess)
	#rabi.plotafterheraldingtest(iqafterherald)
	rabi.plotpopulation_norm(population_norm=population_norm,figname='population'+fprocess)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()

# python compensate_rabi_w.py --plot -n 50 --qubitid Q5 --readcorr -el 40 -es 8e-9 --xtalk_compensate --qubitid_x Q7 --amp_multiplier 0.5 --phase_offset 3.5416 --gateamp_corr 1.4857
# python compensate_rabi_w.py --plot -n 50 --qubitid Q6 --readcorr -el 40 -es 4e-9 --xtalk_compensate --qubitid_x Q7 --amp_multiplier 1.625 --phase_offset 5.8916 --gateamp_corr 3.2019
