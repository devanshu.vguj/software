import datetime
from matplotlib import pyplot,patches
import numpy
import experiment
import json
import re
import trueq
import qubic_trueq
from simp2q import c_simp2q
import random
from itertools import permutations
import os
import functools
import importlib


easy_1q_gates=[trueq.Gate.cliff0, trueq.Gate.cliff1, trueq.Gate.cliff2, trueq.Gate.cliff3, 
		trueq.Gate.cliff4, trueq.Gate.cliff5, trueq.Gate.cliff6, trueq.Gate.cliff7, 
		trueq.Gate.cliff8, trueq.Gate.cliff9, trueq.Gate.cliff10, trueq.Gate.cliff11, 
		trueq.Gate.cliff12, trueq.Gate.cliff13, trueq.Gate.cliff14, trueq.Gate.cliff15, 
		trueq.Gate.cliff16, trueq.Gate.cliff17, trueq.Gate.cliff18, trueq.Gate.cliff19, 
		trueq.Gate.cliff20, trueq.Gate.cliff21, trueq.Gate.cliff22, trueq.Gate.cliff23]
hard_1q_gates=[trueq.Gate.from_generators("X", 45), trueq.Gate.from_generators("Y", 45), trueq.Gate.t]
hard_2q_gates=[trueq.Gate.cx, trueq.Gate.cy, trueq.Gate.cz]

def generate_random_circuit(qubits=[6,5],num_K_cycles=10,include_2q_gates=True,allow_parallel_hard_gates=True,control_target_list=None,random_SU2_gates=False):
	bare_circuit=None
	bare_circuit_collection=(trueq.CircuitCollection(circuits=None if bare_circuit is None else [bare_circuit]))
	circuit_cycles=[]
	for _ in range(num_K_cycles):
		if len(qubits)==1:
			if random_SU2_gates is True:
				easy_cycle={(qubits[0],): trueq.Gate.random(2)}
				hard_cycle={(qubits[0],): trueq.Gate.random(2)}
			else:
				easy_cycle={(qubits[0],): random.choice(easy_1q_gates)}
				hard_cycle={(qubits[0],): random.choice(hard_1q_gates)}
		elif len(qubits)>1:
			if include_2q_gates is True:
				if random_SU2_gates is True:
					easy_cycle={(q,): trueq.Gate.random(2) for q in qubits}
				else:
					easy_cycle={(q,): random.choice(easy_1q_gates + hard_1q_gates) for q in qubits}
				hard_cycle={}
				if control_target_list is not None:
					control_target=random.choice(control_target_list)
				else:
					control_target=random.choice(list(permutations(qubits,2)))
				hard_cycle[control_target]=random.choice(hard_2q_gates)
			elif include_2q_gates is False:
				if random_SU2_gates is True:
					easy_cycle={(q,): trueq.Gate.random(2) for q in qubits}
					hard_cycle={(q,): trueq.Gate.random(2) for q in qubits}
				else:
					easy_cycle={(q,): random.choice(easy_1q_gates) for q in qubits}
					hard_cycle={(q,): random.choice(hard_1q_gates) for q in qubits}
		circuit_cycles.extend([trueq.Cycle(easy_cycle),trueq.Cycle(hard_cycle, immutable=True)])
	if random_SU2_gates is True:
		circuit_cycles.append(trueq.Cycle({(q,): trueq.Gate.random(2) for q in qubits}))
	else:
		circuit_cycles.append(trueq.Cycle({(q,): random.choice(easy_1q_gates) for q in qubits}))
	bare_circuit=trueq.Circuit(circuit_cycles)
	bare_circuit.measure_all()
	bare_circuit_collection.append(bare_circuit)
	return bare_circuit,bare_circuit_collection

def plot_violin(violin,label):
	color=violin["bodies"][0].get_facecolor().flatten()
	labels.append((patches.Patch(color=color),label))


if __name__=="__main__":
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'
	num_bare_circuits=50#100#50#30
	num_rc_circuits=20#30
	num_K_cycles_list=[5,10,25,50,100]#[2,5,10,15,20,30,40]#[5,10,25,50,100]#[2,4,8,16,32]#[5,10,20,40]
	include_2q_gates=True
	allow_parallel_hard_gates=False
	random_SU2_gates=False

	parser,cmdlinestr=experiment.cmdoptions()
	clargs=parser.parse_args()

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	TVD_all=[]
	for num_K_cycles in num_K_cycles_list:
		TVD_row=[]
		for ibare in range(num_bare_circuits):
			simp2q=c_simp2q(**clargs.__dict__)

			delayafterheralding=12e-6
			delaybetweenelement=600e-6
			firsttime=True
			include_readout_calibration=False # Not use TrueQ readout correction
			control_target_list=None
			num_K_cycles=num_K_cycles
			if clargs.sim:
				simp2q.setsim()

			#bare_circuit_collection=trueq.load('circuits_20200812_231158_082575.bin')
			#bare_circuit=bare_circuit_collection[0]
			qubits=[int(re.findall('\d+',q)[0]) for q in clargs.qubitid_list]
			bare_circuit,bare_circuit_collection=generate_random_circuit(qubits=qubits,num_K_cycles=num_K_cycles,include_2q_gates=include_2q_gates,allow_parallel_hard_gates=allow_parallel_hard_gates,control_target_list=control_target_list,random_SU2_gates=random_SU2_gates)
			#print('bare_circuit',bare_circuit)

			bare_circuits_collection=trueq.CircuitCollection()
			for _ in range(num_rc_circuits):
				bare_circuits_collection.append(bare_circuit)

			rc_circuits=trueq.randomly_compile(bare_circuit,n_compilations=num_rc_circuits)
			#print('rc_circuits',rc_circuits)

			rc_circuits_collection=trueq.CircuitCollection()
			rc_circuits_collection.append(rc_circuits)

			for circuits_collection in [bare_circuits_collection,rc_circuits_collection]:
				timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				if include_readout_calibration:
					circuits_collection.append(trueq.make_rcal(circuits_collection.labels))
				circuits=circuits_collection
				circuits.save('circuits_'+timestamp+'.bin')
				bare_circuit_collection.save('barecircuit_'+timestamp+'.bin')

				transpiler=trueq.compilation.get_transpiler(config)
				transpiled_circuit=transpiler.compile(circuits)
				#print('transpiled_circuit',transpiled_circuit)
				#print('circuits.n_circuits',circuits.n_circuits)

				#cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				#print('cirgentime to start',datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat))

				lastcmdarraylength=0
				[qubitid,cmdarray]=qubic_trueq.trueq_circuits_to_qubic2(transpiled_circuit,heralding=delayafterheralding>0)
				#for l in cmdarray:
				#	print(l)
				if len(qubitid)==1:
					gates=[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
				elif len(qubitid)==2:
					gates=[simp2q.qchip.gates[clargs.qubitid_list[0]+clargs.qubitid_list[1]+'CNOT']]+[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
				else:
					print('how many qubits do you have?')
				elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
				destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
				patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
				cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
				simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=False,cmdclearlength=lastcmdarraylength*4)
				lastcmdarraylength=cmdarraylength
				#print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)

				data=simp2q.simp2qacq(clargs.nget)
				fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
				print('save data to ',fprocess)
				if clargs.sim:
					simp2q.sim()
				trueq_return,meas_binned=simp2q.processsimp2q(filename=fprocess,readcorr=True,corr_matrix=numpy.load(clargs.corrmx)) # Use QubiC readout correction
				if len(qubitid)==1:
					qubic_result=[{'0':meas_binned[0][i],'1':meas_binned[1][i]} for i in range(meas_binned.shape[1])]
				elif len(qubitid)==2:
					qubic_result=[{'00':meas_binned[0][i],'01':meas_binned[1][i],'10':meas_binned[2][i],'11':meas_binned[3][i]} for i in range(meas_binned.shape[1])]
				else:
					print('how many qubits do you have?')
				#print('trueq_return',trueq_return)
				#print('qubic_result',qubic_result)

				timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
				with open(resultfilename,'w') as f_qubicresult:
					json.dump(qubic_result,f_qubicresult)

				#beforerb1q_process=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
				#print('beforerb1q_process to start',datetime.datetime.strptime(beforerb1q_process,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat))

				bare_circuit=trueq.load('barecircuit%s.bin'%re.match('(\S+)(_\S+_\S+_\S+)(_\S+_\S+_\S+).dat',resultfilename).groups()[1])[0]
				ideal_results=trueq.Simulator().state(bare_circuit).probabilities().to_results()
				print('ideal_results',ideal_results)

				with open(resultfilename) as file_read:
					result_raw=json.loads(file_read.read())
				#print('result_raw',result_raw)
				#sum_result_plot={key: sum(item.get(key,0) for item in result_raw) for key in functools.reduce(lambda a,b:set(a)|set(b), result_raw)}
				#print('sum_result_raw',sum_result_raw)
				circuits_results={key: sum(item.get(key,0) for item in result_raw)/len(result_raw) for key in functools.reduce(lambda a,b:set(a)|set(b), result_raw)}
				print('avg_result_raw',circuits_results)
				TVD=ideal_results.tvd(trueq.Results(circuits_results))
				TVD_row.append(TVD[0])
				print('TVD',TVD)

				plot_bar=False
				if plot_bar:
					width=0.5
					index=numpy.arange(len(result_raw)+1)
					rect_bar=[0.1,0.1,0.65,0.80]
					rect_his=[0.755,0.1,0.2,0.80]
					pyplot.figure()
					ax_bar=pyplot.axes(rect_bar)
					ax_his=pyplot.axes(rect_his)
					bar00=[ideal_results['00']]+[item['00'] for item in result_raw]
					bar01=[ideal_results['01']]+[item['01'] for item in result_raw]
					bar10=[ideal_results['10']]+[item['10'] for item in result_raw]
					bar11=[ideal_results['11']]+[item['11'] for item in result_raw]
					p1 = ax_bar.bar(index, bar00, width, color='r')
					p2 = ax_bar.bar(index, bar01, width, color='b', bottom=numpy.array(bar00))
					p3 = ax_bar.bar(index, bar10, width, color='g', bottom=numpy.array(bar00)+numpy.array(bar01))
					p4 = ax_bar.bar(index, bar11, width, color='orange', bottom=numpy.array(bar00)+numpy.array(bar01)+numpy.array(bar10))
					bins=numpy.arange(0,1,0.01)
					p5=ax_his.hist([item['00'] for item in result_raw],bins=bins,orientation='horizontal',color='r')
					p6=ax_his.hist([item['01'] for item in result_raw],bins=bins,orientation='horizontal',color='b')
					p7=ax_his.hist([item['10'] for item in result_raw],bins=bins,orientation='horizontal',color='g')
					p8=ax_his.hist([item['11'] for item in result_raw],bins=bins,orientation='horizontal',color='orange')
					ax_his.legend((p1[0],p2[0],p3[0],p4[0]),('|00>','|01>','|10>','|11>'),loc='upper right')
					ax_bar.axhline(ideal_results['00'], linestyle='--', color='k')
					ax_bar.axhline(ideal_results['00']+ideal_results['01'], linestyle='--', color='k')
					ax_bar.axhline(ideal_results['00']+ideal_results['01']+ideal_results['10'], linestyle='--', color='k')
					ax_bar.set_xlabel('RC circuits')
					ax_bar.set_ylabel('Nomizalized Population')
					ax_bar.set_title('Randomized Compliling')
				#	pyplot.savefig(resultfilename+'_population_hist.pdf')
					ax_bar.set_ylim((0,1))
					ax_his.set_ylim((0,1))
		TVD_all.append(TVD_row)

	TVD_filename='TVD'+'_nrc'+str(num_rc_circuits)+'_nbare'+str(num_bare_circuits)+'_K'+str('_'.join(map(str,num_K_cycles_list)))+'_nget'+str(clargs.nget)+'_'+str(''.join(map(str,clargs.qubitid_list)))+'_'+starttime+'.dat'
	numpy.savetxt(TVD_filename,TVD_all)
	TVD_data=numpy.loadtxt(TVD_filename)
	TVD_bare=TVD_data[:,::2]
	TVD_rc=TVD_data[:,1::2]
	labels=[]
	plot_violin(pyplot.violinplot(TVD_bare.tolist(),positions=num_K_cycles_list,showmeans=True,showextrema=False,widths=num_K_cycles_list[-1]/16),"Bare")
	plot_violin(pyplot.violinplot(TVD_rc.tolist(),positions=num_K_cycles_list,showmeans=True,showextrema=False,widths=num_K_cycles_list[-1]/16),"RC")
	pyplot.legend(*zip(*labels), loc=2)
	pyplot.ylim(0,1)
	pyplot.xlabel('Number of Hard Gate Cycles')
	pyplot.ylabel('Total Variational Distance')
	pyplot.grid()
	totaltime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	print('totaltime',datetime.datetime.strptime(totaltime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat))

	pyplot.show()

# python simp2q_rc.py -n 1 --plot --qubitid_list Q6 Q5
