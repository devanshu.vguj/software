template='''OPENQASM 3.0;
qreg q[%(nqubits)d];
creg meas[%(nqubits)d];
%(measqasm)s
barrier q;
'''
meastemp='measure q[%(index)d] -> meas[%(index)d];'
def heraldqasm(qubits):
    ops={}
    ops['nqubits']=len(qubits)
    ops['measqasm']='\n'.join([meastemp%(dict(index=index)) for index in range(ops['nqubits'])])
    return template%(ops)

if __name__=="__main__":
    print(heraldqasm([1,2]))
            #    circuits=[]
#    circuit=[]
#    if isinstance(qubits,list) or isinstance(qubits,tuple):
#        for qubit in qubits:
#            circuit.append({'name': 'read', 'qubit': [qubit]})
#    elif isinstance(qubits,str):
#        circuit.append({'name': 'read', 'qubit': [qubits]})
#    circuits.append(circuit)
#    print('heralding circuits',circuits)
#    return circuits
