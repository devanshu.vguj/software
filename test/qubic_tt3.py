import sys
from matplotlib import pyplot
import numpy
from ether import c_ether
from mem_gateway import c_mem_gateway
import time
def modulation(length=1024,scale=32767,usepad=True):
	if usepad:
		length = length-4
	t=1.0*numpy.arange(length)
	m1=-abs(t-length/2.0)**2
	m1=m1/max(abs(m1))
	m1=m1-min(m1)
	pad=numpy.zeros(4)
	m2=numpy.exp(-(((t-length/2.0))/(length/5.0))**2)
	m2=m2/max(m2)
	m3=numpy.ones(length)/2.0
	m4=length/2.0-abs(t-length/2.0)
	m4=m4/max(m4)
	if usepad:
		result=(numpy.concatenate((pad,m3,pad,m3,pad,m3,pad,m3))*scale).astype(int)
	else:
		result=(numpy.concatenate((m3,m3,m3,m3))*scale).astype(int)
	return  result
def to32bits(arrayin):
	return arrayin
def cmdgen(trig_t,element,dest,start,length,phini,freq):
	ph_binary = int(phini/360.0*2**14)
	#freq_binary=int(1e-9*freq*2**24)
	freq_binary=int(1.0*freq*2**22/250e6)
	cmd={trig_t:((trig_t&0xffffff)<<72)+((element&0xff)<<64)+((dest&0x3)<<62)+((start&0xfff)<<50)+((length&0xfff)<<38)+((ph_binary&0x3fff)<<24)+(freq_binary&0xffffff)}
	print(trig_t,element,dest,start,length,ph_binary,freq_binary,hex(cmd[trig_t]))
	return cmd
def cmdadd(cmddict,newcmddict):
	commonkeys=set(cmddict.keys())&set(newcmddict.keys())
	if not commonkeys:
		cmddict.update(newcmddict)
	else:
		print
		print("ERROR: commonkeys ",commonkeys)
		print
	return cmddict
def signvalue(vin,width=16):
	return vin-2**width if vin>>(width-1) else vin

if __name__=="__main__":
	interface=c_ether('192.168.1.122',port=3000)
	mg=c_mem_gateway(interface, min3=True,memgatewaybug=False)
	tslice=4
	nwave=4
	nel=8
	comp_list=[]
	min_list=[]
	max_list=[]
	freq_start=1e6#120e6#1e6
	freq_stop=1001e6#121e6#1001*1e6
	freq_step=40e6#2e6#40*1e6
	freq_list=numpy.arange(freq_start,freq_stop,freq_step)
	alist=[]
	dlist=[]
	alist.extend(range(0x38000,0x40000))
	#dlist.extend((0x40000-0x38000)*[19897])  # amplitude to internal lo for demodulating
	dlist.extend((0x40000-0x38000)*[12000])  # amplitude to internal lo for demodulating
	alist.extend(range(0x41000,0x50000))
	#dlist.extend((0x50000-0x41000)*[19897])  # amplitude to internal lo for demodulating
	dlist.extend((0x50000-0x41000)*[12000])  # amplitude to internal lo for demodulating
        alist.extend(range(0x41000,0x41010))
        dlist.extend((0x41010-0x41000)*[0])
        alist.extend(range(0x42000,0x42008))
        dlist.extend((0x42008-0x42000)*[0])
        #alist.extend(range(0x43000,0x43040))
        #dlist.extend((0x43040-0x43000)*[0])
	alist.append(0x50000)
	dlist.append(0xff)
	period=300000/4
	alist.extend((0x7,0x8,16))  # period_adc,period_dac0,period_stream
	dlist.extend((period,period,period))
	bufsel=(0b010<<9)+(0b010<<6)+(0b010<<3)+0b010
	alist.append(0x50001)
	dlist.append(bufsel)
	digiloopback=1
	alist.append(0x50004)
	dlist.append(digiloopback)
	result=mg.readwrite(alist=alist,dlist=dlist,write=1)
	for frequency in freq_list:
		alist=[]
		dlist=[]
		cmddict={}
		cmdadd(cmddict,cmdgen(trig_t=0,element=0,dest=3,start=0,length=1,phini=30,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=100,element=0,dest=0,start=0,length=500,phini=30-24e-9*frequency*360,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=106,element=8,dest=0,start=0,length=500,phini=30,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=120,element=1,dest=1,start=0,length=500,phini=30-24e-9*frequency*360,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=126,element=9,dest=1,start=0,length=500,phini=30,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=140,element=2,dest=2,start=0,length=500,phini=30-24e-9*frequency*360,freq=frequency))
#		cmdadd(cmddict,cmdgen(trig_t=146,element=10,dest=2,start=0,length=500,phini=30,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=86,element=0,dest=0,start=0,length=500,phini=30-32e-9*frequency*360,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=90,element=8,dest=0,start=0,length=500+4,phini=30-16e-9*frequency*360,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=92,element=9,dest=1,start=0,length=500+2,phini=30-8e-9*frequency*360,freq=frequency))
		cmdadd(cmddict,cmdgen(trig_t=94,element=10,dest=2,start=0,length=500,phini=30,freq=frequency))
		cmdlist=[v for k,v in sorted(cmddict.items())]
		for i in range(len(cmdlist)):
			#alist.extend([0x40000+2*i,0x40400+2*i,0x40400+2*i+1])
			#dlist.extend([(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff])
			#print([hex(j) for j in [0x40000+2*i,0x40400+2*i,0x40400+2*i+1]])
			#print([hex(j) for j in [(cmdlist[i]>>64)&0xffffffff,cmdlist[i]&0xffffffff,(cmdlist[i]>>32)&0xffffffff]])
                        alist.extend([0x80000+4*i,0x80000+4*i+1,0x80000+4*i+2])
                        dlist.extend([(cmdlist[i]>>64)&0xffffffff,(cmdlist[i]>>32)&0xffffffff,cmdlist[i]&0xffffffff])
		result=mg.readwrite(alist=alist,dlist=dlist,write=1)

		alist=[(0x50002)]
		dlist=[(1)]
		result=mg.readwrite(alist=alist,dlist=dlist,write=1)
		time.sleep(4.5)  # 0x50005 full (need to be done later)

		addr_start=[0x42000,0x4a000,0x4b000]
		for k in range(3):
			alist=[]
			dlist=[]
			alist.extend(range(addr_start[k],addr_start[k]+0x1000))
			dlist=numpy.zeros(len(alist))
			result=mg.readwrite(alist=alist,dlist=dlist,write=0)
			numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
			xy8=numpy.array([signvalue(i&0xffffffff,32) for i in mg.parse_readvalue(result)])
			xy82=xy8.reshape([-1,2])
			xy8comp=xy82[:,0]+1j*xy82[:,1]
			print('xy8comp',xy8comp)
			print('abs(xy8comp.mean())',abs(xy8comp.mean()))
			comp_list.append(xy8comp.mean())

		#addr_start=[0x70000,0x71000,0x72000,0x73000,0x74000,0x75000]
		addr_start=[0x70000,0x71000]
		#for k in range(6):
		for k in range(2):
			alist=[]
			dlist=[]
			alist.extend(range(addr_start[k],addr_start[k]+0x1000))
			dlist=numpy.zeros(len(alist))
			result=mg.readwrite(alist=alist,dlist=dlist,write=0)
			numpy.set_printoptions(precision=None, threshold=None, edgeitems=None, linewidth=200)
			adc_min=numpy.array([signvalue((i&0xffff0000)>>16,16) for i in mg.parse_readvalue(result)])
			adc_max=numpy.array([signvalue(i&0xffff,16) for i in mg.parse_readvalue(result)])
			print('adc_min,adc_max',adc_min,adc_max)
			min_list.append(adc_min.mean())
			max_list.append(adc_max.mean())

	comp_array0=numpy.array(comp_list[::3])
	comp_array1=numpy.array(comp_list[1::3])
	comp_array2=numpy.array(comp_list[2::3])
	pyplot.figure(1)
	pyplot.subplot(211)
	pyplot.plot(freq_list,abs(comp_array0),'o')
	pyplot.subplot(212)
	pyplot.plot(freq_list,numpy.angle(comp_array0),'o')
	pyplot.figure(2)
	pyplot.plot(comp_array0.real,comp_array0.imag,'o')
	pyplot.figure(3)
	pyplot.subplot(211)
	pyplot.plot(freq_list,abs(comp_array1),'o')
	pyplot.subplot(212)
	pyplot.plot(freq_list,numpy.angle(comp_array1),'o')
	pyplot.figure(4)
	pyplot.plot(comp_array1.real,comp_array1.imag,'o')
	pyplot.figure(5)
	pyplot.subplot(211)
	pyplot.plot(freq_list,abs(comp_array2),'o')
	pyplot.subplot(212)
	pyplot.plot(freq_list,numpy.angle(comp_array2),'o')
	pyplot.figure(6)
	pyplot.plot(comp_array2.real,comp_array2.imag,'o')

#	min_array0=numpy.array(min_list[::6])
#	max_array0=numpy.array(max_list[::6])
#	min_array1=numpy.array(min_list[1::6])
#	max_array1=numpy.array(max_list[1::6])
#	min_array2=numpy.array(min_list[2::6])
#	max_array2=numpy.array(max_list[2::6])
#	min_array3=numpy.array(min_list[3::6])
#	max_array3=numpy.array(max_list[3::6])
#	min_array4=numpy.array(min_list[4::6])
#	max_array4=numpy.array(max_list[4::6])
#	min_array5=numpy.array(min_list[5::6])
#	max_array5=numpy.array(max_list[5::6])
	min_array0=numpy.array(min_list[::2])
	max_array0=numpy.array(max_list[::2])
	min_array1=numpy.array(min_list[1::2])
	max_array1=numpy.array(max_list[1::2])
	pyplot.figure(7)
	pyplot.subplot(211)
	pyplot.plot(freq_list,min_array0,'o')
	pyplot.subplot(212)
	pyplot.plot(freq_list,max_array0,'o')
	pyplot.figure(8)
	pyplot.subplot(211)
	pyplot.plot(freq_list,min_array1,'o')
	pyplot.subplot(212)
	pyplot.plot(freq_list,max_array1,'o')
#	pyplot.figure(9)
#	pyplot.subplot(211)
#	pyplot.plot(freq_list,min_array2,'o')
#	pyplot.subplot(212)
#	pyplot.plot(freq_list,max_array2,'o')
#	pyplot.figure(10)
#	pyplot.subplot(211)
#	pyplot.plot(freq_list,min_array3,'o')
#	pyplot.subplot(212)
#	pyplot.plot(freq_list,max_array3,'o')
#	pyplot.figure(11)
#	pyplot.subplot(211)
#	pyplot.plot(freq_list,min_array4,'o')
#	pyplot.subplot(212)
#	pyplot.plot(freq_list,max_array4,'o')
#	pyplot.figure(12)
#	pyplot.subplot(211)
#	pyplot.plot(freq_list,min_array5,'o')
#	pyplot.subplot(212)
#	pyplot.plot(freq_list,max_array5,'o')
	pyplot.show()
	#numpy.savetxt('freq_comp.txt',numpy.column_stack((freq_list,abs(comp_array))))
