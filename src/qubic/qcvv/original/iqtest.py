import datetime
import argparse
import sys
from matplotlib import pyplot,patches
import numpy
import time
import init
import experiment
class c_iqtest(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def iqtestseqs(self,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,elementlength=80,elementstep=2e-6,readoutdrvamp=None):
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		readoutdrvamp0,readoutdrvamp1,readoutdrvamp2=self.rdrvcalc(readoutdrvamp,dcoffset=0)
		modq7rdrv={}
		modq6rdrv={}
		modq5rdrv={}
		modq7read={}
		modq6read={}
		modq5read={}
		if readoutdrvamp:
			modq7rdrv.update(dict(amp=readoutdrvamp0))
			modq6rdrv.update(dict(amp=readoutdrvamp1))
			modq5rdrv.update(dict(amp=readoutdrvamp2))
		run=0
		print('readoutdrvamp0,readoutdrvamp1,readoutdrvamp2',readoutdrvamp0,readoutdrvamp1,readoutdrvamp2)
		for irun in range(elementlength):
			self.readall(trdrv=run,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
			t180=self.seqs.tend()+delay1
			self.seqs.add(t180,         	self.qchip.gates['Q7X180'])
			treaddrv=self.seqs.tend()+elementstep*(1)
			self.readall(trdrv=treaddrv,delayread=delayread,qubitsamps={"Q7":(modq7rdrv,modq7read),"Q6":(modq6rdrv,modq6read),"Q5":(modq5rdrv,modq5read)})
			run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def acqiqtest(self,nget):
		data=iqtest.acqdata(nget)
		return data
	def processiqtest(self,dt,filename,loadname):
		c=self.loadjsondata(filename)
		return [c['accout_0'],c['accout_1'],c['accout_2']]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=2e-6)
	clargs=parser.parse_args()
	iqtest=c_iqtest(**clargs.__dict__)
	iqtest.iqtestseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,readoutdrvamp=clargs.readoutdrvamp)
	iqtest.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			iqtest.sim()
		data=iqtest.acqiqtest(clargs.nget)
		fprocess=iqtest.savejsondata(filename=clargs.filename,extype='iqtest',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
	else:
		fprocess=clargs.processfile
	[rawdata0,rawdata1,rawdata2]=iqtest.processiqtest(dt=clargs.elementstep,filename=fprocess,loadname=clargs.dataset)
	print('adc min max',iqtest.hf.adcminmax())
	iqtest.plotiqtest(rawdata0,rawdata1,rawdata2)
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
