import datetime
from matplotlib import pyplot
import numpy
import experiment
import json
import re
import importlib


class c_simp2q(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,simpseqs=True,**kwargs)
		pass
	def simp2qseqsgen(self,delayafterheralding=12e-6,delaybetweenelement=600e-6,cmdarray=None,gates=None,elementlist={},destlist={},patchlist={},qubitid=None):
		self.qubitid=qubitid
		self.hf.setmap(elementlist=elementlist,destlist=destlist,patchlist=patchlist)
		if gates is not None:
			self.seqs.setgates(gates)
			self.hf.simpmemcmd(self.seqs.gatelist)
		self.seqs.updatecmd(cmdarray)
		(cmdlen,period)=self.hf.simpcmdgen(self.seqs.cmdarray,delaybetweenelement=delaybetweenelement,delayafterheralding=delayafterheralding)
		print('experiment period',period)
		self.seqs.setperiod(period)
		self.bufwidth_dict=self.hf.bufwidth_dict
		return cmdlen

	def simp2qacq(self,nget):
		data=self.acqdata(nget)
		return data
	def processsimp2q(self,filename,loaddataset=None,loaddataset_list=None,readcorr=False,corr_matrix=None):
		c=self.loadjsondata(filename)
		print('proccessimp2q bufwidth_dict',self.bufwidth_dict)
		if len(self.qubitid)==1:
			print('processsimp2q qubitid',self.qubitid)
			data=c[list(c.keys())[0]]
			simp_result=self.process3(data=data,qubitid=self.qubitid[0],lengthperrow=self.bufwidth_dict[self.qubitid[0]],loaddataset=loaddataset)
			population_norm=simp_result['population_norm']
			if readcorr:
				meas_binned=self.readoutcorrection(qubitid_list=self.qubitid,meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=corr_matrix)
				print('corrected meas_binned',meas_binned,meas_binned.shape)
				trueq_result=[]
			else:
				simp_result=self.process_trueq(data,qubitid=self.qubitid[0],lengthperrow=self.bufwidth_dict[self.qubitid[0]],loaddataset=loaddataset)
				trueq_result=[{'0':int(simp_result['0']),'1':int(simp_result['1'])}]
				meas_binned=None
		elif len(self.qubitid)==2:
			print('processsimp2q qubitid',self.qubitid)
			print('hf.accout_dict',self.hf.accout_dict)
			#print('c.keys',c.keys())
			data=[c[self.hf.accout_dict[self.qubitid[0]]],c[self.hf.accout_dict[self.qubitid[1]]]]
			process2q_result=self.process2q(data_list=data,qubitid_list=self.qubitid,lengthperrow_list=[self.bufwidth_dict[qid] for qid in self.qubitid],loaddataset_list=loaddataset_list)
			[meas_binned,meas_sum]=[process2q_result['meas_binned'],process2q_result['meas_sum']]
			# ['00', '01', '10', '11']
			# The order of the bits in the bitstring, from left to right, corresponds to the sorted qubit labels in ascending order.
			# Example: Qubit Map (Q5,Q6)
			# '00': Q5 |0>, Q6 |0>
			# '01': Q5 |0>, Q6 |1>
			# '10': Q5 |1>, Q6 |0>
			# '11': Q5 |1>, Q6 |1>
			if readcorr:
				meas_binned=self.readoutcorrection(qubitid_list=self.qubitid,meas_binned=meas_binned,corr_matrix=corr_matrix)
				print('corrected meas_binned',meas_binned,meas_binned.shape)

#			print('self.qubitid',self.qubitid,type(self.qubitid))
#			[meas_binned,meas_sum]=self.processrb2q(filename=fprocess,loaddataset_list=None)
			print('self.qubitid',self.qubitid)
			outcomes=[str(bin(i))[2:].zfill(len(self.qubitid)) for i in range(2**len(self.qubitid))]
			#print('processsimp2q',meas_sum,meas_binned)
			#print('processsimp2q',meas_sum.shape)
			trueq_result=[{o:int(count) for o,count in zip(outcomes,meas_sum[:,col])} for col in range(meas_sum.shape[1])]
		else:
			print('how many qubit do you have?')
		return [trueq_result,meas_binned]


if __name__=="__main__":
	import trueq
	import qubic_trueq
	starttime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	datetimeformat='%Y%m%d_%H%M%S_%f'
	
	n_random_cycles = [2,4,8,16,32]
	#n_random_cycles = [2,8,32]
	#n_random_cycles = [1,2,3,4,5]
	#n_random_cycles = [4,12,64] # CB
	n_circuits = 30

	from simp2q import c_simp2q
	parser,cmdlinestr=experiment.cmdoptions()
	parser.add_argument('--rbprotocol',help='RB protocol',dest='rbprotocol',type=str,default='SRB')
	clargs=parser.parse_args()
	simp2q=c_simp2q(**clargs.__dict__)

	config=trueq.Config.from_yaml('trueq_gate_'+clargs.qubitcfg[9:-5]+'.yaml')
	print('trueq_gatecfg',config)

	wiremap=importlib.import_module(clargs.wiremapmodule)
	gatesall=wiremap.gatesall
	elementlistall=wiremap.elementlistall
	destlistall=wiremap.destlistall
	patchlistall=wiremap.patchlistall

	if clargs.processfile=='':

		delayafterheralding=12e-6
		delaybetweenelement=600e-6
		#circuits_file=clargs.qubic_file
		firsttime=True

		if clargs.sim:
			simp2q.setsim()
		timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		print('timestamp to start',(datetime.datetime.strptime(timestamp,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		if clargs.rbprotocol=='SRB':
			circuits=qubic_trueq.trueq_srb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=False,readoutcorrection=True,twirl=None)#"SU")
		if clargs.rbprotocol=='IRB':
			circuits=qubic_trueq.trueq_irb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,interleaved_gate=trueq.Gate.cnot,readoutcorrection=True,twirl=None)
		if clargs.rbprotocol=='XRB':
			circuits=qubic_trueq.trueq_xrb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,independent=False,readoutcorrection=True,twirl=None)
		if clargs.rbprotocol=='CB':
			circuits=qubic_trueq.trueq_cb(timestamp=timestamp,qubitid_list=clargs.qubitid_list,n_random_cycles=n_random_cycles,n_circuits=n_circuits,gate=trueq.Gate.cnot,n_decays=20,readoutcorrection=True)
		#loadfilename='circuits_20200612_181515_719418.bin'
		#circuits=trueq.load(loadfilename)
		#pattern='(\S+)_(\S+_\S+_\S+).bin'
		#m=re.match(pattern,loadfilename)
		#timestamp=m.group(2)
		#print(timestamp)

		cirgentime=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		transpiler = trueq.Compiler.from_config(config)
		transpiled_circuit = transpiler.compile(circuits)
		print('cirgentime to start',(datetime.datetime.strptime(cirgentime,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
		trueq_result=[]
		index=0
		lastcmdarraylength=0
		print(transpiled_circuit)
		print(circuits.n_circuits)
		for circuit in transpiled_circuit:
			print('index',index)
			#print(circuit,circuit.gates)
#		for cycle in  circuit:
#			for k,v in cycle.meas.items():
#				print(k,v)
#			for k,v in cycle.gates.items():
#				print(k,v,v.parameters['phi'])
			index=index+1
			[qubitid,cmdarray]=qubic_trueq.trueq_circ_to_qubic2(circuit,heralding=delayafterheralding>0)
#		#print('cmdarray len',len(cmdarray))
			for l in cmdarray:
				print(l)
			gates=[simp2q.qchip.gates[clargs.qubitid_list[0]+clargs.qubitid_list[1]+'CNOT']]+[simp2q.qchip.gates[g] for g in gatesall if any([qid in g for qid in qubitid+['M0']]) and any([x in g for x in ['X90','read','mark']])]
			elementlist={k:elementlistall[k] for k in elementlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			destlist={k:destlistall[k] for k in destlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			patchlist={k:patchlistall[k] for k in patchlistall.keys() if any([qid in k for qid in qubitid+['M0']])}
			cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=cmdarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
#	#	print(cmdarray)
#		cmdarraylength=simp2q.simp2qseqsgen(delayafterheralding=delayafterheralding,delaybetweenelement=delaybetweenelement,cmdarray=testarray,gates=gates if firsttime else None,elementlist=elementlist,destlist=destlist,patchlist=patchlist,qubitid=qubitid)
			simp2q.run(memwrite=firsttime,memclear=firsttime,cmdclear=cmdarraylength<lastcmdarraylength,preread=firsttime,cmdclearlength=lastcmdarraylength*4)
			lastcmdarraylength=cmdarraylength
			print('cmdarray this',cmdarraylength,'last',lastcmdarraylength)
			data=simp2q.simp2qacq(clargs.nget)
			fprocess=simp2q.savejsondata(filename=clargs.filename,extype='simp2q',cmdlinestr=cmdlinestr,data=data)
			print('save data to ',fprocess)
			if clargs.sim:
				simp2q.sim()
			trueq_return,meas_binned=simp2q.processsimp2q(filename=fprocess,loaddataset=clargs.dataset)
			print(trueq_return)
			trueq_result.extend(trueq_return)
			firsttime=False

		print(trueq_result)
		timestampresult=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		resultfilename='circuits_'+timestamp+'_'+timestampresult+'.dat'
		with open(resultfilename,'w') as f_trueqresult:
			json.dump(trueq_result,f_trueqresult)
	else:
		resultfilename=clargs.processfile

##		print('resultfile %s'%resultfilename)
	beforerb1q_process=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
	print('beforerb1q_process to start',(datetime.datetime.strptime(beforerb1q_process,datetimeformat)-datetime.datetime.strptime(starttime,datetimeformat)).seconds)
	result_circuits=qubic_trueq.rb1q_process(resultfilename)
	###XRB###
	if clargs.rbprotocol=='XRB':
		print('###SRB###result_circuits.fit()[0]',result_circuits.fit()[0])
		print('###SRB###e_F',result_circuits.fit()[0].e_F.val,result_circuits.fit()[0].e_F.std)
		print('###SRB###p',result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std)
		print('###SRB###A',result_circuits.fit()[0].A.val,result_circuits.fit()[0].A.std)
		print('###XRB###result_circuits.fit()[1]',result_circuits.fit()[1])
		print('###XRB###e_U',result_circuits.fit()[1].e_U.val,result_circuits.fit()[1].e_U.std)
		print('###XRB###e_S',result_circuits.fit()[1].e_S.val,result_circuits.fit()[1].e_S.std)
		print('###XRB###u',result_circuits.fit()[1].u.val,result_circuits.fit()[1].u.std)
		print('###XRB###A',result_circuits.fit()[1].A.val,result_circuits.fit()[1].A.std)
		#circuits.plot.compare_rb()
	###IRB###
	if clargs.rbprotocol=='IRB':
		print('###IRB###result_circuits.fit()[0]',result_circuits.fit()[0])
		print('###IRB###e_F',result_circuits.fit()[0].e_F.val,result_circuits.fit()[0].e_F.std)
		print('###IRB###e_IU',result_circuits.fit()[0].e_IU.val,result_circuits.fit()[0].e_IU.std)
		print('###IRB###e_IL',result_circuits.fit()[0].e_IL.val,result_circuits.fit()[0].e_IL.std)
		print('###IRB###p',result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std)
		print('###IRB###A',result_circuits.fit()[0].A.val,result_circuits.fit()[0].A.std)
		print('###SRB###result_circuits.fit()[1]',result_circuits.fit()[1])
		print('###SRB###e_F',result_circuits.fit()[1].e_F.val,result_circuits.fit()[1].e_F.std)
		print('###SRB###p',result_circuits.fit()[1].p.val,result_circuits.fit()[1].p.std)
		print('###SRB###A',result_circuits.fit()[1].A.val,result_circuits.fit()[1].A.std)
		print('###Gate Fidelity###',simp2q.gate_fidelity(result_circuits.fit()[0].p.val,result_circuits.fit()[0].p.std,result_circuits.fit()[1].p.val,result_circuits.fit()[1].p.std))
		#circuits.plot.irb_summary()
	###SRB###
	if clargs.rbprotocol=='SRB':
		result_rb=result_circuits.fit()[0]
		fidelity=1-result_rb.e_F.val
		fidelity_std=result_rb.e_F.std
		SPAM=result_rb.A.val
		print('fidelity,fidelity_std,SPAM',fidelity,fidelity_std,SPAM)
	###IRB###
	if clargs.rbprotocol=='CB':
		print('###CB###result_circuits.fit()[0]',result_circuits.fit()[0])
		print('###CB###result_circuits.fit()[0]._names',result_circuits.fit()[0]._names)
		print('###CB###result_circuits.fit()[0]._values',result_circuits.fit()[0]._values)
		print('###CB###result_circuits.fit()[0] std',numpy.sqrt(numpy.diag(result_circuits.fit()[0]._err)))

#python simp2q.py -n 2 --plot --qubitid_list Q6 Q5
