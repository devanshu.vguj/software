import json
import datetime
import argparse
import sys
#from squbic import *
#sys.path.append('../laser_stack/gui/fmc120/qubic')
from matplotlib import pyplot,patches
import numpy
#from ether import c_ether
#from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment

class c_alignment(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		pass
	def alignmentseqs(self,delaybetweenelement=100e-6,loshift=0e-9,readoutdrvamp=None,readwidth=None,fread=None):
		print('readoutdrvamp,readwidth,fread',readoutdrvamp,readwidth,fread)
		modrdrv={}
		modread={}
		if readoutdrvamp is not None:
			modrdrv.update(dict(amp=readoutdrvamp))
		if readwidth is not None:
			modrdrv.update(dict(twidth=readwidth))
			modread.update(dict(twidth=readwidth))
		if fread is not None:
			modrdrv.update(dict(fcarrier=fread))
			modread.update(dict(fcarrier=fread))
		self.seqs.add(360e-9,				   self.qchip.gates['M0mark'])
		self.seqs.add(0e-9,				    self.qchip.gates['Q0readoutdrv'].modify(modrdrv))
		self.seqs.add(0e-9+loshift,			self.qchip.gates['Q0readout'].modify(modread))
		run=self.seqs.tend()+delaybetweenelement
		self.seqs.setperiod(period=run)
	def plotmonwithfft(self,t,buf0,buf1):
		#print(type(t),type(buf0),type(buf1))
		#print(t.shape,buf0.shape,buf1.shape)
		fmax=1./(numpy.diff(t).mean())
		df=fmax/(len(t)-1)
		length=len(t)#//2
		f=numpy.arange(length)*df
		fftval=numpy.fft.fft(buf0)[0:length]
		pyplot.figure(figsize=(8,8))
		pyplot.subplot(221)
		pyplot.plot(t,buf0)
		pyplot.subplot(222)
		pyplot.plot(f,abs(fftval))
		pyplot.subplot(223)
		pyplot.plot(t,buf1)
		pyplot.subplot(224)
		pyplot.plot(f,numpy.angle(fftval))
#		print(f[0:5],f[-5:-1],df)
#		print(t[0:5],t[-5:-1],fmax)

if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(delaybetweenelement=100e-6)
	clargs=parser.parse_args()
	if clargs.processfile=='':
		alignment=c_alignment(**clargs.__dict__)
		alignment.alignmentseqs(delaybetweenelement=clargs.delaybetweenelement,loshift=clargs.loshift,readoutdrvamp=clargs.readoutdrvamp,readwidth=clargs.readwidth,fread=clargs.fread)
		alignment.run(bypass=clargs.bypass)
		alignment_timestamp=datetime.datetime.strftime(datetime.datetime.now(),'%Y%m%d_%H%M%S_%f')
		#alignment_filename='%s.dat'%('_'.join(['alignment',alignment_timestamp]))
		t,buf0,buf1=alignment.acqmonout(avrfactor=clargs.avrfactor,opsel=clargs.opsel,mon_navr=clargs.timezoom,mon_dt=clargs.timeshift,mon_sel0=clargs.mon_sel0,mon_sel1=clargs.mon_sel1)
		jsondata=dict(t=t.tolist(),buf0=buf0.tolist(),buf1=buf1.tolist())
		fprocess=alignment.savejsondata(filename=clargs.filename,extype='alignment',cmdlinestr=cmdlinestr,data=jsondata)
#		with open(alignment_filename,'a') as f:
#			#numpy.savetxt(f,(t,buf0.flatten(),buf1.flatten()))
#			json.dump(jsondata,f)
	else:
		fprocess=clargs.processfile
	with open(fprocess) as f:
		d=json.load(f)
	t=numpy.array(d['t'])
	buf0=numpy.array(d['buf0'])
	buf1=numpy.array(d['buf1'])
	buf0=buf0[:,0]
	buf1=buf1[:,0]
	alignment.plotmonwithfft(t,buf0,buf1)
	pyplot.savefig(fprocess+'.pdf')
	pyplot.show()
	# python alignment.py --plot -o 2 -0 0 -1 2 -a 64 -L 600e-9 -r 0.95 --readwidth 1e-6 -fr 100e6
	# Selection of DLO monitor channel: please match the DLO gate with DLO allocation in hfbridge.py
	# Example: "-1 2", 'Q0readout', 'Q0.read':4, self.lo0elementsdest=['Q0.read']
	# monseloptions='''0(xmeasin_d2),1(ymeasin_d2),2(xlo_w[0]),3(ylo_w[0]),4(xlo_w[1]),5(ylo_w[1]),6(xlo_w[2]),7(ylo_w[2]),8(dac0_in),9(dac1_in),10(dac2_in),11(dac3_in),12(dac4_in),13(dac5_in),14(dac6_in),15(dac7_in)'''
