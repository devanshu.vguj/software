import datetime
import argparse
import sys
#from squbic import *
from matplotlib import pyplot,patches
#from qubic_t1 import cmdadd, cmdgen
import numpy
#from ether import c_ether
#from mem_gateway import c_mem_gateway
import time
import init
import pprint
import experiment
import matplotlib

class c_allxy(experiment.c_experiment):
	def __init__(self,ip='192.168.1.124',port=3000,dt=1.0e-9,regmappath='regmap.json',wavegrppath='wavegrp.json',qubitcfg='qubitcfg.json',initcfg='sqinit',**kwargs):
		experiment.c_experiment.__init__(self,ip=ip,port=port,dt=dt,regmappath=regmappath,wavegrppath=wavegrppath,qubitcfg=qubitcfg,initcfg=initcfg,**kwargs)
		self.qubitid=None
		pass
	def allxyseqs(self,elementlength=80,elementstep=2e-6,delayread=668e-9,delay1=12e-6,delaybetweenelement=600e-6,qubitid='Q5',qubitidread=['Q7','Q6','Q5']):
		self.qubitid=qubitid
		allxygates=[['I','I'],['X180','X180']
			,['Y180','Y180']
			,['X180','Y180']
			,['Y180','X180']
			,['X90','I']
			,['Y90','I']
			,['X90','Y90']
			,['Y90','X90']
			,['X90','Y180']
			,['Y90','X180']
			,['X180','Y90']
			,['Y180','X90']
			,['X90','X180']
			,['X180','X90']
			,['Y90','Y180']
			,['Y180','Y90']
			,['X180','I']
			,['Y180','I']
			,['X90','X90']
			,['Y90','Y90']]
		self.seqs.add(360e-9,				self.qchip.gates['M0mark'])
		print('marker done')
		run=0
		for gates in allxygates:
			therald=run
			self.seqs.add(therald,self.qchip.gates[qubitid+'read'])
			pini=0
			tgate=self.seqs.tend()+delay1
			tini=tgate
			for gate in gates:
				gatemodi={}
				if gate=='I':
					pass
				else:
					gateobj=self.qchip.gates[qubitid+gate]
					pnew=self.qchip.gates[qubitid+gate].pcalc(dt=tgate-tini)[0]
					gatemodi.update(dict(pcarrier=pnew))
					self.seqs.add(tgate,self.qchip.gates[qubitid+gate].modify(gatemodi))
					tgate=self.seqs.tend()
			treaddrv=self.seqs.tend()
			self.seqs.add(treaddrv,self.qchip.gates[qubitid+'read'])
			run=self.seqs.tend()+delaybetweenelement

		self.seqs.setperiod(period=run)
		bufwidth=[self.seqs.countdest(qid +'.read') for qid in qubitidread]
		self.bufwidth_dict=dict(zip(qubitidread,bufwidth))
	def processallxy(self,dt,filename,loaddataset):
		c=self.loadjsondata(filename)
		print('c.keys()',c.keys())
		data=c[list(c.keys())[0]]
		allxy_result=self.process3(data,qubitid=self.qubitid,lengthperrow=self.bufwidth_dict[self.qubitid],training=False,loaddataset=loaddataset)
		return [data,allxy_result['separation'],allxy_result['iqafterherald'],allxy_result['population_norm']]
if __name__=="__main__":
	parser,cmdlinestr=experiment.cmdoptions()
	parser.set_defaults(elementstep=4e-9)
	clargs=parser.parse_args()
	allxy=c_allxy(**clargs.__dict__)
	if clargs.sim:
		allxy.setsim()
	allxy.allxyseqs(delayread=clargs.delayread,delay1=12e-6,delaybetweenelement=clargs.delaybetweenelement,elementlength=clargs.elementlength,elementstep=clargs.elementstep,qubitid=clargs.qubitid,qubitidread=clargs.qubitidread)
	allxy.run(bypass=clargs.bypass or not clargs.processfile=='')
	if clargs.processfile=='':
		if clargs.sim:
			allxy.sim()
		data=allxy.acqdata(clargs.nget)
		fprocess=allxy.savejsondata(filename=clargs.filename,extype='allxy',cmdlinestr=cmdlinestr,data=data)
		print('save data to ',fprocess)
		if clargs.sim:
			allxy.sim()
	else:
		fprocess=clargs.processfile
	[rawdata,separation,iqafterherald,population_norm]=allxy.processallxy(dt=clargs.elementstep,filename=fprocess,loaddataset=clargs.dataset)
	if clargs.readcorr:
		population_norm=allxy.readoutcorrection(qubitid_list=[clargs.qubitid],meas_binned=numpy.vstack((1-population_norm,population_norm)),corr_matrix=numpy.load(clargs.corrmx))[1]
		print('corrected population_norm',population_norm)
#	allxy.plotrawdata(rawdata)
#	allxy.plotafterheraldingtest(iqafterherald)
#	allxy.plotpopulation_norm(population_norm)
	#matplotlib.rc('font',family='Arial', size = 13) # 18
	#font = {'fontname':'Arial', 'size': 15} # 20
	#pyplot.figure(figsize=(10, 6))
	pyplot.figure()
	first_pulses = ['I', 'X180', 'Y180', 'X180', 'Y180',  # end in |0> state
			'X90', 'Y90', 'X90', 'Y90', 'X90', 'Y90',  # end in |0>+|1> state
			'X180', 'Y180', 'X90', 'X180', 'Y90', 'Y180',  # end in |0>+|1> state
			'X180', 'Y180', 'X90', 'Y90']  # end in |1> state
	second_pulses = ['I', 'X180', 'Y180', 'Y180', 'X180', 'I', 'I',
			'Y90', 'X90', 'Y180', 'X180', 'Y90',
			'X90', 'X180', 'X90', 'Y180', 'Y90',
			'I', 'I', 'X90', 'Y90']
	allxy_gates = list(zip(first_pulses,second_pulses))
	pyplot.xticks(range(21), [str(gate[0])+" "+gate[1] for gate in allxy_gates], rotation=90)
	pyplot.plot(population_norm,'.-', markersize = 10)
	pyplot.ylim(-0.05,1.05)
	pyplot.ylabel('Population',fontsize=15)
	pyplot.tight_layout()
	#pyplot.title(r'AllXY sequence with $\alpha$ = 0.50', **font)
	pyplot.axhline(0.0, linestyle='--', color='k')
	pyplot.axhline(0.5, linestyle='--', color='k')
	pyplot.axhline(1.0, linestyle='--', color='k')
	pyplot.savefig('allxy_rcali'+fprocess+'.png',dpi=600)
	pyplot.savefig('allxy_rcali'+fprocess+'.pdf')
	if clargs.plot:
		pyplot.grid()
		pyplot.show()
	# python allxy.py --plot -n 50 --qubitid Q5 --readcorr
